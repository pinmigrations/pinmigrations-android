# ProGuard rules
#   http://developer.android.com/guide/developing/tools/proguard.html

# NOTE: these directories are relative to the config file, not to app-root

# other options here not supported by android "newShrinker"


#################################
# Shrink
###########
# -dontshrink # do shrink!
# Use to debug errors
# https://www.guardsquare.com/en/proguard/manual/usage#shrinkingoptions
# -whyareyoukeeping class_specification

-dontskipnonpubliclibraryclassmembers
######################


#################################
# Obfuscate
###########
-dontobfuscate
#-dontusemixedcaseclassnames
#-useuniqueclassmembernames
#-renamesourcefileattribute SourceFile
#################################


#################################
# Optimize
###########
-dontoptimize
#-optimizations !code/simplification/cast,!field/*,!class/merging/*
#-allowaccessmodification
#-optimizationpasses 1
#################################


#-dontskipnonpubliclibraryclassmembers

# Enable / Disable as needed
# -ignorewarnings

# Timing NOTES
#
# Config-1:
#   -dontskipnonpubliclibraryclasses
#       ~22s with all disabled
#       ~?s with all enabled
#       ~13s for just shrink
#       ~?s for just optimization
#           error
#       ~22s for just obfuscation
