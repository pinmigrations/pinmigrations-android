# Release ProGuard rules (additional to base)
#   http://developer.android.com/guide/developing/tools/proguard.html

# NOTE: these directories are relative to the config file, not to app-root

# other options here not supported by android "newShrinker"

# NOTES!
#Flag -optimizations !code/simplification/cast,!field/*,!class/merging/* is ignored by the built-in shrinker.
#Flag -allowaccessmodification is ignored by the built-in shrinker.
#Flag -optimizationpasses 3 is ignored by the built-in shrinker.
#Flag -useuniqueclassmembernames is ignored by the built-in shrinker.
#Flag -renamesourcefileattribute SourceFile is ignored by the built-in shrinker.


######################
# Shrink
###########
-dontskipnonpubliclibraryclassmembers

######################
# Optimize
###########
#-optimizations !code/simplification/cast,!field/*,!class/merging/*
#-allowaccessmodification
#-optimizationpasses 3
######################


######################
# Obfuscate
###########
-dontusemixedcaseclassnames
#-useuniqueclassmembernames
#-renamesourcefileattribute SourceFile
-keepclassmembers,allowobfuscation class * {
    @javax.inject.* *;
    @dagger.* *;
    <init>();
}
######################
