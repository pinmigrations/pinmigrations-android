# ProGuard rules
#   http://developer.android.com/guide/developing/tools/proguard.html
# 	https://www.guardsquare.com/en/proguard/manual/troubleshooting
# 	https://www.guardsquare.com/en/proguard/manual/usage
# latest sdk rules:
#	https://android.googlesource.com/platform/sdk/+/master/files/

# NOTE: these directories are relative to the config file, not to app-root

# NOTES:
# for only Obfuscation use (in relevant file, not in base):
#   -dontshrink
#   -dontoptimize

-verbose
# preverification is unneccessary on android
-dontpreverify

###########
# Shrink
###########
# fails without this
-dontskipnonpubliclibraryclasses
###########


-keepattributes *Annotation*,SourceFile,LineNumberTable,Signature,InnerClasses,EnclosingMethod,Exceptions
-keepattributes xst.pinmigrations.**

# just keep all app
-keeppackagenames xst.pinmigrations.**
-keep class xst.pinmigrations.** {
	private <fields>;
	<fields>;
	*;
}
-keep class xst.pinmigrations.**.*$* {
	private <fields>;
	<fields>;
	*;
}

-keepclasseswithmembers interface xst.pinmigrations.** { *; }
-keep interface xst.pinmigrations.** { *; }
# also keep the app-specific Timber specialization (in this package)
-keep class timber.** { *; }

# keep enums
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}


# http://stackoverflow.com/a/35742739/1163940
-dontnote android.net.http.*
-dontnote org.apache.commons.codec.**
-dontnote org.apache.http.**

# Retrofit
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8

# OkHttp3
-dontwarn okhttp3.**
-dontnote okhttp3.**
-dontwarn okio.**

# Retrofit
-dontwarn retrofit.**
-keep class retrofit.** { *; }

## Joda Time 2.3
-dontwarn org.joda.convert.**
-dontwarn org.joda.time.**
-keep class org.joda.time.** { *; }
-keep interface org.joda.time.** { *; }


# LoganSquare
# https://github.com/bluelinelabs/LoganSquare#proguard
-keep class com.bluelinelabs.logansquare.** { *; }
-keep @com.bluelinelabs.logansquare.annotation.JsonObject class *
-keep class **$$JsonObjectMapper { *; }
-dontwarn org.w3c.dom.bootstrap.DOMImplementationRegistry
-dontwarn com.fasterxml.jackson.databind.**


# Retrolambda Gradle Plugin
-dontwarn java.lang.invoke.*

# RxJava
#-keep class rx.** { *; }
#-keep interface rx.** { *; }

# Leak Canary
-dontwarn com.squareup.haha.guava.**
-dontwarn com.squareup.haha.perflib.**
-dontwarn com.squareup.haha.trove.**
-dontwarn com.squareup.leakcanary.**
-keep class com.squareup.haha.** { *; }
-keep class com.squareup.leakcanary.** { *; }

# https://sourceforge.net/p/proguard/discussion/182456/thread/e4d73acf
-keep class org.codehaus.** { *; }

##---junit
-dontwarn org.junit.**
-dontwarn org.hamcrest.**
-dontnote junit.framework.**
-dontnote junit.runner.**
-dontnote org.junit.**
-dontwarn android.test.**
-dontwarn android.support.test.**


# Android
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-dontnote com.android.vending.licensing.ILicensingService
# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclassmembers class * extends android.content.Context {
    public void *(android.view.View);
    public void *(android.view.MenuItem);
}


# Marshmallow removed Notification.setLatestEventInfo()
-dontwarn android.app.Notification

# Guava
-dontwarn com.google.common.collect.MinMaxPriorityQueue
-keepclasseswithmembers public class * {
    public static void main(java.lang.String[]);
}
-keepclasseswithmembernames class * {
    native <methods>;
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclassmembers class * extends android.app.Activity { *; }
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
-keepclassmembers class **.R$* { *; }

-dontwarn java.lang.ClassValue


# Explicitly preserve all serialization members. The Serializable interface
# is only a marker interface, so it wouldn't save them.
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}


# Java SE
-dontwarn java.beans.**
-dontwarn java.awt.**
-dontwarn javax.swing.**
-dontwarn javax.inject.**

# Okio
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

# AssertJ ends up in test apk?
-dontwarn org.assertj.**


# specific classes
#-keep class xst.pinmigrations.activities.login.LoginView.*$*

-keep class com.google.common.base.Verify
-keep class com.google.common.base.Preconditions
-keepclasseswithmembers class com.google.common.base.Verify {
    ** verify(...);
    ** verifyNotNull(...);
}
-keepclasseswithmembers class com.google.common.base.Preconditions {
    ** checkNotNull(...);
    void checkArgument(...);
    void checkState(...);
}

-keep class okhttp3.WebSocketListener
-keep class xst.pinmigrations.test.websockets.AT_WebSocketConnectivityListener

-keep class okhttp3.OkHttpClient {
	*;
}

-keep class okhttp3.Request$Builder {
	*;
}

-keep class okhttp3.Response$Builder {
	*;
}

-keep class okhttp3.WebSocket {
	*;
}

-keep class android.support.test.runner.AndroidJUnit4 {
	*;
}
-keep class xst.pinmigrations.app.AT_JUnitRunner {
	*;
}
-keep class android.support.test.runner.AndroidJUnitRunner {
	*;
}

# ProGuard rules
-keep class rx.schedulers.Schedulers {
	*;
}
