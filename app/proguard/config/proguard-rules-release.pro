# Release ProGuard rules (additional to base)
#   http://developer.android.com/guide/developing/tools/proguard.html

# NOTE: these directories are relative to the config file, not to app-root

# this prints the effective configuration used during analysis
-printconfiguration ../output/release/config-release.txt

# Hold onto the mapping.text file
# it can be used to unobfuscate stack traces in the developer console using the retrace tool
-printmapping ../output/release/mapping-release.txt

# print dead code of the input class files
-printusage ../output/release/usages-release.txt

# list classes and class members matched by the various -keep option
-printseeds ../output/release/seeds-release.txt

# write out the internal structure of the class files, after any processing
-dump ../output/release/class_files-release.txt



######################
# Shrink
###########
-dontskipnonpubliclibraryclassmembers

# Use to debug errors
# https://www.guardsquare.com/en/proguard/manual/usage#shrinkingoptions
# -whyareyoukeeping class_specification
# Turn off Logging
# TODO: enable remote-logging for errors at least
-assumenosideeffects class android.util.Log {
  public static int d(...);
  public static int i(...);
  public static int e(...);
  public static int v(...);
}
######################


######################
# Optimize
###########
-optimizations !code/simplification/cast,!field/*,!class/merging/*
-allowaccessmodification
-optimizationpasses 3
######################


######################
# Obfuscate
###########
-dontusemixedcaseclassnames
-useuniqueclassmembernames
-renamesourcefileattribute SourceFile
-keepclassmembers,allowobfuscation class * {
    @javax.inject.* *;
    @dagger.* *;
    <init>();
}
######################

