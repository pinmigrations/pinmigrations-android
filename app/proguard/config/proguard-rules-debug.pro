# ProGuard rules
#   http://developer.android.com/guide/developing/tools/proguard.html

# NOTE: these directories are relative to the config file, not to app-root

# this prints the effective configuration used during analysis
-printconfiguration ../output/debug/config-debug.txt

# Hold onto the mapping.text file
# it can be used to unobfuscate stack traces in the developer console using the retrace tool
-printmapping ../output/debug/mapping-debug.txt

# print dead code of the input class files
-printusage ../output/debug/usages-debug.txt

# list classes and class members matched by the various -keep option
-printseeds ../output/debug/seeds-debug.txt

# write out the internal structure of the class files, after any processing
-dump ../output/debug/class_files-debug.txt


#################################
# Shrink
###########
# -dontshrink # do shrink!
# Use to debug errors
# https://www.guardsquare.com/en/proguard/manual/usage#shrinkingoptions
# -whyareyoukeeping class_specification

-dontskipnonpubliclibraryclassmembers
######################


#################################
# Obfuscate
###########
-dontobfuscate
#-dontusemixedcaseclassnames
#-useuniqueclassmembernames
#-renamesourcefileattribute SourceFile
#################################


#################################
# Optimize
###########
-dontoptimize
#-optimizations !code/simplification/cast,!field/*,!class/merging/*
#-allowaccessmodification
#-optimizationpasses 1
#################################


#-dontskipnonpubliclibraryclassmembers

# Enable / Disable as needed
# -ignorewarnings

# Timing NOTES
#
# Config-1:
#   -dontskipnonpubliclibraryclasses
#       ~22s with all disabled
#       ~?s with all enabled
#       ~13s for just shrink
#       ~?s for just optimization
#           error
#       ~22s for just obfuscation
