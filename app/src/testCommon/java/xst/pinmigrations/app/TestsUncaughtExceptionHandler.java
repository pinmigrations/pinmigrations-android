package xst.pinmigrations.app;

import static org.assertj.core.api.Assertions.fail;

/** fails the individual test on uncaught exceptions */
public class TestsUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
	@Override public void uncaughtException(final Thread t, final Throwable e) {
		fail("uncaught exception", e);
	}
}
