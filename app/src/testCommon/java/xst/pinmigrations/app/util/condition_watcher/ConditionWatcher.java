package xst.pinmigrations.app.util.condition_watcher;

import android.support.annotation.NonNull;

import org.assertj.core.api.Fail;

import java.util.concurrent.TimeUnit;

import timber.log.Timber;
import xst.pinmigrations.app.util.MainUtil;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static xst.pinmigrations.app.util.MainUtil.getMs;
import static xst.pinmigrations.app.util.MainUtil.nanoDiffToMsString;

/**
 * simple polling synchronization mechanism:
 * https://github.com/AzimoLabs/ConditionWatcher
 * API and behavior is modeled after Esspresso's IdlingResource somewhat
 * default intervals and timeouts are short because main usage will be with network-layer stubbed
 * by fakes w short random intervals ~400ms max
 */
public class ConditionWatcher {
	public static final int CONDITION_NOT_MET = 0, CONDITION_MET = 1, TIMEOUT = 2;

	public static final int DEFAULT_TIMEOUT_LIMIT_MS = 1000 * 7;
	public static final int DEFAULT_INTERVAL_MS = 175;

	private int timeoutLimit = DEFAULT_TIMEOUT_LIMIT_MS;
	private int watchInterval = DEFAULT_INTERVAL_MS;

	@NonNull private static final ConditionWatcher INSTANCE = new ConditionWatcher();
	public static final String ERRMSG_WATCH_INTERVAL_NON_ZERO_POS = "watch interval must be non-zero positive";
	public static final String ERRMSG_TIMEOUT_NON_ZERO_POS = "timeout must be non-zero positive";
	public static final String ERRMSG_TIMEOUT_GT_WATCH = "timeout must be greater than watch interval";

	private ConditionWatcher() { } // singleton
	/** get singleton instance */
	public static @NonNull ConditionWatcher getInstance() { return INSTANCE; }

	/** wait for condition and specify interval/timeout */
	public static void waitForCondition(@NonNull final Condition watchCondition,
										final double watchInterval, final TimeUnit watchTimeUnit,
										final double timeout, final TimeUnit timeoutTimeUnit)
			throws InterruptedException {
		setWatchInterval(watchInterval, watchTimeUnit);
		setTimeoutLimit(timeout, timeoutTimeUnit);
		waitForCondition(watchCondition);
	}

	/** blocks current thread until `Condition` is satisfied or timeout */
	public static void waitForCondition(@NonNull final Condition watchCondition) throws InterruptedException {
		final Condition condition = checkNotNull(watchCondition);

		final int watchInterval = INSTANCE.watchInterval;
		final int timeoutLimit = INSTANCE.timeoutLimit;

		verify(watchInterval > 0, ERRMSG_WATCH_INTERVAL_NON_ZERO_POS);
		verify(timeoutLimit > 0, ERRMSG_TIMEOUT_NON_ZERO_POS);
		verify(timeoutLimit >= watchInterval, ERRMSG_TIMEOUT_GT_WATCH);

		int status = CONDITION_NOT_MET;
		int elapsedTime = 0;


		long beforeCheck=0L, afterCheck=0L;
		do {
			beforeCheck = System.nanoTime();
			if (condition.checkCondition()) {
				status = CONDITION_MET;
			} else {
				afterCheck = System.nanoTime(); // records duration of Condition.checkCondition method

				elapsedTime += watchInterval;
				Thread.sleep(watchInterval);
			}

			if (elapsedTime >= timeoutLimit) { status = TIMEOUT; }
		} while (CONDITION_NOT_MET == status);

		if (TIMEOUT == status) { // throws AssertionError (this class is only used in tests so this is appropriate)
			condition.failLog(); // let Condition explain failure
			if (beforeCheck > 0 && afterCheck > 0) {
				Timber.d("check duration: " + nanoDiffToMsString(beforeCheck, afterCheck));
			}
			Fail.fail(condition.getDescription() + " took more than " + MainUtil.msToSecondsString(timeoutLimit));
		}
	}

	public static void setWatchInterval(final double watchInterval, final TimeUnit timeUnit) {
		checkArgument(watchInterval > 0, ERRMSG_WATCH_INTERVAL_NON_ZERO_POS);
		getInstance().watchInterval = getMs(watchInterval, timeUnit);
	}

	public static void setTimeoutLimit(final double timeout, final TimeUnit timeUnit) {
		checkArgument(timeout > 0, ERRMSG_TIMEOUT_NON_ZERO_POS);
		getInstance().timeoutLimit = getMs(timeout, timeUnit);
	}

	public abstract static class Condition {
		/** return true once condition is satisfied */
		public abstract boolean checkCondition();

		/** invoked after timeout, log the reason why condition is not satisfied */
		public void failLog() { }

		/** this is used in test-reporting (for failures), make unique */
		public @NonNull String getDescription() {
			return simpleClassName(); // most are implemented as static inner classes, so this facilitates that
		}

		public @NonNull String simpleClassName() {
			return this.getClass().getSimpleName();
		}
	}
}
