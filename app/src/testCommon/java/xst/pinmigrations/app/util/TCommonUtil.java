package xst.pinmigrations.app.util;

import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import timber.log.Timber;

import static java.util.concurrent.TimeUnit.*;
import static xst.pinmigrations.app.util.MainUtil.*;

public class TCommonUtil {

	public static String threadName() { return Thread.currentThread().getName(); }

	//region Quick & Dirty (eg: for demo-ing functionality)

	public static void endTestWait(final int allowance) throws InterruptedException {
		//noinspection StringConcatenationMissingWhitespace
		Timber.i("sleep to allow other threads to complete " + allowance + "ms");
		threadSleep(allowance);
	}

	public static void endTestWait(final double allowance, TimeUnit timeUnit) throws InterruptedException {
		final int msDelay = getMs(allowance, timeUnit);
		//noinspection StringConcatenationMissingWhitespace
		Timber.d("test sleep to allow other threads to complete: " + msDelay + "ms");
		threadSleep(msDelay);
	}

	/** already over-used, dammit. should be @Deprecated */
	public static void messageThenWait(final String msg, final int delay) throws InterruptedException {
		messageThenWait(msg, delay, MILLISECONDS);
	}
	public static void messageThenWait(final String msg, final double delay, @NonNull final TimeUnit timeUnit) throws InterruptedException {
		final int msDelay = getMs(delay, timeUnit);
		//noinspection StringConcatenationMissingWhitespace
		Timber.i(msg + " sleep for " + msDelay + "ms");
		threadSleep(msDelay);
		Timber.d("done sleeping");
	}
	public static void waitThenMessage(final String msg, final double delay, @NonNull final TimeUnit timeUnit) throws InterruptedException {
		final int msDelay = getMs(delay, timeUnit);
		Timber.d("wait for " + msDelay + "ms");
		threadSleep(msDelay);
		Timber.i(msg);
	}

	public static void threadSleep(final double delay, @NonNull TimeUnit timeUnit) throws InterruptedException {
		threadSleep(getMs(delay, timeUnit));
	}
	private static void threadSleep(final int msDelay) throws InterruptedException {
		if (msDelay <= 0) { return; }
		Thread.sleep(msDelay);
	}
	//endregion
}
