package xst.pinmigrations.app.util;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import timber.log.Timber;

public class TAsyncUtil {
	// short because using the atomic integer provides accuracy in ending the wait
	// actual sleep time is randomized to prevent harmonic deadlock
	static final int ASYNC_PERIOD = 50;
	static final Random rand = new Random();
	private static int sleepPeriod() {
		int period;
		synchronized (rand) {
			period = rand.nextInt(ASYNC_PERIOD - 25) + 25; // rand >= 25
		}

		return period;
	}



	/** block on an AtomicInteger to decrement to zero, timeout after N milliseconds */
	public static void asyncCountdown(final AtomicInteger counter, final int timeoutMs) {
		if (null == counter) { throw new IllegalArgumentException("AtomicInteger counter is null"); }
		if (timeoutMs <= 0) { throw new IllegalArgumentException("timeout  must be greater than 0 ms"); }

		final long start = System.currentTimeMillis();

		long elapsed = 0;
		while (counter.get() > 0 && elapsed < timeoutMs) {
			elapsed = System.currentTimeMillis() - start;
			try {
				Thread.sleep(sleepPeriod());
			} catch (InterruptedException e) {
				Timber.d("async countdown interrupted. elapsed=" + elapsed);
				return;
			}
		}

		Timber.d("done async wait. elapsed=" + elapsed);
	}

	/** block on an AtomicInteger to decrement to `minVal`
	 *  timeout after `timeoutMs` milliseconds
	 *  after counter/interval expires decrement counter is decremented (for chaining)
	 */
	public static void asyncWaitStep(final AtomicInteger counter, final int minVal, final int timeoutMs) {
		if (null == counter) { throw new IllegalArgumentException("AtomicInteger counter is null"); }
		if (timeoutMs <= 0) { throw new IllegalArgumentException("timeout must be greater than 0 ms"); }
		if (minVal <= 0) { throw new IllegalArgumentException("minVal must be greater than 0"); }

		final long start = System.currentTimeMillis();

		long elapsed = 0;
		while (counter.get() > minVal && elapsed < timeoutMs) {
			elapsed = System.currentTimeMillis() - start;
			try {
				Thread.sleep(sleepPeriod());
			} catch (InterruptedException e) { }
		}

		Timber.d("done async step elapsed=" + elapsed + "ms, decrementing");
		counter.decrementAndGet();
	}
}
