package xst.pinmigrations.app.util;

import java.lang.reflect.Field;

@SuppressWarnings("unchecked")
public class TReflectionUtil {
	/** get private field from some object
	 * @throws NoSuchFieldException for typos!
	 * @throws IllegalAccessException shouldn't ever throw this; this uses `field.setAccessible(true)`
	 */
	public static <T, U> U getPrivateField(final T obj, final String fieldName)
			throws NoSuchFieldException, IllegalAccessException {
		// get private field, set access
		final Field field = obj.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);

		// get field from object
		return (U) field.get(obj);
	}

	/** set private field on some object
	 * @throws NoSuchFieldException for typos!
	 * @throws IllegalAccessException shouldn't ever throw this; this uses `field.setAccessible(true)`
	 */
	public static <T> void setPrivateField(final T obj, final String fieldName, final Object newFieldValue)
			throws NoSuchFieldException, IllegalAccessException {
		// get private field, set access
		final Field field = obj.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);

		field.set(obj, newFieldValue);
	}
}
