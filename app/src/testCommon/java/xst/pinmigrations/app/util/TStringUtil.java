package xst.pinmigrations.app.util;

import java.util.Random;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class TStringUtil {
	/** max delta for comparing doubles (in tests!) */
	public static final double MAX_DELTA_DOUBLE_COMPARISONS = 1e-15;

	private static final Random rand = new Random();

	/** maxlength is total chars in sentence including period (full-stop) */
	public static String randomSentence(final int maxLength) {
		if (maxLength < 1) { throw new IllegalArgumentException("length < 1: " + maxLength); }

		final StringBuilder sb = new StringBuilder(maxLength + 10);

		while (sb.length() < maxLength) {
			sb.append( randomAlphabetic( rand.nextInt(8) + 1 ) ); // generate word (1-8 chars per word)
			sb.append(' ');
		}

		return sb.toString().substring(0, maxLength - 1).concat("."); // -1 to add period
	}

	/** lines is exact amount of sentences, each with a terminating period (full-stop) then new-line */
	public static String randomParagraph(final int lines) {
		if (lines < 1) { throw new IllegalArgumentException("length < 1: " + lines); }

		final StringBuilder sb = new StringBuilder(lines * 40);
		for (int i=0; i<lines; i++) {
			sb.append( randomSentence( rand.nextInt(30) + 11 )); // generate sentence (11-40 chars per sentence)
			sb.append("\n");
		}

		return sb.toString();
	}
}
