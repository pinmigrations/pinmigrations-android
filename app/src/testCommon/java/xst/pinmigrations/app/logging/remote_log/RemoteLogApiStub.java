package xst.pinmigrations.app.logging.remote_log;

import java.util.List;

import retrofit2.http.Field;
import rx.Observable;
import xst.pinmigrations.domain.api.BaseApiStub;
import xst.pinmigrations.domain.rx.RxJavaService;

public class RemoteLogApiStub extends BaseApiStub<RemoteLogApi> implements RemoteLogApi {
	public RemoteLogApiStub(final RxJavaService rxJavaService) {
		super(rxJavaService);
	}


	@Override public Observable<Void> ping() {
		return rxJavaService.newVoidObservable();
	}

	@Override public Observable<Void> log(@Field("lines") final List<String> lines) {
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	@Override public void reset() {

	}


	@Override public boolean stubbedMethodsCompleted() {
		throw new RuntimeException("NOT IMPLEMENTED");
	}
}
