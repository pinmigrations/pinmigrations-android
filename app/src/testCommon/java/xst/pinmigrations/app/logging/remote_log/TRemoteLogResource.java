package xst.pinmigrations.app.logging.remote_log;

import android.support.annotation.NonNull;

import xst.pinmigrations.domain.api.ApiGenerator;

import static org.mockito.Mockito.mock;

public class TRemoteLogResource extends RemoteLogResource {
	public TRemoteLogResource(@NonNull final ApiGenerator apiGenerator) {
		super(apiGenerator);
	}

	//region chainable settings/options
	public TRemoteLogResource disabled() {
		this.loggingDisabled = true;
		return this;
	}
	public TRemoteLogResource enabled() {
		this.loggingDisabled = true;
		return this;
	}
	//endregion

	long currentTimeMillis = 0L;
	public void setCurrentTimeMs(final long newTime) {
		currentTimeMillis = newTime;
	}

	@Override protected long currentTimeMillis() {
		return currentTimeMillis;
	}

	public static RemoteLogResource getMock() {
		return mock(RemoteLogResource.class);
	}
}
