package xst.pinmigrations.app.logging;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.System.out;

/**
 * Logger for UnitTests
 */
@SuppressLint("SimpleDateFormat")
public class TestLogger implements TestLog {
	// NOTE: intentially same length
	private static final SimpleDateFormat DEFAULT_TIME_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS");
	private static final SimpleDateFormat FINE_TIME_FORMAT = new SimpleDateFormat(":mm:ss.SSSSS");

	private static SimpleDateFormat timeFormat = DEFAULT_TIME_FORMAT;

	private static boolean logWithTimeStamp = true;
	private static boolean logWithThread = false;
	private static boolean logWithPriority = false;

	@Override public void println(final int priority, @NonNull String msg) {
		if (logWithTimeStamp) {
			out.print( getTimestamp() );
		}
		if (logWithThread) { out.print(shortThreadName() + "| "); }
		if (logWithPriority) { out.print(priorityString(priority)); }

		out.println(msg);
	}


	public static void resetTimeFormatter() { timeFormat = DEFAULT_TIME_FORMAT; }
	public static void setFineTime() { timeFormat = FINE_TIME_FORMAT; }

	public static void logWithThread() { logWithThread = true; }
	public static void omitThread() { logWithThread = false; }

	public static void omitTimestamp() { logWithTimeStamp = false; }
	public static void logWithTimestamp() { logWithTimeStamp = true; }

	public static void omitPriority() { logWithPriority = false; }
	public static void logWithPriority() { logWithPriority = true; }

	private static String priorityString(final int priority) {
		char level = '?';
		switch(priority) {
			case Log.DEBUG: level = 'D'; break;
			case Log.INFO: level = 'I'; break;
			case Log.WARN: level = 'W'; break;
			case Log.ERROR: level = 'E'; break;
			case Log.ASSERT: level = '!'; break;
		}

		return "<" + level + "> ";
	}

	private static final int MAX_THREAD_NAME_LENGTH = 20;
	@SuppressWarnings("MethodWithMultipleReturnPoints")
	private static String shortThreadName() {
		final String fullThreadName = Thread.currentThread().getName();

		if (fullThreadName.length() == MAX_THREAD_NAME_LENGTH) {
			return fullThreadName;
		} else if (fullThreadName.length() < MAX_THREAD_NAME_LENGTH) {
			final int diff = MAX_THREAD_NAME_LENGTH - fullThreadName.length();
			return fullThreadName + LogUtil.getIndentSpaceString(diff);
		} else { // fullThreadName.length() > MAX_THREAD_NAME_LENGTH
			return StringUtils.substring(fullThreadName, -MAX_THREAD_NAME_LENGTH);
		}
	}

	public static String getTimestamp() {
		return "[" + timeFormat.format(new Date()) + "] ";
	}
}
