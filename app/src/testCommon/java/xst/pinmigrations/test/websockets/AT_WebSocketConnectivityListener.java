package xst.pinmigrations.test.websockets;

import android.support.annotation.NonNull;

import org.assertj.core.api.Fail;

import java.util.concurrent.atomic.AtomicInteger;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import timber.log.Timber;

import static com.google.common.base.Preconditions.checkNotNull;

/** used in WebSocketConnectivityTest
 * kept in `testCommon` in order to work-around ClassDefNotFound exception in AndroidTest
 * Root-Cause might be because `OkHttp3.WebSocket` / `OkHttp3.WebSocketListener` are not used in main code
 * so gradle-shrinker decides its ok to remove its code
 */
public class AT_WebSocketConnectivityListener extends WebSocketListener {
	final AtomicInteger counter;
	final String[] responseArray;
	public AT_WebSocketConnectivityListener(@NonNull final AtomicInteger counter, @NonNull final String[] responseArray) {
		this.counter = checkNotNull(counter);
		this.responseArray = responseArray;
	}


	@Override public void onMessage(final WebSocket webSocket, final String text) {
		webSocket.close(1000, "Test over");

		Timber.d("got response, closing WS connection...");
		Timber.d("RESPONSE (WS-thread): " + text);

		synchronized (responseArray) { responseArray[0] = text; } // write response first to prevent race-condition
		counter.decrementAndGet(); // this can be contended
	}

	@Override public void onFailure(WebSocket webSocket, Throwable t1, Response response) {
		Fail.fail("websocket failure", t1);
	}

	@Override public void onOpen(WebSocket webSocket, Response response) {
		Timber.d("WS open, awaiting response...");
	}
}
