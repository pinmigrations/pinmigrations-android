package xst.pinmigrations.test;

import android.support.annotation.NonNull;

import org.assertj.core.api.Fail;

import java.util.concurrent.TimeUnit;

import xst.pinmigrations.activities.IntentAssertsComparator;
import xst.pinmigrations.domain.api.BaseApiStub;
import xst.pinmigrations.domain.login.Login;
import xst.pinmigrations.domain.login.TLogin;
import xst.pinmigrations.domain.login.json.TSignedInResponse;
import xst.pinmigrations.domain.positions.TPosition;

/**
 * base class for ALL tests (unit tests, Robolectric tests, android tests)
 */
public abstract class BaseTest {
	// NOTE: these can be re-used
	public static final IntentAssertsComparator INTENT_ASSERTS = new IntentAssertsComparator();
	public static final TPosition.AssertsComparator POSITION_ASSERTS = new TPosition.AssertsComparator();
	public static final TPosition.UnsavedAssertsComparator UNSAVED_POSITION_ASSERTS = new TPosition.UnsavedAssertsComparator();

	public static final TLogin.AssertsComparator LOGIN_ASSERTS = new TLogin.AssertsComparator();
	public static final xst.pinmigrations.domain.positions.TPositionJson.AssertsComparator POSITION_JSON_ASSERTS = new xst.pinmigrations.domain.positions.TPositionJson.AssertsComparator();

	public static final TSignedInResponse.AssertsComparator SIGNED_IN_RESPONSE_ASSERTS = new TSignedInResponse.AssertsComparator();

	public static final TimeUnit MILLISECONDS = java.util.concurrent.TimeUnit.MILLISECONDS;
	public static final TimeUnit SECONDS = java.util.concurrent.TimeUnit.SECONDS;

	public static final int LOGGED_IN = Login.LOGGED_IN;
	public static final int UNAUTHENTICATED = Login.UNAUTHENTICATED;
	public static final int LOGIN_FAILED = Login.LOGIN_FAILED;

	public static final int STUB_RESP_MIN_WAIT_MS = BaseApiStub.STUB_RESP_MIN_WAIT_MS;


	/* these should ensure consistency (eg: instead of JUnit.fail + AssertJ.fail, args etc.. */

	public static void fail(@NonNull final String failureMessage) { Fail.fail(failureMessage); }
	public static void fail(@NonNull final String failureMessage, final Throwable realCause) {
		Fail.fail(failureMessage, realCause);
	}
	public static void fail(@NonNull final Throwable throwable) {
		fail("test-fail", throwable);
	}
	public static void failBecauseExceptionWasNotThrown(@NonNull final Class<? extends Throwable> expectedExceptionClass) {
		Fail.failBecauseExceptionWasNotThrown(expectedExceptionClass);
	}
}
