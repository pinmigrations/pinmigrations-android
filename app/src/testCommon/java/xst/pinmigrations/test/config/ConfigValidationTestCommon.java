package xst.pinmigrations.test.config;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.function.Consumer;

import timber.log.Timber;

public final class ConfigValidationTestCommon {
	/** lets other source-sets validate their compile-link against this source-set */
	public static String validateConfig() { return "AWESOME"; }

	/** check whether a lib that is not in `main` build-config can be linked against
	 * ApacheCommons-Lang isnt a dependency in main productFlavor, but it is for test/androidTest
	 */
	public static String tryUsingApacheCommons() {
		return RandomStringUtils.randomAlphanumeric(10);
	}


	/** NOTE: invoking these methods from `androidTest` sometimes requires a `fullCleanBuild`
	 * causes IllegalAccessError: Class ref in pre-verified class resolved to unexpected
	 */
	public static Consumer<String> getMethodReference() {
		return System.out::println;
	}
	public static Consumer<String> getLambda() {
		return (s) -> { };
	}
}
