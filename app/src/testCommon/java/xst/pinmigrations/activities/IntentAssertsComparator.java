package xst.pinmigrations.activities;

import android.content.Intent;

import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThat;

public class IntentAssertsComparator implements Comparator<Intent> {
	@Override public int compare(final Intent subject, final Intent expected) {

		assertThat( expected.filterEquals(subject) )
				.overridingErrorMessage("Expected Intent <%s> \n    to match <%s>", subject, expected)
				.isTrue();

		return 0;
	}
}
