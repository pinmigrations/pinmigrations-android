package xst.pinmigrations.domain.login;

import android.support.annotation.NonNull;

import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.test.StatefulStub;

import static com.google.common.base.Preconditions.checkNotNull;

public class TLoginManager extends LoginManager implements StatefulStub {

	public TLoginManager(@NonNull final Login loginState, @NonNull final PrefsManager prefsManager) {
		super(loginState, prefsManager);
	}

	/** NOTE: registers self as stub in app (used in AndroidTests) */
	public TLoginManager(@NonNull final Login loginState, @NonNull final PrefsManager prefsManager,
						 @NonNull final App app) {
		super(loginState, prefsManager);

		app.addStub(this);
	}

	public @NonNull Login getInternalLoginObj() {
		synchronized (this.loginObj) {
			return loginObj;
		}
	}

	/** NOTE: copies fields into final Login in LoginManager, doesnt (cant) set the passed in object */
	public void setLoginObj(@NonNull final Login loginToCopy) {
		final Login login = checkNotNull(loginToCopy);

		synchronized (this.loginObj) {
			this.loginObj.loginState = login.loginState;
			this.loginObj.userEmail = login.userEmail;
			this.loginObj.userPassword = login.userPassword;
			this.loginObj.authenticityToken = login.authenticityToken;
			this.loginObj.appSessionCookie = login.appSessionCookie;
			this.loginObj.rememberUserToken = login.rememberUserToken;
		}
	}

	@Override public void reset() {
		setLoginObj(new TLogin());
	}

	public static TLoginManager loggedInTrueDummyManager(@NonNull final PrefsManager prefsManager) {
		return new TLoginManager( TLogin.newAuthed(TLogin.TEST_EMAIL, TLogin.TEST_PASSWORD), prefsManager );
	}
	public static TLoginManager loggedInFalseDummyManager(@NonNull final PrefsManager prefsManager) {
		return new TLoginManager( TLogin.newPristineUnauthed(), prefsManager );
	}
}
