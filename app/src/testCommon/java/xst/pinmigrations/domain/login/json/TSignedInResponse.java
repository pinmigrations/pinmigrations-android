package xst.pinmigrations.domain.login.json;

import android.support.annotation.NonNull;

import java.util.Comparator;

import rx.Observable;

import static org.assertj.core.api.Assertions.assertThat;

public class TSignedInResponse extends SignedInResponse {
	private TSignedInResponse() {}
	public static @NonNull Builder builder() { return new Builder(); }


	public static class Builder {
		final TSignedInResponse signedInRespObj = new TSignedInResponse();
		public @NonNull TSignedInResponse build() { return signedInRespObj; }

		public Builder isSignedIn(final boolean isSignedIn) { signedInRespObj.isSignedIn = isSignedIn; return this; }
	}
	public @NonNull static TSignedInResponse.Builder newBuilder() {
		return new TSignedInResponse.Builder();
	}


	public @NonNull static TSignedInResponse newSignedInTrue() {
		return newBuilder().isSignedIn(true).build();
	}
	public @NonNull static TSignedInResponse newSignedInFalse() {
		return newBuilder().isSignedIn(false).build();
	}

	public @NonNull static Observable<SignedInResponse> newSignedInTrueObservable() {
		return Observable.just(newSignedInTrue());
	}
	public @NonNull static Observable<SignedInResponse> newSignedInFalseObservable() {
		return Observable.just(newSignedInFalse());
	}

	public static class AssertsComparator implements Comparator<SignedInResponse> {
		@Override public int compare(final SignedInResponse subject, final SignedInResponse expected) {

			assertThat(subject.isSignedIn).as("SignedInResponse.isSignedIn")
					.isEqualTo(expected.isSignedIn);
			return 0;
		}
	}
}
