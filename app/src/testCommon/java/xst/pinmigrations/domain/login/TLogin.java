package xst.pinmigrations.domain.login;

import android.support.annotation.NonNull;

import java.util.Comparator;

import rx.Observable;
import rx.Subscriber;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.assertj.core.api.Assertions.assertThat;

public class TLogin extends Login {
	public static final String NOT_SIGNED_IN_JSON = "{\"signed_in\": false}";
	public static final String SIGNED_IN_JSON = "{\"signed_in\": true}";
	public static final String CSRF_TOKEN_1 = "csrf-token-1";
	public static final String CSRF_TOKEN_2 = "csrf-token-2";
	public static final String CSRF_TOKEN_3 = "csrf-token-3";
	public static final String CSRF_TOKEN_4 = "csrf-token-4";
	public static final String SESSION_1 = "session-1";
	public static final String SESSION_2 = "session-2";
	public static final String SESSION_3 = "session-3";
	public static final String SESSION_4 = "session-4";
	public static final String REMEMBER_TOKEN_1 = "remember-token-1";
	public static final String REMEMBER_TOKEN_2 = "remember-token-2";

	public TLogin() {}
	public static Builder builder() { return new Builder(); }


	public static class Builder {
		final TLogin loginObj = new TLogin();
		public TLogin build() { return loginObj; }

		public Builder loginState(@LoginState int loginState) { loginObj.loginState = loginState; return this; }
		public Builder userEmail(String userEmail) { loginObj.userEmail = userEmail; return this; }
		public Builder userPassword(String userPassword) { loginObj.userPassword = userPassword; return this; }
		public Builder authenticityToken(String authenticityToken) { loginObj.authenticityToken = authenticityToken; return this; }
		public Builder appSessionCookie(String appSessionCookie) { loginObj.appSessionCookie = appSessionCookie; return this; }
		public Builder rememberUserToken(String rememberUserToken) { loginObj.rememberUserToken = rememberUserToken; return this; }
	}

	public static TLogin.Builder newBuilder() {
		return new TLogin.Builder();
	}

	public static final String TEST_EMAIL = "test@test.com";
	public static final String TEST_PASSWORD = "password";

	/** currently signed in */
	public static TLogin newTestAuthed() {
		return newBuilder().userEmail(TEST_EMAIL).userPassword(TEST_PASSWORD)
				.appSessionCookie(SESSION_1).authenticityToken(CSRF_TOKEN_1).rememberUserToken(REMEMBER_TOKEN_1)
				.loginState(LOGGED_IN).build();
	}
	public static TLogin newAuthed(@NonNull final String email, @NonNull final String password) {
		final TLogin loginObj = newTestAuthed();
		loginObj.userEmail = email;
		loginObj.userPassword = password;
		return loginObj;
	}
	/** never signed in or data cleared */
	public static TLogin newPristineUnauthed() {
		return newBuilder().loginState(UNAUTHENTICATED).build();
	}
	public static TLogin newLogoutUnauthed(@NonNull final String email, @NonNull final String password) {
		return newBuilder().userEmail(email).userPassword(password).loginState(UNAUTHENTICATED).build();
	}
	/** previously signed in, logged out */
	public static TLogin newTestLoggedOut() {
		return newBuilder().userEmail(TEST_EMAIL).userPassword(TEST_PASSWORD)
				.appSessionCookie(SESSION_1).authenticityToken(CSRF_TOKEN_1)
				.loginState(UNAUTHENTICATED).build();
	}


	/** performs assertions on Position but doesnt actually implement real Comparator logic */
	public static class AssertsComparator implements Comparator<Login> {
		@Override public int compare(final Login subject, final Login expected) {

			assertThat(subject.authenticityToken).as("loginObj.authenticityToken")
					.isEqualTo(expected.authenticityToken);
			assertThat(subject.appSessionCookie).as("loginObj.appSessionCookie")
					.isEqualTo(expected.appSessionCookie);
			assertThat(subject.rememberUserToken).as("loginObj.rememberUserToken")
					.isEqualTo(expected.rememberUserToken);

			assertThat(subject.userEmail).as("loginObj.userEmail")
					.isEqualTo(expected.userEmail);
			assertThat(subject.userPassword).as("loginObj.userPassword")
					.isEqualTo(expected.userPassword);

			final String subjectState = getLoginStateString(subject.loginState);
			final String expectedState = getLoginStateString(expected.loginState);

			assertThat(subject.loginState)
					.overridingErrorMessage("Expected loginState <%s> to be <%s>", subjectState, expectedState)
					.isEqualTo(expected.loginState);

			return 0;
		}
	}


	/** updates TLoginManager w TLoginObj when subscribed to */
	public static class LoginInterceptObservable implements Observable.OnSubscribe<Void> {
		final TLoginManager loginManager;
		final TLogin loginObj;
		public LoginInterceptObservable(final TLoginManager loginManager, final TLogin loginObj) {
			this.loginManager = checkNotNull(loginManager);
			this.loginObj = checkNotNull(loginObj);
		}

		@Override public void call(final Subscriber<? super Void> subscriber) {
			loginManager.setLoginObj(loginObj);

			// NOTE: Retrofit APIs that return Observable<Void>, dont invoke onNext, only onCompleted
			subscriber.onCompleted();
		}

		public static @NonNull Observable<Void> create(final TLoginManager loginManager, final TLogin loginObj) {
			return Observable.unsafeCreate(new LoginInterceptObservable(loginManager, loginObj));
		}
	}
}
