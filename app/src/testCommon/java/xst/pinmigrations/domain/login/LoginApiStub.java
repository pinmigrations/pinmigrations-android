package xst.pinmigrations.domain.login;

import java.util.ArrayDeque;
import java.util.ArrayList;

import rx.Observable;
import xst.pinmigrations.domain.api.BaseApiStub;
import xst.pinmigrations.domain.login.json.SignedInResponse;
import xst.pinmigrations.domain.rx.RxJavaService;

/**
 * technically usable anywhere, only used in RF tests
 */
public class LoginApiStub extends BaseApiStub<LoginApi> implements LoginApi {
	public LoginApiStub(final RxJavaService rxJavaService) { super(rxJavaService); }

	//region checkSigninStatus

	@Override public Observable<SignedInResponse> checkSigninStatus(
			final String host, final String origin, final String referer) {

		synchronized (checkSigninStatusParams) {
			checkSigninStatusParams.add(CheckSigninStatusParams.create(host, origin, referer));
		}
		synchronized (onCheckSigninStatus) {
			// NOTE: not working with composition of delay
			return apiResponse( onCheckSigninStatus.removeFirst() );
		}
	}

	public void putCheckSigninStatus(final Observable<SignedInResponse> obs) {
		synchronized (onCheckSigninStatus) { onCheckSigninStatus.addLast(obs); }
	}
	public ArrayList<CheckSigninStatusParams> getCheckSigninStatusParams() {
		synchronized (checkSigninStatusParams) { return checkSigninStatusParams; }
	}
	protected final ArrayDeque<Observable<SignedInResponse>> onCheckSigninStatus = new ArrayDeque<>();
	public boolean checkSigninStatusCompleted() {
		synchronized (onCheckSigninStatus) { return 0 == onCheckSigninStatus.size(); }
	}
	protected final ArrayList<CheckSigninStatusParams> checkSigninStatusParams = new ArrayList<>();

	public static class CheckSigninStatusParams {
		public String host, origin, referer;
		public static CheckSigninStatusParams create(final String host, final String origin, final String referer) {
			CheckSigninStatusParams params = new CheckSigninStatusParams();
			params.host = host;
			params.origin = origin;
			params.referer = referer;
			return params;
		}
	}
	//endregion


	//region login

	@Override public Observable<Void> login(final String email, final String password) {
		synchronized (loginParams) {
			 loginParams.add(LoginParams.create(email, password));
		}
		synchronized (onLogin) {
			return apiResponse( onLogin.removeFirst() );
		}
	}

	public void putLogin(final Observable<Void> obs) { synchronized (onLogin) { onLogin.addLast(obs); } }
	public ArrayList<LoginParams> getLoginParams() { synchronized (loginParams) { return loginParams; } }
	protected final ArrayList<LoginParams> loginParams = new ArrayList<>();
	protected final ArrayDeque<Observable<Void>> onLogin = new ArrayDeque<>();
	public boolean loginCompleted() { synchronized (onLogin) { return 0 == onLogin.size(); } }
	public static class LoginParams {
		public String email, password;
		public static LoginParams create(final String email, final String password) {
			LoginParams params = new LoginParams();
			params.email = email;
			params.password = password;
			return params;
		}
	}
	//endregion


	//region logout

	@Override public Observable<Void> logout() {
		synchronized (onLogout) { return apiResponse( onLogout.removeFirst() ); }
	}

	public void putLogout(final Observable<Void> obs) {
		synchronized (onLogout) { onLogout.addLast(obs); }
	}
	protected final ArrayDeque<Observable<Void>> onLogout = new ArrayDeque<>();
	public boolean logoutCompleted() {
		synchronized (onLogout) { return 0 == onLogout.size(); }
	}
	//endregion

	@Override public void reset() {
		synchronized (onCheckSigninStatus) { onCheckSigninStatus.clear(); }
		synchronized (checkSigninStatusParams) { checkSigninStatusParams.clear(); }

		synchronized (onLogin) { onLogin.clear(); }
		synchronized (loginParams) { loginParams.clear(); }

		synchronized (onLogout) { onLogout.clear(); }
	}


	@Override public boolean stubbedMethodsCompleted() {
		return loginCompleted() && logoutCompleted() && checkSigninStatusCompleted();
	}
}
