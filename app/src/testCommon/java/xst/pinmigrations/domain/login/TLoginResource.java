package xst.pinmigrations.domain.login;

import android.support.annotation.NonNull;

import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.network.NetworkService;

public class TLoginResource extends LoginResource {
	public TLoginResource(@NonNull final ApiGenerator apiGenerator,
						  @NonNull final LoginManager loginManager,
						  @NonNull final NetworkService networkService,
						  @NonNull final RemoteLogResource remoteLogResource) {
		super(apiGenerator, loginManager, networkService, remoteLogResource);
	}

	public LoginApi getApi() { return api; }
}
