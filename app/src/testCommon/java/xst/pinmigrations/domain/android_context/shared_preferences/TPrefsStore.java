package xst.pinmigrations.domain.android_context.shared_preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class TPrefsStore extends PrefsStore {
	public TPrefsStore(@NonNull final Context ctx, @NonNull final String prefsFileName) {
		super(ctx, prefsFileName);
	}

	public @NonNull SharedPreferences getPrefs() { return prefs; }
}
