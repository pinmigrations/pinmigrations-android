package xst.pinmigrations.domain.android_context;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;

public class TGoogleApiService extends GoogleApiService {
	public TGoogleApiService(@NonNull final Context ctx) {
		super(ctx);
	}

	private boolean useRealImplementation = false;
	public void useRealImplementation(final boolean useReal) {
		useRealImplementation = useReal;
	}

	protected int availabilityStatus = ConnectionResult.SUCCESS;
	@Override public int queryAvailability() {
		if (useRealImplementation) {
			return super.queryAvailability();
		} else { return availabilityStatus; }
	}
	public void setAvailabilityStatus(final int status) { availabilityStatus = status; }


	protected boolean isAvailable = true;
	@Override public boolean isAvailable() {
		if (useRealImplementation) {
			return super.isAvailable();
		} else { return isAvailable; }
	}
	public void setIsAvailable(final boolean avail) { isAvailable = avail; }
}
