package xst.pinmigrations.domain.activity_context.ui_service;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.rx.RxJavaService;

public class UiServiceSpy extends UiService {

	public UiServiceSpy(
			@NonNull final App app,
			@NonNull final BaseActivity activity,
			@NonNull final RxJavaService rxJavaService) {
		super(app, activity, rxJavaService);
	}

	//region Important Messages
	protected final ArrayList<String> importantMessages = new ArrayList<>();
	/** super delegates to full-arg overload */
	@Override public void showImportantMessage(@NonNull final String text) { super.showImportantMessage(text); }

	/** record message then delegate to real implementation */
	@Override public void showImportantMessage(
			@NonNull final String text, final double delay, @NonNull final TimeUnit timeUnit) {
		synchronized (importantMessages) { importantMessages.add(text); }
		super.showImportantMessage(text, delay, timeUnit);
	}
	public @NonNull ArrayList<String> getImportantMessages() {
		synchronized (importantMessages) { return importantMessages; }
	}
	//endregion

	//region Quick Notices
	protected final ArrayList<String> quickNotices = new ArrayList<>();


	/** super delegates to full-arg overload */
	@Override public void showQuickNotice(final String text) { super.showQuickNotice(text); }

	@Override public void showQuickNotice(
			@NonNull final String text, final double delay, @NonNull final TimeUnit timeUnit) {
		synchronized (quickNotices) { quickNotices.add(text); }
		super.showQuickNotice(text, delay, timeUnit);
	}
	public @NonNull ArrayList<String> getQuickNotices() {
		synchronized (quickNotices) { return quickNotices; }
	}
	//endregion
}
