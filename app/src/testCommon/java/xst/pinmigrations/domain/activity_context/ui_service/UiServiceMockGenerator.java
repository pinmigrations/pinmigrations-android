package xst.pinmigrations.domain.activity_context.ui_service;

import android.support.annotation.NonNull;

import org.mockito.Mockito;

import java.util.HashMap;

import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.rx.RxJavaService;

public final class UiServiceMockGenerator extends UiServiceGenerator {

	public UiServiceMockGenerator() {
		super(Mockito.mock(App.class), Mockito.mock(RxJavaService.class));
	}

	protected final HashMap<Class<? extends BaseActivity>, UiService> generatedUiServiceMocks = new HashMap<>();

	/** creates or retrieves UiService mock */
	public @NonNull UiService newUiService(@NonNull final BaseActivity activity) {
		final Class<? extends BaseActivity> activityClass = activity.getClass();

		UiService uiServiceMock;
		synchronized (generatedUiServiceMocks) {
			uiServiceMock = generatedUiServiceMocks.get(activityClass);
		}

		if (null == uiServiceMock) {
			uiServiceMock = Mockito.mock(UiService.class);
			synchronized (generatedUiServiceMocks) {
				generatedUiServiceMocks.put(activityClass, uiServiceMock);
			}
		}

		return uiServiceMock;
	}

	public @NonNull UiService getGeneratedUiService(@NonNull final Class<? extends BaseActivity> activityClass) {
		synchronized (generatedUiServiceMocks) {
			return generatedUiServiceMocks.get(activityClass);
		}
	}
}
