package xst.pinmigrations.domain.activity_context.ui_service;

import android.support.annotation.NonNull;

import java.util.HashMap;

import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.test.StatefulStub;

import static com.google.common.base.Preconditions.checkNotNull;

public final class UiServiceSpyGenerator extends UiServiceGenerator implements StatefulStub {

	public UiServiceSpyGenerator(@NonNull final App app, final RxJavaService rxJavaService) {
		super(app, rxJavaService);
		app.addStub(this);
	}

	protected final HashMap<Class<? extends BaseActivity>, UiServiceSpy> generatedUiServiceSpies = new HashMap<>();

	/** creates or retrieves UiService mock */
	public @NonNull UiService newUiService(@NonNull final BaseActivity activity) {
		final Class<? extends BaseActivity> activityClass = activity.getClass();

		UiServiceSpy uiServiceSpy;
		synchronized (generatedUiServiceSpies) {
			uiServiceSpy = generatedUiServiceSpies.get(activityClass);
		}

		if (null == uiServiceSpy) {
			uiServiceSpy = new UiServiceSpy(checkNotNull(getApp()), activity, rxJavaService);
			synchronized (generatedUiServiceSpies) {
				generatedUiServiceSpies.put(activityClass, uiServiceSpy);
			}
		}

		return uiServiceSpy;
	}

	public @NonNull UiServiceSpy getGeneratedUiService(@NonNull final Class<? extends BaseActivity> activityClass) {
		synchronized (generatedUiServiceSpies) {
			return generatedUiServiceSpies.get(activityClass);
		}
	}

	@Override public void reset() {
		generatedUiServiceSpies.clear();
	}
}
