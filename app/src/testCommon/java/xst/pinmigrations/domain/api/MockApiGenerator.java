package xst.pinmigrations.domain.api;

import android.support.annotation.NonNull;

import org.mockito.Mockito;

import java.util.HashMap;

import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.rx.RxJavaService;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Verify.verifyNotNull;
import static xst.pinmigrations.domain.api.ApiUtil.API_LIST;

/** generates Retrofit API mocks for RF tests */
public class MockApiGenerator extends ApiGenerator {
	public MockApiGenerator() {
		super(Mockito.mock(NetworkService.class), Mockito.mock(RxJavaService.class));
	}

	protected final HashMap<Class<?>, Object> generatedApiMocks = new HashMap<>();

	@Override @SuppressWarnings("unchecked")
	public @NonNull <S> S createService(@NonNull final Class<S> serviceClass) {
		checkArgument(API_LIST.contains(serviceClass), "invalid API class");

		S serviceMock = (S) generatedApiMocks.get(serviceClass);
		if (null != serviceMock) {
			return serviceMock;
		} else {
			serviceMock = Mockito.mock(serviceClass);
			generatedApiMocks.put(serviceClass, serviceMock);
			return serviceMock;
		}
	}

	@SuppressWarnings("unchecked")
	public @NonNull <S> S getApiMock(@NonNull final Class<S> serviceClass) {
		return (S) verifyNotNull(generatedApiMocks.get(serviceClass));
	}
}
