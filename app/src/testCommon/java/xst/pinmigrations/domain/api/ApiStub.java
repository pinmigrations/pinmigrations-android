package xst.pinmigrations.domain.api;

import xst.pinmigrations.test.StatefulStub;

public interface ApiStub<T> extends StatefulStub { }
