package xst.pinmigrations.domain.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.HashMap;

import xst.pinmigrations.app.App;
import xst.pinmigrations.app.logging.remote_log.RemoteLogApi;
import xst.pinmigrations.app.logging.remote_log.RemoteLogApiStub;
import xst.pinmigrations.domain.login.LoginApi;
import xst.pinmigrations.domain.login.LoginApiStub;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.positions.PositionsApi;
import xst.pinmigrations.domain.positions.PositionsApiStub;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.test.StatefulStub;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.api.ApiUtil.API_LIST;

/** generates Retrofit API stubs for AT tests */
public class StubApiGenerator extends ApiGenerator implements StatefulStub {
	@NonNull final App app;
	public StubApiGenerator(@NonNull final NetworkService networkService,
							@NonNull final RxJavaService rxJavaService,
							@NonNull final App app) {
		super(networkService, rxJavaService);
		this.app = checkNotNull(app);
		this.app.addStub(this);
	}

	protected final HashMap<Class<?>, ApiStub<?>> generatedApiStubs = new HashMap<>();

	@Override @SuppressWarnings("unchecked")
	/** creates or retrieves ApiStubs */
	public @NonNull <S> S createService(@NonNull final Class<S> serviceClass) {
		checkArgument(API_LIST.contains(serviceClass), "invalid API class");

		ApiStub<?> apiStub = generatedApiStubs.get(serviceClass);
		if (null == apiStub) {
			if (serviceClass.equals(LoginApi.class)) {
				apiStub = new LoginApiStub(rxJavaService);
				generatedApiStubs.put(LoginApi.class, apiStub);
			} else if (serviceClass.equals(PositionsApi.class)) {
				apiStub = new PositionsApiStub(rxJavaService);
				generatedApiStubs.put(PositionsApi.class, apiStub);
			} else if (serviceClass.equals(RemoteLogApi.class)) {
				apiStub = new RemoteLogApiStub(rxJavaService);
				generatedApiStubs.put(RemoteLogApi.class, apiStub);
			} else {
				throw new RuntimeException("ApiStub not implemented for " + serviceClass.getCanonicalName());
			}
		}

		return (S) apiStub;
	}

	@SuppressWarnings("unchecked")
	public @Nullable <S> ApiStub<S> getApiStub(@NonNull final Class<S> serviceClass) {
		return (ApiStub<S>) generatedApiStubs.get(serviceClass);
	}

	@Override public void reset() {
		for (final ApiStub<?> apiStub: generatedApiStubs.values()) { apiStub.reset(); }
	}
}
