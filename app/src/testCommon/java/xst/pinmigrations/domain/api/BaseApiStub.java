package xst.pinmigrations.domain.api;

import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import xst.pinmigrations.domain.rx.RxJavaService;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class BaseApiStub<T> implements ApiStub<T> {
	public static final int MIN_API_DELAY_MS = 125;
	public static final int MAX_API_DELAY_MS = 450;
	public static final int STUB_RESP_MIN_WAIT_MS = MAX_API_DELAY_MS + 200;

	@NonNull final protected RxJavaService rxJavaService;
	public BaseApiStub(final RxJavaService rxJavaService) {
		this.rxJavaService = checkNotNull(rxJavaService);
	}

	protected <R> Observable<R> apiResponse(@NonNull final Observable<R> obs) {
		return rxJavaService.randomDelayObservable(obs, MIN_API_DELAY_MS, MAX_API_DELAY_MS, TimeUnit.MILLISECONDS);
	}

	/**
	 * return true if all method-stub lists are cleared
	 * this is a facility method, implement *Completed for each stubbed method!
	 */
	public abstract boolean stubbedMethodsCompleted();
}
