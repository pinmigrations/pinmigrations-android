package xst.pinmigrations.domain.positions;

import android.support.annotation.NonNull;

import java.util.List;

import retrofit2.http.Field;
import retrofit2.http.Path;
import rx.Observable;
import xst.pinmigrations.domain.api.BaseApiStub;
import xst.pinmigrations.domain.positions.json.PositionJson;
import xst.pinmigrations.domain.rx.RxJavaService;

public class PositionsApiStub extends BaseApiStub<PositionsApi> implements PositionsApi {
	public PositionsApiStub(final RxJavaService rxJavaService) { super(rxJavaService); }

	@Override public void reset() {

	}

	@Override public Observable<List<PositionJson>> allPositions() {
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	@Override public Observable<String> feedHash() {
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	@Override public Observable<PositionJson> create(
			@Field("location[lat]") @NonNull final String lat,
			@Field("location[lng]") @NonNull final String lng,
			@Field("location[ord]") @NonNull final String ord,
			@Field("location[description]") @NonNull final String description,
			@Field("location[guid]") @NonNull final String guid) {
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	@Override public Observable<PositionJson> read(@Path("id") final long posId) {
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	@Override public Observable<PositionJson> update(
			@Path("id") final long posId,
			@Field("location[id]") @NonNull final String id,
			@Field("location[guid]") @NonNull final String guid,
			@Field("location[lat]") @NonNull final String lat,
			@Field("location[lng]") @NonNull final String lng,
			@Field("location[ord]") @NonNull final String ord,
			@Field("location[description]") @NonNull final String description) {
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	@Override public Observable<Void> destroy(@Path("id") final long posId) {
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	@Override public Observable<List<PositionJson>> positionsForGuid(@Path("guid") @NonNull final String guid) {
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	@Override public boolean stubbedMethodsCompleted() {
		throw new RuntimeException("NOT IMPLEMENTED");
	}
}
