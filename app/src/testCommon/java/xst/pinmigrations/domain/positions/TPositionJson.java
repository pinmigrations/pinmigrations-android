package xst.pinmigrations.domain.positions;

import java.util.ArrayList;
import java.util.Comparator;

import xst.pinmigrations.domain.positions.json.PositionJson;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class TPositionJson {
	public static Builder builder() { return new Builder(); }


	public static class Builder {
		final PositionJson pos = new PositionJson();
		public PositionJson build() { return pos; }

		public Builder id(long id) { pos.id = id; return this; }
		public Builder guid(String guid) { pos.guid = guid; return this; }
		public Builder lat(String lat) { pos.lat = lat; return this; }
		public Builder lng(String lng) { pos.lng = lng; return this; }
		public Builder ord(int ord) { pos.ord = ord; return this; }
		public Builder description(String description) { pos.description = description; return this; }
	}

	/** performs assertions on Position but doesnt actually implement real Comparator logic */
	public static class AssertsComparator implements Comparator<PositionJson> {
		@Override public int compare(final PositionJson subject, final PositionJson expected) {

			assertThat(subject.id).as("pos.id")
					.isEqualTo(expected.id);
			assertThat(subject.lat).as("pos.lat")
					.isEqualTo(expected.lat);
			assertThat(subject.lng).as("pos.lng")
					.isEqualTo(expected.lng);

			assertThat(subject.description).as("pos.description")
					.isEqualTo(expected.description);
			assertThat(subject.guid).as("pos.guid")
					.isEqualTo(expected.guid);
			assertThat(subject.ord).as("pos.ord")
					.isEqualTo(expected.ord);

			return 0;
		}
	}


	public abstract static class Factory {
		private Factory(){} // no instances
		public static PositionJson justCoords() {
			return new Builder()
					.lat("" + TPosition.Factory.generateLat())
					.lng("" + TPosition.Factory.generateLng())
					.build();
		}

		public static PositionJson withCoordsAndGuid() {
			return new Builder()
					.lat("" + TPosition.Factory.generateLat())
					.lng("" + TPosition.Factory.generateLng())
					.guid(TPosition.Factory.generateGuid())
					.build();
		}

		public static PositionJson realistic() {
			return new Builder()
					.id(TPosition.Factory.generateId())
					.lat("" + TPosition.Factory.generateLat())
					.lng("" + TPosition.Factory.generateLng())
					.guid(TPosition.Factory.generateGuid())
					.description(TPosition.Factory.generateDescription())
					.build();
		}

		public static ArrayList<PositionJson> listRealistic(final int listItems) {
			final ArrayList<PositionJson> list = new ArrayList<>(listItems);
			for (int i=0; i<listItems; i++) { list.add(realistic()); }
			return list;
		}

		public static ArrayList<PositionJson> listJustCoords(final int listItems) {
			final ArrayList<PositionJson> list = new ArrayList<>(listItems);
			for (int i=0; i<listItems; i++) { list.add(justCoords()); }
			return list;
		}

		public static ArrayList<PositionJson> listWithCoordsAndGuid(final int listItems) {
			final ArrayList<PositionJson> list = new ArrayList<>(listItems);
			for (int i=0; i<listItems; i++) { list.add(withCoordsAndGuid()); }
			return list;
		}
	}
}
