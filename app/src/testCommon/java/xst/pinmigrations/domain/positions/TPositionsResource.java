package xst.pinmigrations.domain.positions;

import android.support.annotation.NonNull;

import java.util.List;

import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.login.LoginManager;

/** exposes its ApiGenerator (may be Mock/Stub) */
public class TPositionsResource extends PositionsResource {
	public TPositionsResource(@NonNull final ApiGenerator apiGenerator,
							  @NonNull final RemoteLogResource remoteLogResource,
							  @NonNull final LoginManager loginManager) {
		super(apiGenerator, remoteLogResource, loginManager);
	}

	public PositionsApi getApi() { return api; }

	protected void setPositions(final List<Position> newPositions) {
		this.cacheNewPositions(newPositions);
	}
}
