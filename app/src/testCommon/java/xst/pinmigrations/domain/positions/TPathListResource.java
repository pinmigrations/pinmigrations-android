package xst.pinmigrations.domain.positions;

import android.support.annotation.NonNull;

import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.paths.PathListResource;

/** no special behavior yet */
public class TPathListResource extends PathListResource {
	public TPathListResource(
			@NonNull final PositionsResource positionsResource,
			@NonNull final PositionsSamplesResource positionsSamplesResource,
			@NonNull final RemoteLogResource remoteLogResource) {
		super(positionsResource, positionsSamplesResource, remoteLogResource);
	}
}
