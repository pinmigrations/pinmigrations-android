package xst.pinmigrations.domain.positions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import xst.pinmigrations.app.util.TStringUtil;
import xst.pinmigrations.domain.positions.json.PositionJson;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static xst.pinmigrations.app.util.TStringUtil.MAX_DELTA_DOUBLE_COMPARISONS;

public abstract class TPosition {
	public static final String FEED_HASH = "1234", FEED_HASH_2 = "5678";

	public static final String EMPTY_POSITIONS_JSON = "[]";
	public static final String POSITION_JSON_RAW_1 = "{\"description\":\"Julio arana\",\"guid\":\"\",\"id\":58,\"lat\":\"20.61222\",\"lng\":\"-103.3374\",\"ord\":0}";
	public static final String POSITION_JSON_1 = '[' + POSITION_JSON_RAW_1 + ']';
	public static final Position P1_EXPECTED = new Position(58, 20.61222, -103.3374, "Julio arana", "", 0);
	public static final PositionJson PJ1_EXPECTED = new PositionJson(P1_EXPECTED);

	public static final String POSITION_JSON_2 = "[{\"id\":144,\"lat\":\"33.74889\",\"lng\":\"-117.88055\",\"description\":null,\"guid\":\"70356a36-1075-4356-9915-109f58238548\",\"ord\":0}]";
	public static final Position P2_EXPECTED = new Position(144, 33.74889, -117.88055, "", "70356a36-1075-4356-9915-109f58238548", 0);
	public static final PositionJson PJ2_EXPECTED = new PositionJson(P2_EXPECTED);

	/** P3/P4 have same GUID */
	public static final String GUID_FOR_P3_AND_P4 = "6d261f0a-c00f-441a-bcca-b8014d91aeec";
	public static final String RAW_POSITION_JSON_3 = "{\"id\":3,\"lat\":\"33.12345\",\"lng\":\"-117.12345\",\"description\":\"d3\",\"guid\":\"6d261f0a-c00f-441a-bcca-b8014d91aeec\",\"ord\":0}";
	public static final Position P3_EXPECTED = new Position(3, 33.12345, -117.12345, "d3", "6d261f0a-c00f-441a-bcca-b8014d91aeec", 0);
	public static final PositionJson PJ3_EXPECTED = new PositionJson(P2_EXPECTED);
	public static final String RAW_POSITION_JSON_4 = "{\"id\":4,\"lat\":\"33.56789\",\"lng\":\"-117.56789\",\"description\":\"d4\",\"guid\":\"6d261f0a-c00f-441a-bcca-b8014d91aeec\",\"ord\":1}";
	public static final Position P4_EXPECTED = new Position(4, 33.56789, -117.56789, "d4", "6d261f0a-c00f-441a-bcca-b8014d91aeec", 1);
	public static final PositionJson PJ4_EXPECTED = new PositionJson(P2_EXPECTED);
	public static final String PLIST_P3_AND_P4 = '[' + RAW_POSITION_JSON_3 + ',' + RAW_POSITION_JSON_4 + ']';

	public static final String POSITION_JSON_5 = "[" +
			"{\"id\":1,\"lat\":\"33.74889\",\"lng\":\"-117.88055\",\"description\":\"\",\"guid\":\"70356a36-1075-4356-9915-109f58238548\",\"ord\":0}," +
			"{\"id\":2,\"lat\":\"33.74889\",\"lng\":\"-117.88055\",\"description\":\"\",\"guid\":\"70356a36-1075-4356-9915-109f58238548\",\"ord\":1}," +
			"{\"id\":99,\"lat\":\"10.74889\",\"lng\":\"-10.88055\",\"description\":\"\",\"guid\":\"12345a36-1075-4356-9915-109f58238548\",\"ord\":0}" +
		"]";
	public static final Position P5A_EXPECTED = new Position(1, 33.74889, -117.88055, "", "70356a36-1075-4356-9915-109f58238548", 0);
	public static final Position P5B_EXPECTED = new Position(2, 33.74889, -117.88055, "", "70356a36-1075-4356-9915-109f58238548", 1);
	public static final Position P5C_EXPECTED = new Position(99, 10.74889, -10.88055, "", "98765a36-1075-4356-9915-109f58238548", 0);

	public static final String POSITION_JSON_6_GUID = "70356a36-1075-4356-9915-109f58238548";
	public static final String POSITION_JSON_6 = "[" +
			"{\"id\":1,\"lat\":\"33.74889\",\"lng\":\"-117.88055\",\"description\":\"\",\"guid\":\"70356a36-1075-4356-9915-109f58238548\",\"ord\":0}," +
			"{\"id\":2,\"lat\":\"33.74889\",\"lng\":\"-117.88055\",\"description\":\"\",\"guid\":\"70356a36-1075-4356-9915-109f58238548\",\"ord\":1}" +
			"]";
	public static final Position P6A_EXPECTED = new Position(1, 33.74889, -117.88055, "", "70356a36-1075-4356-9915-109f58238548", 0);
	public static final Position P6B_EXPECTED = new Position(2, 33.74889, -117.88055, "", "70356a36-1075-4356-9915-109f58238548", 1);

	public static Builder builder() { return new Builder(); }

	public static class Builder {
		final Position pos = new Position(0);
		public Position build() { return pos; }

		public Builder id(long id) { pos.id = id; return this; }
		public Builder guid(String guid) { pos.guid = guid; return this; }
		public Builder lat(double lat) { pos.lat = lat; return this; }
		public Builder lng(double lng) { pos.lng = lng; return this; }
		public Builder ord(int ord) { pos.ord = ord; return this; }
		public Builder description(String description) { pos.description = description; return this; }
	}

	/** performs assertions on Position but doesnt actually implement real Comparator logic (eg: for sorting) */
	public static class AssertsComparator implements Comparator<Position> {
		@Override public int compare(final Position subject, final Position expected) {

			assertThat(subject.id).as("pos.id")
					.isEqualTo(expected.id);
			assertThat(subject.lat).as("pos.lat")
					.isCloseTo(expected.lat, within(MAX_DELTA_DOUBLE_COMPARISONS));
			assertThat(subject.lng).as("pos.lng")
					.isCloseTo(expected.lng, within(MAX_DELTA_DOUBLE_COMPARISONS));

			assertThat(subject.description).as("pos.description")
					.isEqualTo(expected.description);
			assertThat(subject.guid).as("pos.guid")
					.isEqualTo(expected.guid);
			assertThat(subject.ord).as("pos.ord")
					.isEqualTo(expected.ord);

			return 0;
		}
	}

	/** same as AssertsComparator, except omits id field (unsaved have no ID) */
	public static class UnsavedAssertsComparator implements Comparator<Position> {
		@Override public int compare(final Position subject, final Position expected) {

			assertThat(subject.lat).as("pos.lat")
					.isCloseTo(expected.lat, within(MAX_DELTA_DOUBLE_COMPARISONS));
			assertThat(subject.lng).as("pos.lng")
					.isCloseTo(expected.lng, within(MAX_DELTA_DOUBLE_COMPARISONS));

			assertThat(subject.description).as("pos.description")
					.isEqualTo(expected.description);
			assertThat(subject.guid).as("pos.guid")
					.isEqualTo(expected.guid);
			assertThat(subject.ord).as("pos.ord")
					.isEqualTo(expected.ord);

			return 0;
		}
	}

	public abstract static class Factory {
		private Factory() { } // no instances

		private static final Random rand = new Random(System.currentTimeMillis());
		private static final double OVERFLOW_GUARD = 0.0001;

		public static Position justCoords() {
			return new Builder()
					.lat(generateLat())
					.lng(generateLng())
					.build();
		}

		public static Position withCoordsAndGuid() {
			return new Builder()
					.lat(generateLat())
					.lng(generateLng())
					.guid(generateGuid())
					.build();
		}

		public static Position realistic() {
			return new Builder()
					.id(generateId())
					.lat(generateLat())
					.lng(generateLng())
					.guid(generateGuid())
					.description(generateDescription())
					.build();
		}

		public static ArrayList<Position> listRealistic(final int total) {
			final ArrayList<Position> list = new ArrayList<>(total);
			for (int i=0; i<total; i++) { list.add(realistic()); }
			return list;
		}

		public static ArrayList<Position> listJustCoords(final int total) {
			final ArrayList<Position> list = new ArrayList<>(total);
			for (int i=0; i<total; i++) { list.add(justCoords()); }
			return list;
		}

		public static ArrayList<Position> listWithCoordsAndGuid(final int total) {
			final ArrayList<Position> list = new ArrayList<>(total);
			for (int i=0; i<total; i++) { list.add(withCoordsAndGuid()); }
			return list;
		}

		/** all positions have same guid */
		public static ArrayList<Position> pathRealistic(final int total) {
			final ArrayList<Position> list = new ArrayList<>(total);
			String guid = null;
			for (int i=0; i<total; i++) {
				final Position p = realistic();
				if (0 == i) { guid = p.guid; }
				else { p.guid = guid; }
				list.add(p);
			}
			return list;
		}

		public static List<Position> pathSetRealistic(final int pathsCount, final int maxPosPerPath, final int maxTotal) {
			final ArrayList<Position> list = new ArrayList<>(maxTotal + maxPosPerPath); // avoid re-alloc if random gens more

			for (int i=0; i<pathsCount; i++) {
				int numPos = rand.nextInt(maxPosPerPath + 1);
				numPos = (0 == numPos) ? 1 : numPos;
				list.addAll(pathRealistic(numPos));
			}

			if (list.size() > maxTotal) { list.subList(maxTotal, list.size()).clear(); }

			return list;
		}

		public static String generateGuid() { return PositionsUtil.newGuid(); }

		public static long generateId() {
			return rand.nextInt(Integer.MAX_VALUE - 1) + 1;
		}

		public static double generateLat() {
			final double r = (rand.nextDouble() * Position.MAX_LAT * 2); // times 2 for full pos/neg range, since r is never negative
			final double lat = r - Position.MAX_LAT; // center range back over positives/negatives
			return lat < 0 ? lat + OVERFLOW_GUARD : lat - OVERFLOW_GUARD ; // prevent hitting exact max/min values
		}

		public static double generateLng() {
			final double r = (rand.nextDouble() * Position.MAX_LNG * 2); // times 2 for full pos/neg range, since r is never negative
			final double lng = r - Position.MAX_LNG; // center range back over positives/negatives
			return lng < 0 ? lng + OVERFLOW_GUARD : lng - OVERFLOW_GUARD ; // prevent hitting exact max/min values
		}

		public static String generateDescription() {
			return TStringUtil.randomParagraph( rand.nextInt(4) + 1 );
		}
	}
}
