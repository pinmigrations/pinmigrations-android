package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Headers;
import okhttp3.HttpUrl;

public class TNetUtil {
	public static final HttpUrl GOOGLE_URL = HttpUrl.parse("http://www.google.com");
	public static final HttpUrl FACEBOOK_URL = HttpUrl.parse("http://www.facebook.com");


	//region misc / used by tests
	public static String encodeUri(final String urlData) throws UnsupportedEncodingException {
		return java.net.URLEncoder.encode(urlData, "UTF-8");
	}

	public static Map<String, String> decodeFormBody(@NonNull final String rawBody) throws UnsupportedEncodingException {
		final HashMap<String, String> retVals = new HashMap<>();

		final String[] valuePairs = URLDecoder.decode(rawBody, "UTF-8").split("&");
		for (final String valuePair : valuePairs) {
			final String[] pair = valuePair.split("=");

			retVals.put(pair[0], pair[1]);
		}

		return retVals;
	}


	/** read headers from Request and parse cookies
	 * NOTE: Cookie.parseAll(Headers) only works for response-headers
	 *
	 */
	public static Map<String, String> requestCookies(@NonNull final Headers reqHeaders) {
		final HashMap<String, String> retVals = new HashMap<>();

		final List<String> cookieStrings = reqHeaders.values("Cookie");

		for (final String cStr: cookieStrings) {
			final String[] cookiePairs = cStr.split(";");
			for (final String cookiePair : cookiePairs) {
				final String[] pair = cookiePair.split("=");
				final String cookieKey = pair[0].trim();
				final String cookieValue = pair[1].trim();
				retVals.put(cookieKey, cookieValue);
			}
		}

		return retVals;
	}
}
