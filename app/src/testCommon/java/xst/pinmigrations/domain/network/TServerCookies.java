package xst.pinmigrations.domain.network;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

/** container is used to build a set of cookies for MockResponses
 * uses underlying CookieJar logic for add/remove and finally for generating the header
 */
public class TServerCookies extends InMemoryCookieJar {
	public TServerCookies(final HttpUrl domain) {
		super(domain);
	}

	public void setResponseSessionCookie( @NonNull final String name, @NonNull final String value) {
		super.setCookie( CookieUtil.newApiSessionCookie(name, value, defaultDomain) );
	}

	public void setResponsePersistentCookie( @NonNull final String name, @NonNull final String value) {
		super.setCookie( CookieUtil.newApiPersistentCookie(name, value, defaultDomain) );
	}

	public void setResponseClearCookie(@NonNull final String name) {
		super.setCookie( CookieUtil.newEpochStartClearCookie(name, defaultDomain) );
	}



	public ArrayList<String> getServerCookieHeaders() {
		final ArrayList<String> headers = new ArrayList<>();

		final Collection<Cookie> allCookies = allCookies();
		for (Cookie c: allCookies) {
			headers.add(c.toString());
		}
		return headers;
	}



	// region super-class overrides

	/** potentially confusing, dont use */
	@Override @Deprecated public void clearMatching(@NonNull final String name, @NonNull final HttpUrl url) {
		throw new UnsupportedOperationException("use `setResponseClearCookie`");
	}
	/** potentially confusing, dont use */
	@Override @Deprecated public void setCookie(@NonNull final String name, @NonNull final String value) {
		throw new UnsupportedOperationException("use `setResponse*Cookie`");
	}
	/** potentially confusing, dont use */
	@Override @Deprecated public void setCookie( @NonNull final String name, @NonNull final String value, final long expiration,
									 @NonNull final String domain, @NonNull final String path) {
		throw new UnsupportedOperationException("use `setResponse*Cookie`");
	}
	/** potentially confusing, dont use */
	@Override @Deprecated public void setCookie(@NonNull final Cookie cookie) {
		throw new UnsupportedOperationException("use `setResponse*Cookie`");
	}
	//endregion
}
