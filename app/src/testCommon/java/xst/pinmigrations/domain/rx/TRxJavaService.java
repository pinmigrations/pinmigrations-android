package xst.pinmigrations.domain.rx;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.schedulers.TestScheduler;

/**
 * returns Schedulers.trampoline for Android-MainThread and IO
 */
public final class TRxJavaService extends RxJavaService {
	@Nullable public Scheduler ioScheduler;
	@Nullable public Scheduler androidMainScheduler;

	public @NonNull Scheduler ioScheduler() {
		return (null == ioScheduler) ? Schedulers.trampoline() : ioScheduler;
	}
	public @NonNull TestScheduler ioAsTestScheduler() {
		final Scheduler scheduler = ioScheduler();
		if (scheduler instanceof TestScheduler) { return (TestScheduler) scheduler; }
		else { throw new RuntimeException("IO Scheduler is not a TestScheduler"); }
	}

	public @NonNull Scheduler androidMainScheduler() {
		return (null == androidMainScheduler) ? Schedulers.trampoline() : androidMainScheduler;
	}
	public @NonNull TestScheduler androidMainAsTestScheduler() {
		final Scheduler scheduler = androidMainScheduler();
		if (scheduler instanceof TestScheduler) { return (TestScheduler) scheduler; }
		else { throw new RuntimeException("AndroidMain Scheduler is not a TestScheduler"); }
	}

	public static TRxJavaService newServiceAllImmediate() {
		final TRxJavaService service = new TRxJavaService();
		service.ioScheduler = Schedulers.immediate();
		service.androidMainScheduler = Schedulers.immediate();
		return service;
	}

	public static TRxJavaService newServiceAllTrampoline() {
		final TRxJavaService service = new TRxJavaService();
		service.ioScheduler = Schedulers.trampoline();
		service.androidMainScheduler = Schedulers.trampoline();
		return service;
	}

	public static TRxJavaService newServiceAllTest() {
		final TRxJavaService service = new TRxJavaService();
		service.ioScheduler = new TestScheduler();
		service.androidMainScheduler = new TestScheduler();
		return service;
	}

	public static TRxJavaService newServiceRobolectricIntegrationTests() {
		final TRxJavaService service = new TRxJavaService();
		service.ioScheduler = Schedulers.immediate();
		service.androidMainScheduler = Schedulers.immediate();
		return service;
	}

	public static TRxJavaService newServiceRobolectricFunctionalTests() {
		final TRxJavaService service = new TRxJavaService();
		service.ioScheduler = Schedulers.immediate();
		service.androidMainScheduler = Schedulers.immediate();
		return service;
	}

	/** same schedulers as prod */
	public static TRxJavaService newServiceDefault() {
		final TRxJavaService service = new TRxJavaService();
		service.ioScheduler = Schedulers.io();
		service.androidMainScheduler = AndroidSchedulers.mainThread();
		return service;
	}
}
