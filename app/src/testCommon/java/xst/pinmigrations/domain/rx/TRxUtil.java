package xst.pinmigrations.domain.rx;

import android.support.annotation.NonNull;

import rx.Observable;
import rx.observables.BlockingObservable;
import rx.observers.TestSubscriber;

import static com.google.common.base.Preconditions.checkNotNull;

public class TRxUtil {
	public static <T> TestSubscriber<T> createAndSubscribeTestSubscriber(@NonNull final Observable<T> observable) {
		final TestSubscriber<T> subscriber = new TestSubscriber<>();
		observable.subscribe(subscriber);
		return subscriber;
	}

	public static <T> TestSubscriber<T> createAndSubscribeTestSubscriberFromBlocking(
			@NonNull final BlockingObservable<T> blockingObservable) {
		final TestSubscriber<T> subscriber = new TestSubscriber<>();
		checkNotNull(blockingObservable).subscribe(subscriber);
		return subscriber;
	}
}
