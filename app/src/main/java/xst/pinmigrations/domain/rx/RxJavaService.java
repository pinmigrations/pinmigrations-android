package xst.pinmigrations.domain.rx;

import android.support.annotation.NonNull;

import org.jetbrains.annotations.Contract;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subjects.AsyncSubject;
import xst.pinmigrations.app.IgnoreArg;
import xst.pinmigrations.app.util.MainUtil;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Async Subject publishes only last item, and only after source invokes onCompleted
 * 	http://reactivex.io/RxJava/javadoc/rx/subjects/AsyncSubject.html
 */
public class RxJavaService {
	public @NonNull Scheduler ioScheduler() {
		return verifyNotNull( Schedulers.io() );
	}

	public @NonNull Scheduler androidMainScheduler() {
		return verifyNotNull( AndroidSchedulers.mainThread() );
	}

	public @NonNull Observable<Integer> delayedObservable(final double delay, final TimeUnit timeUnit) {
		final int msDelay = MainUtil.getMs(delay, timeUnit);
		final Observable<Integer> obs = Observable.just(1);

		if (msDelay <= 0) { return obs; }
		else {
			return obs.delay(msDelay, MILLISECONDS, ioScheduler());
		}
	}

	private final Random rand = new Random();
	public @NonNull <T> Observable<T> randomDelayObservable(@NonNull Observable<T> obs,
								  final double minDelay, final double maxDelay, final TimeUnit timeUnit) {
		checkArgument(minDelay >=0, "min delay cant be negative");
		checkArgument(maxDelay >= minDelay, "max-delay must be >= min-delay");

		final int msMin = MainUtil.getMs(minDelay, timeUnit);
		final int msMax = MainUtil.getMs(maxDelay, timeUnit);
		final int msDelay = rand.nextInt(msMax - msMin) + msMin;

		return delayedReturnObservable(checkNotNull(obs), msDelay, MILLISECONDS);
	}


	/** delays the completion (onNext/onComplete/ onError?) return value but not execution of `obs` */
	public @NonNull <T> Observable<T> delayedReturnObservable(@NonNull final Observable<T> obs, final double delay, final TimeUnit timeUnit) {
		final int msDelay = MainUtil.getMs(delay, timeUnit);
		return Observable.zip(
				Observable.timer(msDelay, MILLISECONDS, ioScheduler()),
				obs,
				new TimerZipFn<>());
	}
	/** "map" fn to join Obs<Long> w Obs<T> */
	public static final class TimerZipFn<Long,T> implements Func2<Long,T, T> {
		@Contract(pure = true) @Override public T call(final Long zero, final T tObj) { return tObj; }
	}


	/** invocation of `obs` is delayed until time has elapsed */
	public @NonNull <T> Observable<T> delayedExecutionObservable(@NonNull final Observable<T> obs, final double delay, final TimeUnit timeUnit) {
		return Observable.defer(new DelayExecFactory<T>(obs, delay, timeUnit));
	}
	public class DelayExecFactory<T> implements Func0< Observable<T> > {
		@NonNull final Observable<T> obs;
		@NonNull final Observable<Integer> delayedObs;
		public DelayExecFactory(@NonNull final Observable<T> sourceObservable, final double delay, final TimeUnit timeUnit) {
			this.obs = checkNotNull(sourceObservable);
			this.delayedObs = delayedObservable(delay, timeUnit); // note: not subscribed yet!
		}
		@Override public Observable<T> call() { // executed when return-value(observable) of `delayedExecutionObservable()` is subscribed
			final AsyncSubject<T> asyncSubject = AsyncSubject.create();

			delayedObs.subscribe(new DelayExecSubscriber<T>(asyncSubject, obs));
			return asyncSubject; // caller of `delayExecObs` gets this return val
		}
	}
	static class DelayExecSubscriber<T> extends BaseTransformSubjectSubscriber<Integer, T> {
		protected final Observable<T> obs;
		public DelayExecSubscriber(@NonNull final AsyncSubject<T> subject, @NonNull final Observable<T> obs) {
			super(subject);
			this.obs = checkNotNull(obs);
		}
		@Override public void onNext(@IgnoreArg final Integer ignore) { // executed when `delayedObs` completes
			obs.subscribe(subject); // finally, subscribe caller to source-observable
		}
	}

	/** just invokes `onCompleted` when subscribed (as per ReactiveStreams/RxJava2) */
	public static class JustVoidObservable implements Observable.OnSubscribe<Void> {
		@Override public void call(final Subscriber<? super Void> subscriber) {
			subscriber.onCompleted();
		}
	}

	/** returns a Observable<Void> that immediately completes when subscribed to */
	public Observable<Void> newVoidObservable() {
		return Observable.unsafeCreate(new JustVoidObservable());
	}
}
