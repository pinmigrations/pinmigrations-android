package xst.pinmigrations.domain.rx;

import android.support.annotation.NonNull;

import rx.Subscriber;
import rx.subjects.AsyncSubject;

import static com.google.common.base.Preconditions.checkNotNull;

/** intermediary that processes a source observable before propagating down the chain to the AsyncSubject
 * 	the source observable is type T and this subscriber transforms it to type U
 * 	same as BaseSubjectSubscriber, but the Observable types differe here. eg: T -> U instead of just T -> T
 * 	note: onComplete are NOT forwarded to subject as this will cause pre-mature termination
 * 	note: errors ARE propagated to subject
 */
public abstract class BaseTransformSubjectSubscriber<T, U> extends Subscriber<T> {
	/** next/error/complete are forwarded to subject */
	@NonNull protected final AsyncSubject<U> subject;

	public BaseTransformSubjectSubscriber(@NonNull final AsyncSubject<U> subject) {
		this.subject = checkNotNull(subject);
	}

	/**
	 * ensure that subject is subscribed to an Observable with a terminal event
	 * because this subscribers' onCompleted is not propagated
	 */
	@Override public abstract void onNext(final T t);

	/** let downstream handle errors, eg: usually UI level */
	@Override public void onError(final Throwable throwable) {
		subject.onError(throwable);
	}

	/** dont propagate onCompleted to subject (causes pre-mature termination) */
	@Override public void onCompleted() { }
}
