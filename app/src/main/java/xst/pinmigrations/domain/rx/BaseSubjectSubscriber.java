package xst.pinmigrations.domain.rx;

import android.support.annotation.NonNull;

import rx.Subscriber;
import rx.subjects.AsyncSubject;

import static com.google.common.base.Preconditions.checkNotNull;

/** intermediary that processes a source observable before propagating down the chain to the AsyncSubject
 * 	note: onComplete are NOT forwarded to subject as this will cause pre-mature termination
 * 	note: errors ARE propagated to subject
*/
public abstract class BaseSubjectSubscriber<T> extends Subscriber<T> {

	@NonNull protected final AsyncSubject<T> subject;

	public BaseSubjectSubscriber(@NonNull final AsyncSubject<T> subject) {
		this.subject = checkNotNull(subject);
	}

	/**
	 * ensure that subject is subscribed to an Observable with a terminal event
	 * because this subscribers' onCompleted is not propagated
	 */
	@Override public abstract void onNext(final T t);

	/** let downstream handle errors, eg: usually UI level */
	@Override public void onError(final Throwable throwable) {
		subject.onError(throwable);
	}

	/** dont propagate onCompleted to subject (causes pre-mature termination) */
	@Override public void onCompleted() { }
}
