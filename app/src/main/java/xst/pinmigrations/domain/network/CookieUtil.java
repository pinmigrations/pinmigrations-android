package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;

import okhttp3.Cookie;
import okhttp3.internal.http.HttpDate;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * NOTE: domains for local url (localhost/127.0.0.1) are uniformally normalized to 127.0.0.1
 */
public class CookieUtil {
	public static final long MS_PER_YEAR = 31556952000L;
	public static final long START_OF_EPOCH = 0L;
	public static final String DEFAULT_PATH = "/";

	public static @NonNull Cookie copyCookie(@NonNull final Cookie cookie) {
		final Cookie c = checkNotNull(cookie);
		return newCookie(c.name(), c.value(), c.domain(), c.expiresAt(), c.path(), c.httpOnly(), c.hostOnly(), c.secure());
	}

	public static @NonNull Collection<Cookie> copyCookieCollection(@NonNull final Collection<Cookie> collection) {
		final ArrayList<Cookie> copy = new ArrayList<>(collection.size());

		for (final Cookie c: collection) {
			copy.add( copyCookie(c) );
		}

		return copy;
	}

	public static @NonNull Cookie newCookie(@NonNull final String name, @NonNull final String value, @NonNull final String domain) {
		return newCookie(name, value, domain, HttpDate.MAX_DATE, DEFAULT_PATH);
	}

	public static @NonNull Cookie newCookie(@NonNull final String name, @NonNull final String value, @NonNull final String domain,
											final long expiresAt, @NonNull final String path) {
		return newCookie(name, value, domain, expiresAt, path, false, false, false);
	}

	/** new Cookie from values (null-checking)
	 * NOTE: if no expiration was sent by server, HttpDate.MAX_DATE indicates non-persistent
	 */
	public static @NonNull Cookie newCookie(@NonNull final String name, @NonNull final String value,
								@NonNull final String domain, final long expiresAt, @NonNull final String path,
											final boolean httpOnly, final boolean hostOnly, final boolean secure) {
		final String cookieDomain = NetworkUtil.localNormalizedDomain(domain);

		// NOTE: if no expiration was sent by server, HttpDate.MAX_DATE indicates non-persistent
		final Cookie.Builder builder = new Cookie.Builder()
				.name(checkNotNull(name))
				.value(checkNotNull(value))
				.domain(cookieDomain)
				.path(checkNotNull(path));

		if (HttpDate.MAX_DATE != expiresAt) { builder.expiresAt(expiresAt); }

		if (httpOnly) { builder.httpOnly(); }
		if (hostOnly) { builder.hostOnlyDomain( cookieDomain ); }
		if (secure) { builder.secure(); }

		return builder.build();
	}


	public static @NonNull Cookie newApiSessionCookie(@NonNull final String name,
													  @NonNull final String value,
													  @NonNull final String domain) {
		return newCookie(name, value, domain, HttpDate.MAX_DATE, "/", true, false, false);
	}
	public static @NonNull Cookie newApiPersistentCookie(@NonNull final String name,
														 @NonNull final String value,
														 @NonNull final String domain) {
		return newCookie(name, value, domain, oneYearExpiration(), "/", true, false, false);
	}
	/** sets cookie with expiration at start of epoch (to clear it) */
	public static @NonNull Cookie newEpochStartClearCookie(@NonNull final String name,
														 @NonNull final String domain) {
		return newCookie(name, "", domain, START_OF_EPOCH, "/");
	}

	public static long oneYearExpiration() {
		return System.currentTimeMillis() + MS_PER_YEAR;
	}
}
