package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;
import xst.pinmigrations.app.logging.LogUtil;

/*
	NOTE: see https://github.com/square/okhttp/wiki/Interceptors
 	# Application interceptors
		+ Don't need to worry about intermediate responses like redirects and retries.
		+ Are always invoked once, even if the HTTP response is served from the cache.
		+ Observe the application's original intent. Unconcerned with OkHttp-injected headers like `If-None-Match`
		+ Permitted to short-circuit and not call `Chain.proceed()`
		+ Permitted to retry and make multiple calls to `Chain.proceed()`
	# Network Interceptors
		+ Able to operate on intermediate responses like redirects and retries.
		+ Not invoked for cached responses that short-circuit the network.
		+ Observe the data just as it will be transmitted over the network.
		+ Access to the Connection that carries the request: `chain.connection()`

	# examples:
		OkHttpClient client = new OkHttpClient.Builder()
    		.addNetworkInterceptor(new VerboseLoggingInterceptor()) // verbose-network interceptor
    		.build();
		OkHttpClient client = new OkHttpClient.Builder()
    		.addInterceptor(new VerboseLoggingInterceptor()) // verbose-app interceptor
    		.build();
    	// etc...
 */

public class BaseLoggingInterceptor implements LoggingInterceptor {
	public static final int LOG_LEVEL_DEFAULT = Log.DEBUG;

	final protected String requestFormat, responseFormat;
	final protected int headerFormat;
	protected int logLevel = LOG_LEVEL_DEFAULT;
	public BaseLoggingInterceptor(final String reqFormat, final String respFormat, final int headerFormat) {
		this.requestFormat = reqFormat;
		this.responseFormat = respFormat;

		if (HEADER_FORMAT_VERBOSE != headerFormat
				&& HEADER_FORMAT_CONCISE != headerFormat
				&& HEADER_FORMAT_NONE != headerFormat) {
			throw new IllegalArgumentException("invalid header format");
		}
		this.headerFormat = headerFormat;
	}
	public BaseLoggingInterceptor(final String reqFormat, final String respFormat, final int headerFormat, final int logLevel) {
		this(reqFormat, respFormat, headerFormat);
		this.logLevel = logLevel;
	}

	@Override
	public Response intercept(final Chain chain) throws IOException {
		final Request request = chain.request();
		final long t1 = System.nanoTime();

		// req log vals
		final String reqUrl = request.url().toString();
		final String reqMethod = request.method();

		final String connectionSettings =
				(REQUEST_FORMAT_MIN.equals(requestFormat) || null == chain.connection())
					? ""
					: chain.connection().toString();

		final String reqHeaders = formatHeaders(request.headers());

		// log and exec
		logRequest(reqMethod, reqUrl, connectionSettings, reqHeaders);
		final Response response = chain.proceed(request);
		final long t2 = System.nanoTime();

		// resp log vals
		final String respUrl = response.request().url().toString();
		final int respHttpCode = response.code();
		final String respHttpMessage = response.message();
		final String requestTime = LogUtil.getMillisDiffString(t1, t2);
		final String respHeaders = formatHeaders(response.headers());


		logResponse(respUrl, respHttpCode, respHttpMessage, requestTime, respHeaders);
		return response;
	}


	@Override public void netLog(@NonNull final String msg) {
		LogUtil.log(this.logLevel, msg);
	}

	@Override public void logRequest(@NonNull String reqMethod, @NonNull String reqUrl, @NonNull String connectionSettings,
									 @NonNull String reqHeaders) {

		LogUtil.log(this.logLevel, this.requestFormat, reqMethod, reqUrl, connectionSettings, reqHeaders);
	}


	@Override public void logResponse(@NonNull String respUrl, int respHttpCode, @NonNull String respHttpMessage,
									  @NonNull String reqTime, @NonNull String respHeaders) {
		LogUtil.log(this.logLevel, this.responseFormat, respUrl, respHttpCode, respHttpMessage, reqTime, respHeaders);
	}

	@Override public int logLevel() {
		return this.logLevel;
	}

	@Override public String formatHeaders(@NonNull final Headers headers) {
		switch (headerFormat) {
			case HEADER_FORMAT_CONCISE:
				return NetLogUtil.getHeaderStringShort(headers);
			case HEADER_FORMAT_VERBOSE:
				return NetLogUtil.getHeaderStringMultiline(headers, 4);
			case HEADER_FORMAT_NONE:
			default:
				return "";
		}
	}

	public static final int HEADER_FORMAT_CONCISE = 0;
	public static final int HEADER_FORMAT_VERBOSE = 1;
	public static final int HEADER_FORMAT_NONE = 2;

	public final static String EQ_LINE_LONG = "========================================";

	// METHOD, URL, CONN-SETTINGS, HEADERS
	public final static String REQUEST_FORMAT_CONCISE = "[%s | %s] %s // %s";
	public final static String REQUEST_FORMAT_MIN = "[%s | %s] %s %s"; // note: last arg (headers) is empty-string
	public final static String REQUEST_FORMAT_DEV = "\n" +
			EQ_LINE_LONG + "\n" +
			"REQUEST [%s | %s]" + "\n" +
			"    on %s " + "\n" +
			"HEADERS:" + "\n" +
			"%s" + "\n" +
			EQ_LINE_LONG + "\n";

	// URL, HTTP-CODE, HTTP-MSG, REQ-TIME (units included), HEADERS
	public final static String RESPONSE_FORMAT_CONCISE = "[%s | %s %s] (%s) // %s";
	public final static String RESPONSE_FORMAT_MIN = "[%s | %s %s] (%s)%s"; // note: last arg (headers) is empty-string
	public final static String RESPONSE_FORMAT_DEV =  "\n" +
			EQ_LINE_LONG + "\n" +
			"RESPONSE [ %s | %s %s ] in %s" + "\n" +
			"HEADERS:" + "\n" +
			"%s" + "\n" +
			EQ_LINE_LONG + "\n";
}
