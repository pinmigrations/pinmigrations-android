package xst.pinmigrations.domain.network;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.Map;
import okhttp3.Headers;
import xst.pinmigrations.app.logging.LogUtil;

/**
 * used in LoggingInterceptors
 */
public final class NetLogUtil {
	public static @NonNull String getHeaderStringMultiline(@NonNull final Headers headers) {
		return getHeaderStringMultiline(headers, 0);
	}

	public static @NonNull String getHeaderStringMultiline(@NonNull final Headers headers,
														   @IntRange(from = 0) final int indentCount) {
		final Map<String, List<String>> rawHeaders = headers.toMultimap();

		final StringBuilder sb = new StringBuilder();
		for (final String h: rawHeaders.keySet()) {
			if (indentCount > 0) { LogUtil.addSpaces(sb, indentCount); }
			sb.append(h);
			sb.append(": ");

			final List<String> headerValues = rawHeaders.get(h);
			for (String hVal: headerValues) {
				sb.append(hVal);

				if (headerValues.size() > 1) {
					sb.append(", ");
				}
			}
			sb.append("\n");
		}

		return sb.toString();
	}

	public static @NonNull String getHeaderStringShort(@NonNull final Headers headers) {
		final Map<String, List<String>> rawHeaders = headers.toMultimap();
		final StringBuilder sb = new StringBuilder();
		for (final String h: rawHeaders.keySet()) {
			sb.append("< ");
			sb.append(h);
			sb.append(":");

			final List<String> headerValues = rawHeaders.get(h);
			for (final String hVal: headerValues) {
				sb.append(hVal);
				if (headerValues.size() > 1) {
					sb.append(",");
				}
			}
			sb.append(" >");
		}

		return sb.toString();
	}
}
