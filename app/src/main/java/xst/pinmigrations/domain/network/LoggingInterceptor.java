package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import okhttp3.Headers;
import okhttp3.Interceptor;
public interface LoggingInterceptor extends Interceptor {
	void netLog(@NonNull String msg);

	void logRequest(@NonNull String reqMethod, @NonNull String url, @NonNull String connectionSettings,
					@NonNull String requestHeaders);


	void logResponse(@NonNull String url, int statusCode, @NonNull String httpMessage,
					@NonNull String reqTime, @NonNull String respHeaders);

	int logLevel();

	String formatHeaders(@NonNull Headers headers);
}
