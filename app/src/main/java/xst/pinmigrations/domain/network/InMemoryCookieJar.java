package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.network.CookieUtil.newCookie;

/** TODO: encapsulate cookie-store instead of extending
 */
public class InMemoryCookieJar extends InMemoryCookieStore implements CookieJar, AppCookieJar {
	@NonNull protected final String defaultDomain;
	@NonNull protected final HttpUrl defaultDomainBaseUrl;
	public InMemoryCookieJar(@NonNull final HttpUrl defaultDomainBaseUrl) {
		this.defaultDomainBaseUrl = NetworkUtil.localNormalizedDomain( checkNotNull(defaultDomainBaseUrl) );
		this.defaultDomain = this.defaultDomainBaseUrl.host();
	}

	//region OkHttp CookieJar
	@Override
	public void saveFromResponse(final HttpUrl url, final List<Cookie> cookies) {
		for (final Cookie c: cookies) {
			if (c.expiresAt() < System.currentTimeMillis()) {
				clearCookie(c.name());
			} else {
				setCookie(c);
			}
		}
	}

	@Override
	public List<Cookie> loadForRequest(final HttpUrl url) {
		return validCookiesForUrl(url);
	}
	//endregion

	@Override
	public void clearCookie(@NonNull final String name) {
		clearMatching(name, defaultDomainBaseUrl);
	}

	@Override
	public void setCookie(@NonNull final String name, @NonNull final String value) {
		setCookie( newCookie(name, value, defaultDomain) );
	}

	@Override
	public void setCookie(@NonNull final String name, @NonNull final String value, final long expiration,
						  @NonNull final String domain, @NonNull final String path) {
		setCookie( newCookie(name, value, domain, expiration, path) );
	}

	@Override
	public @NonNull Optional<Cookie> getCookie(@NonNull final String name) {
		return getCookie(name, defaultDomain);
	}


	@Override public List<Cookie> validCookiesForUrl(@NonNull final HttpUrl url) {
		final HttpUrl checkUrl = NetworkUtil.localNormalizedDomain(checkNotNull(url));

		final ArrayList<Cookie> validCookies = new ArrayList<>();
		final Collection<Cookie> allCookies = allCookies();

		for (final Cookie c: allCookies) {
			if (c.matches(checkUrl) && c.expiresAt() > System.currentTimeMillis()) {
				validCookies.add(c);
			}
		}

		return validCookies;
	}
}
