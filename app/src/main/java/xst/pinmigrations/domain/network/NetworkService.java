package xst.pinmigrations.domain.network;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.login.DefaultHeadersInterceptor;
import xst.pinmigrations.domain.login.LoginAppLayerInterceptor;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.LoginNetLayerInterceptor;
import xst.pinmigrations.domain.services.BaseService;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;

/**
 */
public class NetworkService extends BaseService {

	/** NOTE: this break login POST requests */
	public static final String PLATFORM_HEADER_KEY = "APP_PLATFORM";

	/** NOTE: this break login POST requests */
	public static String platformHdrVal = "android-" + BuildConfig.BUILD_TYPE;

	/* NOTE: this break login POST requests */
	public static final String REQUESTED_WITH_HEADER_KEY = "X-Requested-With";
	/* NOTE: this break login POST requests */
	public static final String REQUESTED_WITH_HEADER_VAL = "XMLHttpRequest";

	public static final String AUTHENTICITY_TOKEN_HEADER_SEND = "X-CSRF-Token";
	public static final String AUTHENTICITY_TOKEN_HEADER_RECEIVE = "csrf_token";

	public static final String HOST_HEADER_KEY = "Host";
	public static final String ORIGIN_HEADER_KEY = "Origin";
	public static final String REFERER_HEADER_KEY = "Referer";

	@NonNull final String host;
	@NonNull final String referer;
	@NonNull final String origin;
	@NonNull final String domain;
	@NonNull final String wsUrl;


	@Nullable protected volatile OkHttpClient httpClient; // lazy initialization, can be accessed by multiple threads
	@NonNull final LoginManager loginManager;

	@NonNull final HttpUrl baseHttpUrl;

	/** host should include port number (if applicable)
	 * NOTE: that OkHttpClient is lazily created
	 */
	public NetworkService(@NonNull final HttpUrl baseUrl, @NonNull final LoginManager loginManager) {
		this.baseHttpUrl = NetworkUtil.localNormalizedDomain(checkNotNull(baseUrl));

		this.host = this.baseHttpUrl.host();
		this.domain = this.baseHttpUrl.host();
		this.referer = this.baseHttpUrl.resolve("/").toString();
		this.wsUrl = NetworkUtil.websocketUrlFor(this.baseHttpUrl);
		this.origin = NetworkUtil.originFromUrl(this.baseHttpUrl);

		this.loginManager = checkNotNull(loginManager);
		this.loginManager.setNetworkService(this);
	}


	private final Object clientLock = new Object();
	/** lazily returns the OkHttpClient configured for the application
	 * note: double-check lock: https://en.wikipedia.org/wiki/Double-checked_locking#Usage_in_Java
	 * */
	public final @NonNull OkHttpClient getHttpClient() {
		OkHttpClient result = httpClient; // slightly less cheap access (volatile)
		if (null == result) { // cheap/un-safe check
			synchronized(clientLock) { // expensive synchronization
				result = httpClient;
				if (null == result) { // protects against race-condition in entering after un-safe check
					httpClient = result = createApplicationHttpClient();
				}
			}
		}

		return result; // cheap access to local copy
	}


	/** retrieves the CookieJar from the HttpClient */
	public @NonNull AppCookieJar getCookieJar() {
		return verifyNotNull( NetworkUtil.cookieJarFrom(getHttpClient()) );
	}


	//region URL

	// base values
	public final @NonNull HttpUrl getBaseHttpUrl() { return baseHttpUrl; }

	// for api headers
	public @NonNull String getApiHost() { return host; }
	public @NonNull String getApiOrigin() { return origin; }
	public @NonNull String getApiReferer() { return referer; }
	public @NonNull String getApiDomain() { return domain; }
	//endregion

	//region OkHttpClient (over-rideable)

	/**
	 * builder methods and `createApplicationHttpClient` are protected
	 * and provide customization/extension points for tests
	 *
	 * new*Client are package-private also for tests
	 */

	/** build client from builder methods and params */
	protected final OkHttpClient buildHttpClient(@NonNull final LoggingInterceptor logInterceptor,
												  @NonNull final LoginManager loginManager,
												  @NonNull final CookieJar cookieJar) {

		final LoggingInterceptor loggingInterceptor = checkNotNull(logInterceptor);
		OkHttpClient.Builder builder = baseHttpClientBuilder(logInterceptor, cookieJar);
		builder = addNetSettings(builder);
		builder = addLoginInterceptors(builder, loginManager, loggingInterceptor);

		return builder.build();
	}
	

	/** basic settings that should normally not be overridden */
	@CheckResult
	protected OkHttpClient.Builder baseHttpClientBuilder(@NonNull final LoggingInterceptor logInterceptor,
																			@NonNull final CookieJar cookieJar) {
		final LoggingInterceptor loggingInterceptor = checkNotNull(logInterceptor);
		return new OkHttpClient.Builder()
				.cookieJar(checkNotNull(cookieJar))
				.addNetworkInterceptor(loggingInterceptor)
				.addInterceptor(new DefaultHeadersInterceptor())
				.followRedirects(true);
	}

	/** add read/write/connect timeout settings */
	@CheckResult
	protected OkHttpClient.Builder addNetSettings(@NonNull final OkHttpClient.Builder builder) {
		return builder
				.connectTimeout(NetworkUtil.CLIENT_CONNECT_TIMEOUT, TimeUnit.SECONDS)
				.readTimeout(NetworkUtil.CLIENT_READ_TIMEOUT, TimeUnit.SECONDS)
				.writeTimeout(NetworkUtil.CLIENT_WRITE_TIMEOUT, TimeUnit.SECONDS);
	}

	/** add interceptors for auth (override to make un-authed client) */
	@CheckResult
	protected OkHttpClient.Builder addLoginInterceptors(@NonNull final OkHttpClient.Builder builder,
														@NonNull final LoginManager loginManager,
														@NonNull final LoggingInterceptor logInterceptor) {
		return builder
				.addInterceptor(new LoginAppLayerInterceptor(loginManager, this))
				.addNetworkInterceptor( new LoginNetLayerInterceptor(logInterceptor) );
	}

	/** use when developing/debugging network related functionality (very verbose and detailed) */
	final OkHttpClient newNetDebugHttpClient() {
		return buildHttpClient(new NetDebugLoggingInterceptor(), loginManager, newInMemoryCookieJar());
	}
	/** use when developing/debugging non-network code (omits headers) */
	final OkHttpClient newDevMinHttpClient() {
		return buildHttpClient(new MinLoggingInterceptor(), loginManager, newInMemoryCookieJar());
	}
	/** use on prod, short, single-line request strings */
	final OkHttpClient newProdHttpClient() {
		return buildHttpClient(new ConciseLoggingInterceptor(), loginManager, newInMemoryCookieJar());
	}

	/** saves cookies in main (RAM) memory, use for all functional/unit tests */
	@NonNull CookieJar newInMemoryCookieJar() {
		return new InMemoryCookieJar(baseHttpUrl);
	}

	/** build OkHttpClient according to environment / config settings
	 * this is the top-level client creation entry-point, override this to skip other builder logic
	 */
	@NonNull protected OkHttpClient createApplicationHttpClient() {
		if (App.loggingNetVerbose()) {
			return newNetDebugHttpClient();
		} else if (BuildConfig.DEBUG) {
			return newDevMinHttpClient();
		} else {
			return newProdHttpClient();
		}
	}
	//endregion
}
