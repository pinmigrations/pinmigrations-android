package xst.pinmigrations.domain.network;

import xst.pinmigrations.domain.ExceptionData;

/**
 * Checked-Exception. Indicates an irrecoverable issue with back-end service
 * eg: the database is corrupted, or service is unreachable despite functional network connection
 * these should trigger remote-logging and automated alerts to dev-team, etc...
 */
public class SevereBackendIssue extends Exception {
	@SuppressWarnings("unused") // Exception implements Serializable
	public static final long serialVersionUID = 42L;

	public static final int UNKNOWN = 0;
	public static final int EMPTY_FEED_HASH = 0xdead0000;
	public static final int NO_POSITION_DATA = 0xdead0001;

	public final ExceptionData data;
	public final int type;

	// default
	public SevereBackendIssue(final String msg) {
		super(msg);
		data = null;
		this.type = UNKNOWN;
	}
	public SevereBackendIssue(final String msg, int type) {
		super(msg);
		this.data = null;
		this.type = type;
	}


	// allows adding data for context
	public SevereBackendIssue(final String msg, final ExceptionData extraData) {
		super(msg);
		this.data = extraData;
		this.type = UNKNOWN;
	}
	public SevereBackendIssue(final String msg, int type, final ExceptionData extraData) {
		super(msg);
		this.data = null;
		this.type = UNKNOWN;
	}


	// Exception Chaining
	public SevereBackendIssue(final Throwable cause) {
		super(cause);
		this.data = null;
		this.type = UNKNOWN;
	}
	public SevereBackendIssue(final String msg, final Throwable cause) {
		super(msg, cause);
		this.data = null;
		this.type = UNKNOWN;
	}
	public SevereBackendIssue(final String msg, final Throwable cause, final ExceptionData extraData) {
		super(msg, cause);
		this.data = extraData;
		this.type = UNKNOWN;
	}
}
