package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.Collection;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

public interface CookieStore {
	void setCookie(@NonNull Cookie cookie);

	/** returns null if cookie does not exist */
	@NonNull Optional<Cookie> getCookie(@NonNull String name, @NonNull String domain);

	void clearMatching(@NonNull String name, @NonNull HttpUrl url);

	@NonNull Collection<Cookie> allCookies();

	void clearAllCookies();
}
