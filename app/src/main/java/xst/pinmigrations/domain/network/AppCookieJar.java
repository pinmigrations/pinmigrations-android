package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.List;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

public interface AppCookieJar extends CookieStore {
	void clearCookie(@NonNull String name);

	void setCookie(@NonNull String name, @NonNull String value);

	void setCookie(@NonNull String name, @NonNull String value, long expiration,
				   @NonNull String domain, @NonNull String path);

	@NonNull Optional<Cookie> getCookie(@NonNull String name);

	/** return all non-expired cookies that match this url */
	List<Cookie> validCookiesForUrl(@NonNull HttpUrl url);
}
