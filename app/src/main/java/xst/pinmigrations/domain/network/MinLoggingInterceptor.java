package xst.pinmigrations.domain.network;

import android.util.Log;

import okhttp3.Interceptor;

/*
	makes network requests easy to read
	use as default and for tests
 */
public class MinLoggingInterceptor extends BaseLoggingInterceptor implements Interceptor {
	public MinLoggingInterceptor() {
		super(REQUEST_FORMAT_MIN, RESPONSE_FORMAT_MIN, HEADER_FORMAT_NONE, Log.VERBOSE);
	}
	public MinLoggingInterceptor(final int logLevel) {
		super(REQUEST_FORMAT_MIN, RESPONSE_FORMAT_MIN, HEADER_FORMAT_NONE, logLevel);
	}
}
