package xst.pinmigrations.domain.network;

import android.util.Log;

import okhttp3.Interceptor;
/*
	makes network requests stand out visually in logs
	use when dev/debug network related functionality
 */
public class NetDebugLoggingInterceptor extends BaseLoggingInterceptor implements Interceptor {
	public NetDebugLoggingInterceptor() {
		super(REQUEST_FORMAT_DEV, RESPONSE_FORMAT_DEV, HEADER_FORMAT_VERBOSE, Log.DEBUG);
	}
	public NetDebugLoggingInterceptor(final int logLevel) {
		super(REQUEST_FORMAT_DEV, RESPONSE_FORMAT_DEV, HEADER_FORMAT_VERBOSE, logLevel);
	}
}
