package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.google.common.base.Objects;
import com.google.common.base.Optional;

import java.util.Collection;
import java.util.HashMap;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.network.CookieUtil.copyCookie;

/**
 * base for in-memory cookie stores
 * only implements CRUD ops
 */
public class InMemoryCookieStore implements CookieStore {

	static class Key {
		@NonNull public String name;
		@NonNull public String domain;
		public Key(@NonNull final String name, @NonNull final String domain) {
			this.name = checkNotNull(name);
			this.domain = NetworkUtil.localNormalizedDomain(  checkNotNull(domain) );
		}

		public Key(@NonNull final String name, @NonNull final HttpUrl url) {
			this(name, checkNotNull(url).host());
		}

		public static Key from(@NonNull final String name, @NonNull final HttpUrl url) {
			return new Key(name, url);
		}
		public static Key from(@NonNull final String name, @NonNull final String domain) {
			return new Key(name, domain);
		}

		@Override public boolean equals(@Nullable final Object other) {
			if ( !(other instanceof Key) ) { return false; } // note: this handles null-case
			final Key otherKey = (Key) other;

			return name.equals(otherKey.name) && domain.equals(otherKey.domain);
		}

		@Override public int hashCode() {
			return Objects.hashCode(name, domain);
		}
	}

	private final HashMap<Key, Cookie> cookieStore = new HashMap<>();

	@Override public void setCookie(@NonNull final Cookie cookie) {
		final Key cookieKey = new Key(cookie.name(), cookie.domain());
		synchronized (cookieStore) {
			cookieStore.put(cookieKey, cookie);
		}
	}

	@Override public @NonNull Optional<Cookie> getCookie(@NonNull final String name, @NonNull final String domain) {
		final Key cookieKey = new Key(name, domain);

		Cookie c;

		synchronized (cookieStore) { c = cookieStore.get(cookieKey); }

		return (null == c) ? Optional.absent() : Optional.of( copyCookie(c) );
	}

	@Override public void clearMatching(@NonNull final String name, @NonNull final HttpUrl url) {
		final HttpUrl checkUrl = NetworkUtil.localNormalizedDomain(checkNotNull(url));
		final Collection<Cookie> allCookies = allCookies();

		for (final Cookie c: allCookies) {
			if (c.name().equals(name) &&  c.matches(checkUrl)) {
				synchronized (cookieStore) {
					cookieStore.remove(new Key(c.name(), c.domain()));
				}
			}
		}
	}

	@Override public @NonNull Collection<Cookie> allCookies() {
		Collection<Cookie> cookies;
		synchronized (cookieStore) {
			cookies = cookieStore.values();
		}
		return CookieUtil.copyCookieCollection( cookies );
	}

	@Override public void clearAllCookies() {
		synchronized (cookieStore) { cookieStore.clear(); }
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		final Collection<Cookie> allCookies = allCookies();
		final int numCookies = allCookies.size();

		sb.append("CookieStore#");
		sb.append(this.hashCode());
		sb.append("<size=");
		sb.append(numCookies);
		sb.append("> [");

		if (numCookies > 0) {
			for (final Cookie c: allCookies) {
				sb.append("\n    ");
				sb.append(c.toString());
			}
			sb.append("\n]");
		} else {
			sb.append("]");
		}

		return sb.toString();
	}

	@VisibleForTesting(otherwise = VisibleForTesting.NONE)
	protected HashMap<Key, Cookie> cookieStore() { return cookieStore; }
}
