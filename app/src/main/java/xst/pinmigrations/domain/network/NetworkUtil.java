package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Response;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;

/**
 * https://square.github.io/okhttp/3.x/okhttp/okhttp3/HttpUrl.html
 * https://square.github.io/okhttp/3.x/okhttp/okhttp3/HttpUrl.Builder.html
 */
public final class NetworkUtil {
	public static final String LOCAL_DOMAIN = "127.0.0.1";

	public static final String SET_COOKIE_HEADER = "set-cookie";
	public static final String COOKIE_HEADER = "Cookie";
	public static final String COOKIE_SEPARATOR = ";";

	public static final int CLIENT_CONNECT_TIMEOUT = 30;
	public static final int CLIENT_READ_TIMEOUT = 20;
	public static final int CLIENT_WRITE_TIMEOUT = 10;


	//region OkHttp3 Util
	public static boolean responseSuccess(@NonNull final Response response) {
		//noinspection ResultOfMethodCallIgnored (PreCondition)
		checkNotNull(response);
		return response.code() >= 200 && response.code() < 400;
	}
	public static boolean responseClientError(@NonNull final Response response) {
		//noinspection ResultOfMethodCallIgnored (PreCondition)
		checkNotNull(response);
		return response.code() >= 400 && response.code() < 500;
	}
	public static boolean responseServerError(@NonNull final Response response) {
		return checkNotNull(response).code() >= 500;
	}

	public static AppCookieJar cookieJarFrom(@NonNull final OkHttpClient client) {
		return (AppCookieJar) verifyNotNull( checkNotNull(client).cookieJar() );
	}
	//endregion


	public static String websocketUrlFor(@NonNull final HttpUrl url) {
		return url.toString().replace("http://", "ws://");
	}

	/** returns baseUrl for HttpUrl (w/o trailing slash) */
	public static String originFromUrl(@NonNull final HttpUrl url) {
		final String rootUrl = url.resolve("/").toString();
		return rootUrl.substring(0, rootUrl.length() - 1); // remove trailing slash
	}


	public static boolean urlIsLocal(@NonNull final HttpUrl url) {
		return urlIsLocal((checkNotNull(url).host()));
	}
	public static boolean urlIsLocal(@NonNull final String host) {
		final String checkHost = checkNotNull(host);
		return checkHost.equals("localhost") || checkHost.equals("127.0.0.1");
	}

	public static @NonNull String localNormalizedDomain(@NonNull final String domain) {
		final String checkDomain = checkNotNull(domain);
		return urlIsLocal(checkDomain) ? LOCAL_DOMAIN : checkDomain;
	}

	public static @NonNull HttpUrl localNormalizedDomain(@NonNull final HttpUrl domain) {
		final HttpUrl checkDomain = checkNotNull(domain);
		return urlIsLocal(checkDomain) ? convertToLocal(domain) : checkDomain;
	}

	static @NonNull HttpUrl convertToLocal(@NonNull final HttpUrl domain) {
		return checkNotNull(domain).newBuilder().host(LOCAL_DOMAIN).build();
	}
}
