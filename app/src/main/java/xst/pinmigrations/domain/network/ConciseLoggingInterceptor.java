package xst.pinmigrations.domain.network;

import android.util.Log;

import okhttp3.Interceptor;

/*
	logs as much request/response info as possible compactly
	use as on prod and for client-logs (eg: sent over HTTP)
 */
public class ConciseLoggingInterceptor extends BaseLoggingInterceptor implements Interceptor {
	public ConciseLoggingInterceptor() {
		super(REQUEST_FORMAT_CONCISE, RESPONSE_FORMAT_CONCISE, HEADER_FORMAT_CONCISE, Log.INFO);
	}
	public ConciseLoggingInterceptor(final int logLevel) {
		super(REQUEST_FORMAT_CONCISE, RESPONSE_FORMAT_CONCISE, HEADER_FORMAT_CONCISE, logLevel);
	}
}
