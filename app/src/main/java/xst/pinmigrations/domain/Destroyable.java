package xst.pinmigrations.domain;

public interface Destroyable {

	/**
	 * invoked to clear resources. make sure this is idempotent (can be invoked several times without crashing everything)
	 */
	void destroy();
}
