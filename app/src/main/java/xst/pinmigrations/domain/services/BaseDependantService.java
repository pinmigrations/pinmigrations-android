package xst.pinmigrations.domain.services;

/**
 * Dependant Services have other services or objects as dependencies
 * and shouldn't be constructed without providing them
 */
public abstract class BaseDependantService extends BaseService {

	/**
	 * optional: enforce by adding private ctor in sub-classes
	 * private SomeService() { throw new UnsupportedOperationException("dont use this"); }
	 */
	protected BaseDependantService() { }
}
