package xst.pinmigrations.domain.services;

import xst.pinmigrations.app.logging.hashlog.SingletonHashLogger;

/** services in the modular sense, not as in distributed SOA */
public abstract class BaseService extends SingletonHashLogger { }
