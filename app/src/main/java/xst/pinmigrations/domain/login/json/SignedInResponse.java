package xst.pinmigrations.domain.login.json;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class SignedInResponse {
	@JsonField(name = "signed_in")
	public boolean isSignedIn;

	@Override public String toString() {
		return "SignedInResponse#" + this.hashCode() + " signed_in=" + isSignedIn;
	}
}
