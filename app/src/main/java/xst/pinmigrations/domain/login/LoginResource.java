package xst.pinmigrations.domain.login;

import android.support.annotation.NonNull;

import rx.Observable;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.subjects.AsyncSubject;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.login.json.SignedInResponse;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.rx.BaseSubjectSubscriber;
import xst.pinmigrations.domain.rx.BaseTransformSubjectSubscriber;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;

public class LoginResource {

	@NonNull final LoginManager loginManager;
	@NonNull final LoginApi api;
	@NonNull final NetworkService networkService;
	@NonNull final RemoteLogResource remoteLogResource;

	public LoginResource(@NonNull final ApiGenerator apiGenerator,
						 @NonNull final LoginManager loginManager,
						 @NonNull final NetworkService networkService,
						 @NonNull final RemoteLogResource remoteLogResource) {

		this.loginManager = checkNotNull(loginManager);
		this.networkService = checkNotNull(networkService);
		this.api = checkNotNull(apiGenerator).createService(LoginApi.class);
		this.remoteLogResource = checkNotNull(remoteLogResource);
	}

	//region Login

	/** login to remote server. this is public/service method */
	public @NonNull Observable<Login> login(@NonNull final String email, @NonNull final String password) {
		checkArgument( !checkNotNull(email).isEmpty(), "email cant be empty");
		checkArgument( !checkNotNull(password).isEmpty(), "password cant be empty");

		loginManager.setCredentials(email, password);

		final AsyncSubject<Login> loginSubject = AsyncSubject.create();
		final LoginPreReqSubscriber loginSubscriber = new LoginPreReqSubscriber(loginSubject);

		return Observable.defer(() -> {
			// loginSubscriber will initiate login-request if user isnt already logged in
			checkLoginStatus().subscribe(loginSubscriber);

			return loginSubject;
		}).cache();
	}
	class LoginPreReqSubscriber extends BaseSubjectSubscriber<Login> {
		public LoginPreReqSubscriber(@NonNull final AsyncSubject<Login> subject) { super(subject); }
		@Override public void onNext(final Login rxLoginObj) {
			final Login loginObj = verifyNotNull(rxLoginObj);

			// NOTE: at this point LoginManager is up to date with current login-state
			if ( !loginManager.isAuthed() ) { // not signed-in, make real login-request
				final LoginRequestSubscriber loginRequestSubscriber = new LoginRequestSubscriber(subject);
				api.login(loginManager.userName(), loginManager.userPassword())
					.subscribe(loginRequestSubscriber);

			} else { // already signed-in, dont re-attempt
				subject.onNext(loginObj);
				subject.onCompleted();
			}
		}
	}
	// NOTE: LoginApi.logout returns Observable<Void>, so never calls onNext, only onCompleted
	class LoginRequestSubscriber extends BaseTransformSubjectSubscriber<Void, Login> {
		public LoginRequestSubscriber(@NonNull final AsyncSubject<Login> subject) { super(subject); }
		@Override public void onNext(final Void aVoid) { /* never invoked for Observable<Void> */ }
		@Override public void onCompleted() {

			// NOTE: at this point LoginManager is up to date with current login-state
			subject.onNext( loginManager.getLoginObj() );
			subject.onCompleted();
			// Observable.just( loginManager.getLoginObj() ).subscribe(subject);
		}

		@Override public void onError(final Throwable throwable) {
			// default implementation from super
			subject.onError(throwable);
		}
	}
	//endregion


	//region Sign-in Check
	volatile boolean initialized = false;
	public boolean isInitialized() { return initialized; }

	public @NonNull Observable<Login> softLoginCheck() {
		return Observable.defer(softLoginObservableFactory).cache();
	}
	Func0< Observable<Login> > softLoginObservableFactory = new Func0< Observable<Login> >() {
		@Override public Observable<Login> call() {
			if (initialized) {
				return Observable.just( loginManager.getLoginObj() );
			} else {
				return checkLoginStatus();
			}
		}
	};

	/** query remote api for login status
	 * executes an api request to `sign_in_check` (prepares non-default params)
	 * updates LoginManager with response and returns same `Login` obj
	 */
	@SuppressWarnings("NonBooleanMethodNameMayNotStartWithQuestion") // boolean response is in Observable
	public @NonNull Observable<Login> checkLoginStatus() {
		return api.checkSigninStatus(
					networkService.getApiHost(),
					networkService.getApiOrigin(),
					networkService.getApiReferer())
				.map(signInCheckUpdater);
	}

	protected final Func1<SignedInResponse, Login> signInCheckUpdater =
		new Func1<SignedInResponse, Login>() {
			@Override public Login call(final SignedInResponse signedInResponse) {
				loginManager.updateLoginFromJsonResponse(checkNotNull( signedInResponse )); // validate again
				return loginManager.getLoginObj();
			}
		};
	//endregion


	//region Logout

	/** logout even if return-observable is not subscribed to */
	public Observable<Login> logout() {
		final AsyncSubject<Login> logoutSubject = AsyncSubject.create();
		final LogoutUpdater logoutUpdater = new LogoutUpdater(logoutSubject);

		final Observable<Login> logoutObs = Observable.defer(() -> {
			// loginSubscriber will initiate login-request if user isnt already logged in
			api.logout().subscribe(logoutUpdater);

			return logoutSubject;
		}).cache(); // cache so it doesnt re-exec

		logoutObs.subscribe(); // forces execution even if return val is never subscribed-to
		return logoutObs;
	}
	class LogoutUpdater extends BaseTransformSubjectSubscriber<Void, Login> {
		public LogoutUpdater(@NonNull final AsyncSubject<Login> subject) { super(subject); }
		@Override public void onNext(final Void aVoid) { /* may never be called */}
		@Override public void onCompleted() {
			subject.onNext(loginManager.getLoginObj());
			subject.onCompleted();
		}
	}
	//endregion

	public Observable<Void> ping() {
		return remoteLogResource.ping();
	}
}
