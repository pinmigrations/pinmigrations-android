package xst.pinmigrations.domain.login;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import com.google.common.collect.ImmutableList;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Verify.verify;

public class Login {
	//region CONSTANTS

	@Retention(RetentionPolicy.SOURCE)
	@IntDef({UNAUTHENTICATED, LOGGED_IN, LOGIN_FAILED})
	public @interface LoginState {}
	public static final int UNAUTHENTICATED = 0;
	public static final int LOGGED_IN = 1;
	public static final int LOGIN_FAILED = 2;
	public static final ImmutableList<Class<?>> PREFS_TYPES = ImmutableList.of(
			String.class,
			Integer.class,
			Boolean.class,
			Long.class,
			Float.class
	);
	//endregion

	//region FIELDS
	protected volatile int loginState = UNAUTHENTICATED;

	@NonNull String userEmail = "";
	@NonNull String userPassword = "";
	@NonNull String authenticityToken = "";
	@NonNull String appSessionCookie = "";
	@NonNull String rememberUserToken = "";
	//endregion

	public Login() {}
	public Login(@NonNull final String email, @NonNull final String password) {
		this.userEmail = checkNotNull(email);
		this.userPassword = checkNotNull(password);
	}
	public Login(@LoginState final int loginState, @NonNull final String email, @NonNull final String password,
				 @NonNull final String authenticityToken, @NonNull final String appSessionCookie,
				 @NonNull final String rememberUserToken) {
		this.userEmail = checkNotNull(email);
		this.userPassword = checkNotNull(password);

		this.authenticityToken = checkNotNull(authenticityToken);
		this.appSessionCookie = checkNotNull(appSessionCookie);
		this.rememberUserToken = checkNotNull(rememberUserToken);
		this.loginState = loginState;
	}


	public Login(@NonNull final Login other) {
		final Login fromCopy = checkNotNull(other);

		this.userEmail = checkNotNull(fromCopy.userEmail);
		this.userPassword = checkNotNull(fromCopy.userPassword);
		this.authenticityToken = checkNotNull(fromCopy.authenticityToken);
		this.loginState = checkNotNull(fromCopy.loginState);
		this.appSessionCookie = checkNotNull(fromCopy.appSessionCookie);
		this.rememberUserToken = checkNotNull(fromCopy.rememberUserToken);
	}

	/** true if logged in */
	public boolean isLoggedIn() {
		return LOGGED_IN == loginState;
	}
	/** true if not logged in or login attempt failed */
	public boolean notAuthed() {
		return UNAUTHENTICATED == loginState || LOGIN_FAILED == loginState;
	}
	/** true if login attempt failed */
	public boolean loginAttemptFailed() {
		return LOGIN_FAILED == loginState;
	}
	/** true if not logged in, and no failed login attempts */
	public boolean isUnauthed() { return UNAUTHENTICATED == loginState; }

	//region package-private

	void clearForLogout() {
		loginState = Login.UNAUTHENTICATED;
		authenticityToken = "";
		appSessionCookie = "";
		rememberUserToken = "";
	}

	/** update `loginState` triggers validations if setting to LOGGED_IN */
	void setLoginState(@LoginState final int newState) {
		loginState = newState;
		validateState();
	}

	void validateState() {
		if (LOGGED_IN == loginState) {
			// state verification
			verify( !isNullOrEmpty(authenticityToken), "logged-in but csrf-token missing");
			verify( !isNullOrEmpty(appSessionCookie), "logged-in but session-cookie missing"); //!error LoginActIntgTest.clickingLogout test
			verify( !isNullOrEmpty(rememberUserToken), "logged-in but remember-token missing");
		} else if (LOGIN_FAILED == loginState) {
			verify( isNullOrEmpty(rememberUserToken), "log-in-failed but kept invalid remember-token");
			verify( isNullOrEmpty(userEmail), "log-in-failed but kept invalid email");
			verify( isNullOrEmpty(userPassword), "log-in-failed but kept invalid password");
		} else if (UNAUTHENTICATED == loginState) {
			verify( isNullOrEmpty(rememberUserToken), "unauthenticated but kept invalid remember-token");
		} else {
			throw new AssertionError("invalid login-state");
		}
	}

	@Override
	public String toString() {
		return
			"authenticityToken: " + authenticityToken + "\n" +
			"appSessionCookie: " + appSessionCookie + "\n" +
			"rememberUserToken: " + rememberUserToken + "\n" +
			"loginState: " + getLoginStateString(loginState);
	}


	// TODO: use enums
	public static String getLoginStateString(final int loginState) {
		switch (loginState) {
			case UNAUTHENTICATED: return "UNAUTHENTICATED";
			case LOGGED_IN: return "LOGGED_IN";
			case LOGIN_FAILED: return "LOGIN_FAILED";
			default: throw new IllegalArgumentException("invalid loginState");
		}
	}
}
