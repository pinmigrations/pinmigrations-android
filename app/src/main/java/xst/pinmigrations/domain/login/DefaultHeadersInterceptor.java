package xst.pinmigrations.domain.login;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static xst.pinmigrations.domain.network.NetworkService.platformHdrVal;
import static xst.pinmigrations.domain.network.NetworkService.PLATFORM_HEADER_KEY;
import static xst.pinmigrations.domain.network.NetworkService.REQUESTED_WITH_HEADER_KEY;
import static xst.pinmigrations.domain.network.NetworkService.REQUESTED_WITH_HEADER_VAL;

/**
 * Application-layer Interceptor
 * NOTE: X-Requested-With, XMLHttpRequest breaks (POST) login-requests
 * NOTE: APP_PLATFORM also breaks (POST) login-requests
 * these are OK (required?) for subsequent API requests
 */
public class DefaultHeadersInterceptor implements Interceptor {
	public static final String DNT_HEADER = "DNT";
	public static final String DNT_HDR_VAL = "1";

	public DefaultHeadersInterceptor() {}

	public static final String CONTENT_TYPE_HDR = "Content-Type";

	@Override
	public Response intercept(final Chain chain) throws IOException {
		// make new request-obj with new headers
		final Request.Builder reqBuilder = chain.request().newBuilder()
				.addHeader(REQUESTED_WITH_HEADER_KEY, REQUESTED_WITH_HEADER_VAL)
				.addHeader(PLATFORM_HEADER_KEY, platformHdrVal)
				.addHeader(DNT_HEADER, DNT_HDR_VAL);

		return chain.proceed( reqBuilder.build() );
	}
}
