package xst.pinmigrations.domain.login;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;
import xst.pinmigrations.domain.login.json.SignedInResponse;

import static xst.pinmigrations.domain.network.NetworkService.HOST_HEADER_KEY;
import static xst.pinmigrations.domain.network.NetworkService.ORIGIN_HEADER_KEY;
import static xst.pinmigrations.domain.network.NetworkService.REFERER_HEADER_KEY;

public interface LoginApi {
	@Headers("Cache-Control: no-store, no-cache") // prevents potential 304 and potential stale data
	@GET("/check_signin_status")
	Observable<SignedInResponse> checkSigninStatus(@Header(HOST_HEADER_KEY) String host,
												   @Header(ORIGIN_HEADER_KEY) String origin,
												   @Header(REFERER_HEADER_KEY) String referer);

	@Headers("Cache-Control: no-store, no-cache")
	@FormUrlEncoded
	@POST("/login")
	Observable<Void> login(@Field("user[email]") String email, @Field("user[password]") String password);

	@Headers("Cache-Control: no-store, no-cache")
	@GET("/logout")
	Observable<Void> logout();
}
