package xst.pinmigrations.domain.login;

import android.support.annotation.NonNull;

import java.net.UnknownHostException;

import javax.inject.Inject;

import rx.Subscriber;
import timber.log.Timber;
import xst.pinmigrations.app.IgnoreArg;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.domain.network.AppCookieJar;
import xst.pinmigrations.domain.network.NetworkService;

import static com.google.common.base.Verify.verifyNotNull;
import static xst.pinmigrations.domain.login.LoginUtil.REMEMBER_ME_COOKIE;
import static xst.pinmigrations.domain.login.LoginUtil.SERVICE_SESSION_COOKIE;

/**
 * tries to determine login-state at app initialization
 */
public class LoginInitializer {
	@Inject LoginResource loginResource;
	@Inject NetworkService networkService;
	@Inject LoginManager loginManager;

	public LoginInitializer(@NonNull final InjectorComponent injector) { injector.inject(this); }

	public void initializeLogin() {
		setupCookieJar(loginManager.getLoginObj(), verifyNotNull( networkService.getCookieJar() ));

		loginResource.checkLoginStatus().subscribe(loginInitSubscriber);
	}

	/** update LoginObj from cookies/headers (Session/Csrf/Remember-Token) */
	protected void setupCookieJar(@NonNull final Login savedLoginState, @NonNull final AppCookieJar jar) {
		jar.setCookie(SERVICE_SESSION_COOKIE, savedLoginState.appSessionCookie);
		jar.setCookie(REMEMBER_ME_COOKIE, savedLoginState.rememberUserToken);
	}

	final Subscriber<Login> loginInitSubscriber = new Subscriber<Login>() {
		@Override public void onNext(@IgnoreArg final Login loginObj) {
			loginResource.initialized = true;
		}
		@Override public void onCompleted() { this.unsubscribe(); }

		// NOTE: cant properly handle errors in this context
		@Override public void onError(final Throwable err) {
			if (err instanceof UnknownHostException) {
				Timber.d("no internet connection");
			} else {
				Timber.e(err);
			}
		}
	};
}
