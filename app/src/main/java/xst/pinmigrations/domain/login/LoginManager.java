package xst.pinmigrations.domain.login;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Optional;

import okhttp3.Cookie;
import okhttp3.HttpUrl;
import okhttp3.Response;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.login.json.SignedInResponse;
import xst.pinmigrations.domain.network.AppCookieJar;
import xst.pinmigrations.domain.network.NetworkService;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;
import static xst.pinmigrations.domain.login.Login.LOGGED_IN;
import static xst.pinmigrations.domain.login.Login.LOGIN_FAILED;
import static xst.pinmigrations.domain.login.Login.UNAUTHENTICATED;
import static xst.pinmigrations.domain.login.LoginUtil.REMEMBER_ME_COOKIE;
import static xst.pinmigrations.domain.login.LoginUtil.SERVICE_SESSION_COOKIE;
import static xst.pinmigrations.domain.network.NetworkService.AUTHENTICITY_TOKEN_HEADER_RECEIVE;

/**
 * single instance manages authentication state throughout application
 * the manager is used instead of sharing/setting a plain `Login` object
 * delegates/coordinates with LoginIntercepter, LoginService, LoginResource, etc..
 * most methods synchronize on the Login-object that this class manages
 */
public class LoginManager {
	@NonNull protected final Login loginObj;
	@NonNull protected final PrefsManager prefs;

	public LoginManager(@NonNull final Login loginState, @NonNull final PrefsManager prefsManager) {
		this.loginObj = checkNotNull(loginState);
		this.prefs = checkNotNull(prefsManager);
	}

	@Nullable NetworkService networkService;
	public void setNetworkService(@NonNull final NetworkService networkService) {
		this.networkService = checkNotNull(networkService);
	}

	protected @NonNull AppCookieJar getCookieJar() {
		return checkNotNull(networkService).getCookieJar();
	}

	/** returns a copy of the internal login obj `Login` */
	@NonNull public Login getLoginObj() {
		synchronized (loginObj) { return new Login(loginObj); }
	}

	protected void persistLogin() {
		prefs.updateLoginState(loginObj.appSessionCookie, loginObj.rememberUserToken);
	}

	@NonNull String userName() {
		synchronized (loginObj) { return verifyNotNull( loginObj.userEmail ); }
	}
	// TODO: encrypt and save to disk (dont store plaintext)
	@NonNull String userPassword() {
		synchronized (loginObj) { return verifyNotNull( loginObj.userPassword ); }
	}

	@NonNull String getCsrf() {
		synchronized (loginObj) { return verifyNotNull( loginObj.authenticityToken ); }
	}
	@NonNull String getRememberToken() {
		synchronized (loginObj) { return verifyNotNull( loginObj.rememberUserToken ); }
	}
	@NonNull String getSession() {
		synchronized (loginObj) { return verifyNotNull( loginObj.appSessionCookie ); }
	}

	void setCredentials(@NonNull final String email, @NonNull final String password) {
		synchronized (loginObj) {
			loginObj.userEmail = checkNotNull(email);
			loginObj.userPassword = checkNotNull(password);
		}
	}

	/** true if logged in */
	public boolean isAuthed() {
		synchronized (loginObj) { return loginObj.isLoggedIn(); }
	}
	/** true if not logged in or login attempt failed */
	public boolean notAuthed() {
		synchronized (loginObj) { return loginObj.notAuthed(); }
	}
	/** true if not logged in, and no failed login attempts */
	public boolean isUnauthed() {
		synchronized (loginObj) { return loginObj.isUnauthed(); }
	}
	/** true if login attempt failed */
	public boolean loginAttemptFailed() {
		synchronized (loginObj) { return loginObj.loginAttemptFailed(); }
	}

	//region VALIDATION

	/** validate LoginObj state against known values (including server json response)
	 *
	 * at this point:
	 * 	1. LoginInterceptor updated Login in LoginManager with cookie values
	 * 	2. LoginManager updated Login from Json Response
	 */
	void validateAndPersist(@NonNull final SignedInResponse response) {
		if (response.isSignedIn) { // this just verifies that it is consistent with the JSON response
			verify(UNAUTHENTICATED != loginObj.loginState, "unauthenticated but server responded logged-in");
			verify(LOGIN_FAILED != loginObj.loginState, "login-failed but server responded logged-in");
		} else { // response is signed-out
			verify(LOGGED_IN != loginObj.loginState, "loginState success but server responded logged-out");
		}

		loginObj.validateState(); //!error LoginActIntgTest.clickingLogout test
		persistLogin();
	}

	/** validate LoginObj state against known values
	 *
	 * at this point:
	 * 	1. LoginInterceptor updated Login in LoginManager with cookie values
	 * 	2. LoginManager updated Login from Json Response
	 */
	void validateAndPersist() {
		loginObj.validateState();
		persistLogin();
	}
	//endregion

	//region Login Modification
	void updateLoginFromNetVals(@NonNull HttpUrl requestUrl, @NonNull final Response response) {
		synchronized (loginObj) {
			updateAuthValues(checkNotNull(requestUrl), checkNotNull(response)); // update auth values from headers/cookies

			if ( LoginUtil.isLoginRequest( checkNotNull(requestUrl)) ) { // for login requests, extra update rules
				updateAfterLoginRequest();
			}

			if ( !LoginUtil.isCheckSigninRequest(requestUrl)) { // for non check-signin, can validate/persist now
				validateAndPersist();
			} // else for check-sign-in, update/validate once json is parsed
		}
	}

	/** update LoginObj from server's JSON response */
	void updateLoginFromJsonResponse(@NonNull final SignedInResponse signedInResponse) {
		final SignedInResponse response = checkNotNull(signedInResponse);

		synchronized (loginObj) {
			if (response.isSignedIn) {
				if ( !isNullOrEmpty(loginObj.rememberUserToken)) {
					loginObj.loginState = LOGGED_IN;
				} else {
					loginObj.loginState = UNAUTHENTICATED;
				}
			} else {
				loginObj.rememberUserToken = ""; // invalid. clear if present
				getCookieJar().clearCookie(REMEMBER_ME_COOKIE);
			}

			validateAndPersist(signedInResponse);
		}
	}

	/** update LoginObj from cookies/headers (Session/Csrf/Remember-Token) */
	protected void updateAuthValues(@NonNull HttpUrl requestUrl, @NonNull final Response response) {
		final AppCookieJar cookieJar = getCookieJar();

		// update session-cookie/csrf-token
		final String authToken = response.header(AUTHENTICITY_TOKEN_HEADER_RECEIVE);
		if ( !isNullOrEmpty(authToken) ) {
			loginObj.authenticityToken = authToken; // only update if new one is sent
		}

		final Optional<Cookie> appSessionCookie = cookieJar.getCookie(SERVICE_SESSION_COOKIE);
		loginObj.appSessionCookie = appSessionCookie.isPresent() ? appSessionCookie.get().value() : "";

		final Optional<Cookie> rememberUserCookie = cookieJar.getCookie(REMEMBER_ME_COOKIE);
		final String rememberCookieVal = rememberUserCookie.isPresent() ? rememberUserCookie.get().value() : "";

		if ( LoginUtil.isLoginRequest(requestUrl) ) { // should only receive remember-token for login requests
			if ( isNullOrEmpty( rememberCookieVal ) ) { // only change remb-token if is login-request, updated/validated later
				loginObj.loginState = LOGIN_FAILED;
				loginObj.userEmail = loginObj.userPassword = "";
			} else {
				loginObj.loginState = LOGGED_IN;
				loginObj.rememberUserToken = rememberCookieVal;
			}
		} else if ( LoginUtil.isLogoutRequest(requestUrl) ) {
			LoginUtil.clearCookieJarForLogout(cookieJar);
			loginObj.clearForLogout();
		} else if ( !LoginUtil.isCheckSigninRequest(requestUrl) ) { // dont set state yet for check-signin-in
			loginObj.loginState = !isNullOrEmpty(loginObj.rememberUserToken) ? LOGGED_IN : UNAUTHENTICATED;
		}
	}

	/**
	 * distinguish between un-authed and login-failures
	 * PreCondition: LoginObj already updated from response cookies/headers
	 */
	protected void updateAfterLoginRequest() {
		if ( isNullOrEmpty( loginObj.rememberUserToken ) ) { // NOTE: empty remember-token occurs on logout
			loginObj.loginState = LOGIN_FAILED;
			loginObj.userEmail = "";
			loginObj.userPassword = "";
		}
	}
	//endregion
}
