package xst.pinmigrations.domain.login;


import android.support.annotation.NonNull;

import javax.security.auth.login.LoginException;

import okhttp3.HttpUrl;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsStore;
import xst.pinmigrations.domain.network.AppCookieJar;

import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager.PREF_KEY_REMEMBER_TOKEN;
import static xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager.PREF_KEY_SESSION;

public final class LoginUtil {
	public static final String SERVICE_SESSION_COOKIE = "_PinMigrations_session";
	public static final String REMEMBER_ME_COOKIE = "remember_user_token";
	public static final String AUTHENTICITY_TOKEN_HEADER_SEND = "X-CSRF-Token";
	public static final String AUTHENTICITY_TOKEN_HEADER_RECEIVE = "csrf_token";

	private LoginUtil() { throw new UnsupportedOperationException("dont instantiate util class"); }

	//region Service Util
	public static void clearCookieJarForLogout(@NonNull final AppCookieJar jar) {
		checkNotNull(jar).clearCookie(SERVICE_SESSION_COOKIE);
		jar.clearCookie(REMEMBER_ME_COOKIE);
	}

	static boolean isLoginRequest(@NonNull final HttpUrl url) {
		return checkNotNull(url).pathSize() >= 1 && url.pathSegments().get(0).equals("login");
	}
	static boolean isCheckSigninRequest(@NonNull final HttpUrl url) {
		return checkNotNull(url).pathSize() >= 1 && url.pathSegments().get(0).equals("check_signin_status");
	}
	static boolean isLogoutRequest(@NonNull final HttpUrl url) {
		return checkNotNull(url).pathSize() >= 1 && url.pathSegments().get(0).equals("logout");
	}

	public static Login setupLoginState(@NonNull final PrefsStore prefsStore) {
		final PrefsStore store = checkNotNull(prefsStore);

		final Login loginObj = new Login();
		loginObj.rememberUserToken = store.getString(PREF_KEY_REMEMBER_TOKEN);
		loginObj.appSessionCookie = store.getString(PREF_KEY_SESSION);

		return loginObj;
	}

	public static LoginException notLoggedInException() { return new LoginException("not logged in"); }
}
