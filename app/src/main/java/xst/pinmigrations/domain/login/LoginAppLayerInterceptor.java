package xst.pinmigrations.domain.login;


import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import xst.pinmigrations.domain.network.NetworkService;

import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.login.LoginUtil.AUTHENTICITY_TOKEN_HEADER_SEND;
import static xst.pinmigrations.domain.network.NetworkService.PLATFORM_HEADER_KEY;
import static xst.pinmigrations.domain.network.NetworkService.REQUESTED_WITH_HEADER_KEY;


/**
 * Application-layer Interceptor
 * NOTE: X-Requested-With, XMLHttpRequest breaks (POST) login-requests
 */
public class LoginAppLayerInterceptor implements Interceptor {
	@NonNull private final LoginManager loginManager;
	@NonNull final NetworkService networkService;
	public LoginAppLayerInterceptor(@NonNull final LoginManager loginManager, @NonNull final NetworkService networkService) {
		this.loginManager = checkNotNull(loginManager);
		this.networkService = checkNotNull(networkService);
	}

	@Override public Response intercept(final Chain chain) throws IOException {
		// make new request-obj with new headers if we have a CSRF token
		final Request originalRequest = chain.request();
		final HttpUrl originalReqestUrl = originalRequest.url();
		final boolean isLoginRequest = LoginUtil.isLoginRequest(originalReqestUrl);

		// clean up if login req
		Request.Builder builder = originalRequest.newBuilder();
		if (isLoginRequest) {
			builder = builder.removeHeader(REQUESTED_WITH_HEADER_KEY)  // AJAX server logic breaks login
						.removeHeader(PLATFORM_HEADER_KEY); // AJAX server logic breaks login
		}

		// set CSRF headers if CSRF info available
		final String csrfToken = loginManager.getCsrf();
		if (!csrfToken.isEmpty()) {
			builder = builder.addHeader(AUTHENTICITY_TOKEN_HEADER_SEND, csrfToken);
		}

		// continue request chain
		final Response response = chain.proceed( builder.build() );

		// afterwards, have response
		// update Login (in LoginManager) after every request
		loginManager.updateLoginFromNetVals(originalReqestUrl, response);

		return response; // continue
	}
}
