package xst.pinmigrations.domain.login;


import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import xst.pinmigrations.app.logging.LogUtil;
import xst.pinmigrations.domain.network.LoggingInterceptor;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Network-layer Interceptor
 */
public class LoginNetLayerInterceptor implements Interceptor {
	@NonNull final LoggingInterceptor loggingInterceptor;
	public LoginNetLayerInterceptor(final LoggingInterceptor loggingInterceptor) {
		this.loggingInterceptor = checkNotNull(loggingInterceptor);
	}

	@Override
	public Response intercept(final Chain chain) throws IOException {
		final boolean isLoginRequest = LoginUtil.isLoginRequest(chain.request().url());

		// continue request chain
		final Response originalResponse = chain.proceed(chain.request());

		Response rewrittenResponse = null;
		// note: Retrofit (rightly) throws errors when it gets 302-redirects
		// rewrite response to avoid this unneccessary second request
		if ( isLoginRequest && originalResponse.isRedirect() ) { // 302 redirect only occurs with successful logins
			rewrittenResponse = originalResponse.newBuilder() // override response for Retrofit
					.removeHeader("Location")
					.code(200)
					.message("OK")
					.build();

			// use LogIntercepter to log the original response (later LogInterceptor will log the rewritten response)
			LogUtil.log(loggingInterceptor.logLevel(), "REWROTE RESPONSE (original below)");
			loggingInterceptor.logResponse(originalResponse.request().url().toString(), originalResponse.code(),
					originalResponse.message(), "?ms", loggingInterceptor.formatHeaders(originalResponse.headers()));
		}

		// log and exec
		return null == rewrittenResponse ? originalResponse : rewrittenResponse;
	}
}
