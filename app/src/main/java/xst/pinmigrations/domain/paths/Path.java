package xst.pinmigrations.domain.paths;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.positions.PositionsUtil;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

public class Path implements Destroyable {
	// data
	@NonNull public final ArrayList<Position> positions;
	@NonNull public String guid = "";

	//region CTOR
	public Path() {
		this.positions = new ArrayList<>();
	}
	public Path(final int size) {
		verify(size >= 0, "size must be positive or zero");
		this.positions = new ArrayList<>(size);
	}
	public Path(final Path other) {
		this( other.positions.size() );
		replacePositions( other.positions );
	}
	//endregion

	public Path(@NonNull final ArrayList<Position> positions) {
		this( checkNotNull(positions).size() );
		replacePositions( positions );
	}

	public void replacePositions(@NonNull final ArrayList<Position> positions) {
		if (positions.size() > 1) {
			PositionsUtil.sortPositionsByOrd(positions);
		}

		if (positions.size() > 0) {
			this.guid = positions.get(0).guid;
		}

		this.positions.clear();
		this.positions.addAll(positions);
	}

	public int getPositionsCount() { return this.positions.size(); }

	public boolean isEmpty() { return this.positions.isEmpty(); }

	@Override public void destroy() { positions.clear(); }
}
