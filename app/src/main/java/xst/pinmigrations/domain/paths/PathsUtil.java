package xst.pinmigrations.domain.paths;

import android.support.annotation.NonNull;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Func1;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.positions.PositionsUtil;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Paths / PathList Util
 */
public class PathsUtil {
	// TODO: use array utilities to automate this (eg: make FP)
	@SuppressWarnings("MethodWithMultipleReturnPoints") // needs refactor anyway
	public static ArrayList<Path> groupPositionsIntoPaths(@NonNull final ArrayList<Position> positionList) {
		final ArrayList<Position> positions = checkNotNull(positionList);
		if (0 == positions.size()) { return getEmptyPathsList(); }

		// SORT
		PositionsUtil.sortPositionsByGuid(positions);

		// GROUP
		final ArrayList<Path> paths = new ArrayList<>();
		String currentGuid = null;
		ArrayList<Position> currentPositions = null;
		for (final Position p: positions) {
			//noinspection ConstantConditions (invalid analysis, only null on first iteration)
			if (null == currentGuid && null == currentPositions) {
				currentGuid = p.guid;
				currentPositions = new ArrayList<>();
				currentPositions.add(p);
				continue;
			}

			if (currentGuid.equals(p.guid)) {
				currentPositions.add(p);
			} else {
				paths.add(new Path(currentPositions));

				currentGuid = p.guid;
				currentPositions = new ArrayList<>();
				currentPositions.add(p);
			}
		}
		// collect last group
		//noinspection ConstantConditions (invalid analysis: positions.size() always > 1, so `currentPositions` never null here)
		paths.add(new Path(currentPositions));

		return paths;
	}

	public static ArrayList<Path> getEmptyPathsList() { return new ArrayList<>(); }

	// TODO: use FP here also
	public static @NonNull ArrayList<Position> flattenPathList(@NonNull final PathList pathListOriginal) {
		final PathList pathList = checkNotNull(pathListOriginal);

		final int posCount = pathList.getPositionsCount();
		if (0 == posCount) { return PositionsUtil.getEmptyPositionsList(); }

		final ArrayList<Position> positionsList = new ArrayList<>(posCount);
		for (Path path: pathList.paths) { positionsList.addAll(path.positions); }
		return positionsList;
	}

	public static PathList emptyPathList() { return new PathList(); }

	@NonNull public static final Func1<ArrayList<Position>, PathList>
			positionsToPathListMapper = new Func1<ArrayList<Position>, PathList>() {
		@Override public PathList call(final ArrayList<Position> positionList) {
			final ArrayList<Position> positions = checkNotNull(positionList);
			return new PathList( groupPositionsIntoPaths(positions) );
		}
	};

	@NonNull public static final Func1<List<Position>, Path>
			positionsToSinglePathMapper = new Func1<List<Position>, Path>() {
		@Override public Path call(final List<Position> positionList) {
			final ArrayList<Position> positions = Lists.newArrayList( checkNotNull(positionList) );
			return new Path( groupPositionsIntoPaths(positions).get(0) );
		}
	};
}
