package xst.pinmigrations.domain.paths;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import xst.pinmigrations.domain.Destroyable;

import static com.google.common.base.Preconditions.checkNotNull;

public class PathList implements Destroyable {
	@NonNull public final ArrayList<Path> paths;

	//region CTOR
	public PathList() {
		this.paths = new ArrayList<>();
	}
	public PathList(@NonNull final PathList other) {
		this.paths = checkNotNull(other).paths;
	}

	public PathList(@NonNull final ArrayList<Path> paths) {
		this.paths = checkNotNull(paths);
	}
	//endregion

	public int getPositionsCount() {
		int count = 0;
		for (final Path p: this.paths) { count += p.getPositionsCount(); }
		return count;
	}
	public int getPathsCount() { return paths.size(); }

	public boolean isEmpty() { return paths.isEmpty(); }

	public void clear() { paths.clear(); }

	public void replacePaths(@NonNull final ArrayList<Path> newPaths) {
		paths.clear();
		paths.addAll( checkNotNull(newPaths) );
	}

	public void replacePaths(@NonNull final PathList pathList) {
		paths.clear();
		paths.addAll( checkNotNull(pathList).paths );
	}

	@Override public void destroy() {
		if (paths.size() > 0) {
			for (final Path p: paths) { p.destroy(); }
			paths.clear();
		}
	}
}
