package xst.pinmigrations.domain.paths;

import android.support.annotation.NonNull;

import rx.Observable;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.positions.PositionsResource;
import xst.pinmigrations.domain.positions.PositionsSamplesResource;
import xst.pinmigrations.domain.services.BaseDependantService;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * PathListResource: CRUD / Queries on Path/PathList
 */
public class PathListResource extends BaseDependantService {
	@NonNull protected final PositionsResource positionsResource;
	@NonNull protected final PositionsSamplesResource positionsSamplesResource;
	@NonNull protected final RemoteLogResource remoteLogResource;

	private PathListResource() { throw new UnsupportedOperationException("dont use this"); }
	public PathListResource(@NonNull final PositionsResource positionsResource,
							@NonNull final PositionsSamplesResource positionsSamplesResource,
							@NonNull final RemoteLogResource remoteLogResource) {

		this.positionsResource = checkNotNull(positionsResource);
		this.positionsSamplesResource = checkNotNull(positionsSamplesResource);
		this.remoteLogResource = checkNotNull(remoteLogResource);
	}

	public @NonNull Observable<PathList> getAllPaths() {
		return positionsResource.getAllPositions()
				.map( PathsUtil.positionsToPathListMapper )
				.cache();
	}

	public @NonNull Observable<PathList> softGetAllPaths() {
		return positionsResource.softGetAllPositions()
				.map( PathsUtil.positionsToPathListMapper );
	}

	public @NonNull Observable<PathList> getSamplePathList() {
		return positionsSamplesResource.getSamplePositions()
				.map( PathsUtil.positionsToPathListMapper )
				.cache();
	}

	public @NonNull Observable<Path> findByGuid(@NonNull final String guid) {
		return positionsResource.findPositionsForGuid(guid)
				.map( PathsUtil.positionsToSinglePathMapper )
				.cache();
	}

	public @NonNull Observable<Void> ping() { return remoteLogResource.ping(); }
}
