package xst.pinmigrations.domain.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.aurae.retrofit2.LoganSquareConverterFactory;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.rx.RxJavaService;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.api.ApiUtil.API_LIST;

public class ApiGenerator {
	@NonNull final NetworkService networkService;
	@NonNull final RxJavaService rxJavaService;

	@Nullable Retrofit retrofit;

	public ApiGenerator(@NonNull final NetworkService networkService, @NonNull final RxJavaService rxJavaService) {
		this.networkService = checkNotNull(networkService);
		this.rxJavaService = checkNotNull(rxJavaService);
	}

	/** lazy-create then return retrofit-generator */
	protected @NonNull Retrofit getRetrofitGenerator() {
		if (null == retrofit) {
			retrofit = new Retrofit.Builder()
					.baseUrl( networkService.getBaseHttpUrl() )
					.client( networkService.getHttpClient() )
					.addConverterFactory( LoganSquareConverterFactory.create() )
					.addCallAdapterFactory( RxJavaCallAdapterFactory.createWithScheduler( rxJavaService.ioScheduler()) )
					.build();
		}

		return retrofit;
	}

	public @NonNull <S> S createService(@NonNull final Class<S> serviceClass) {
		checkArgument(API_LIST.contains( checkNotNull(serviceClass) ), "invalid API class");

		return getRetrofitGenerator().create(serviceClass);
	}
}
