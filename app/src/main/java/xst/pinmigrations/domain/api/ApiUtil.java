package xst.pinmigrations.domain.api;

import com.google.common.collect.ImmutableList;

import xst.pinmigrations.app.logging.remote_log.RemoteLogApi;
import xst.pinmigrations.domain.login.LoginApi;
import xst.pinmigrations.domain.positions.PositionsApi;

public class ApiUtil {
	public static final ImmutableList<Class<?>> API_LIST = ImmutableList.of(
			LoginApi.class,
			PositionsApi.class,
			RemoteLogApi.class
	);
}
