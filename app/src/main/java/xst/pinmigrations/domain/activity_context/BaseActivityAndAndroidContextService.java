package xst.pinmigrations.domain.activity_context;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.services.BaseDependantService;

import static com.google.common.base.Preconditions.checkNotNull;

/**
	Activity Services are bound to the activity's lifecycle
 */
public abstract class BaseActivityAndAndroidContextService extends BaseDependantService implements Destroyable {
	// avoid memory leaks!
	@NonNull private final WeakReference<App> appRef;
	protected final @Nullable App getApp() { return appRef.get(); }

	// avoid memory leaks!
	@NonNull private final WeakReference<BaseActivity> activityRef;
	protected final @Nullable BaseActivity getActivity() { return activityRef.get(); }

	private BaseActivityAndAndroidContextService() { throw new UnsupportedOperationException("dont use this"); }
	public BaseActivityAndAndroidContextService(@NonNull final App app, @NonNull final BaseActivity activity) {
		this.appRef = new WeakReference<>(checkNotNull(app));
		this.activityRef = new WeakReference<>(checkNotNull(activity));

		app.watchRefsOn(this);
	}


	@Override public void destroy() {
		appRef.clear();
	}
}
