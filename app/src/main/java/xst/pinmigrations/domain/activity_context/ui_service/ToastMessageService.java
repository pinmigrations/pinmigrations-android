package xst.pinmigrations.domain.activity_context.ui_service;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.activity_context.BaseActivityAndAndroidContextService;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Toast messages service
 */
public class ToastMessageService extends BaseActivityAndAndroidContextService {
	// avoid memory leaks!
	@NonNull private final WeakReference<LayoutInflater> layoutInflaterRef;
	final LayoutInflater getLayoutInflator() { return layoutInflaterRef.get(); }

	// avoid memory leaks!

	@NonNull final UiService uiService;

	// package-private so only other services (eg: UiService) can instantiate (and manage it)
	ToastMessageService(@NonNull final App app, @NonNull final BaseActivity act, @NonNull final UiService uiService) {
		super(app, act);
		this.layoutInflaterRef = new WeakReference<>(act.getLayoutInflater());
		this.uiService = uiService;
	}

	protected void makeToast(final String text, @LayoutRes final int layout, final int viewGroupId, final int textViewId,
				   final int toastLength, final int gravity, int xOffset, int yOffset) {

		final LayoutInflater layoutInflater = getLayoutInflator();
		final BaseActivity activity = getActivity();
		if (null == layoutInflater || null == activity) { return; }

		final View toastLayout = layoutInflater.inflate(layout, (ViewGroup) activity.findViewById(viewGroupId));

		final Toast toast = new Toast(checkNotNull( getApp() ));
		toast.setGravity(gravity, xOffset, yOffset);
		toast.setDuration(toastLength);
		toast.setView(toastLayout);
		((TextView) toastLayout.findViewById(textViewId)).setText(text);
		toast.show();
	}


	//region Quick Notice

	void showQuickNotice(@NonNull final String text) {
		uiService.runOnUiThread( new QuickNoticeRunner(checkNotNull(text)) );
	}
	class QuickNoticeRunner implements Runnable {
		final String text;
		public QuickNoticeRunner(@NonNull final String text) { this.text = text; }
		@Override public void run() {
			makeQuickNoticeToast(text);
		}
	}
	protected void makeQuickNoticeToast(final String text) {
		makeToast(text, R.layout.toast_small, R.id.toastSmallContainer, R.id.toastSmallText,
				Toast.LENGTH_SHORT, Gravity.TOP|Gravity.CENTER, 0, 20);
	}
	//endregion


	//region Important Message

	final void showImportantMessage(final String text) {
		uiService.runOnUiThread( new ImportantMessageRunner(text) );
	}
	class ImportantMessageRunner implements Runnable {
		final String text;
		public ImportantMessageRunner(@NonNull final String text) { this.text = text; }
		@Override public void run() {
			makeImportantMessageToast(text);
		}
	}

	protected void makeImportantMessageToast(final String text) {
		makeToast(text, R.layout.toast_large, R.id.toastLargeContainer, R.id.toastLargeText,
				Toast.LENGTH_LONG, Gravity.TOP|Gravity.CENTER, 0, 40);
	}
	//endregion


	@Override public final void destroy() {
		super.destroy();
		layoutInflaterRef.clear();
	}
}
