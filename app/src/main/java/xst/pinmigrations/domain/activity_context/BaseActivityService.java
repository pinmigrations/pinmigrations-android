package xst.pinmigrations.domain.activity_context;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.services.BaseDependantService;

import static com.google.common.base.Preconditions.checkNotNull;

/**
	Activity Services are bound to the activity's lifecycle
 */
public abstract class BaseActivityService extends BaseDependantService implements Destroyable {
	// avoid memory leaks!
	@NonNull private final WeakReference<BaseActivity> activityRef;
	protected final @Nullable BaseActivity getActivity() { return activityRef.get(); }

	private BaseActivityService() { throw new UnsupportedOperationException("dont use this"); }
	public BaseActivityService(@NonNull final BaseActivity activity) {
		this.activityRef = new WeakReference<>(checkNotNull(activity));

		activity.getApp().watchRefsOn(this);
	}


	@Override public void destroy() {
		activityRef.clear();
	}
}
