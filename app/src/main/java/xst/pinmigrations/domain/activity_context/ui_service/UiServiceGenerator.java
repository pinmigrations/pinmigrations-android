package xst.pinmigrations.domain.activity_context.ui_service;

import android.support.annotation.NonNull;

import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.android_context.BaseAndroidContextService;
import xst.pinmigrations.domain.rx.RxJavaService;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * TODO: figure out where to move this
 * note: make a UiService without Activity dep?
 * note: can use `Context` for toasts and for running code on UI-thread
 */
public class UiServiceGenerator extends BaseAndroidContextService {

	@NonNull final RxJavaService rxJavaService;
	public UiServiceGenerator(@NonNull final App app, @NonNull final RxJavaService rxJavaService) {
		super(app);
		this.rxJavaService = checkNotNull(rxJavaService);
	}

	public @NonNull UiService newUiService(@NonNull final BaseActivity activity) {
		return new UiService(checkNotNull(getApp()), checkNotNull(activity), rxJavaService);
	}
}
