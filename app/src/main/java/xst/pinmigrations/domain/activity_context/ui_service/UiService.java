package xst.pinmigrations.domain.activity_context.ui_service;

import android.support.annotation.NonNull;

import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import rx.functions.Action1;
import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.app.App;
import xst.pinmigrations.app.util.MainUtil;
import xst.pinmigrations.domain.activity_context.BaseActivityService;
import xst.pinmigrations.domain.rx.RxJavaService;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;


/**
 * main link between activity / view logic
 * util for toast messages, post on UI thread
 * NOTE: do not re-use between activities
 * see @ToastMessageService / UiService for good architectural separation without memory leaks
 * readability, and without excessive null-checks (@NonNull determinism with final fields)
 */
public class UiService extends BaseActivityService {
	@NonNull ToastMessageService messageService;
	@NonNull final RxJavaService rxJavaService;
	public UiService(@NonNull App app, @NonNull final BaseActivity activity, @NonNull final RxJavaService rxJavaService) {
		super(activity);
		this.messageService = new ToastMessageService(app, activity, this);
		this.rxJavaService = checkNotNull(rxJavaService);
	}


	/* Toasts */
	public void showImportantMessage(@NonNull final String text) {
		showImportantMessage(text, 0, MILLISECONDS);
	}
	public void showImportantMessage(@NonNull final String text, double delay, @NonNull final TimeUnit timeUnit) {
		final int msDelay = MainUtil.getMs(delay, timeUnit);
		if (0 == msDelay) { // delay is zero
			messageService.showImportantMessage(text);
		} else { // delay is positive
			rxJavaService.delayedObservable(msDelay, MILLISECONDS).subscribe(new ImportantMessageDisplayer(text));
		}
	}
	class ImportantMessageDisplayer implements Action1<Integer> {
		@NonNull final String text;
		public ImportantMessageDisplayer(@NonNull final String text) { this.text = text; }
		@Override public void call(final Integer integer) {
			messageService.showImportantMessage(text);
		}
	}


	public void showQuickNotice(final String text) {
		showQuickNotice(text, 0, MILLISECONDS);
	}
	public void showQuickNotice(@NonNull final String text, final double delay, @NonNull final TimeUnit timeUnit) {
		final int msDelay = MainUtil.getMs(delay, timeUnit);

		if (0 == msDelay) { // delay is zero
			messageService.showQuickNotice(text);
		} else { // delay is positive
			rxJavaService.delayedObservable(msDelay, MILLISECONDS).subscribe(new QuickNoticeDisplayer(text));
		}
	}
	class QuickNoticeDisplayer implements Action1<Integer> {
		@NonNull final String text;
		public QuickNoticeDisplayer(@NonNull final String text) { this.text = text; }
		@Override public void call(final Integer integer) {
			messageService.showQuickNotice(text);
		}
	}


	/** checks if err is a simple internet connectivity error and prompts user to connect
	 * @return true only if it was a net-error and user was prompted
	 */
	public boolean handlePotentialNetConnectError(@NonNull final Throwable err) {
		if (err instanceof UnknownHostException) {
			showImportantMessage("Network Error! Are you connected?", 2.5, SECONDS);
			return true;
		}
		return false;
	}


	public void runOnUiThread(@NonNull final Runnable r) {
		final BaseActivity activity = getActivity();
		if (null == activity) { return; }

		activity.runOnUiThread( checkNotNull(r) );
	}

	@Override public void destroy() {
		super.destroy();
		messageService.destroy();
	}
}
