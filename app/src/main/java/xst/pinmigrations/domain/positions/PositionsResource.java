package xst.pinmigrations.domain.positions;

import android.support.annotation.NonNull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.pushtorefresh.javac_warning_annotation.Warning;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.LoginException;

import rx.Observable;
import rx.exceptions.Exceptions;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.subjects.AsyncSubject;
import timber.log.Timber;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.LoginUtil;
import xst.pinmigrations.domain.network.SevereBackendIssue;
import xst.pinmigrations.domain.positions.json.PositionJson;
import xst.pinmigrations.domain.rx.BaseSubjectSubscriber;
import xst.pinmigrations.domain.rx.BaseTransformSubjectSubscriber;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;

public class PositionsResource {
	@NonNull protected final PositionsApi api;
	@NonNull final RemoteLogResource remoteLogResource;
	@NonNull final LoginManager loginManager;

	public PositionsResource(@NonNull final ApiGenerator apiGenerator, @NonNull final RemoteLogResource remoteLogResource,
							 @NonNull final LoginManager loginManager) {
		this.api = checkNotNull(
				apiGenerator.createService(PositionsApi.class)
		);
		this.remoteLogResource = checkNotNull(remoteLogResource);
		this.loginManager = loginManager;
	}

	//region FeedHash / Feed-Cache
	@NonNull protected String cachedFeedHash = "";
	@NonNull String updateFeedHash(final String newFeedHash) {
		final String oldFeedHash = verifyNotNull(cachedFeedHash);

		cachedFeedHash = verifyNotNull(newFeedHash);
		verify(!cachedFeedHash.isEmpty(), "feed hash cant be empty");

		return oldFeedHash;
	}

	@NonNull protected ImmutableList<Position> cachedAllPositions = ImmutableList.of();
	protected void cacheNewPositions(final List<Position> newPositions) {
		cachedAllPositions = ImmutableList.copyOf( verifyNotNull(newPositions) );
	}
	@NonNull ArrayList<Position> getCachedPositions() {
		return Lists.newArrayList(cachedAllPositions);
	}
	//endregion

	//region Get-All

	/** retrieve all positions as a single flat list (all guids) only public "get-all" method.
	 * may invoke onError w `SevereBackendIssue`
	 * 1. query remote
	 * 2. if feed-hash changed query for new, otherwise returns cached
	 */
	public @NonNull Observable<ArrayList<Position>> getAllPositions() {
		final AsyncSubject<ArrayList<Position>> allPositionsSubject = AsyncSubject.create();

		return Observable.defer(() -> {
			api.feedHash().subscribe(new FeedUpdateSubscriber(allPositionsSubject));

			return allPositionsSubject;
		}).cache();
	}

	/** intermediary that updates `PositionResource` cached feed-hash
	 * also checks if feed-hash is null and propagates `SevereBackendIssue` if so */
	class FeedUpdateSubscriber extends BaseTransformSubjectSubscriber<String, ArrayList<Position>> {
		public FeedUpdateSubscriber(final AsyncSubject<ArrayList<Position>> subject) {
			super(subject);
		}
		@Override public void onNext(final String newFeedHash) {
			if (isNullOrEmpty(newFeedHash)) { // retrofit returns null for empty-string (where string was expected)
				subject.onError(new SevereBackendIssue("got empty feed hash", SevereBackendIssue.EMPTY_FEED_HASH));
				return;
			}

			final String oldFeedHash = updateFeedHash(newFeedHash);
			if (!oldFeedHash.equals(newFeedHash)) { // feed-hash changed, re-query remote
				allPositionsRemote().subscribe(subject);
			} else { // cached-positions still valid
				Observable.just(getCachedPositions()).subscribe(subject);
			}
		}

	}


	/** query remote (w error-checking), parse json, update caches */
	@NonNull Observable<ArrayList<Position>> allPositionsRemote() {
		return apiAllPositionsCheckedQuery()
				.compose(PositionsUtil.modelListTransformer)
				.map(domainListCacher)
				.cache();
	}

	/** wraps invocations of API.allPositions with a check for empty-data and app-specific checked exception */
	@NonNull Observable<List<PositionJson>> apiAllPositionsCheckedQuery() {
		final AsyncSubject<List<PositionJson>> subject = AsyncSubject.create();
		return Observable.defer(() -> {

			api.allPositions().subscribe(new ModelListEmptyChecker(subject));

			return subject;
		});
	}
	/** just checks if model-list and passes SevereBackendIssue if it is */
	static class ModelListEmptyChecker extends BaseSubjectSubscriber<List<PositionJson>> {
		public ModelListEmptyChecker(@NonNull final AsyncSubject<List<PositionJson>> subject) { super(subject); }
		@Override public void onNext(final List<PositionJson> modelListOriginal) {
			final List<PositionJson> modelList = checkNotNull(modelListOriginal);

			if (modelList.size() > 0) { // got data, pass it along to subject
				Observable.just(modelList).subscribe(subject);
			} else { // severe-error, notify all!
				final Exception exc = new SevereBackendIssue("no position data", SevereBackendIssue.NO_POSITION_DATA);
				subject.onError(exc);
			}
		}
	}

	/** needs get/update so not static */
	@NonNull protected final Func1<List<Position>, ArrayList<Position>>
			domainListCacher = new Func1<List<Position>, ArrayList<Position>>() {
				@Override public ArrayList<Position> call(final List<Position> positions) {
					if (positions.size() <= 0) {
						throw Exceptions.propagate(new SevereBackendIssue(""));
					}
					cacheNewPositions(positions);

					return getCachedPositions();
				}
			};


	public @NonNull Observable<ArrayList<Position>> softGetAllPositions() {
		return Observable.defer(softGetAllObservableFactory);
	}
	Func0< Observable<ArrayList<Position>> > softGetAllObservableFactory = new Func0< Observable<ArrayList<Position>> >() {
		@Override public @NonNull Observable<ArrayList<Position>> call() {
			Timber.d("soft-get-all obs fact. cached-size=" + cachedAllPositions.size());
			if (cachedAllPositions.size() > 0) {
				return Observable.just( getCachedPositions() );
			} else {
				return getAllPositions();
			}
		}
	};
	//endregion


	@Warning("unimplemented")
	/* checks cached positions and if missing (eg: deleted remotely) queries remote */
	public @NonNull Observable<Position> softGetPosition(final long id) {
		final int index = cachedAllPositions.indexOf( new Position(id) );
		if (-1 != index) {
			return Observable.just( new Position( cachedAllPositions.get(index) ) );
		}

		throw new AssertionError("invalid ID " + id); // TODO: remove after implementing correctly
		// return api.read(id).map(modelToDomainMapper);
	}

	@Warning("unimplemented")
	/* query remote */
	public @NonNull Observable<Position> getPosition(final long id) {
		// NOTE: update cached in list also, return a copy of cached pos
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	public @NonNull Observable<List<Position>> findPositionsForGuid(@NonNull final String guid) {
		return api.positionsForGuid( checkNotNull(guid) )
				.compose(PositionsUtil.modelListTransformer)
				.cache();
	}

	public @NonNull Observable<Position> update(@NonNull final Position position) throws LoginException {
		if ( loginManager.notAuthed() ) { throw LoginUtil.notLoggedInException(); }

		final Position p = checkNotNull(position);
		return api.update(p.id, p.id+"", p.guid, p.lat+"", p.lng+"", p.ord+"", p.description)
				.map(modelToDomainMapper)
				.cache();
	}

	public @NonNull Observable<Void> delete(final long positionId) throws LoginException {
		if ( loginManager.notAuthed() ) { throw LoginUtil.notLoggedInException(); }

		final Observable<Void> deleteObs =
				api.destroy(positionId).cache();

		deleteObs.subscribe(); // subscribe so it starts immediately

		return deleteObs;
	}

	public @NonNull Observable<Position> create(@NonNull final Position position) throws LoginException {
		if ( loginManager.notAuthed() ) { throw LoginUtil.notLoggedInException(); }

		final Position p = checkNotNull(position);
		return api.create(p.lat+"", p.lng+"", p.ord+"", p.description, p.guid)
				.map(modelToDomainMapper)
				.cache();
	}


	@NonNull protected final static Func1<PositionJson, Position>
			modelToDomainMapper = new Func1<PositionJson, Position>() {
		@Override public @NonNull Position call(final PositionJson positionModel) {
			return new Position(positionModel);
		}
	};

	public @NonNull Observable<Void> ping() {
		return remoteLogResource.ping();
	}


	public static class PositionNotFoundException extends Exception {
		public static final long serialVersionUID = 1L;
	}
}
