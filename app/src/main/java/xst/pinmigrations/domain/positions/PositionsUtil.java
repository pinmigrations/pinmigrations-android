package xst.pinmigrations.domain.positions;

import android.support.annotation.NonNull;

import com.bluelinelabs.logansquare.LoganSquare;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import rx.Observable;
import rx.Observable.Transformer;
import rx.functions.Func1;
import xst.pinmigrations.domain.positions.json.PositionJson;
import xst.pinmigrations.ui.Comparators;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;
import static java.util.regex.Pattern.CASE_INSENSITIVE;

/**
 * Positions util
 * contains no state
 */
public final class PositionsUtil {
	private PositionsUtil() { throw new UnsupportedOperationException("dont instantiate util class"); }

	public static @NonNull Observable<List<PositionJson>> rxParseInputStream(@NonNull final InputStream inputStream) throws IOException {
		return Observable.just(parseInputStream(inputStream));
	}
	public static @NonNull List<PositionJson> parseInputStream(@NonNull final InputStream inputStream) throws IOException {
		final InputStream is = verifyNotNull(inputStream);
		List<PositionJson> list = LoganSquare.parseList(is, PositionJson.class);
		//noinspection ResultOfMethodCallIgnored
		verifyNotNull(list);
		return list;
	}

	protected static final Pattern GUID_REGEX = Pattern.compile("^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$", CASE_INSENSITIVE);
	public static boolean isValidGuid(@NonNull final String guid) {
		if ( checkNotNull(guid).isEmpty() ) { return false; }
		return GUID_REGEX.matcher(guid).matches();
	}

	public static String newGuid() {
		return UUID.randomUUID().toString();
	}

	/** Retrofit API generates this, also from file (for samples) */
	public static @NonNull Observable<List<Position>> rxModelListToDomain(final Observable<List<PositionJson>> modelList) {
		return modelList
				.flatMap(modelListFlattener)
				.map(modelToDomainMapper)
				.toList();
	}
	// need these for AndroidTests! (dont use any service instance members)
	@NonNull public static final Func1<List<PositionJson>, Observable<PositionJson>>
		modelListFlattener = new Func1<List<PositionJson>, Observable<PositionJson>>() {
			@Override public Observable<PositionJson> call(final List<PositionJson> jsonList) {
				return Observable.from(jsonList);
			}
		};
	@NonNull public static final Func1<PositionJson, Position>
		modelToDomainMapper = new Func1<PositionJson, Position>() {
			@Override public Position call(final PositionJson modelObj) {
				return new Position(modelObj);
			}
		};

	@NonNull final static Transformer<List<PositionJson>, List<Position>>
		modelListTransformer = new Transformer<List<PositionJson>, List<Position>>() {
			@Override public Observable<List<Position>> call(final Observable<List<PositionJson>> observable) {
				return rxModelListToDomain(observable);
			}
	};

	public static @NonNull ArrayList<Position> getEmptyPositionsList() { return new ArrayList<>(); }

	public static void sortPositionsByGuid(@NonNull final List<Position> positions) {
		Collections.sort(positions, POSITION_GUID_COMPARATOR);
	}
	public static void sortPositionsByOrd(@NonNull final List<Position> positions) {
		Collections.sort(positions, POSITION_ORD_COMPARATOR);
	}


	public static final PositionIdComparator POSITION_ID_COMPARATOR = new PositionIdComparator();
	public static final PositionLatComparator POSITION_LAT_COMPARATOR = new PositionLatComparator();
	public static final PositionLngComparator POSITION_LNG_COMPARATOR = new PositionLngComparator();
	public static final PositionDescriptionComparator POSITION_DESC_COMPARATOR = new PositionDescriptionComparator();
	public static final PositionGuidComparator POSITION_GUID_COMPARATOR = new PositionGuidComparator();
	public static final PositionOrdComparator POSITION_ORD_COMPARATOR = new PositionOrdComparator();

	final static class PositionIdComparator implements Comparator<Position> {
		@Override public int compare(final Position p1, final Position p2) {
			return Comparators.longCompareAscending(p1.id, p2.id);
		}
	}
	final static class PositionLatComparator implements Comparator<Position> {
		@Override public int compare(final Position p1, final Position p2) {
			return Comparators.doubleCompareAscending(p1.lat, p2.lat);
		}
	}
	final static class PositionLngComparator implements Comparator<Position> {
		@Override public int compare(final Position p1, final Position p2) {
			return Comparators.doubleCompareAscending(p1.lng, p2.lng);
		}
	}
	final static class PositionDescriptionComparator implements Comparator<Position> {
		@Override public int compare(final Position p1, final Position p2) {
			return Comparators.stringCompareAscending(p1.description, p2.description);
		}
	}
	final static class PositionGuidComparator implements Comparator<Position> {
		@Override public int compare(final Position p1, final Position p2) {
			return Comparators.stringCompareAscending(p1.guid, p2.guid);
		}
	}
	final static class PositionOrdComparator implements Comparator<Position> {
		@Override public int compare(final Position p1, final Position p2) {
			return Comparators.longCompareAscending(p1.ord, p2.ord);
		}
	}
}
