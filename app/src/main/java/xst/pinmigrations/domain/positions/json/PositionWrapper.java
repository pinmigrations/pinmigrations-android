package xst.pinmigrations.domain.positions.json;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import xst.pinmigrations.domain.positions.Position;

@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.ANNOTATIONS_ONLY)
public class PositionWrapper {
	public PositionWrapper() { }
	public PositionWrapper(final Position p) {
		this.location = new PositionJson(p);
	}

	@JsonField(name = "location") public PositionJson location;

	/*
	@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.ANNOTATIONS_ONLY)
	public static class PositionJson {
		public PositionJson() {}
		public PositionJson(@NonNull final Position pos) {
			final Position p = checkNotNull(pos);
			this.id = p.id;
			this.lat = "" + p.lat;
			this.lng = "" + p.lng;
			this.description = p.description;
			this.guid = p.guid;
			this.ord = p.ord;
		}

		@JsonField public long id;
		@JsonField public String guid;

		@JsonField public String lat;
		@JsonField public String lng;

		@JsonField public String description;

		@JsonField public long ord;
	}
	*/
}
