package xst.pinmigrations.domain.positions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import xst.pinmigrations.domain.maps.Location;
import xst.pinmigrations.domain.positions.json.PositionJson;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

public class Position {
	public static final long DEFAULT_ID = 0L;

	// data
	public long id = DEFAULT_ID;
	public double lat=0.0, lng=0.0;
	public @NonNull String description="", guid="";
	public long ord=0;

	//region CTOR
	private Position(){ }
	public Position(@NonNull final Position position) {
		this.id = checkNotNull(position).id;
		this.lat = position.lat;
		this.lng = position.lng;
		setDescription(position.description);
		setGuid(position.guid);
		this.ord = position.ord;
		validate();
	}
	public Position(@NonNull final PositionJson positionJson) throws NumberFormatException {
		this.id = checkNotNull(positionJson).id;

		this.lat = Double.parseDouble(positionJson.lat);
		this.lng = Double.parseDouble(positionJson.lng);

		setDescription(positionJson.description);
		setGuid(positionJson.guid);

		this.ord = positionJson.ord;
		validate();
	}
	public Position(@NonNull final Location location) {
		this(location.position);
	}
	/** minimally valid position object */
	public Position(final long id){ this.id = id; }
	public Position(final long id, final double lat, final double lng,
					@NonNull final String description, @NonNull final String guid, final long ord) {
		this.id = id;
		this.lat = lat;
		this.lng = lng;
		setDescription(description);
		setGuid(guid);
		this.ord = ord;
		validate();
	}
	//endregion

	public void setDescription(@Nullable final String description) {
		this.description = (null == description) ? "" : description;
	}
	public void setGuid(@Nullable final String guid) {
		this.guid = (null == guid) ? "" : guid;
	}

	protected void validate() {
		verify(this.lat <= MAX_LAT && this.lat >= MIN_LAT, "invalid latitude");
		verify(this.lng <= MAX_LNG && this.lng >= MIN_LNG, "invalid longitude");
		// note: GUID isnt validated on construction
	}

	@Override public String toString() { return "Position(id=" + id + ", ord=" + ord + ", guid=" + guid + " )"; }

	/* this and hash-code only use ID field */
	@Override public boolean equals(@Nullable final Object other) {
		if (this == other) { return true; }
		if ( !(other instanceof Position) ) { return false; }  // note: this handles null-case
		final Position otherPosition = (Position) other;

		return id == otherPosition.id;
	}

	/* ID field */
	@Override public int hashCode() { return (int) id; }

	//region util
	public static final double MIN_LAT = -85.0d;
	public static final double MAX_LAT = 85.0d;
	public static final double MIN_LNG = -180.0d;
	public static final double MAX_LNG = 180.0d;
	//endregion
}
