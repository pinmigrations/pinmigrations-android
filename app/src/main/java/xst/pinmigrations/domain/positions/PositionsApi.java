package xst.pinmigrations.domain.positions;

import android.support.annotation.NonNull;

import java.util.List;

import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;
import xst.pinmigrations.domain.positions.json.PositionJson;

public interface PositionsApi {
	/** GET /feed
	 *	note: no login required
	 */
	@Headers("Cache-Control: no-store, no-cache")
	@GET("/feed")
	Observable<List<PositionJson>> allPositions();


	/** GET /feed-hash
	 *	note: no login required
	 */
	@Headers("Cache-Control: no-store, no-cache")
	@GET("/feed-hash")
	Observable<String> feedHash();


	/** POST /locations.json
	 *	note: returns 201 (created) on success, returns 401 (unauthorized) if not logged in
	 *	content-type: application/x-www-form-urlencoded;
	 * 	sample-req: location%5Blat%5D=33.73812&location%5Blng%5D=-117.9225193&location%5Bord%5D=0&location%5Bdescription%5D=TEST-POSITION
	 *	decoded-sample-req:
	 *		location[lat]=33.73812
	 *		location[lng]=-117.9225193
	 *		location[ord]=0
	 *		location[description]=TEST-POSITION
	 * 	sample-resp: {"id":25,"lat":"33.73812","lng":"-117.92252","description":"TEST-POSITION","guid":"91a7bd3d-a75a-4807-b708-9d0303bf57c7","ord":0}
	 */
	@POST("/locations.json")
	@FormUrlEncoded
	@Headers("Cache-Control: no-store, no-cache")
	Observable<PositionJson> create(@Field("location[lat]") @NonNull String lat,
									   @Field("location[lng]") @NonNull String lng,
									   @Field("location[ord]") @NonNull String ord,
									   @Field("location[description]") @NonNull String description,
									@Field("location[guid]") @NonNull String guid);



	/** GET /locations/:id.json
	 *	note: returns 201 (created) on success, returns 401 (unauthorized) if not logged in
	 *	content-type: application/x-www-form-urlencoded;
	 * 	sample-req: location%5Blat%5D=33.73812&location%5Blng%5D=-117.9225193&location%5Bord%5D=0&location%5Bdescription%5D=TEST-POSITION
	 *	decoded-sample-req:
	 *		location[lat]=33.73812
	 *		location[lng]=-117.9225193
	 *		location[ord]=0
	 *		location[description]=TEST-POSITION
	 * 	sample-resp: {"id":25,"lat":"33.73812","lng":"-117.92252","description":"TEST-POSITION","guid":"91a7bd3d-a75a-4807-b708-9d0303bf57c7","ord":0}
	 */
	@GET("/locations/{id}.json")
	@Headers("Cache-Control: no-store, no-cache")
	Observable<PositionJson> read(@Path("id") long posId);


	/** PUT /locations/:id.json
	 *	note: returns 201 (created) on success, returns 401 (unauthorized) if not logged in
	 *	content-type: application/x-www-form-urlencoded;
	 * 	sample: location%5Blat%5D=33.73812&location%5Blng%5D=-117.9225193&location%5Bord%5D=0&location%5Bdescription%5D=TEST-POSITION
	 */
	@PUT("/locations/{id}.json")
	@FormUrlEncoded
	@Headers("Cache-Control: no-store, no-cache")
	Observable<PositionJson> update(@Path("id") long posId,
									   @Field("location[id]") @NonNull String id,
									   @Field("location[guid]") @NonNull String guid,
									   @Field("location[lat]") @NonNull String lat,
									   @Field("location[lng]") @NonNull String lng,
									   @Field("location[ord]") @NonNull String ord,
									   @Field("location[description]") @NonNull String description);


	/** DELETE /locations/:id.json
	 *	note: returns 204 (no content) on success, returns 401 (unauthorized) if not logged in
	 */
	@DELETE("/locations/{id}.json")
	@Headers("Cache-Control: no-store, no-cache")
	Observable<Void> destroy(@Path("id") long posId);


	/** GET /locations/find/:guid.json
	 *	note: no login required
	 */
	@GET("/locations/find/{guid}.json")
	@Headers("Cache-Control: no-store, no-cache")
	Observable<List<PositionJson>> positionsForGuid(@Path("guid") @NonNull String guid);
}
