package xst.pinmigrations.domain.positions.json;

import android.support.annotation.NonNull;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import xst.pinmigrations.domain.positions.Position;

import static com.google.common.base.Preconditions.checkNotNull;

@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.ANNOTATIONS_ONLY)
public class PositionJson {
	public PositionJson() {}
	public PositionJson(@NonNull final Position pos) {
		final Position p = checkNotNull(pos);
		this.id = p.id;
		this.lat = "" + p.lat;
		this.lng = "" + p.lng;
		this.description = p.description;
		this.guid = p.guid;
		this.ord = p.ord;
	}

	@JsonField public long id;
	@JsonField public String guid;

	@JsonField public String lat;
	@JsonField public String lng;

	@JsonField public String description;

	@JsonField public long ord;
}
