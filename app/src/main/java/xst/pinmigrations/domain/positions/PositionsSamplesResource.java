package xst.pinmigrations.domain.positions;

import android.support.annotation.NonNull;

import com.google.common.collect.Lists;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.rx.RxJavaService;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;

public class PositionsSamplesResource {
	@NonNull final AndroidService assetsService;
	@NonNull final RxJavaService rxJavaService;

	public PositionsSamplesResource(@NonNull final AndroidService androidService,
									@NonNull final RxJavaService rxJavaService) {
		this.assetsService = checkNotNull(androidService);
		this.rxJavaService = checkNotNull(rxJavaService);
	}


	@NonNull protected ArrayList<Position> cachedSamplePositions = Lists.newArrayList();
	void cacheNewPositions(final List<Position> newPositions) {
		cachedSamplePositions = Lists.newArrayList( verifyNotNull(newPositions) );
	}
	ArrayList<Position> getCachedPositions() {
		return Lists.newArrayList(cachedSamplePositions);
	}

	public @NonNull Observable<Position> update(@NonNull final Position position) {
		final int index = cachedSamplePositions.indexOf( checkNotNull(position) );
		if (-1 != index) {
			cachedSamplePositions.set(index, position);
			return Observable.just( position );
		} else {
			throw new AssertionError("(update) invalid position");
		}
	}

	public @NonNull Observable<Void> delete(final long positionId) {
		final int index = cachedSamplePositions.indexOf( new Position(positionId) );
		if (-1 != index) {
			cachedSamplePositions.remove(index);
			return rxJavaService.newVoidObservable();
		} else {
			throw new AssertionError("(update) invalid position");
		}
	}

	@SuppressWarnings("MethodWithMultipleReturnPoints") // caching short-circuit!
	public @NonNull Observable<ArrayList<Position>> getSamplePositions() {
		if (cachedSamplePositions.size() > 0) {
			return Observable.just(getCachedPositions());
		}

		return Observable.defer(() -> {
			try {
				final InputStream fileInputStream = assetsService.getAssetFile("example-positions.json");

				return PositionsUtil.rxParseInputStream(fileInputStream)
						.compose(PositionsUtil.modelListTransformer)
						.map(samplesDomainListCacher);
			} catch (final IOException e) {
				return Observable.error(e);
			}
		});
	}


	public @NonNull Observable<Position> getSamplePosition(final long id) {
		final int index = cachedSamplePositions.indexOf( new Position(id) );
		if (-1 != index) {
			return Observable.just( new Position( cachedSamplePositions.get(index) ) );
		}

		throw new AssertionError("invalid ID " + id); // TODO: remove after implementing correctly
		// return api.read(id).map(modelToDomainMapper);
	}

	@NonNull protected final Func1<List<Position>, ArrayList<Position>>
			samplesDomainListCacher = new Func1<List<Position>, ArrayList<Position>>() {
				@Override public ArrayList<Position> call(final List<Position> positions) {
					// save them to cache (they never change)
					cacheNewPositions(positions);
					return getCachedPositions();
				}
	};
}
