package xst.pinmigrations.domain.android_context.shared_preferences;

import android.support.annotation.NonNull;

import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkNotNull;

public class Entry {
	public static final ImmutableList<Class<?>> PREFS_TYPES = ImmutableList.of(
			String.class,
			Integer.class,
			Boolean.class,
			Long.class,
			Float.class
	);

	public static final String DEFAULT_STRING = "";
	public static final int DEFAULT_INT = Integer.MIN_VALUE;
	public static final boolean DEFAULT_BOOL = false;
	public static final long DEFAULT_LONG = Long.MIN_VALUE;
	public static final float DEFAULT_FLOAT = Float.MIN_VALUE;

	@NonNull public final KeyType keyType;
	@NonNull public final Object value;
	Entry(@NonNull final KeyType keyType, @NonNull final Object prefValue) {
		this.keyType = checkNotNull(keyType);
		this.value = PrefsUtil.validatePrefValueAndKeyType(this.keyType, prefValue);
	}

	public static class KeyType {
		@NonNull public final String key;
		@NonNull public final Class<?> type;
		KeyType(@NonNull final String prefKey, @NonNull Class<?> valType) {
			this.key = checkNotNull(prefKey);
			this.type = PrefsUtil.validatePrefValType(valType);
		}
	}

	public static class StringEntry extends Entry {
		StringEntry(@NonNull final String prefKey, @NonNull final String prefValue) {
			super(new StringKeyType(prefKey), prefValue);
		}
	}
	public static class IntEntry extends Entry {
		IntEntry(@NonNull final String prefKey, final int prefValue) {
			super(new IntKeyType(prefKey), prefValue);
		}
	}
	public static class BoolEntry extends Entry {
		BoolEntry(@NonNull final String prefKey, final boolean prefValue) {
			super(new BoolKeyType(prefKey), prefValue);
		}
	}
	public static class LongEntry extends Entry {
		LongEntry(@NonNull final String prefKey, final long prefValue) {
			super(new LongKeyType(prefKey), prefValue);
		}
	}
	public static class FloatEntry extends Entry {
		FloatEntry(@NonNull final String prefKey, final float prefValue) {
			super(new FloatKeyType(prefKey), prefValue);
		}
	}

	public static class StringKeyType extends KeyType {
		StringKeyType(@NonNull final String prefKey) { super(prefKey, String.class); }
	}
	public static class IntKeyType extends KeyType {
		IntKeyType(@NonNull final String prefKey) { super(prefKey, Integer.class); }
	}
	public static class BoolKeyType extends KeyType {
		BoolKeyType(@NonNull final String prefKey) { super(prefKey, Boolean.class); }
	}
	public static class LongKeyType extends KeyType {
		LongKeyType(@NonNull final String prefKey) { super(prefKey, Long.class); }
	}
	public static class FloatKeyType extends KeyType {
		FloatKeyType(@NonNull final String prefKey) { super(prefKey, Float.class); }
	}
}
