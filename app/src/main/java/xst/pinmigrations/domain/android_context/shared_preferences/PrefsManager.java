package xst.pinmigrations.domain.android_context.shared_preferences;

import android.support.annotation.NonNull;

import rx.Observable;
import rx.functions.Func1;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.StringEntry;
import xst.pinmigrations.ui.UiUtil;

import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.android_context.shared_preferences.PrefsUtil.stringEntry;

public class PrefsManager {
	public static final String MAP_ORIENTATION_PREF = "MAP_ORIENTATION_PREF";
	public static final int DEFAULT_MAP_ORIENTATION = UiUtil.SCREEN_ROTATE_LANDSCAPE;
	public static final String PREF_KEY_REMEMBER_TOKEN = "REMEMBER_TOKEN_PREF";
	public static final String PREF_KEY_SESSION = "SESSION_PREF";

	@NonNull final PrefsResource prefsResource;
	public PrefsManager(@NonNull final PrefsResource prefsResource) {
		this.prefsResource = checkNotNull(prefsResource);
	}

	public void updateLoginState(@NonNull final String session, @NonNull final String rememberToken) {
		final StringEntry sessionEntry = stringEntry(PREF_KEY_SESSION, checkNotNull(session));
		final StringEntry rembTokenEntry = stringEntry(PREF_KEY_REMEMBER_TOKEN, checkNotNull(rememberToken));

		prefsResource.putEntries(sessionEntry, rembTokenEntry);
	}

	public @NonNull Observable<Integer> getMapOrientationPref() {
		return prefsResource.getInt(MAP_ORIENTATION_PREF).map(mapOrientationPrefDefaultIntercept);
	}
	protected static final Func1<Integer, Integer> mapOrientationPrefDefaultIntercept = new Func1<Integer, Integer>() {
		@Override public Integer call(final Integer pref) {
			return (Entry.DEFAULT_INT == pref) ? DEFAULT_MAP_ORIENTATION : pref;
		}
	};
	public void updateMapOrientationPref(final int orientation) {
		prefsResource.putInt(MAP_ORIENTATION_PREF, orientation);
	}
}
