package xst.pinmigrations.domain.android_context.shared_preferences;

import android.support.annotation.NonNull;

import java.util.HashMap;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Action0;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.KeyType;
import xst.pinmigrations.domain.rx.RxJavaService;

import static com.google.common.base.Preconditions.checkNotNull;

public class PrefsResource {
	@NonNull final PrefsStore store;

	@NonNull final RxJavaService rxJavaService;
	@NonNull final Scheduler scheduler;

	public PrefsResource(@NonNull final PrefsStore store, @NonNull final RxJavaService rxJavaService) {
		this.store = checkNotNull(store);
		this.rxJavaService = checkNotNull(rxJavaService);
		this.scheduler = checkNotNull( rxJavaService.ioScheduler() );
	}

	protected /* @NonNull?? */ Scheduler.Worker worker() { return scheduler.createWorker(); }

	public Observable<Boolean> contains(@NonNull final String prefKey) {
		return Observable.just( store.contains(prefKey) ).subscribeOn(scheduler);
	}

	//region Get/Put String (+ single Entry)

	public @NonNull Observable<String> getString(@NonNull final String prefKey) {
		return Observable.just( store.getString(prefKey) ).subscribeOn(scheduler);
	}
	public @NonNull Observable<Integer> getInt(@NonNull final String prefKey) {
		return Observable.just( store.getInt(prefKey) ).subscribeOn(scheduler);
	}
	public @NonNull Observable<Boolean> getBool(@NonNull final String prefKey) {
		return Observable.just( store.getBool(prefKey) ).subscribeOn(scheduler);
	}
	public @NonNull Observable<Long> getLong(@NonNull final String prefKey) {
		return Observable.just( store.getLong(prefKey) ).subscribeOn(scheduler);
	}
	public @NonNull Observable<Float> getFloat(@NonNull final String prefKey) {
		return Observable.just( store.getFloat(prefKey) ).subscribeOn(scheduler);
	}

	public void putString(@NonNull final String prefKey, @NonNull final String prefValue) {
		putEntry(PrefsUtil.stringEntry(prefKey, prefValue));
	}
	public void putInt(@NonNull final String prefKey, @NonNull final Integer prefValue) {
		putEntry(PrefsUtil.intEntry(prefKey, prefValue));
	}
	public void putBool(@NonNull final String prefKey, @NonNull final Boolean prefValue) {
		putEntry(PrefsUtil.boolEntry(prefKey, prefValue));
	}
	public void putLong(@NonNull final String prefKey, @NonNull final Long prefValue) {
		putEntry(PrefsUtil.longEntry(prefKey, prefValue));
	}
	public void putFloat(@NonNull final String prefKey, @NonNull final Float prefValue) {
		putEntry(PrefsUtil.floatEntry(prefKey, prefValue));
	}
	public void putEntry(@NonNull final Entry entry) {
		worker().schedule(new PutEntryAction(entry));
	}
	class PutEntryAction implements Action0 {
		@NonNull final Entry entry;
		public PutEntryAction(@NonNull final Entry entry) { this.entry = checkNotNull(entry); }
		@Override public void call() { store.putEntry(entry); }
	}
	//endregion

	//region Get/Put Entries

	public @NonNull Observable<HashMap<String, Entry>> getEntries(@NonNull final KeyType... entryKeys) {
		return Observable.just( store.getEntries(entryKeys) ).subscribeOn(scheduler);
	}
	public void putEntries(@NonNull final Entry... entries) {
		worker().schedule(new PutEntriesAction(entries));
	}
	class PutEntriesAction implements Action0 {
		@NonNull final Entry[] entries;
		public PutEntriesAction(@NonNull final Entry[] entries) { this.entries = checkNotNull(entries); }
		@Override public void call() { store.putEntries(entries); }
	}
	//endregion

	//region Remove / Remove All

	public void remove(@NonNull final String prefKey) {
		worker().schedule(new RemoveAction(prefKey));
	}
	class RemoveAction implements Action0 {
		@NonNull final String key;
		public RemoveAction(@NonNull final String key) { this.key = checkNotNull(key); }
		@Override public void call() { store.remove(key); }
	}

	public void removeAll(@NonNull final String... entryKeys) {
		worker().schedule(new RemoveAllAction(entryKeys));
	}
	class RemoveAllAction implements Action0 {
		@NonNull final String[] entries;
		public RemoveAllAction(@NonNull final String[] entries) { this.entries = checkNotNull(entries); }
		@Override public void call() { store.removeAll(entries); }
	}
	//endregion
}
