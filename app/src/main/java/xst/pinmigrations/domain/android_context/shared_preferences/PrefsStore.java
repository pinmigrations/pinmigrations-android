package xst.pinmigrations.domain.android_context.shared_preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.common.collect.Maps;

import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;
import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_BOOL;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_FLOAT;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_INT;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_LONG;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_STRING;

/**
 * blocking SharedPreferences access-logic
 * value can be empty-string, cannot be null
 */
public class PrefsStore {
	@NonNull final SharedPreferences prefs;
	public PrefsStore(@NonNull final Context ctx, @NonNull final String prefsFileName) {
		prefs = checkNotNull(ctx)
				.getSharedPreferences(checkNotNull(prefsFileName), MODE_PRIVATE);
	}

	/** blocks */
	public boolean contains(@NonNull final String prefKey) {
		return prefs.contains(checkNotNull(prefKey));
	}

	//region GET
	public String getString(@NonNull final String prefKey) {
		return prefs.getString(checkNotNull(prefKey), DEFAULT_STRING);
	}
	public int getInt(@NonNull final String prefKey) {
		return prefs.getInt(checkNotNull(prefKey), DEFAULT_INT);
	}
	public boolean getBool(@NonNull final String prefKey) {
		return prefs.getBoolean(checkNotNull(prefKey), DEFAULT_BOOL);
	}
	public long getLong(@NonNull final String prefKey) {
		return prefs.getLong(checkNotNull(prefKey), DEFAULT_LONG);
	}
	public float getFloat(@NonNull final String prefKey) {
		return prefs.getFloat(checkNotNull(prefKey), DEFAULT_FLOAT);
	}
	//endregion

	//region batch Entry/Entries
	@SuppressLint("ApplySharedPref") // invoked asynchronously by PrefsResource
	void putEntry(@NonNull final Entry entry) {
		final SharedPreferences.Editor editor = prefs.edit();
		PrefsUtil.putEditorEntry(editor, checkNotNull(entry));
		editor.commit();
	}

	@SuppressLint("ApplySharedPref") // invoked asynchronously by PrefsResource
	void putEntries(@NonNull final Entry... entries) {
		final SharedPreferences.Editor editor = prefs.edit();
		for (final Entry entry: checkNotNull(entries)) {
			PrefsUtil.putEditorEntry(editor, checkNotNull(entry));
		}
		editor.commit();
	}

	/** all entries will have empty string if prefs doesnt contain value */
	@NonNull HashMap<String, Entry> getEntries(@NonNull final Entry.KeyType... entryKeys) {
		final HashMap<String, Entry> entries = Maps.newHashMap();

		for (final Entry.KeyType keyType: checkNotNull(entryKeys)) {
			entries.put(keyType.key, PrefsUtil.getPrefEntry(prefs, keyType));
		}
		return entries;
	}


	//region Remove
	@SuppressLint("ApplySharedPref") // invoked asynchronously by PrefsResource
	void remove(@NonNull final String prefKey) {
		final SharedPreferences.Editor editor = prefs.edit();
		editor.remove(checkNotNull(prefKey));
		editor.commit();
	}

	@SuppressLint("ApplySharedPref") // invoked asynchronously by PrefsResource
	void removeAll(@NonNull final String... entryKeys) {
		final SharedPreferences.Editor editor = prefs.edit();
		for (final String key: checkNotNull(entryKeys)) {
			editor.remove( key );
		}

		editor.commit();
	}
	//endregion
}
