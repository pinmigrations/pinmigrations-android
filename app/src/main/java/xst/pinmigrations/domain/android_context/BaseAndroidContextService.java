package xst.pinmigrations.domain.android_context;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.services.BaseDependantService;

import static com.google.common.base.Preconditions.checkNotNull;

/**
	Activity Services are bound to the activity's lifecycle
 */
public abstract class BaseAndroidContextService extends BaseDependantService implements Destroyable {
	// avoid memory leaks!
	@NonNull private final WeakReference<App> appRef;
	protected final @Nullable App getApp() { return appRef.get(); }

	private BaseAndroidContextService() { throw new UnsupportedOperationException("dont use this"); }
	public BaseAndroidContextService(@NonNull final App app) {
		this.appRef = new WeakReference<>(checkNotNull(app));
		app.watchRefsOn(this);
	}


	@Override public void destroy() {
		appRef.clear();
	}
}
