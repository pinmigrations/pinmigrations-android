package xst.pinmigrations.domain.android_context;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import timber.log.Timber;
import xst.pinmigrations.domain.services.BaseService;

import static com.google.common.base.Preconditions.checkNotNull;

/*
	http://stackoverflow.com/a/18755642/1163940
	https://developers.google.com/android/reference/com/google/android/gms/common/GoogleApiAvailability#isGooglePlayServicesAvailable%28android.content.Context%29
 */
public class GoogleApiService extends BaseService {
	final Context context;
	final GoogleApiAvailability googleApiAvailability;

	public static final int UPDATE_GOOGLE_PLAY_SERVICES = 0xf2f2f2;

	/*
		This service requires Context in order to function properly
	 */
	private GoogleApiService() { throw new UnsupportedOperationException("dont use this"); }
	public GoogleApiService(@NonNull final Context ctx) {
		this.context = checkNotNull(ctx);
		this.googleApiAvailability = GoogleApiAvailability.getInstance();
	}

	/*
		query service and log result
		https://developers.google.com/android/reference/com/google/android/gms/common/ConnectionResult
	 */
	private static final String _GPS = "Google Play Services";

	@SuppressWarnings({"OverlyComplexMethod", "OverlyLongMethod"})
	public int queryAvailability() {
		final int playServicesStatus = googleApiAvailability.isGooglePlayServicesAvailable(context);
		switch (playServicesStatus) {
			/* service errors */
			case ConnectionResult.SERVICE_MISSING:
				Timber.v("Service is missing, " + _GPS + " is unavailable");
				break;
			case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
				Timber.v("Service Update Required, " + _GPS + " is unavailable");
				break;
			case ConnectionResult.API_UNAVAILABLE:
				Timber.v(_GPS + " API is unavailable (updating wont fix)");
				break;
			case ConnectionResult.INTERNAL_ERROR:
				Timber.v("Remote Service error retrying should solve this but for now, " + _GPS + " is unavailable");
				break;
			case ConnectionResult.SERVICE_INVALID:
				Timber.v("installed Google Play services is not authentic, " + _GPS + " is unavailable");
				break;

			/* network */
			case ConnectionResult.NETWORK_ERROR:
				Timber.v("Network Error " + _GPS + " is unavailable");
				break;
			case ConnectionResult.TIMEOUT:
				Timber.v("Connection Timeout, " + _GPS + " is unavailable");
				break;

			/* auth */
			case ConnectionResult.SIGN_IN_REQUIRED:
				//service available but user not signed in
				Timber.v("User not signed in (Sign-In Required), " + _GPS + " is unavailable");
				break;
			case ConnectionResult.SIGN_IN_FAILED:
				Timber.v("Sign-In Failed, " + _GPS + " is unavailable");
				break;
			case ConnectionResult.RESTRICTED_PROFILE:
				// Profile is restricted by google so can not be used for play services
				Timber.v("Google Profile is restricted, " + _GPS + " is unavailable");
				break;
			case ConnectionResult.INVALID_ACCOUNT:
				Timber.v("Invalid account, " + _GPS + " is unavailable");
				break;
			case ConnectionResult.RESOLUTION_REQUIRED:
				Timber.v("Resolution required, " + _GPS + " is unavailable");
				// use `startResolutionForResult(Activity, int)`
				break;

			/* GOOD */
			case ConnectionResult.SUCCESS:
				Timber.v(_GPS + " available");
				break;
			default:
				Timber.v("Unknown return code: " + playServicesStatus + ". "+ _GPS + " is unavailable");
				break;
		}
		return playServicesStatus;
	}

	public boolean isAvailable() {
		return ConnectionResult.SUCCESS == queryAvailability();
	}
	public final boolean updateRequired() {
		return ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED == queryAvailability();
	}

	// get update GPS dialog from `GoogleApAvailability.getErrorDialog()`
	public final Dialog getUpdateRequiredErrorDialog(final Activity activity, final DialogInterface.OnCancelListener cancelListener) {
		return googleApiAvailability.getErrorDialog (activity, ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED, UPDATE_GOOGLE_PLAY_SERVICES, cancelListener);
	}

	// forward params to `GoogleApAvailability.getErrorDialog()`
	// https://developers.google.com/android/reference/com/google/android/gms/common/GoogleApiAvailability
	public final Dialog getErrorDialog(final Activity activity, final int errorCode, final int requestCode, final DialogInterface.OnCancelListener cancelListener) {
		return googleApiAvailability.getErrorDialog (activity, errorCode, requestCode, cancelListener);
	}

	public static boolean isUpdateRequired(final int status) { return status == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED; }
	public static boolean isNetworkIssue(final int status) { return status == ConnectionResult.NETWORK_ERROR || status == ConnectionResult.TIMEOUT; }
}
