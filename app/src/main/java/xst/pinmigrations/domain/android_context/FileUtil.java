package xst.pinmigrations.domain.android_context;

import android.support.annotation.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.google.common.base.Preconditions.checkNotNull;

public final class FileUtil {

	@SuppressWarnings("ThrowFromFinallyBlock") // deliberately checking for masked exception, we're ok
	public static @NonNull String readFile(@NonNull final InputStream inputStream) throws IOException {
		final InputStream is = checkNotNull(inputStream, "input-stream == null");

		boolean readErrorOccurred = false;
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new InputStreamReader(is));

			// do reading, usually loop until end of file reading
			final StringBuilder sb = new StringBuilder();
			String mLine;
			while ((mLine = reader.readLine()) != null) {
				sb.append(mLine);
			}

			return sb.toString();
		} catch (final IOException readException) {
			readErrorOccurred = true;
			throw readException;
		} finally { // always close the IoStream
			if (reader != null) {
				try {
					reader.close();
				} catch (final IOException closeException) {
					/* if there was already be a read exception, dont mask it with another */
					if (!readErrorOccurred) { // no read-error so this is the only exception
						throw closeException;
					}
				}
			}
		}
	}
}
