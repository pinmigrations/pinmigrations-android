package xst.pinmigrations.domain.android_context;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;

import java.io.IOException;
import java.io.InputStream;

import xst.pinmigrations.domain.services.BaseDependantService;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;

/** TODO absorb AndroidAssetsService and rename to AndroidService */
public final class AndroidService extends BaseDependantService {
	@NonNull final Resources resources;
	@NonNull final AssetManager assetManager;
	@NonNull final LayoutInflater layoutInflater;
	@NonNull final DisplayMetrics displayMetrics;
	final float deviceDensity;

	private AndroidService() { throw new UnsupportedOperationException("dont use this"); }
	public AndroidService(@NonNull final Context context) {
		final Context ctx = checkNotNull(context);
		this.resources = ctx.getResources();
		this.assetManager = ctx.getAssets();
		this.layoutInflater = LayoutInflater.from(ctx);
		this.displayMetrics = resources.getDisplayMetrics();
		this.deviceDensity = displayMetrics.density;
	}

	public @NonNull Resources getResources() { return resources; }

	public @NonNull DisplayMetrics getDisplayMetrics() { return displayMetrics; }

	public float getDeviceDensity() { return deviceDensity; }

	public @NonNull Configuration getConfiguration() { return resources.getConfiguration(); }

	public int getSmallestScreenWidthDp() { return resources.getConfiguration().smallestScreenWidthDp; }

	public @NonNull LayoutInflater getLayoutInflater() { return layoutInflater; }

	public @NonNull Bitmap getBitmapFromDrawable(@DrawableRes int resourceCode) {
		return BitmapFactory.decodeResource(this.resources, resourceCode);
	}

	//region Assets
	public @NonNull InputStream getAssetFile(@NonNull final String filename) throws IOException {
		//noinspection ResultOfMethodCallIgnored
		checkNotNull(filename, "filename is null");

		final InputStream is = this.assetManager.open(filename);
		return verifyNotNull(is);
	}

	public @NonNull String getAssetFileAsString(@NonNull final String path) throws IOException {
		return FileUtil.readFile( getAssetFile( checkNotNull(path) ) );
	}
	//endregion
}
