package xst.pinmigrations.domain.android_context.shared_preferences;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import xst.pinmigrations.domain.android_context.shared_preferences.Entry.BoolEntry;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.BoolKeyType;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.FloatEntry;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.FloatKeyType;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.IntEntry;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.IntKeyType;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.LongEntry;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.LongKeyType;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.StringEntry;
import xst.pinmigrations.domain.android_context.shared_preferences.Entry.StringKeyType;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_BOOL;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_FLOAT;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_INT;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_LONG;
import static xst.pinmigrations.domain.android_context.shared_preferences.Entry.DEFAULT_STRING;

public class PrefsUtil {
	public static final String ERRMSG_PREF_VAL_TYPE = "invalid shared prefs val type";

	public static @NonNull StringEntry stringEntry(@NonNull final String prefKey, @NonNull final String prefValue) {
		return new StringEntry(prefKey, prefValue);
	}
	public static @NonNull IntEntry intEntry(@NonNull final String prefKey, @NonNull final Integer prefValue) {
		return new IntEntry(prefKey, prefValue);
	}
	public static @NonNull BoolEntry boolEntry(@NonNull final String prefKey, @NonNull final Boolean prefValue) {
		return new BoolEntry(prefKey, prefValue);
	}
	public static @NonNull LongEntry longEntry(@NonNull final String prefKey, @NonNull final Long prefValue) {
		return new LongEntry(prefKey, prefValue);
	}
	public static @NonNull FloatEntry floatEntry(@NonNull final String prefKey, @NonNull final Float prefValue) {
		return new FloatEntry(prefKey, prefValue);
	}

	public static @NonNull StringKeyType stringKey(@NonNull final String prefKey) {
		return new StringKeyType(prefKey);
	}
	public static @NonNull IntKeyType intKey(@NonNull final String prefKey) {
		return new IntKeyType(prefKey);
	}
	public static @NonNull BoolKeyType boolKey(@NonNull final String prefKey) {
		return new BoolKeyType(prefKey);
	}
	public static @NonNull LongKeyType longKey(@NonNull final String prefKey) {
		return new LongKeyType(prefKey);
	}
	public static @NonNull FloatKeyType floatKey(@NonNull final String prefKey) {
		return new FloatKeyType(prefKey);
	}

	static @NonNull Class<?> validatePrefValType(@NonNull final Class<?> valType) {
		// only ones implemented so far
		if (Entry.PREFS_TYPES.contains(checkNotNull( valType ))) {
			return valType;
		} else {
			throw new IllegalArgumentException(ERRMSG_PREF_VAL_TYPE);
		}
	}

	static <T> T validatePrefValueAndKeyType(@NonNull final Entry.KeyType keyType, @NonNull final T objectArg) {
		checkArgument(keyType.type.isInstance(checkNotNull( objectArg )), "mismatched pref val & key-type");
		return objectArg;
	}


	static void putEditorEntry(@NonNull final SharedPreferences.Editor prefsEditor, @NonNull final Entry prefsEntry) {
		final SharedPreferences.Editor editor = checkNotNull(prefsEditor);
		final Entry entry = checkNotNull(prefsEntry);

		if (String.class.equals(entry.keyType.type)) {
			editor.putString(entry.keyType.key, (String) entry.value);
		} else if (Integer.class.equals(entry.keyType.type)) {
			editor.putInt(entry.keyType.key, (Integer) entry.value);
		} else if (Boolean.class.equals(entry.keyType.type)) {
			editor.putBoolean(entry.keyType.key, (Boolean) entry.value);
		} else if (Long.class.equals(entry.keyType.type)) {
			editor.putLong(entry.keyType.key, (Long) entry.value);
		} else if (Float.class.equals(entry.keyType.type)) {
			editor.putFloat(entry.keyType.key, (Float) entry.value);
		} else {
			throw new AssertionError("invalid Entry.type for pref");
		}
	}

	static Entry getPrefEntry(@NonNull SharedPreferences sharedPrefs,
						 @NonNull final Entry.KeyType keyType) {
		final SharedPreferences prefs = checkNotNull(sharedPrefs);
		final String key = checkNotNull(keyType).key;
		checkArgument(Entry.PREFS_TYPES.contains( checkNotNull(keyType.type) ), ERRMSG_PREF_VAL_TYPE);

		if (String.class.equals(keyType.type)) {
			return stringEntry(key, prefs.getString(key, DEFAULT_STRING));
		} else if (Integer.class.equals(keyType.type)) {
			return intEntry(key, prefs.getInt(key, DEFAULT_INT));
		} else if (Boolean.class.equals(keyType.type)) {
			return boolEntry(key, prefs.getBoolean(key, DEFAULT_BOOL));
		} else if (Long.class.equals(keyType.type)) {
			return longEntry(key, prefs.getLong(key, DEFAULT_LONG));
		} else if (Float.class.equals(keyType.type)) {
			return floatEntry(key, prefs.getFloat(key, DEFAULT_FLOAT));
		} else {
			throw new AssertionError("invalid Entry.type for pref");
		}
	}
}
