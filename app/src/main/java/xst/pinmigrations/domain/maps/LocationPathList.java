package xst.pinmigrations.domain.maps;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.paths.Path;
import xst.pinmigrations.domain.paths.PathList;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

public class LocationPathList implements Destroyable {
	@NonNull public final ArrayList<LocationPath> paths;

	//region CTOR
	public LocationPathList() {
		this.paths = new ArrayList<>();
	}
	public LocationPathList(final int size) {
		verify(size >= 0, "size must be positive or zero");
		this.paths = new ArrayList<>( size );
	}
	public LocationPathList(@NonNull final LocationPathList other) {
		this.paths = checkNotNull(other).paths;
	}
	public LocationPathList(@NonNull final ArrayList<LocationPath> paths) {
		this.paths = checkNotNull(paths);
	}
	public LocationPathList(@NonNull final PathList pathList) {
		this( checkNotNull(pathList).getPathsCount() );

		for (final Path path: pathList.paths) {
			paths.add( new LocationPath(path) );
		}
	}
	//endregion

	public int getLocationsCount() {
		int count = 0;
		for (final LocationPath p: this.paths) { count += p.getLocationsCount(); }
		return count;
	}
	public int getPathsCount() { return paths.size(); }

	public boolean isEmpty() { return paths.isEmpty(); }

	public void clear() { paths.clear(); }

	public void replacePathLocations(@NonNull final LocationPathList pathList) {
		replacePathLocations( checkNotNull(pathList).paths );
	}
	public void replacePathLocations(@NonNull final List<LocationPath> newPaths) {
		destroyPaths();
		paths.clear();
		paths.addAll(newPaths);
	}
	public void replacePaths(@NonNull final List<Path> pathList) {
		destroyPaths();
		paths.clear();
		for (final Path path: checkNotNull(pathList)) {
			paths.add( new LocationPath(path) );
		}
	}
	public void replacePaths(@NonNull final PathList pathList) {
		destroyPaths();
		paths.clear();
		replacePaths(pathList.paths);
	}

	@Override public void destroy() { destroyPaths(); }

	protected void destroyPaths() {
		if (paths.size() > 0) {
			for (final LocationPath p: paths) { p.destroy(); }
		}
	}
}
