package xst.pinmigrations.domain.maps;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.ui.Comparators;

import static com.google.common.base.Preconditions.checkNotNull;

public class LocationsUtil {
	public static void sortLocationsByGuid(@NonNull final List<Location> locations) {
		Collections.sort( checkNotNull(locations), LOCATION_GUID_COMPARATOR);
	}
	public static void sortLocationsByOrd(@NonNull final List<Location> locations) {
		Collections.sort( checkNotNull(locations), LOCATION_ORD_COMPARATOR);
	}

	public static ArrayList<Location> positionsToLocations(@NonNull final ArrayList<Position> positions) {
		final ArrayList<Location> locations = new ArrayList<>( checkNotNull(positions).size() );
		for (final Position p: positions) {
			locations.add(new Location(p));
		}
		return locations;
	}

	public static Location newUnsavedLocation(@NonNull final Position position, final long unsavedId) {
		final Location unsavedLocation = new Location( checkNotNull(position) );
		unsavedLocation.unsavedId = unsavedId;

		return unsavedLocation;
	}

	public static @NonNull LocationPath emptyLocationPath() { return new LocationPath(); }
	public static @NonNull LocationPathList emptyLocationPathList() { return new LocationPathList(); }

	public static final LocationOrdComparator LOCATION_ORD_COMPARATOR = new LocationOrdComparator();
	public static final LocationGuidComparator LOCATION_GUID_COMPARATOR = new LocationGuidComparator();
	static class LocationOrdComparator implements Comparator<Location> {
		@Override public int compare(final Location loc1, final Location loc2) {
			return Comparators.longCompareAscending(loc1.position.ord, loc2.position.ord);
		}
	}
	final static class LocationGuidComparator implements Comparator<Location> {
		@Override public int compare(final Location loc1, final Location loc2) {
			return Comparators.stringCompareAscending(loc1.position.guid, loc2.position.guid);
		}
	}
}
