package xst.pinmigrations.domain.maps;

import android.graphics.Color;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import static com.google.common.base.Preconditions.checkNotNull;

public class MapsUtil {

	/* Map Options & Defaults */
	public static final int MAP_TYPE = GoogleMap.MAP_TYPE_NORMAL;
	public static final LatLng LAT_LNG_SANTA_ANA = new LatLng(33.73812, -117.9225193);
	public static final float ZOOM_DEFAULT_SW360 = 9.0f;
	public static final float ZOOM_DEFAULT_SW600 = 11.25f;
	public static final CameraPosition CAMERA_POSITION_DEFAULT_SW360 = CameraPosition.fromLatLngZoom(LAT_LNG_SANTA_ANA, ZOOM_DEFAULT_SW360);
	public static final CameraPosition CAMERA_POSITION_DEFAULT_SW600 = CameraPosition.fromLatLngZoom(LAT_LNG_SANTA_ANA, ZOOM_DEFAULT_SW600);
	public static final float ZOOM_MAX = 12.0f;
	public static final float ZOOM_MIN = 3.0f;
	public static final boolean COMPASS_DEFAULT = false;
	public static final boolean ZOOM_CONTROLS_DEFAULT = false;
	public static final boolean TILT_DEFAULT = false;
	public static final boolean ROTATE_GESTURES_DEFAULT = true;
	public static final boolean SCROLL_GESTURES_DEFAULT = true;
	public static final boolean ZOOM_GESTURES_DEFAULT = true;
	public static final boolean MAP_TOOLBAR_DEFAULT = false;

	/** default marker height */
	public static final int DEFAULT_MARKER_HEIGHT = 39;
	/** offset between the default InfoWindow bottom edge and it's content bottom edge */
	public static final int DEFAULT_INFOWINDOW_OFFSET = 20;

	public static final Object CURRENT_MARKER_TAG = new Object();

	public static @NonNull PolylineOptions getDefaultPolylineOptions() {
		return (new PolylineOptions())
				.width(5)
				.color(Color.RED);
	}

	public static @NonNull PolylineOptions setupPolylineOptions(@NonNull final ArrayList<LatLng> points) {
		return getDefaultPolylineOptions().addAll( checkNotNull(points) );
	}


	public static @NonNull MarkerOptions getDefaultMarkerOptions(@NonNull final Location location) {
		final MarkerOptions options = new MarkerOptions();

		options.position( checkNotNull(location).latLng );
		// note: text is set in InfoWindow Adapters
		// note: default info window doesnt show without snippet/title non-empty strings
		options.title(" ");
		options.snippet(" ");
		options.visible(true);

		return options;
	}
	public static @NonNull MarkerOptions getEditableMarkerOptions(@NonNull final Location location) {
		return getDefaultMarkerOptions(location).draggable(true);
	}
	public static @NonNull MarkerOptions getCurrentMarkerOptions() {
		return new MarkerOptions()
				.position(LAT_LNG_SANTA_ANA) // need non-null default, even if hidden
				.draggable(true)
				.visible(true);
	}
}
