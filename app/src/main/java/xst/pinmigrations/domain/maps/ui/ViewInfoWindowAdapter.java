package xst.pinmigrations.domain.maps.ui;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

import xst.pinmigrations.R;
import xst.pinmigrations.domain.maps.Location;

import static com.google.common.base.Preconditions.checkNotNull;

public class ViewInfoWindowAdapter implements InfoWindowAdapter {
	@NonNull protected final View infowindowView;
	@NonNull protected final TextView textView;

	public ViewInfoWindowAdapter(@NonNull final LayoutInflater layoutInflater) {
		infowindowView = checkNotNull(layoutInflater).inflate(R.layout.map_view_infowindow, null);
		textView = (TextView) infowindowView.findViewById(R.id.view_infowindow_text);
	}

	@Override public @Nullable View getInfoWindow(final Marker marker) {
		return null;
	}

	@Override public @NonNull View getInfoContents(final Marker marker) {
		final Location location = (Location) checkNotNull( marker.getTag() );
		textView.setText( location.displayDescription );
		return infowindowView;
	}
}
