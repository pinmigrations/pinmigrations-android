package xst.pinmigrations.domain.maps;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import xst.pinmigrations.R;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.services.BaseDependantService;
import xst.pinmigrations.ui.UiUtil;

import static com.google.common.base.Preconditions.checkNotNull;

public class MapsServiceGenerator extends BaseDependantService {
	@NonNull final AndroidService androidService;

	BitmapDescriptor currentPin;
	BitmapDescriptor locationPin;
	BitmapDescriptor originPin;

	private MapsServiceGenerator() { throw new UnsupportedOperationException("dont use this"); }
	public MapsServiceGenerator(@NonNull final AndroidService androidService) {
		this.androidService = checkNotNull(androidService);
		init();
	}

	/** load and scale icons */
	void init() {
		final float scale = getIconScale();

		currentPin = BitmapDescriptorFactory.fromBitmap(
				UiUtil.scaleBitmap(
						androidService.getBitmapFromDrawable(R.drawable.marker_green), scale));
		originPin = BitmapDescriptorFactory.fromBitmap(
				UiUtil.scaleBitmap(
					androidService.getBitmapFromDrawable(R.drawable.marker_red), scale));
		locationPin = BitmapDescriptorFactory.fromBitmap(
				UiUtil.scaleBitmap(
				androidService.getBitmapFromDrawable(R.drawable.marker_white), scale));
	}

	/** percentage stretch/shrink of marker icons */
	float getIconScale() {
		final int smallestScreenWidth = androidService.getSmallestScreenWidthDp();
		final float density = androidService.getDeviceDensity();

		if (smallestScreenWidth < 600) {
			if (density > 1.5f) {
				return 0.8f;
			} else {
				return 0.9f;
			}
		} else {
			if (density > 1.5f) {
				return 0.9f;
			} else if (density > 1.0f) {
				return 1.0f;
			} else { // density <= 1.0f
				return 1.1f;
			}
		}
	}

	public @NonNull MapsService newMapService(@NonNull final GoogleMap map) {
		return new MapsService(map, androidService.getSmallestScreenWidthDp(), currentPin, locationPin, originPin);
	}
}
