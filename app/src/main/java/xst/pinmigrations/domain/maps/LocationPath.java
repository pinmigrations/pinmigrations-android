package xst.pinmigrations.domain.maps;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import java.util.ArrayList;

import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.paths.Path;
import xst.pinmigrations.domain.positions.Position;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

/*
https://developers.google.com/android/reference/com/google/android/gms/maps/model/Polyline
 */

public class LocationPath implements Destroyable {
	// data
	@NonNull public final ArrayList<Location> locations;
	@NonNull public String guid = "";

	// UI
	public Polyline line;

	//region CTOR
	public LocationPath() {
		this.locations = new ArrayList<>();
	}
	public LocationPath(final int size) {
		verify(size >= 0, "size must be positive or zero");
		this.locations = new ArrayList<>(size);
	}
	public LocationPath(@NonNull final LocationPath other) {
		this( checkNotNull(other).locations.size() );
		replaceLocations(other.locations);
	}

	public LocationPath(@NonNull final ArrayList<Location> locations) {
		this( checkNotNull(locations).size() );
		replaceLocations(checkNotNull( locations ));
	}
	public LocationPath(@NonNull final Path path) {
		this( checkNotNull(path).getPositionsCount() );

		for (final Position position: path.positions) {
			locations.add( new Location(position) );
		}

		if (locations.size() > 0) {
			this.guid = locations.get(0).position.guid;
		}
	}
	//endregion

	public void setLine(@NonNull final Polyline line) {
		this.line = checkNotNull(line);
		this.line.setTag(this);
	}

	public void replaceLocations(@NonNull final ArrayList<Location> newLocations) {
		verify( checkNotNull(newLocations).size() > 0, "new-locations size must be greater than zero");

		if (newLocations.size() > 1) {
			LocationsUtil.sortLocationsByOrd(newLocations);
		}

		if (newLocations.size() > 0) {
			this.guid = newLocations.get(0).position.guid;
		}

		locations.clear();
		locations.addAll(newLocations);
	}

	public void replaceLocations(@NonNull final Path newPath) {
		replaceLocations( LocationsUtil.positionsToLocations(newPath.positions) );
	}

	protected @NonNull final ArrayList<LatLng> getPoints() {
		final ArrayList<LatLng> points = new ArrayList<>(this.locations.size());
		for (final Location location: locations) {
			points.add(location.latLng);
		}
		return points;
	}

	public int getLocationsCount() { return locations.size(); }
	public boolean isEmpty() { return locations.isEmpty(); }

	@Override public void destroy() {
		clearGuid();
		clearLocations();
		clearLine();
	}

	// for re-rendering
	public void clearUi() {
		for (final Location loc: locations) { loc.clearUi(); }
		clearLine();
	}

	protected void clearLocations() {
		if ( !locations.isEmpty() ) {
			for (final Location loc: locations) { loc.destroy(); }
			locations.clear();
		}
	}
	protected void clearGuid() { guid = ""; }
	protected void clearLine() {
		if (null != line) {
			line.setTag(null);
			line.remove();
			line = null;
		}
	}
}
