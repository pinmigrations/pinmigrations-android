package xst.pinmigrations.domain.maps;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import timber.log.Timber;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.positions.Position;

import static com.google.common.base.Preconditions.checkNotNull;

/*
	https://developers.google.com/android/reference/com/google/android/gms/maps/model/LatLng
 */

/** UI-Equivalent of Position, with Maps UI elements attached (Marker,LatLng) */
public class Location implements Destroyable {
	public static final long UNSAVED_DEFAULT_ID = 0;
	public static final long DELETED_ID = Long.MIN_VALUE;
	public static final String DEFAULT_DISPLAY_DESCRIPTION = " ";

	@Nullable public Marker marker;

	public long unsavedId = UNSAVED_DEFAULT_ID;
	@NonNull public Position position;
	@NonNull public LatLng latLng;
	@NonNull public String displayDescription = DEFAULT_DISPLAY_DESCRIPTION;

	//region CTOR
	private Location() { throw new UnsupportedOperationException("dont use this"); }
	public Location(@NonNull final Position position) {
		this.position = checkNotNull(position);
		this.latLng = new LatLng(position.lat, position.lng);
		this.position = position;
		setDisplayDescription(position.description);
	}

	public Location(@NonNull final Location other) {
		this.unsavedId = other.unsavedId;
		this.position = new Position(other.position);
		setDisplayDescription(position.description);
		this.latLng = new LatLng(other.latLng.latitude, other.latLng.longitude);
	}

	/** NOTE: use monotonically decreasing, unique negative numbers for id to indicate its unsaved status */
	public Location(@NonNull final LatLng latLng, final long id) {
		this.latLng = checkNotNull(latLng);
		this.position = new Position(id);
		setDisplayDescription(position.description);
		this.unsavedId = id;
	}
	//endregion


	// note: marker needs non-empty string in order to render, uses single-space for empty-string
	void setDisplayDescription(@NonNull final String description) {
		this.displayDescription = checkNotNull(description).isEmpty() ? DEFAULT_DISPLAY_DESCRIPTION : description;
	}
	/** update description in Position/display */
	// note: allows setting empty-string desc, while still displaying (empty) marker
	public void updateDescription(@NonNull final String description) {
		position.description = checkNotNull(description);
		setDisplayDescription(description);
	}

	public void setMarker(@NonNull final Marker marker) {
		this.marker = checkNotNull(marker);
		this.marker.setTag(this);
	}

	/** null-safe, can invoke if no window or marker present */
	public void showInfoWindow() {
		if (null != marker) {
			marker.showInfoWindow();
		}
	}
	/** null-safe, can invoke if no window or marker present */
	public void hideInfoWindow() {
		if (null != marker) {
			marker.hideInfoWindow();
		}
	}

	@Override public void destroy() {
		clearUi();
	}

	public void markForDeletion() {
		this.unsavedId = DELETED_ID;
		this.position.id = DELETED_ID;

		hideInfoWindow();

		if (null != marker) {
			marker.remove();
		}
	}

	public boolean isMarkedForDeletion() {
		return this.unsavedId == DELETED_ID;
	}

	// for re-rendering
	public void clearUi() {
		if (null != marker) {
			hideInfoWindow();
			marker.setTag(null);
			marker.remove();
			marker = null;
		}
	}

	@Override public String toString() {
		final long id = (UNSAVED_DEFAULT_ID == unsavedId) ? position.id : unsavedId;
		return "Location(id=" + id + ", ord=" + position.ord + ", desc=" + position.description + ", guid" + position.guid + ")";
	}

	/* this and hash-code only use ID field */
	@Override public boolean equals(@Nullable final Object other) {
		if (this == other) { return true; }
		if ( !(other instanceof Location) ) { return false; }  // note: this handles null-case
		final Location otherLocation = (Location) other;

		if (position.id != Position.DEFAULT_ID) {
			return position.id == otherLocation.position.id;
		} else {
			return unsavedId == otherLocation.unsavedId;
		}
	}

	/* ID field */
	@Override public int hashCode() {
		if (position.id != Position.DEFAULT_ID) {
			return (int) position.id;
		} else {
			return (int) unsavedId;
		}
	}
}
