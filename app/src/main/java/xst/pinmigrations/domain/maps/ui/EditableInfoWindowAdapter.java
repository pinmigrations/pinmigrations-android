package xst.pinmigrations.domain.maps.ui;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

import xst.pinmigrations.R;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.maps.Location;

import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.maps.Location.DEFAULT_DISPLAY_DESCRIPTION;
import static xst.pinmigrations.domain.maps.MapsUtil.CURRENT_MARKER_TAG;

public class EditableInfoWindowAdapter implements InfoWindowAdapter, Destroyable {
	@NonNull protected final View editableInfowindowView;
	@NonNull protected final View editingInfowindowView;
	@NonNull protected final TextView descriptionText;

	protected boolean isEditing = false;
	protected boolean isReOpening = false;

	public EditableInfoWindowAdapter(@NonNull final LayoutInflater layoutInflater) {
		editableInfowindowView = checkNotNull(layoutInflater).inflate(R.layout.map_editable_infowindow, null);
		descriptionText = (TextView) editableInfowindowView.findViewById(R.id.editable_infowindow_description);
		editingInfowindowView = checkNotNull(layoutInflater).inflate(R.layout.map_editing_infowindow, null);
	}

	@Override public @Nullable View getInfoWindow(final Marker marker) {
		return null;
	}

	@Override public @Nullable View getInfoContents(final Marker marker) {
		final Object markerTag = checkNotNull( marker.getTag() );
		//noinspection ObjectEquality (object's purpose is equality-checking)
		if (CURRENT_MARKER_TAG == markerTag) { return null; } // dont care about dragging current marker

		final Location location = (Location) markerTag;
		if ( isEditing() ) {
			return editingInfowindowView;
		} else {
			return getEditableInfowindowView(location);
		}
	}

	public @NonNull View getEditableInfowindowView(@NonNull final Location location) {
		final String description = location.displayDescription;
		if (description.isEmpty() || DEFAULT_DISPLAY_DESCRIPTION.equals(description)) {
			descriptionText.setVisibility(View.GONE);
		} else {
			descriptionText.setVisibility(View.VISIBLE);
			descriptionText.setText(description);
		}

		// show / hide to force redraw ?
		return editableInfowindowView;
	}

	public void setEditing() { isEditing = true; }
	public void setEditable() { isEditing = false; }
	public boolean isEditing() { return isEditing; }
	public void setReOpening(final boolean isReOpening) { this.isReOpening = isReOpening; }
	public boolean isReOpening() { return isReOpening; }

	@Override public void destroy() {
		// all fields final, so far
	}
}
