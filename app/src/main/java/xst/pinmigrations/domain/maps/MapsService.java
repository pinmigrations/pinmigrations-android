package xst.pinmigrations.domain.maps;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.services.BaseService;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

/**
 * Docs
 * + https://developers.google.com/android/reference/com/google/android/gms/maps/GoogleMapOptions.html
 * + https://developers.google.com/android/reference/com/google/android/gms/maps/MapView
 */
public final class MapsService extends BaseService implements Destroyable {

	@NonNull final GoogleMap map;
	@NonNull final BitmapDescriptor currentPin;
	@NonNull final BitmapDescriptor locationPin;
	@NonNull final BitmapDescriptor originPin;
	final int smallestScreenWidth;

	private MapsService() { throw new UnsupportedOperationException("dont use this"); }
	public MapsService(@NonNull final GoogleMap map,
					   final int smallestScreenWidth,
					   @NonNull final BitmapDescriptor currentPin,
					   @NonNull final BitmapDescriptor locationPin,
					   @NonNull final BitmapDescriptor originPin) {
		this.map = map;
		this.smallestScreenWidth = smallestScreenWidth;
		this.currentPin = currentPin;
		this.locationPin = locationPin;
		this.originPin = originPin;
	}

	public void setupMap() {
		map.setMapType(MapsUtil.MAP_TYPE);

		map.setMaxZoomPreference(MapsUtil.ZOOM_MAX);
		map.setMinZoomPreference(MapsUtil.ZOOM_MIN);

		map.getUiSettings().setCompassEnabled(MapsUtil.COMPASS_DEFAULT);
		map.getUiSettings().setZoomControlsEnabled(MapsUtil.ZOOM_CONTROLS_DEFAULT);
		map.getUiSettings().setZoomGesturesEnabled(MapsUtil.ZOOM_GESTURES_DEFAULT);
		map.getUiSettings().setTiltGesturesEnabled(MapsUtil.TILT_DEFAULT);
		map.getUiSettings().setRotateGesturesEnabled(MapsUtil.ROTATE_GESTURES_DEFAULT);
		map.getUiSettings().setScrollGesturesEnabled(MapsUtil.SCROLL_GESTURES_DEFAULT);

		map.getUiSettings().setMapToolbarEnabled(MapsUtil.MAP_TOOLBAR_DEFAULT);

		if (smallestScreenWidth < 600) {
			map.moveCamera(CameraUpdateFactory.newCameraPosition(MapsUtil.CAMERA_POSITION_DEFAULT_SW360));
		} else {
			map.moveCamera(CameraUpdateFactory.newCameraPosition(MapsUtil.CAMERA_POSITION_DEFAULT_SW600));
		}
	}

	protected @NonNull MarkerOptions setupLocationMarkerOptions(@NonNull final Location location) {
		return MapsUtil.getDefaultMarkerOptions( checkNotNull(location) ).icon(locationPin);
	}
	protected @NonNull MarkerOptions setupEditableLocationMarkerOptions(@NonNull final Location location) {
		return MapsUtil.getEditableMarkerOptions( checkNotNull(location) ).icon(locationPin);
	}
	protected @NonNull MarkerOptions setupOriginMarkerOptions(@NonNull final Location location) {
		return MapsUtil.getDefaultMarkerOptions( checkNotNull(location) ).icon(originPin);
	}
	protected @NonNull MarkerOptions setupEditableOriginMarkerOptions(@NonNull final Location location) {
		return MapsUtil.getEditableMarkerOptions( checkNotNull(location) ).icon(originPin);
	}

	public @NonNull MarkerOptions getCurrentMarkerOptions() {
		return MapsUtil.getCurrentMarkerOptions().icon(currentPin);
	}

	public void renderViewPaths(@NonNull final LocationPathList pathList) {
		final int locationsCount = checkNotNull(pathList).getLocationsCount();
		if (locationsCount > 0) {
			for (final LocationPath path: pathList.paths) {
				for (int i=0, len = path.getLocationsCount(); i<len; i++) {
					final Location location = path.locations.get(i);
					if (0 == i) {
						location.setMarker( map.addMarker( setupOriginMarkerOptions( location )));
					} else {
						location.setMarker( map.addMarker( setupLocationMarkerOptions( location )));
					}
				}

				path.setLine( map.addPolyline(MapsUtil.setupPolylineOptions( path.getPoints() )));
			}
		}
	}

	public void reRenderEditablePath(@NonNull final EditableLocationPath locationPath) {
		final int locationsCount = checkNotNull(locationPath).getLocationsCount();
		verify(locationsCount > 0, "no editable locations to re-render");

		locationPath.clearUi();

		final ArrayList<Location> locations = locationPath.locations;
		for (int i=0; i<locationsCount; i++) {
			final Location location = locations.get(i);
			if (0 == i) {
				location.setMarker( map.addMarker( setupEditableOriginMarkerOptions( location )));
			} else {
				location.setMarker( map.addMarker( setupEditableLocationMarkerOptions( location )));
			}
		}

		locationPath.setLine( map.addPolyline(MapsUtil.setupPolylineOptions( locationPath.getPoints() )));
	}

	@Override public final void destroy() { map.clear(); }
}
