package xst.pinmigrations.domain.maps;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import timber.log.Timber;
import xst.pinmigrations.domain.paths.Path;
import xst.pinmigrations.domain.positions.Position;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

/*
https://developers.google.com/android/reference/com/google/android/gms/maps/model/Polyline
 */

public class EditableLocationPath extends LocationPath {
	public int unsavedLocationIdCounter = -1; // monotonically decreasing negative value

	// UI
	public Polyline line;

	//region CTOR
	public EditableLocationPath(@NonNull final String guid) {
		super();
		this.guid = checkNotNull(guid);
	}
	public EditableLocationPath(final int size) { super(size); }
	public EditableLocationPath(@NonNull final LocationPath other) {
		this( other.locations.size() );
		replaceLocations(locations);
	}
	public EditableLocationPath(@NonNull final Path path) {
		this( checkNotNull(path).getPositionsCount() );

		for (final Position position: path.positions) {
			locations.add( new Location(position) );
		}

		if (locations.size() > 0) {
			this.guid = locations.get(0).position.guid;
		}
	}
	//endregion

	/* NOTE: redraw line after adding */
	public @NonNull Location addNewLocation(@NonNull final LatLng latLng) {
		final int ord = locations.size();
		final Position position = new Position(Position.DEFAULT_ID, checkNotNull(latLng).latitude, latLng.longitude, "", this.guid, ord);
		final Location unsavedLocation = LocationsUtil.newUnsavedLocation(position, unsavedLocationIdCounter);
		unsavedLocationIdCounter -= 1;

		locations.add( unsavedLocation );
		return unsavedLocation;
	}

	// TODO: combine into a single `remove(Location)`

	/* NOTE: redraw line after removing */
	public void removeByIndex(final int index) {
		verify(locations.size() > index, "index out-of-range for locations");
		locations.remove(index);
	}

	/* NOTE: redraw line after removing */
	public void removeById(final long id) {
		final Location matchCopy = new Location(new Position(id));

		final int index = locations.indexOf(matchCopy);
		if (index >= 0) {
			verify(locations.size() > index, "index out-of-range for locations");

			locations.remove(index);
		} else {
			Timber.d("invalid id: " + id); // TODO: remove
			throw new AssertionError("invalid id for location");
		}
	}

	/* NOTE: redraw line after adding */
	public void updateLocationByIndex(final int index, @NonNull final Location location) {
		verify(index >= 0 && index < locations.size(), "index out-of-range for locations");
		locations.set(index, checkNotNull(location));
	}
	public void updateLocationById(final long id, @NonNull final Location location) {
		final Location matchCopy = new Location(new Position(id));

		final int index = locations.indexOf(matchCopy);
		if (index >= 0) {
			locations.set(index, checkNotNull(location));
		} else {
			Timber.d("invalid id: " + id); // TODO: remove
			throw new AssertionError("invalid id for location");
		}
	}
}
