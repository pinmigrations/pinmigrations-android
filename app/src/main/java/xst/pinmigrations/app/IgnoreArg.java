package xst.pinmigrations.app;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** indicates that a particular argument is ignored in the implementation
 * useful when interfacing with libraries that allow subclassing/callbacks/ etc..
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.PARAMETER})
public @interface IgnoreArg {
	/**
	 * optional explanation
	 */
	String value() default "";
}
