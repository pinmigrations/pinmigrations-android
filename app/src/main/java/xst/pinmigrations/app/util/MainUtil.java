package xst.pinmigrations.app.util;

/**
 * Preconditions (asserts) / Verification
 */

import android.support.annotation.FloatRange;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public final class MainUtil {
	public static StackTraceElement getStackTraceElement(@IntRange(from=0, to=Integer.MAX_VALUE) final int depth) {
		checkArgument(depth >= 0, "delay must be positive");

		// DO NOT switch this to Thread.getCurrentThread().getStackTrace(). The test will pass
		// because Robolectric runs them on the JVM but on Android the elements are different.
		final StackTraceElement[] stackTrace = new Throwable().getStackTrace();

		if (stackTrace.length <= depth) {
			throw new IllegalStateException(
					"Synthetic stacktrace didn't have enough elements: are you using proguard?");
		}
		return stackTrace[depth];
	}

	public static final double ONE_MILLION_DOUBLE = 1000000.0;
	public static final double ONE_THOUSAND_DOUBLE = 1000.0;
	public static final double ONE_THOUSAND_INT = 1000;

	/** return value guaranteed to be >= 0 */
	public static int getMs(@FloatRange(from=0.0, to=Double.MAX_VALUE) final double delay,
							@NonNull final TimeUnit timeUnit) {
		checkArgument(delay >= 0, "delay must be positive");
		switch( checkNotNull(timeUnit) ) {
			case SECONDS: return (int) (delay * ONE_THOUSAND_INT);
			case MILLISECONDS: return (int) delay;
			default: throw new IllegalArgumentException("only accepts seconds/milliseconds");
		}
	}

	/** NOTE: nano-time is not accurate ACROSS threads */
	public static double nanoDiffToMs(final long start, final long end) {
		return (end - start) / ONE_MILLION_DOUBLE;
	}

	/** NOTE: nano-time is not accurate ACROSS threads */
	public static String nanoDiffToMsString(final long start, final long end) {
		checkArgument(start < end, "start-time must be less than end-time");
		return nanoDiffToMs(start, end) + "ms";
	}

	public static String msToSecondsString(final long ms) {
		checkArgument(ms >= 0, "ms must be positive");
		return ( ms / ONE_THOUSAND_DOUBLE ) + "s";
	}
}
