package xst.pinmigrations.app.logging.hashlog;

/*
	Logging methods for instances (but not instance members)

	Known Object Hierarchy Roots:
	+ BaseActivity
	+ BaseFragment
	+ HashLogger (root for BaseService, BaseRetainedDataHashLogging, BaseController, etc..)
	+ HashLogInstanceWrapper (?)

	HashLogger is the reference implementation
*/
public interface HashLogInstance extends HashLog {
	/**
	 * HashLog-Text: returns string specifying class, object hash then text
	 *  eg: `MyClass@1234 did something...`
	 *  should delegate to `LogUtil.hashLogText`
	 */
	String _HLT(String text);

	/**
	 * HashLog-Method: returns string specifying class, method and object hash.
	 *  eg: `MyClass::someMethod @1234`
	 *  should delegate to `LogUtil.hashLogMethod`
	 */
	String _HLM(String methodName);

	/**
	 * End HashLog-Method: returns string specifying end of method
	 * eg: `END MyClass::someMethod @1234`
	 */
	String _END_HLM(String methodName);


	// HashLog-Object: returns hash-log for this object, then some text and finally the hash-log of some other object(s)
	// eg: `MyClass@1234 created OtherClass@5678`
	String _HLO(String text, Object... others);

	// HashLog-Formatted: returns hash-log for this object then a formatted-string from objects
	// NOTE: can only handle `%s` replacements
	// usage: `_HLF("a=%s, b=%s", obj1, obj2);`
	// output: `MyClass@1234 a=OtherClass@5678 b=OtherClass@9012`
	String _HLF(String format, Object... objects);


	// HashLog-Method Formatted: returns hash-log for this object and method then a formatted-string from objects
	// NOTE: can only handle `%s` replacements
	// usage: `_HLF("onCreate", "a=%s, b=%s", obj1, obj2);`
	// output: `MyActivity::onCreate a=OtherClass@5678 b=OtherClass@9012`
	String _HLMF(String methodName, String format, Object... objects);
}
