package xst.pinmigrations.app.logging.hashlog;

import xst.pinmigrations.app.logging.LogUtil;

/**
 *  base-class for new implementors of `MemberLog`
 *  methods implemented with simplest defaults
 *
 *  Most Important over-rides in order:
 *  + getLong
 *  + getShort
 *  + toString
 *  + getRecursiveShort
 *  + getRecursiveLong
 *  + all-others with indent-spaces param
 */
public abstract class MemberLogger extends HashLogger implements MemberLog {
	@Override
	public String getShort() { return classHashString(); }

	@Override
	public String getShort(final int indentSpaces) {
		return LogUtil.indentEachLine(getShort(), indentSpaces);
	}

	@Override
	public String getMin() { return hashString(); }

	@Override
	public String getMin(final int indentSpaces) {
		return LogUtil.indentEachLine(getMin(), indentSpaces);
	}

	@Override
	public String getLong() { return toString(); }

	@Override
	public String getLong(final int indentSpaces) {
		return LogUtil.indentEachLine(toString(), indentSpaces);
	}

	@Override
	public String getRecursiveShort() { return getShort(); }

	@Override
	public String getRecursiveShort(final int indentSpaces) {
		return LogUtil.indentEachLine(getRecursiveShort(), indentSpaces);
	}

	@Override
	public String getRecursiveLong() { return getLong(); }

	@Override
	public String getRecursiveLong(final int indentSpaces) {
		return LogUtil.indentEachLine(getRecursiveLong(), indentSpaces);
	}
}
