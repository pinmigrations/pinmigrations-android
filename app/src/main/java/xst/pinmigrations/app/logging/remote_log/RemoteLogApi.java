package xst.pinmigrations.app.logging.remote_log;

import java.util.List;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

public interface RemoteLogApi {
	@Headers("Cache-Control: no-store, no-cache")
	@GET("/ping")
	Observable<Void> ping();

	// maybe use `console_logs` as in web for consistency
	@FormUrlEncoded
	@POST("log")
	Observable<Void> log(@Field("lines") List<String> lines);
}
