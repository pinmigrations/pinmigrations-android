package xst.pinmigrations.app.logging.hashlog;

import xst.pinmigrations.app.logging.LogUtil;

public abstract class HashLogger implements HashLogInstance {
	@Override
	public String hashString() { return LogUtil.hashString(this); }

	@Override
	public String classHashString() { return LogUtil.classHashString(this); }

	@Override
	public String _HLT(final String text) { return LogUtil.hashLogText(this, text); }

	@Override
	public String _HLM(final String methodName) { return LogUtil.hashLogMethod(this, methodName); }
	@Override
	public String _END_HLM(final String methodName) { return LogUtil.endHashLogMethod(this, methodName); }

	@Override
	public String _HLO(final String text, final Object... others) {
		return LogUtil.hashLogObject(this, text, others);
	}

	@Override
	public String _HLF(final String format, final Object... objects) {
		return LogUtil.hashLogFormatted(this, format, objects);
	}

	@Override
	public String _HLMF(final String methodName, final String format, final Object... objects) {
		return LogUtil.hashLogMethodFormatted(this, methodName, format, objects);
	}
}
