package xst.pinmigrations.app.logging.hashlog;

import xst.pinmigrations.app.logging.LogUtil;

/**
 * same as HashLogger but for singleton objects so omits the object-ref hash
 * uses anonymous versions of logging methods, and just class-name instead hash-string
 */
public abstract class SingletonHashLogger extends HashLogger {
	@Override
	public String hashString() { return LogUtil.className(this); }

	@Override
	public String classHashString() { return LogUtil.className(this); }

	@Override
	public String _HLT(final String text) { return LogUtil.hashLogTextAnon(this, text); }

	@Override
	public String _HLM(final String methodName) { return LogUtil.hashLogMethodAnon(this, methodName); }
	@Override
	public String _END_HLM(final String methodName) { return LogUtil.endHashLogMethodAnon(this, methodName); }

	@Override
	public String _HLO(final String text, final Object... others) {
		return LogUtil.hashLogObjectAnon(this, text, others);
	}

	@Override
	public String _HLF(final String format, final Object... objects) {
		return LogUtil.hashLogFormattedAnon(this, format, objects);
	}

	@Override
	public String _HLMF(final String methodName, final String format, final Object... objects) {
		return LogUtil.hashLogMethodFormattedAnon(this, methodName, format, objects);
	}
}
