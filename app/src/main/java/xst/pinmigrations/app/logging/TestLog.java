package xst.pinmigrations.app.logging;

import android.support.annotation.NonNull;

// allows moving TestLogger (and its deps) out of main project
public interface TestLog {
	void println(final int priority, @NonNull String msg);
}
