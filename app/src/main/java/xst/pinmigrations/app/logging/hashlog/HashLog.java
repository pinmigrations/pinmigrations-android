package xst.pinmigrations.app.logging.hashlog;

/*
	Known Object Hierarchy Roots:
	+ BaseActivity
	+ BaseFragment
	+ HashLogger (root for BaseService, BaseRetainedDataHashLogging, BaseController, etc..)
	+ HashLogInstanceWrapper

	HashLogger is the reference implementation
 */
public interface HashLog {
	// NOTE: overriding only affects how the object is logged from other classes not from its own logging methods
	// eg: as a member of another class
	String classHashString();
	// NOTE: overriding only affects how the object is logged from other classes not from its own logging methods
	// eg: as a member of another class
	String hashString();
}
