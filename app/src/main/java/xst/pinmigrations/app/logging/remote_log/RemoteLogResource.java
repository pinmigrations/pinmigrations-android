package xst.pinmigrations.app.logging.remote_log;

import android.support.annotation.NonNull;

import rx.Observable;
import rx.Subscriber;
import xst.pinmigrations.domain.api.ApiGenerator;
import static com.google.common.base.Preconditions.checkNotNull;

public class RemoteLogResource {
	public static final int MS_PER_HOUR = 3600000;
	public static final int MS_PER_20_MIN = 60000;
	public static final int MS_PER_30_MIN = 1800000;

	/** initialized to now minus one hour, so first ping request will succeed */
	protected long lastPing = currentTimeMillis() - MS_PER_HOUR;

	protected boolean loggingDisabled = false;

	@NonNull final RemoteLogApi remoteLogApi;
	public RemoteLogResource(@NonNull final ApiGenerator apiGenerator) {
		this.remoteLogApi = checkNotNull(apiGenerator).createService(RemoteLogApi.class);
	}

	//region Ping

	/** true if 20m or more have passed since last ping */
	boolean pingDebounceElapsed() {
		return currentTimeMillis() > (lastPing + MS_PER_20_MIN);
	}
	/** NOTE: the return value will be null */
	public @NonNull Observable<Void> ping() {
		if (pingDebounceElapsed()) {
			lastPing = currentTimeMillis();
			final Observable<Void> pingObs = remoteLogApi.ping().cache(); // cache so it doesnt re-exec
			pingObs.subscribe(); // forces execution even if return val is never subscribed-to
			return pingObs;
		} else {
			return Observable.unsafeCreate(new DummyVoidResponse());
		}
	}

	static class DummyVoidResponse implements Observable.OnSubscribe<Void> {
		@Override public void call(final Subscriber<? super Void> subscriber) {
			subscriber.onCompleted();
		}
	}

	/** return current time in ms since epoch (as System.currentTimeMillis) */
	protected long currentTimeMillis() {
		return System.currentTimeMillis();
	}
	//endregion


	//region Remote Logs
	public void disableLogs() { loggingDisabled = true; }
	public void enableLogs() { loggingDisabled = false; }

	//endregion
}
