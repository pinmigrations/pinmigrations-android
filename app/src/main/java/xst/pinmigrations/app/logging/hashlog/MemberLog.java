package xst.pinmigrations.app.logging.hashlog;

/*
	Logging methods for tracking instances (but not instance members)
*/
public interface MemberLog extends HashLog {
	/**
	 * Short Member String (may omit fields, uses class-hash-string)
	 *  eg: ``
	 */
	String getShort();
	/** same as getShort but with indent */
	String getShort(int indentSpaces);


	/**
	 * Minimum Member String (may omit fields, and their Class, uses hash-string)
	 *  eg: ``
	 */
	String getMin();
	/** same as getShort but with indent */
	String getMin(int indentSpaces);


	/**
	 * Long Member String: (all potentially relevant fields, one per line)
	 *  eg: ``
	 */
	String getLong();
	/** same as getLong but with indent */
	String getLong(int indentSpaces);


	/**
	 * Recursive Member String
	 * same as getShort, but invokes `getRecursiveShort()` on all potentially relevant members
	 *  eg: ``
	 */
	String getRecursiveShort();
	/** same as getLong but with indent and increasing indent in each sublevel (4-spaces) */
	String getRecursiveShort(int indentSpaces);


	/**
	 * Recursive Member String
	 * same as getLong, but invokes `getRecursiveLong()` on all potentially relevant members
	 *  eg: ``
	 */
	String getRecursiveLong();
	/** same as getLong but with indent and increasing indent in each sublevel (4-spaces) */
	String getRecursiveLong(int indentSpaces);
}
