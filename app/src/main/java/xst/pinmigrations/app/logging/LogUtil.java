package xst.pinmigrations.app.logging;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;

import timber.log.Timber;
import xst.pinmigrations.app.logging.hashlog.HashLog;
import xst.pinmigrations.app.logging.hashlog.MemberLog;

@SuppressWarnings({"MethodWithMultipleReturnPoints", "WeakerAccess", "OverlyLongMethod"})
public final class LogUtil {
	public static final String LINE_LONG_PLUS = "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
	public static final String LINE_LONG_MINUS = "----------------------------------------------------------------------";
	public static final String LINE_LONG_EQUALS = "======================================================================";
	public static final String LINE_SHORT_MINUS = "----------------------------------";
	public static final String SPACE = " ";

	/** facility method to forward logs to Timber */
	public static void log(final int logLevel, @NonNull final String msg, @NonNull final Object... args) {
		switch(logLevel) {
			case Log.VERBOSE: Timber.v(msg, args); break;
			case Log.DEBUG: Timber.d(msg, args); break;
			case Log.INFO: Timber.i(msg, args); break;
			case Log.ASSERT: Timber.i(msg, args); break;
			case Log.WARN: Timber.w(msg, args); break;
			case Log.ERROR: Timber.e(msg, args); break;
			default: Timber.d(msg, args); break;
		}
	}
	public static String getStacktrace(@NonNull final Throwable t) {
		final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		final PrintStream printStream = new PrintStream(new ByteArrayOutputStream());
		t.printStackTrace(printStream);

		try {
			return new String(outStream.toByteArray(), "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			return "\n<ERROR GETTING STACK TRACE>\n";
		}
	}


	//region Strings/Formatting
	public static final String NULL_STRING = "null";
	public static final String NEW_LINE = "\n";
	public static @NonNull String getIndentSpaceString(final int indentSpaces) {
		if (indentSpaces > 0) {
			final StringBuilder sb = new StringBuilder();
			for (int i=0; i<indentSpaces; i++) { sb.append(" "); }
			return sb.toString();
		} else { return ""; }
	}
	public static void addSpaces(final StringBuilder sb, final int spaces) {
		for (int i=0; i<spaces; i++) { sb.append(' '); }
	}

	// NOTE: no trailing NEW-LINE
	public static String indentEachLine(final String strWithNewLines, final int spacesIndent) {
		final String INDENT = (spacesIndent > 0) ? getIndentSpaceString(spacesIndent): "";

		// simplest/fastest cases
		if (null == strWithNewLines) { return INDENT + NULL_STRING + NEW_LINE; }
		if (strWithNewLines.isEmpty()) { return NEW_LINE; }

		final StringBuilder sb = new StringBuilder();
		final String[] lines = strWithNewLines.split(NEW_LINE);
		for (final String line: lines) {
			if (!line.isEmpty()) {
				if (spacesIndent > 0) { sb.append(INDENT); }
				sb.append(line);
				sb.append(NEW_LINE);
			}
		}

		return sb.toString();
	}

	public static String prependStringsToList(final String[] suffixList, final String... varArgsPrepend) {
		final StringBuilder sb = new StringBuilder();
		for (final String prependStr: varArgsPrepend) {
			sb.append(prependStr);
		}
		for (final String suffixStr: suffixList) {
			sb.append(suffixStr);
		}
		return sb.toString();
	}

	public static String formatStringsArray(@NonNull final String format, @NonNull final String[] objs) {
		return String.format(format, (Object[]) objs);
	}
	//endregion


	//region Timestamps
	@SuppressLint("DefaultLocale")
	public static String getMillisDiffString(final long t1, final long t2) {
		return String.format("%.2fms", (t2 - t1) / 1e6d);
	}
	//endregion


	//region MemberLog
	public static final String ERROR_VALUES_ARRAY_NULL = "<ERROR-NULL-VALUES-ARRAY->";
	public static final String ERROR_FIELDS_AND_VALUES_NULL = "<ERROR-NULL-FIELDS-AND-VALUES>";
	public static final String MEMBER_MISSING = "<MISSING>";
	public static final String MEMBER_EMPTY_STRING = "<EMPTY-STR>";

	public static final String EQ = "=";
	public static final String COMMA_SPACE = ", ";
	public static final String ML_PREFIX = "{";
	public static final String ML_SUFFIX = "}";

	private static boolean emptyOrNull(final Object[] arr) { return null == arr || 0 == arr.length; }
	private static boolean emptyOrNull(final String s) { return null == s || 0 == s.length(); }

	// builds member string (no recursion, uses hash-string/class-hash-string depending on if there is a field-name)
	@SuppressWarnings("OverlyComplexMethod")
	public static String minMembersString(final String[] fieldNames, Object[] fieldValues) {
		if ( emptyOrNull(fieldValues) && emptyOrNull(fieldNames) ) { return ERROR_FIELDS_AND_VALUES_NULL; }
		if ( emptyOrNull(fieldValues) ) { return ERROR_VALUES_ARRAY_NULL; }

		final StringBuilder sb = new StringBuilder();
		final int namesLen = (null == fieldNames) ? -1 : fieldNames.length;
		final int valuesLen = fieldValues.length;

		sb.append(ML_PREFIX);
		for (int i=0; i<valuesLen; i++) {
			final String fieldName = (i<namesLen) ? fieldNames[i] : "";

			String fieldValue;
			if (emptyOrNull(fieldName)) {
				fieldValue = _classHashString(fieldValues[i]);
			} else {
				fieldValue = _hashString(fieldValues[i]);
			}

			if (emptyOrNull(fieldName)) {
				if (fieldValue.isEmpty()) {
					// no field name or field value
					sb.append(MEMBER_MISSING);
				} else {
					sb.append(fieldValue);
				}
			} else {
				if (!fieldValue.isEmpty()) {
					// have field name and field value
					sb.append(fieldName);
					sb.append(EQ);
					sb.append(fieldValue);
				} else {
					// have field name but value is empty-string
					sb.append(fieldName);
					sb.append(EQ);
					sb.append(MEMBER_EMPTY_STRING);
				}
			}

			if (i < valuesLen-1) { sb.append(COMMA_SPACE); }
		}

		sb.append(ML_SUFFIX);

		return sb.toString();
	}
	// pre-pends prefix and uses `minMembersString`
	public static String shortMembersString(final String prefix, final String[] fieldNames, final Object[] fieldValues) {
		return prefix + minMembersString(fieldNames, fieldValues);
	}

	// NOTE: use these in recursive methods (getShortRecursive, etc..)
	//
	private static String _getShortRecursive(final Object o) {
		if (o instanceof MemberLog) {
			return ((MemberLog) o).getRecursiveShort();
		} else if (o instanceof HashLog) {
			return ((HashLog) o).classHashString();
		} else if (o instanceof String) {
			return (String) o;
		} else {
			return classHashString(o);
		}
	}
	private static String _getLongRecursive(final Object o) {
		if (o instanceof MemberLog) {
			return ((MemberLog) o).getRecursiveLong();
		} else if (o instanceof HashLog) {
			return ((HashLog) o).classHashString();
		} else if (o instanceof String) {
			return (String) o;
		} else {
			return classHashString(o);
		}
	}

	// NOTE: type erasure makes getting the parameterized type for these impossible
	@SuppressWarnings("rawtypes")
	public static String getEmptyCollectionString(final String containedType, final Collection collection) {
		if (null == collection) { return NULL_STRING; }
		final String collectionClass = className(collection);

		String typeParameter;
		if (emptyOrNull(containedType)) { typeParameter = "<T>"; }
		else { typeParameter = "<" + containedType + ">"; }

		if (collection.isEmpty()) {
			return collectionClass + typeParameter + "[EMPTY-COLLECTION]";
		} else {
			// dont use this, just here to detect logging logic errors
			return collectionClass + typeParameter + "[size=" + collection.size() + "]";
		}
	}

	@SuppressWarnings("rawtypes")
	public static String getEmptyCollectionString(final Collection collection) {
		return getEmptyCollectionString("", collection);
	}
	@SuppressWarnings("rawtypes")
	public static String getEmptyMapString(final String keyType, final String valueType, final Map map) {
		if (null == map) { return NULL_STRING; }

		final String keyParam = emptyOrNull(keyType) ? "K" : keyType;
		final String valueParam = emptyOrNull(keyType) ? "V" : valueType;
		final String typeParameters = "<" + keyParam + "," + valueParam + ">";

		final String mapClass = className(map);
		if (map.isEmpty()) {
			return mapClass + typeParameters + "[EMPTY-MAP]";
		} else {
			// dont use this, just here to detect logging logic errors
			return mapClass + typeParameters + "[size=" + map.size() + "]";
		}
	}
	@SuppressWarnings("rawtypes")
	public static String getEmptyMapString(final Map map) {
		return getEmptyMapString("", "", map);
	}
	//endregion


	//region HashLog
	public static final String NULL_CLASS = "NULL-CLASS";
	public static String className(final Object o) {
		if (null == o) { return NULL_CLASS; }
		return o.getClass().getSimpleName();
	}
	// hash-string: at-symbol and object hash (and classname if not a project class)
	// eg:
	// 		`@1234` for instances of `HashLog`
	// 		`ClassName@1234` for others
	public static String hashString(final Object o) {
		if (null == o) { return NULL_STRING; }
		if (o instanceof HashLog) {
			return generateHashString(o);
		} else {
			return classHashString(o);
		}
	}
	public static String classHashString(final Object obj) {
		if (null == obj) { return NULL_STRING; }
		return className(obj) + generateHashString(obj);
	}
	private static String generateHashString(final Object obj) {
		if (null == obj) { return NULL_STRING; }
		return "@" + obj.hashCode();
	}

	// used internally to delegate to correct implementation
	private static String _hashString(final Object o) {
		if (null == o) { return NULL_STRING; }
		if (o instanceof HashLog) {
			// these will probably delegate to public `hashString`
			// but may be overriden
			return ((HashLog) o).hashString();
		} else if (o instanceof String) {
			return (String) o;
		} else {
			return classHashString(o);
		}
	}
	// used internally to delegate to correct implementation
	private static String _classHashString(final Object o) {
		if (null == o) { return NULL_STRING; }
		if (o instanceof HashLog) {
			// these will probably delegate to public `classHashString`
			// but may be overriden
			return ((HashLog) o).classHashString();
		} else if (o instanceof String) {
			return (String) o;
		} else {
			return classHashString(o);
		}
	}

	public static final String END_PREFIX = "END ";
	public static String hashLogMethod(final Object obj, final String methodName) {
		return className(obj) + "::" + methodName + " " + hashString(obj);
	}
	private static String hashLogMethodPartial(final Object obj, final String methodName) {
		return className(obj) + "::" + methodName + "(" + hashString(obj) + ")";
	}
	public static String endHashLogMethod(final HashLog hashLoggable, final String methodName) {
		return END_PREFIX + hashLogMethod(hashLoggable, methodName);
	}

	public static String hashLogText(final Object obj, final String text) {
		return classHashString(obj) + " " + text;
	}

	public static String hashLogObject(final Object obj1, final String text, final Object... others) {
		return hashLogText(obj1, text) + " " + hashLogList(others);
	}

	// NOTE: `hashLogList` and `hashLoggifyList` are similar but to avoid overhead, keep implementations separate
	private static String hashLogList(final Object[] objects) {
		if (null == objects) { return NULL_STRING; }
		final int objectsLength = objects.length;
		if (0 == objectsLength) { return ""; }
		if (1 == objectsLength) { return classHashString(objects[0]); }

		final StringBuilder sb = new StringBuilder();
		for (Object o: objects) {
			sb.append(", ");
			sb.append(_classHashString(o));
		}

		// skip initial comma-space in string builder
		// (instead of building with extra trailing comma-space then deleting at end)
		return sb.substring(2);
	}
	/** call appropriate toString (hash-logging) for each object, return as Object[] for formatting fns */
	private static String[] hashLoggifyList(final Object[] objects) {
		final int objectsLength = objects.length;
		final String[] stringsList = new String[objectsLength];
		if (0 == objectsLength) { return stringsList; }

		for (int i=0; i<objectsLength; i++) {
			stringsList[i] = _classHashString(objects[i]);
		}

		return stringsList;
	}

	/**
		NOTE: can only handle `%s` formatting strings
	 */
	public static String hashLogFormatted(final Object obj1, final String format, final Object... objects) {
		return classHashString(obj1) + " " + formatStringsArray(format, hashLoggifyList(objects));
	}
	// same as `hashLogFormatted` but prepends `hashLogMethodPartial()`
	public static String hashLogMethodFormatted(final Object obj1, final String methodName, final String format, final Object... objects) {
		return hashLogMethodPartial(obj1, methodName) + " " + formatStringsArray(format, hashLoggifyList(objects));
	}

	// Anonymous (for singletons)
	public static String hashLogTextAnon(final Object obj, final String text) {
		return className(obj) + " " + text;
	}
	public static String hashLogMethodAnon(final Object obj, final String methodName) {
		return className(obj) + "::" + methodName;
	}
	public static String endHashLogMethodAnon(final HashLog hashLoggable, final String methodName) {
		return END_PREFIX + hashLogMethodAnon(hashLoggable, methodName);
	}
	public static String hashLogObjectAnon(final Object anonObj, final String text, final Object... others) {
		return hashLogTextAnon(anonObj, text) + " " + hashLogList(others);
	}
	public static String hashLogFormattedAnon(final Object anonObj, final String format, final Object... objects) {
		final String formatted = formatStringsArray(format, hashLoggifyList(objects));
		return className(anonObj) + " " + formatted;
	}
	public static String hashLogMethodFormattedAnon(final Object anonObj, final String methodName, final String format, final Object... objects) {
		final String formatted = formatStringsArray(format, hashLoggifyList(objects));
		return hashLogMethodAnon(anonObj, methodName) + " " + formatted;
	}
	//endregion


	//region Static Log
	public static String staticLogMethod(final StackTraceElement elem) {
		return classNameForStackTraceElem(elem) + "::" + elem.getMethodName();
	}
	public static String endStaticLogMethod(final StackTraceElement elem) {
		return END_PREFIX + staticLogMethod(elem);
	}
	public static String staticLogText(final StackTraceElement elem, final String text) {
		return classNameForStackTraceElem(elem) + SPACE + text;
	}
	public static String staticLogObject(final StackTraceElement elem, final String text, final Object... others) {
		return staticLogText(elem, text) + " " + hashLogList(others);
	}
	public static String staticLogFormatted(final StackTraceElement elem, final String format, final Object... objects) {
		return classNameForStackTraceElem(elem) + " " + formatStringsArray(format, hashLoggifyList(objects));
	}
	public static String staticLogMethodFormatted(final StackTraceElement elem, final Object... objects) {
		return prependStringsToList(hashLoggifyList(objects), classNameForStackTraceElem(elem), "::", elem.getMethodName(), SPACE);
	}

	private static String classNameForStackTraceElem(final StackTraceElement elem) {
		final String className = elem.getClassName();
		return elem.getClassName().substring(className.lastIndexOf('.') + 1);
	}
	//endregion

	//region Assertions

	public static String __assertMessageTag(final StackTraceElement elem) {
		return classNameForStackTraceElem(elem) + "." + elem.getMethodName() + ":" + elem.getLineNumber();
	}
	//endregion
}
