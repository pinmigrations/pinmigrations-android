package xst.pinmigrations.app.logging.hashlog;

import xst.pinmigrations.app.logging.LogUtil;
import xst.pinmigrations.app.util.MainUtil;

/*
	Logging methods for static methods
	+ pseudo-reflection to determine class/method names as in Timber
	+ @see Util.getStackTraceElement
*/
public class StaticLog {
	public static String _SLM() {
		return LogUtil.staticLogMethod(getStackTraceElement());
	}
	public static String _END_SLM() {
		return LogUtil.endStaticLogMethod(getStackTraceElement());
	}
	public static String _SLT(final String text) {
		return LogUtil.staticLogText(getStackTraceElement(), text);
	}

	public static String _SLO(final String text, final Object... others) {
		return LogUtil.staticLogObject(getStackTraceElement(), text, others);
	}
	public static String _SLF(final String format, final Object... objects) {
		return LogUtil.staticLogFormatted(getStackTraceElement(), format, objects);
	}
	public static String _SLMF(final String format, final Object... objects) {
		return LogUtil.staticLogMethodFormatted(getStackTraceElement(), format, objects);
	}

	// NOTE: stack trace depth changes based on context of invocation
	private static final int CALL_STACK_INDEX = 3;
	// eg: copy/paste of this exact code will likely not work, re-compute depth
	private final static StackTraceElement getStackTraceElement() {
		return MainUtil.getStackTraceElement(CALL_STACK_INDEX);
	}
	//endregion
}
