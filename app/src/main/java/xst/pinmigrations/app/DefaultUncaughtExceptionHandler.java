package xst.pinmigrations.app;

import timber.log.Timber;

public class DefaultUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
	@Override
	public void uncaughtException(final Thread t, final Throwable e) {
		// TODO: report error to client-log (remote logging)
		Timber.e("========================");
		Timber.e("FATAL UNCAUGHT EXCEPTION");
		Timber.e("========================");
		Timber.e(e);
		Timber.e("========================");

		// TODO: restart activity (not app?)
		// http://stackoverflow.com/a/2903866/1163940

		// ?
		System.exit(2);
	}
}
