package xst.pinmigrations.app;

import android.app.Application;
import android.support.annotation.NonNull;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import org.jetbrains.annotations.Contract;

import java.util.ArrayList;

import timber.log.AppTimber;
import timber.log.Timber;
import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.app.di.AppComponent;
import xst.pinmigrations.app.di.AppModule;
import xst.pinmigrations.app.di.BaseComponent;
import xst.pinmigrations.app.di.DaggerInjectorComponent;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.app.di.DaggerAppComponent;
import xst.pinmigrations.app.di.DaggerBaseComponent;
import xst.pinmigrations.domain.login.LoginInitializer;
import xst.pinmigrations.test.StatefulStub;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;

public class App extends Application {
	//region Exec-Mode


	public static final int EXEC_UNIT_TESTS = -1, EXEC_DEVICE_TESTS = -2, EXEC_NORMAL = -3;
	public static void setExecModeUnitTests() {
		sExecMode = EXEC_UNIT_TESTS;
	}
	public static void setExecModeDeviceTests() {
		sExecMode = EXEC_DEVICE_TESTS;
	}

	@Contract(pure = true)
	public static boolean normalExec() { return EXEC_NORMAL == sExecMode; }
	@Contract(pure = true)
	public static boolean unitTestExec() { return EXEC_UNIT_TESTS == sExecMode; }
	@Contract(pure = true)
	public static boolean deviceTestExec() { return EXEC_DEVICE_TESTS == sExecMode; }

	private static int sExecMode = EXEC_NORMAL;
	//endregion


	//region Logging
	public static final int LOGGING_NORMAL = 10, LOGGING_DISABLED = 11, LOGGING_NET_VERBOSE = 12;

	@Contract(pure = true)
	public static boolean loggingDisabled() { return LOGGING_DISABLED == sLoggingMode; }
	@Contract(pure = true)
	public static boolean loggingNetVerbose() { return LOGGING_NET_VERBOSE == sLoggingMode; }

	public static void setLoggingModeNetVerbose() {
		sLoggingMode = LOGGING_NET_VERBOSE;
	}
	public static void setLoggingModeNormal() {
		sLoggingMode = LOGGING_NORMAL;
	}

	public static void enableLogging() {
		sLoggingMode = LOGGING_NORMAL;
	}
	public static void disableLogging() {
		sLoggingMode = LOGGING_DISABLED;
	}

	private static int sLoggingMode = LOGGING_NORMAL;

	/**
	 * test for tree count is to protect against double-loading in tests
	 * TODO: switch to RemoteLogger on Release builds
	 * NOTE: RemoteLogger not implemented
	 */
	private void setupLogging() {
		if (0 == Timber.treeCount()) {
			Timber.plant(new AppTimber());
		}
	}
	//endregion


	@SuppressWarnings("MethodWithMultipleReturnPoints") // short-circuit needed for LeakCanary
	@Override
	public void onCreate() {
		if (normalExec() && LeakCanary.isInAnalyzerProcess(this)) {
			// This process is dedicated to LeakCanary for heap analysis, dont init app
			return;
		}

		setupLogging();

		super.onCreate();

		// DI setup
		if (!unitTestExec() && !deviceTestExec() ) { // handled separately by TestApp/AndroidTestApp during tests
			// setup handler for uncaught exceptions
			Thread.setDefaultUncaughtExceptionHandler(new DefaultUncaughtExceptionHandler());

			// this is no-op on test/release builds
			mRefWatcher = installLeakCanary(this);

			// DI
			final AppComponent appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
			mBaseComponent = DaggerBaseComponent.builder().appComponent(appComponent).build();
		}


		// Login setup
		initializeLogin();
	}

	protected void initializeLogin() {
		// single-use object, so dont need to @Inject this into app
		(new LoginInitializer(getInjector())).initializeLogin();
	}

	//region LeakCanary
	public static boolean sDisableLeakCanary = true;
	private RefWatcher mRefWatcher;
	protected RefWatcher installLeakCanary(final App app) {
		return RefWatcher.DISABLED;
	}
	public void watchRefsOn(@NonNull final Object o) {
		if (null != mRefWatcher) {
			mRefWatcher.watch(o);
		}
	}
	//endregion

	//region DI
	protected BaseComponent mBaseComponent;
	public BaseComponent getBaseComponent() {
		return verifyNotNull(mBaseComponent);
	}
	protected InjectorComponent mInjectorComponent;
	public InjectorComponent getInjector() {
		if (null == mInjectorComponent) {
			mInjectorComponent = DaggerInjectorComponent.builder().baseComponent(mBaseComponent).build();
		}

		return mInjectorComponent;
	}
	//endregion

	//region Test Helpers

	private final ArrayList<StatefulStub> stubs = new ArrayList<>();
	public void addStub(@NonNull final StatefulStub stub) {
		stubs.add( checkNotNull(stub) );
	}
	public @NonNull ArrayList<StatefulStub> getStubs() {
		return stubs;
	}
	//endregion

	//region debug / release builds
	@Contract(pure = true)
	public static boolean isDebugBuild() {
		return BuildConfig.DEBUG;
	}
	//endregion
}
