package xst.pinmigrations.app.di;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import okhttp3.HttpUrl;
import xst.pinmigrations.app.App;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
	App getApplication();

	@Named("PREFS_FILE_NAME") String getPrefsFileName();
	@Named("BASE_URL") HttpUrl getBaseUrl();
}
