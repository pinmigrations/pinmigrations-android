package xst.pinmigrations.app.di;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import xst.pinmigrations.app.App;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceGenerator;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.android_context.GoogleApiService;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsResource;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsStore;
import xst.pinmigrations.domain.maps.MapsServiceGenerator;
import xst.pinmigrations.domain.rx.RxJavaService;

/**
 * Services / objects in this module all require the application context in android
 */
@Module
public final class AndroidContextModule {

	@AppScope @Provides
	public PrefsStore providePrefsStore(final App app, @Named("PREFS_FILE_NAME") final String prefsFileName) {
		return new PrefsStore(app, prefsFileName);
	}

	@AppScope @Provides
	public PrefsResource providePrefsResource(final PrefsStore prefsStore, final RxJavaService rxJavaService) {
		return new PrefsResource(prefsStore, rxJavaService);
	}

	@AppScope @Provides
	public PrefsManager providePrefsManager(final PrefsResource prefsResource) {
		return new PrefsManager(prefsResource);
	}

	@AppScope @Provides
	public UiServiceGenerator provideUiServiceGenerator(final App app, final RxJavaService rxJavaService) {
		return new UiServiceGenerator(app, rxJavaService);
	}

	@AppScope @Provides
	public AndroidService provideAndroidResourcesService(final App app) {
		return new AndroidService(app);
	}

	@AppScope @Provides
	public GoogleApiService provideGoogleApiService(final App app) {
		return new GoogleApiService(app);
	}

	@AppScope @Provides
	public MapsServiceGenerator provideMapsServiceGenerator(final AndroidService androidService) {
		return new MapsServiceGenerator(androidService);
	}
}
