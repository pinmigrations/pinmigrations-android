package xst.pinmigrations.app.di;

import android.support.annotation.NonNull;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import xst.pinmigrations.app.App;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
public class AppModule {
	@NonNull final App mApp;

	public AppModule(@NonNull final App app) {
		mApp = checkNotNull(app);
	}

	@Singleton @Provides
	public App provideApp() {
		return mApp;
	}

	@Singleton @Provides @Named("PREFS_FILE_NAME")
	public String providePrefsFileName() {
		return "PinMigrations-SharedPreferences";
	}

	@Singleton @Provides @Named("BASE_URL")
	public HttpUrl provideBaseUrl() {
		return HttpUrl.parse("http://pinmigrations.herokuapp.com");
	}
}
