package xst.pinmigrations.app.di;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsStore;
import xst.pinmigrations.domain.login.Login;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.LoginUtil;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.rx.RxJavaService;

/*
	provides services w/o deps or only on other services, not on Activity/Context
 */

@Module
public final class ServicesModule {
	@AppScope @Provides
	public Login provideInitialLoginState(final PrefsStore prefsStore) {
		return LoginUtil.setupLoginState(prefsStore);
	}

	@AppScope @Provides
	public LoginManager provideLoginManager(final Login initialLoginState, final PrefsManager prefsManager) {
		return new LoginManager(initialLoginState, prefsManager);
	}

	@AppScope @Provides
	public RxJavaService provideRxJavaService() {
		return new RxJavaService();
	}


	@AppScope @Provides
	public NetworkService provideNetworkService(final LoginManager loginManager, @Named("BASE_URL") final HttpUrl rootUrl) {
		return new NetworkService(rootUrl, loginManager);
	}
}
