package xst.pinmigrations.app.di;

import dagger.Component;
import xst.pinmigrations.activities.admin.AdminActivity;
import xst.pinmigrations.activities.admin.AdminPresenter;
import xst.pinmigrations.activities.admin.EditPositionActivity;
import xst.pinmigrations.activities.admin.PositionDialogFragment;
import xst.pinmigrations.activities.admin.SortablePositionTableView;
import xst.pinmigrations.activities.login.LoginActivity;
import xst.pinmigrations.activities.login.LoginPresenter;
import xst.pinmigrations.activities.map.EditMapMvpView;
import xst.pinmigrations.activities.map.EditMapPresenter;
import xst.pinmigrations.activities.map.LocationDialogFragment;
import xst.pinmigrations.activities.map.MapActivity;
import xst.pinmigrations.activities.map.NewMapMvpView;
import xst.pinmigrations.activities.map.NewMapPresenter;
import xst.pinmigrations.activities.map.ViewAllMapMvpView;
import xst.pinmigrations.activities.map.ViewAllMapPresenter;
import xst.pinmigrations.activities.welcome.WelcomeActivity;
import xst.pinmigrations.app.di.scopes.ActivityScope;
import xst.pinmigrations.domain.login.LoginInitializer;

@ActivityScope
@Component(dependencies = BaseComponent.class)
public interface InjectorComponent {
	// initializers
	void inject(LoginInitializer initializer);

	// activities
	void inject(WelcomeActivity act);
	void inject(LoginActivity act);
	void inject(AdminActivity act);
	void inject(MapActivity act);
	void inject(EditPositionActivity act);

	// presenters
	void inject(AdminPresenter prsnt);
	void inject(LoginPresenter prsnt);
	void inject(ViewAllMapPresenter prsnt);
	void inject(NewMapPresenter prsnt);
	void inject(EditMapPresenter prsnt);

	// fragments / views
	void inject(PositionDialogFragment frag);
	void inject(SortablePositionTableView view);
	void inject(ViewAllMapMvpView view);
	void inject(EditMapMvpView view);
	void inject(NewMapMvpView view);
	void inject(LocationDialogFragment frag);
}
