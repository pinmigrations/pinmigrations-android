package xst.pinmigrations.app.di;

import dagger.Module;
import dagger.Provides;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.LoginResource;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.paths.PathListResource;
import xst.pinmigrations.domain.positions.PositionsResource;
import xst.pinmigrations.domain.positions.PositionsSamplesResource;
import xst.pinmigrations.domain.rx.RxJavaService;

/*
	provides resource services w potential deps on other resource-servies,
	these all depend on NetworkService (directly or transitively) and ApiGenerator
 */
@Module
public final class ResourcesModule {
	@AppScope @Provides
	public ApiGenerator provideApiGenerator(final NetworkService networkService, final RxJavaService rxJavaService) {
		return new ApiGenerator(networkService, rxJavaService);
	}

	@AppScope @Provides
	public RemoteLogResource provideRemoteLogResource(final ApiGenerator apiGenerator) {
		return new RemoteLogResource(apiGenerator);
	}

	@AppScope @Provides
	public PositionsResource providePositionsResource(final ApiGenerator apiGenerator,
													  final RemoteLogResource remoteLogResource, final LoginManager loginManager) {
		return new PositionsResource(apiGenerator, remoteLogResource, loginManager);
	}


	@AppScope @Provides
	public PositionsSamplesResource providePositionsSamplesResource(final AndroidService androidService, final RxJavaService rxJavaService) {
		return new PositionsSamplesResource(androidService, rxJavaService);
	}


	@AppScope @Provides
	public PathListResource providePathListResource(final PositionsResource positionsResource,
													final PositionsSamplesResource positionsSamplesResource,
													final RemoteLogResource remoteLogResource) {
		return new PathListResource(positionsResource, positionsSamplesResource, remoteLogResource);
	}

	@AppScope @Provides
	public LoginResource provideLoginResource(final ApiGenerator apiGenerator, final LoginManager loginManager,
											  final NetworkService networkService, final RemoteLogResource remoteLogResource) {
		return new LoginResource(apiGenerator, loginManager, networkService, remoteLogResource);
	}
}
