package xst.pinmigrations.app.di;

import dagger.Component;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceGenerator;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.android_context.GoogleApiService;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.LoginResource;
import xst.pinmigrations.domain.maps.MapsServiceGenerator;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.paths.PathListResource;
import xst.pinmigrations.domain.positions.PositionsResource;
import xst.pinmigrations.domain.positions.PositionsSamplesResource;
import xst.pinmigrations.domain.rx.RxJavaService;

/*
	top-level component (base interface) with factory methods
	gives dependent components (the middle tier) access to each others' objects
	eg: anything that is injected at `ActivityInjectorComponent`, must be declared here
	dependencies amongst objects in *Module are resolved without declaring here
 */
@AppScope
@Component(dependencies = AppComponent.class,
		modules = { ServicesModule.class, AndroidContextModule.class, ResourcesModule.class})
public interface BaseComponent {
	// services (no deps on Context/Activity)
	RxJavaService getRxJavaService();
	UiServiceGenerator getUiServiceGenerator();
	LoginManager getLoginManager();
	NetworkService getNetworkService();

	// Context-Dependent Services
	AndroidService getAndroidResourcesService();
	GoogleApiService getGoogleApiService();
	PrefsManager getPrefsManager();
	MapsServiceGenerator getMapsServiceGenerator();

	// resources
	RemoteLogResource getRemoteLogResource();
	PositionsResource getPositionsResource();
	PathListResource getPathListResource();
	PositionsSamplesResource getPositionsSamplesResource();
	LoginResource getLoginResource();
}
