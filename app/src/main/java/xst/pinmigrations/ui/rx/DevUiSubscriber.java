package xst.pinmigrations.ui.rx;

import rx.exceptions.Exceptions;

/**
 * TODO: encapsulate/use SafeSubscriber (can't extend it)
 */
public abstract class DevUiSubscriber<T> extends BaseUiSubscriber<T> {
	/** kill app on error (during DEV!) */
	@Override public void onError(final Throwable e) {
		throw Exceptions.propagate(e);
	}
}
