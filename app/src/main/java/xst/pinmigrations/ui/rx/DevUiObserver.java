package xst.pinmigrations.ui.rx;

import rx.exceptions.Exceptions;

public abstract class DevUiObserver<T> extends BaseUiObserver<T> {
	/** kill app on error (during DEV!) */
	@Override public void onError(final Throwable e) {
		throw Exceptions.propagate(e);
	}
}
