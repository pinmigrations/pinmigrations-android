package xst.pinmigrations.ui.rx;

/**
 * NOTE: will probably have to DI this
 */
public abstract class ProdUiSubscriber<T> extends BaseUiSubscriber<T> {
	/**  */
	@Override public final void onError(final Throwable e) {
		// TODO: send remote log if possible (have internet conn)

		// implementing classes should still implement
	}

	/** attempt to recover from error */
	public abstract void afterError(final Throwable e);
}
