package xst.pinmigrations.ui.rx;

import rx.Subscriber;

public abstract class BaseUiSubscriber<T> extends Subscriber<T> {
	/**
	 * subscribing to "Singles" so irrelevant
	 * NOTE: UiSubscriber are re-used frequently
	 */
	@Override public void onCompleted() { }
}
