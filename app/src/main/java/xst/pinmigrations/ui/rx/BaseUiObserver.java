package xst.pinmigrations.ui.rx;

import rx.Observer;

public abstract class BaseUiObserver<T> implements Observer<T> {
	@Override public void onCompleted() { }
}
