package xst.pinmigrations.ui.rx;

/** Relay never emits onCompleted/onError so ok to keep single Subscriber instance */
public abstract class BaseRelayObserver<T> extends BaseUiObserver<T> {
	@Override public void onError(final Throwable e)  { /* never invoked (by design) */ }
	@Override public void onCompleted() { /* never invoked (by design) */ }
}
