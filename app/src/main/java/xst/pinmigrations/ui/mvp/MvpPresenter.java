package xst.pinmigrations.ui.mvp;

import xst.pinmigrations.activities.BaseActivity;

/*
	<A> is the activity type
	https://github.com/Syhids/android-architecture/tree/todo-mvp-fragmentless
	https://medium.com/upday-devs/android-architecture-patterns-part-2-model-view-presenter-8a6faaae14a5#.o7opy2cvb
 */
public interface MvpPresenter<A extends BaseActivity> {
	void hideSoftKeyboard();
}
