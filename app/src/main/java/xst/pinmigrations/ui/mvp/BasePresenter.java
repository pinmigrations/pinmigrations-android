package xst.pinmigrations.ui.mvp;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

import rx.subscriptions.CompositeSubscription;
import xst.pinmigrations.activities.StandardActivity;
import xst.pinmigrations.activities.StandardActivity.Lifecycle;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.ui.rx.BaseRelayObserver;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class BasePresenter<A extends StandardActivity> implements MvpPresenter<A>, Destroyable {

	private final WeakReference<StandardActivity> activityRef;
	protected final CompositeSubscription mCompositeSubscription = new CompositeSubscription();

	public final @Nullable StandardActivity getActivity() { return activityRef.get(); }

	public BasePresenter(@NonNull final StandardActivity activity) {
		this.activityRef = new WeakReference<>( checkNotNull(activity) );
	}

	/** subscribe to lifecycle once all init is complete (ie: DI/UI, etc..) */
	protected void subscribeToLifecycle() {
		// note: android's on* methods are already invoked on main thread
		mCompositeSubscription.add( // get LifeCycle notifications
				checkNotNull( getActivity()) .getLifecycleRelay()
						.subscribe( new LifecycleObserver() ));
	}

	@Override public void hideSoftKeyboard() {
		final StandardActivity activity = getActivity();
		if (null != activity) { activity.hideSoftKeyboard(); }
	}

	/** invoke before sub-class destroy */
	@CallSuper @Override public void destroy() {
		mCompositeSubscription.unsubscribe();
		activityRef.clear();
	}


	//region lifecycle relay
	/** invoked in activity's onStart, on Android Main thread. no default behavior, dont call super */
	public void onStart() {}
	/** invoked in activity's onResume, on Android Main thread. no default behavior, dont call super */
	public void onResume() {}
	/** invoked in activity's onPause, on Android Main thread. no default behavior, dont call super */
	public void onPause() {}
	/** invoked in activity's onStop, on Android Main thread. no default behavior, dont call super */
	public void onStop() {}
	/** invoked in activity's onDestroy, on Android Main thread */
	final public void onDestroy() { destroy(); }
	//endregion

	class LifecycleObserver extends BaseRelayObserver<Lifecycle> {
		@Override public void onNext(final Lifecycle lifecycle) {
			switch (lifecycle) {
				case START: onStart(); break;
				case RESUME: onResume(); break;
				case PAUSE: onPause(); break;
				case STOP: onStop(); break;
				case DESTROY: onDestroy(); break;
				default: throw new AssertionError("invalid lifecycle event: " + lifecycle);
			}
		}
	}
}
