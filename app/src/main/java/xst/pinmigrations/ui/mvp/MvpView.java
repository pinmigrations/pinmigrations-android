package xst.pinmigrations.ui.mvp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import xst.pinmigrations.domain.activity_context.ui_service.UiService;

/*
	<P> is the corresponding Presenter type
	https://github.com/Syhids/android-architecture/tree/todo-mvp-fragmentless
	https://medium.com/upday-devs/android-architecture-patterns-part-2-model-view-presenter-8a6faaae14a5#.o7opy2cvb
 */
public interface MvpView<P> {
	void setPresenter(@NonNull P presenter);
	void setUiService(@NonNull UiService uiService);
	@Nullable UiService getUiService();

	void showQuickNotice(@NonNull String text);
	void showImportantMessage(@NonNull String text);
}
