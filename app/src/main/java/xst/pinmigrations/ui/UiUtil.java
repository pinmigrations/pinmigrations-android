package xst.pinmigrations.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.GridLayout;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;

import timber.log.Timber;
import xst.pinmigrations.app.logging.LogUtil;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

public final class UiUtil {
	public static final int SCREEN_ROTATE_NORMAL = 0xdead00;
	public static final int SCREEN_ROTATE_PORTRAIT = 0xdead01;
	public static final int SCREEN_ROTATE_LANDSCAPE = 0xdead02;

	public static final int APP_TABLET_SW_WIDTH = 600;

	//region Android UI
	public static void hideSoftKeyboard(final @NonNull Activity currentActivity) {
		final InputMethodManager inputMethodManager =
				(InputMethodManager) checkNotNull(currentActivity).getSystemService(Context.INPUT_METHOD_SERVICE);

		final View currentFocusedView = currentActivity.getCurrentFocus();
		if (null != currentFocusedView) {
			inputMethodManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), 0);
		}
	}
	//endregion

	//region Activity manip

	/** start activitiy without Transition animations */
	public static void startActivity(final Activity caller, final Intent intent) {
		caller.startActivity(intent);
		caller.overridePendingTransition(0, 0);
	}
	/** start activitiy without Transition animations */
	public static <T extends Activity> void startActivity(final Activity caller, Class<T> activityClass) {
		startActivity(caller, new Intent(caller, activityClass));
	}
	//endregion

	//region GridLayout manip
	// http://stackoverflow.com/questions/26514673/decreasing-row-column-count-of-gridlayout-in-android
	public static void changeGridColumnCount(final GridLayout gridLayout, final int columnCount) {
		if (gridLayout.getColumnCount() != columnCount) {
			final int viewsCount = gridLayout.getChildCount();
			for (int i = 0; i < viewsCount; i++) {
				gridLayout.getChildAt(i).setLayoutParams(new GridLayout.LayoutParams());
			}
			gridLayout.setColumnCount(columnCount);
		}
	}
	//endregion

	//region View manip
	public static ViewGroup getParent(final View view) {
		return (ViewGroup) view.getParent();
	}
	public static void removeView(View view) {
		final ViewGroup parent = getParent(view);
		if (parent != null) { parent.removeView(view); }
	}
	public static void replaceView(View currentView, View newView) {
		final ViewGroup parent = getParent(currentView);
		if (parent == null) { return; }

		final int index = parent.indexOfChild(currentView);
		removeView(currentView);
		removeView(newView);
		parent.addView(newView, index);
	}
	public static String getViewHierarchy(final View view) {
		if (null == view) { return LogUtil.NULL_STRING; }
		final ViewGroup parent = getParent(view);
		return getViewHierarchy(parent);
	}
	public static String getViewHierarchy(final ViewGroup viewGroup) {
		if (null == viewGroup) { return LogUtil.NULL_STRING; }

		final int numViews = viewGroup.getChildCount();
		if (0 == numViews ) { return "(no-children) " + viewGroup.toString(); }

		final StringBuilder sb = new StringBuilder();
		sb.append("root: ");
		sb.append(viewGroup.toString());
		sb.append("\n");

		View v;
		for (int i=0; i<numViews; i++) {
			v = viewGroup.getChildAt(i);
			sb.append("    ");
			sb.append(v.toString());
			sb.append("\n");
		}

		return sb.toString();
	}
	public static View getEmptyView(final Activity activity) {
		return new View(activity);
	}

	/** get all children for a view (if-any). Breadth-First Search
	 * http://stackoverflow.com/a/11263152/1163940
	 */
	public static ArrayList<View> getAllChildren(@NonNull final View v) {
		final ArrayList<View> visited = new ArrayList<>();
		final ArrayList<View> unvisited = new ArrayList<>();
		unvisited.add( checkNotNull(v) );

		while (!unvisited.isEmpty()) {
			View child = unvisited.remove(0);
			visited.add(child);

			if ( !(child instanceof ViewGroup) ) { continue; } // not viewgroup, no children
			ViewGroup group = (ViewGroup) child;
			final int childCount = group.getChildCount();
			for (int i=0; i<childCount; i++) {
				unvisited.add( group.getChildAt(i) );
			}
		}

		return visited;
	}
	//endregion

	//region Bitmaps
	public static @NonNull Bitmap scaleBitmap(@NonNull final Bitmap bitmap, final float scale) {
		verify(scale > 0.0f, "scale must be positive & non-zero");
		return Bitmap.createScaledBitmap( checkNotNull(bitmap),
				Math.round(bitmap.getWidth() * scale),
				Math.round(bitmap.getHeight() * scale), false);
	}
	//endregion

	//region Screen Rotation / Orientation

	public static void lockOrientation(final Activity activity) {
		final Display display = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		final int rotation = display.getRotation();
		final int tempOrientation = activity.getResources().getConfiguration().orientation;

		int orientation = 0;
		switch(tempOrientation) {
			case Configuration.ORIENTATION_LANDSCAPE:
				if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90) {
					orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
				} else {
					orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
				}
				break;
			case Configuration.ORIENTATION_PORTRAIT:
				if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_270) {
					orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				} else {
					orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				}
				break;
			default: return;
		}

		activity.setRequestedOrientation(orientation);
	}

	public static void lockToLandscape(final Activity act) { act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); }
	public static void lockToPortrait(final Activity act) { act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); }

	public static void unlockOrientation(final Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
	}

	public static int getSimpleCurrentOrientation(@NonNull final Activity activity) {
		final Point size = new Point(); // size data container
		final Display display = checkNotNull(activity).getWindowManager().getDefaultDisplay();

		display.getSize(size);

		if (size.x < size.y) {
			return SCREEN_ROTATE_PORTRAIT;
		} else {
			return SCREEN_ROTATE_LANDSCAPE;
		}
	}
	public static boolean isPortrait(@NonNull final Activity activity) {
		return getSimpleCurrentOrientation(activity) == SCREEN_ROTATE_PORTRAIT;
	}
	public static boolean isLandscape(@NonNull final Activity activity) {
		return getSimpleCurrentOrientation(activity) == SCREEN_ROTATE_LANDSCAPE;
	}
	public static boolean isLandscape(@NonNull final DisplayMetrics displayMetrics) {
		return checkNotNull(displayMetrics).widthPixels > displayMetrics.heightPixels;
	}
	public static boolean isPortrait(@NonNull final DisplayMetrics displayMetrics) {
		return checkNotNull(displayMetrics).heightPixels > displayMetrics.widthPixels;
	}
	//endregion

	//region DP/SP/Pixels
	/** multiply density x DP, round up */
	public static int getPixelsFromDp(final float deviceDensity, final float dp) {
		return (int)(dp * deviceDensity + 0.5f);
	}
	//endregion

	//region Android OS MemLeaks mgmt
	/**
	 *
	 */
	private static final Field TEXT_LINE_CACHED;
	static {
		Field textLineCached = null;
		try {
			textLineCached = Class.forName("android.text.TextLine").getDeclaredField("sCached");
			textLineCached.setAccessible(true);
		} catch (final Exception ex) {
			Timber.e("exception with MemLeak-Hack");
			Timber.e(ex);
		}
		TEXT_LINE_CACHED = textLineCached;
	}
	/**
	 * resolves Android OS memory leak:
	 * 	http://stackoverflow.com/a/30397901/1163940
	 */
	public static void clearTextLineCache() {
		// If the field was not found for whatever reason just return.
		if (TEXT_LINE_CACHED == null) { return; }

		Object cached = null;
		try {
			cached = TEXT_LINE_CACHED.get(null); // Get reference to the TextLine sCached array
		} catch (final Exception ignore) { }

		if (cached != null) {
			// Clear the array
			for (int i = 0, size = Array.getLength(cached); i < size; i ++) {
				Array.set(cached, i, null);
			}
		}
	}
	//endregion
}
