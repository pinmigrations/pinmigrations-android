package xst.pinmigrations.ui;

import android.support.annotation.Nullable;

public final class Comparators {
	public static int longCompareAscending(final long n1, final long n2) {
		if (n1 == n2) { return 0; }
		else { return n1 < n2 ? -1 : 1; }
	}

	public static int longCompareDescending(final long n1, final long n2) {
		if (n1 == n2) { return 0; }
		else { return n1 > n2 ? -1 : 1; }
	}

	public static int doubleCompareAscending(final double n1, final double n2) {
		if (n1 == n2) { return 0; }
		else { return n1 < n2 ? -1 : 1; }
	}

	public static int doubleCompareDescending(final double n1, final double n2) {
		if (n1 == n2) { return 0; }
		else { return n1 > n2 ? -1 : 1; }
	}

	/** null/empty string are last */
	public static int stringCompareAscending(@Nullable final String s1, @Nullable final String s2) {
		if (null == s1 && null == s2) { return 0; }
		if (null == s1) { return 1; }
		if (null == s2) { return -1; }

		if (s1.isEmpty() && s2.isEmpty()) { return 0; }
		if (s1.isEmpty()) { return 1; }
		if (s2.isEmpty()) { return -1; }
		return s1.compareTo(s2);
	}

	/** null/empty string are last */
	public static int stringCompareDescending(@Nullable final String s1, @Nullable final String s2) {
		if (null == s1 && null == s2) { return 0; }
		if (null == s1) { return 1; }
		if (null == s2) { return -1; }

		if (s1.isEmpty() && s2.isEmpty()) { return 0; }
		if (s1.isEmpty()) { return 1; }
		if (s2.isEmpty()) { return -1; }
		return s2.compareTo(s1);
	}
}
