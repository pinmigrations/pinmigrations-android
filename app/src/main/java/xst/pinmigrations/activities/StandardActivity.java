package xst.pinmigrations.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.jakewharton.rxrelay.BehaviorRelay;

import butterknife.Unbinder;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;
import xst.pinmigrations.ui.UiUtil;

import static xst.pinmigrations.activities.StandardActivity.Lifecycle.DESTROY;
import static xst.pinmigrations.activities.StandardActivity.Lifecycle.PAUSE;
import static xst.pinmigrations.activities.StandardActivity.Lifecycle.RESUME;
import static xst.pinmigrations.activities.StandardActivity.Lifecycle.START;
import static xst.pinmigrations.activities.StandardActivity.Lifecycle.STOP;

/**
	StandardActivity implements several common activity functions, all optional
	+ retaining data using a fragment
	+ implement a single click-listener, the activity
	+ implement `destroy()`, invoked by this base-class
	+ proper UiService destroy
 	+ implement UpdateRetainedData automatically
 */
public abstract class StandardActivity extends BaseActivity implements Destroyable {
	public enum Lifecycle { CREATE, START, RESUME, PAUSE, STOP, DESTROY }

	@NonNull protected final BehaviorRelay<Lifecycle> lifecycleRelay = BehaviorRelay.create();
	public @NonNull Observable<Lifecycle> getLifecycleRelay() {
		return lifecycleRelay;
	}

	//region UiService
	@Nullable private UiService mUiService;
	public @Nullable UiService getUiService() { return mUiService; }
	protected void setUiService(@NonNull final UiService uiService) { this.mUiService = uiService; }
	//endregion

	protected final CompositeSubscription mCompositeSubscription = new CompositeSubscription();

	//region Butterknife

	@Nullable protected Unbinder mUnbinder;
	//endregion

	//region Lifecycle
	/** invoked when an Activity is finishing, expect full-recreation from `onCreate` after this */
	@Override public abstract void destroy();

	/** manages teardown for retained-data, UiService, UiBinder if they were setup */
	private void _destroy() {
		// Timber.d("destroying " + getSubclassName());

		mCompositeSubscription.unsubscribe();

		if (null != mUiService) {
			mUiService.destroy();
			mUiService = null;
		}

		if (null != mUnbinder) {
			mUnbinder.unbind();
			mUnbinder = null;
		}
	}

	// NOTE: CREATE not relayed since there are probably no objects around when Activity is created
	// their initialization should occur in ctor/inits

	@Override protected void onStart() {
		super.onStart();
		lifecycleRelay.call(START);
	}

	@Override protected void onResume() {
		super.onResume();
		lifecycleRelay.call(RESUME);
	}

	@Override protected void onStop() {
		super.onStop();
		lifecycleRelay.call(STOP);
	}

	@Override protected void onPause() {
		super.onPause();
		lifecycleRelay.call(PAUSE);
	}

	@Override @CallSuper protected void onDestroy() {
		super.onDestroy();

		lifecycleRelay.call(DESTROY);

		destroy(); // sub-class's destroy
		_destroy(); // common cleanup

		// mem-leak hack
		UiUtil.clearTextLineCache();
	}
	//endregion
}
