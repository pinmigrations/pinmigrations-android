package xst.pinmigrations.activities.map;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.maps.GoogleMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import xst.pinmigrations.R;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.maps.MapsService;
import xst.pinmigrations.domain.maps.MapsServiceGenerator;
import xst.pinmigrations.ui.UiUtil;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

/*
	TODO:
		* setup search field and listener here (functionally-identical for all MapMvpViews)
		* determine if ref to Fragment is needed
*/
public abstract class MapMvpViewBase implements Destroyable {
	@Inject MapsServiceGenerator mMapsServiceGenerator;
	@Inject AndroidService mAndroidService;

	MapsService mMapsService;

	@NonNull final GoogleMap mMap;
	@NonNull final View mRootView;
	@Nullable UiService mUiService;

	@BindView(R.id.map_btn1) Button mBtn1;
	@BindView(R.id.map_btn2) Button mBtn2;
	@BindView(R.id.map_btn3) Button mBtn3;
	@BindView(R.id.map_btn4) Button mBtn4;
	@BindView(R.id.map_navButtonsContainer) LinearLayout mNavBtnCtr;
	@BindView(R.id.map_actionButtonsContainer) LinearLayout mActionBtnCtr;


	private Unbinder mUnbinder;
	public boolean isLandscape;
	// TODO: add search field

	/** NOTE: perform DI immediately after invoking super ctor */
	public MapMvpViewBase(@NonNull final GoogleMap map, @NonNull final View rootView) {
		mMap = checkNotNull(map);
		mRootView = checkNotNull(rootView);
		mUnbinder = ButterKnife.bind(this, rootView);
	}

	/* invoke after super-ctor and DI */
	protected void init() {
		isLandscape = UiUtil.isLandscape( mAndroidService.getDisplayMetrics() );
		setupButtons();
		setupMap();
	}

	/** retrieve MapService and setup map defaults. needs to be invoked after DI */
	protected void setupMap() {
		verify(mMapsServiceGenerator != null);
		mMapsService = mMapsServiceGenerator.newMapService(mMap);
		mMapsService.setupMap();
		setupMapListeners();
		fixMapPadding();
	}

	// https://developers.google.com/maps/documentation/android-api/map#map_padding
	protected void fixMapPadding() {
		final int actionBtnCtrHeight = mNavBtnCtr.getHeight();
		if (isLandscape) {
			mMap.setPadding(0, 0, 0, 0); // left, top, right, bottom
		} else {
			mMap.setPadding(0, 0, 0, actionBtnCtrHeight); // left, top, right, bottom
		}
	}

	/** setup button text, listeners */
	abstract void setupButtons();
	/** remove all listeners, clear button text */
	void teardownButtons() {
		resetButton(mBtn1);
		resetButton(mBtn2);
		resetButton(mBtn3);
		resetButton(mBtn4);
	}
	/** LocationPath/PathList */
	abstract void teardownData();
	/** long-click, map de-select, marker/line click */
	abstract void setupMapListeners();
	/** remove */
	void teardownMapListeners() {
		mMap.setOnMarkerClickListener( null );
		mMap.setOnPolylineClickListener( null );
		mMap.setOnMapClickListener( null );
		mMap.setInfoWindowAdapter( null );

		mMap.setOnInfoWindowCloseListener( null );
		mMap.setOnInfoWindowClickListener( null );
	}

	@Override final public void destroy() {
		teardownData();

		if (null != mMapsService) {
			mMapsService.destroy();
			mMapsService = null;
		}

		teardownMapListeners();
		teardownButtons();
		mUnbinder.unbind();
		mUiService = null;
	}

	//region util
	protected void disableButton(@NonNull final Button button) {
		checkNotNull(button).setText("");
		button.setOnClickListener(null);
		button.setVisibility(View.GONE);
		button.setClickable(false);
	}
	protected void resetButton(@NonNull final Button button) {
		checkNotNull(button).setText("");
		button.setOnClickListener(null);
		button.setVisibility(View.VISIBLE);
		button.setClickable(true);
	}
	//

	//region UiService
	@NonNull UiService useUiService() { return checkNotNull( getUiService() ); }
	public void setUiService(@NonNull final UiService uiService) { mUiService = checkNotNull(uiService); }
	@Nullable public UiService getUiService() { return mUiService; }
	public void showQuickNotice(@NonNull final String text) { checkNotNull(mUiService).showQuickNotice(text); }
	public void showImportantMessage(@NonNull final String text) { checkNotNull(mUiService).showImportantMessage(text); }
	//endregion
}
