package xst.pinmigrations.activities.map;

import xst.pinmigrations.ui.mvp.MvpPresenter;

public class Contracts {
	public static class EditMap {
		interface Presenter extends MvpPresenter<MapActivity> { }
		interface MvpView extends xst.pinmigrations.ui.mvp.MvpView<EditMap.Presenter> { }
	}

	public static class ViewAllMap {
		interface Presenter extends MvpPresenter<MapActivity> { }
		interface MvpView extends xst.pinmigrations.ui.mvp.MvpView<ViewAllMap.Presenter> { }
	}

	public static class NewMap {
		interface Presenter extends MvpPresenter<MapActivity> { }
		interface MvpView extends xst.pinmigrations.ui.mvp.MvpView<NewMap.Presenter> { }
	}
}
