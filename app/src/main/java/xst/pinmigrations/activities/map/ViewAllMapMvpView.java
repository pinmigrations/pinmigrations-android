package xst.pinmigrations.activities.map;

import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnPolylineClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.map.Contracts.ViewAllMap;
import xst.pinmigrations.app.IgnoreArg;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.maps.Location;
import xst.pinmigrations.domain.maps.LocationPath;
import xst.pinmigrations.domain.maps.LocationPathList;
import xst.pinmigrations.domain.maps.ui.ViewInfoWindowAdapter;
import xst.pinmigrations.domain.paths.PathList;

import static com.google.common.base.Preconditions.checkNotNull;

/*
	Links:
		* https://developers.google.com/android/reference/com/google/android/gms/maps/GoogleMap.OnMarkerClickListener
		* https://developers.google.com/android/reference/com/google/android/gms/maps/GoogleMap.OnPolylineClickListener
		* https://developers.google.com/android/reference/com/google/android/gms/maps/GoogleMap.OnMapClickListener
*/
public class ViewAllMapMvpView extends MapMvpViewBase implements ViewAllMap.MvpView, Destroyable {
	// mBtn1 -> EditPath
	// mBtn2 -> CloseAll

	ViewAllMapPresenter mPresenter; // technically nullable, but always set immediately after construction
	@NonNull final LocationPathList mLocationPathList;
	@NonNull String selectedGuid = "";

	public ViewAllMapMvpView(@NonNull final GoogleMap map, @NonNull final View rootView, @NonNull final InjectorComponent injector) {
		super(map, rootView);
		injector.inject(this);
		mLocationPathList = new LocationPathList();
		init();
	}
	@Override public void setPresenter(@NonNull final ViewAllMap.Presenter presenter) {
		this.mPresenter = (ViewAllMapPresenter) checkNotNull(presenter);
	}

	void setPathList(@NonNull final PathList pathList) {
		mLocationPathList.replacePaths(pathList);
		mMapsService.renderViewPaths( mLocationPathList );
	}

	void newPath() { mPresenter.newPath(); }
	void editPath() {
		if (selectedGuid.isEmpty()) {
			useUiService().showImportantMessage("Please select a path first");
		} else {
			mPresenter.editPath(selectedGuid);
		}
	}

	void closeAllInfowindows() { /* only one ever open for now */ }

	//region UI setup
	@Override void setupButtons() {
		mBtn1.setText("Edit Path");
		mBtn1.setOnClickListener(mClickListener);

		mBtn2.setText("Close All");
		mBtn2.setOnClickListener(mClickListener);
		mBtn2.setClickable(true); // unclickable on NEW/EDIT
		mBtn2.setVisibility(View.VISIBLE); // hidden on NEW/EDIT

		mBtn3.setText("New");
		mBtn3.setOnClickListener(mClickListener);
		disableButton(mBtn4);
	}
	@Override void setupMapListeners() {
		mMap.setOnMarkerClickListener(markerGuidSelectListener);
		mMap.setOnPolylineClickListener(polyLineGuidSelectListener);
		mMap.setOnMapClickListener(mapClickGuidDeselectListener);

		mMap.setInfoWindowAdapter(new ViewInfoWindowAdapter(mAndroidService.getLayoutInflater()));
	}
	@Override void teardownData() { mLocationPathList.destroy(); }
	protected final View.OnClickListener mClickListener = new View.OnClickListener() {
		@Override public void onClick(final View v) {
			switch( v.getId() ) {
				case R.id.map_btn1: // Edit Path button
					editPath();
					break;
				case R.id.map_btn2: // Close All button
					closeAllInfowindows();
					break;
				case R.id.map_btn3: // New button
					newPath();
					break;
				case R.id.map_btn4: throw new AssertionError("btn4 clicked, should be disabled on ViewAllMap");
			}
		}
	};
	//endregion


	protected final OnMarkerClickListener markerGuidSelectListener = new OnMarkerClickListener() {
		@Override public boolean onMarkerClick(final Marker marker) {
			final Location location = (Location) checkNotNull( marker.getTag() );
			selectedGuid = location.position.guid;
			return false; // also trigger default behavior (camera move / infowindow)
		}
	};
	protected final OnPolylineClickListener polyLineGuidSelectListener = new OnPolylineClickListener() {
		@Override public void onPolylineClick(final Polyline polyline) {
			final LocationPath path = (LocationPath) checkNotNull( polyline.getTag() );
			selectedGuid = path.guid;
		}
	};
	protected final OnMapClickListener mapClickGuidDeselectListener = new OnMapClickListener() {
		@Override public void onMapClick(@IgnoreArg final LatLng ignore) {
			selectedGuid = "";
		}
	};
}
