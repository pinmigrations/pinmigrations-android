package xst.pinmigrations.activities.map;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.security.auth.login.LoginException;

import timber.log.Timber;
import xst.pinmigrations.activities.map.Contracts.EditMap;
import xst.pinmigrations.app.IgnoreArg;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.maps.Location;
import xst.pinmigrations.domain.paths.Path;
import xst.pinmigrations.domain.paths.PathListResource;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.positions.PositionsResource;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.ui.rx.DevUiSubscriber;

import static com.google.common.base.Preconditions.checkNotNull;

public class EditMapPresenter extends MapMvpPresenterBase implements EditMap.Presenter, LocationDialogFragment.EditListener {
	@Inject RxJavaService mRxJavaService;
	@Inject PathListResource mPathListResource;
	@Inject PositionsResource mPositionsResource;
	@Inject LoginManager mLoginManager;

	@NonNull final EditMapMvpView mMapView;
	@NonNull final String mEditGuid;

	protected boolean notifiedDemoMode = false;

	public EditMapPresenter(@NonNull final MapActivity activity, @NonNull final EditMapMvpView mapView,
							@NonNull final InjectorComponent injector, @NonNull GMapFragment fragment,
							@NonNull final String editGuid) {
		super(activity, fragment);
		mMapView = checkNotNull(mapView);
		mMapView.setPresenter(this);
		checkNotNull(injector).inject(this);
		mEditGuid = checkNotNull(editGuid);
		subscribeToLifecycle();
	}

	@Override public void onResume() {
		mCompositeSubscription.add(
				mPathListResource.findByGuid(mEditGuid)
						.observeOn( mRxJavaService.androidMainScheduler() )
						.subscribe(new PathSubscriber())
		);

		if (mLoginManager.notAuthed() && !notifiedDemoMode) {
			notifiedDemoMode = true;
			mMapView.showQuickNotice("not logged in running in demo mode");
		}
	}

	//region nav
	public void newPath() { checkNotNull( getFragment() ).newPath(); }
	public void viewAll() { checkNotNull( getFragment() ).viewAll(); }
	//endregion

	public void editDialogForLocation(@NonNull final Location location) {
		final GMapFragment fragment = checkNotNull( getFragment() );
		fragment.editDialogForLocation(location, this);
	}

	/** new location created on map  */
	public void saveNewLocation(@NonNull final Location unsavedLocation) {
		if (mLoginManager.isAuthed()) {
			try {
				mCompositeSubscription.add(
						mPositionsResource.create(unsavedLocation.position)
								.observeOn(mRxJavaService.androidMainScheduler())
								.subscribe(new CreateLocationSubscriber(unsavedLocation))
				);
			} catch (final LoginException ignore) { // this should not occur
				throw new AssertionError("loginManager reports logged-in, but got login-exception");
			}
		} else { Timber.v("not authed, no save to server"); }
	}

	/** update to server from LocationDialog / MvpView */
	@Override public void updateLocation(@NonNull final Location location) {
		if (mLoginManager.isAuthed()) {
			try {
				mCompositeSubscription.add(
						mPositionsResource.update(location.position)
								.observeOn(mRxJavaService.androidMainScheduler())
								.subscribe(new UpdateLocationSubscriber(location))
				);
			} catch (final LoginException ignore) { // this should not occur
				throw new AssertionError("loginManager reports logged-in, but got login-exception");
			}
		} else { Timber.v("not authed, no update to server"); }
	}

	/** delete from LocationDialog */
	@Override public void deleteLocation(@NonNull final Location location) {
		mMapView.removeLocation( checkNotNull(location) );

		if (mLoginManager.isAuthed()) {
			try {
				mCompositeSubscription.add(
						mPositionsResource.delete(location.position.id)
								.observeOn(mRxJavaService.androidMainScheduler())
								.subscribe(new DeleteLocationSubscriber(location))
				);
			} catch (final LoginException ignore) { // this should not occur
				throw new AssertionError("loginManager reports logged-in, but got login-exception");
			}
		} else { Timber.v("not authed, no delete on server"); }
	}

	// UI over map hides existing icons / infowindows
	@Override public void dialogDismissed(@NonNull Location location) {
		mMapView.editDoneForLocation(location);
	}

	@Override public void destroy() {
		super.destroy();
		mMapView.destroy();
	}

	class PathSubscriber extends DevUiSubscriber<Path> {
		@Override public void onNext(final Path pathList) {
			if ( pathList.getPositionsCount() > 0 ) {
				mMapView.setEditPath(pathList);
			} else {
				mMapView.showImportantMessage("Error: no data");
			}
		}
		@Override public void onError(final Throwable throwable) {
			if (!checkNotNull(mMapView.getUiService()).handlePotentialNetConnectError(throwable)) {
				mMapView.showImportantMessage("an error occured while loading the path");
				Timber.e(throwable);
			}
		}
	}

	static class CreateLocationSubscriber extends DevUiSubscriber<Position> {
		@NonNull final Location unsavedLocation;
		public CreateLocationSubscriber(@NonNull final Location unsavedLocation) {
			this.unsavedLocation = checkNotNull(unsavedLocation);
		}
		@Override public void onCompleted() { }
		@Override public void onNext(final Position position) {
			unsavedLocation.position.id = position.id;
			unsavedLocation.unsavedId = Location.UNSAVED_DEFAULT_ID;
			Timber.v("saved position:" + position);
		}
	}
	static class UpdateLocationSubscriber extends DevUiSubscriber<Position> {
		@NonNull final Location oldLocation;
		public UpdateLocationSubscriber(@NonNull final Location oldLocation) {
			this.oldLocation = checkNotNull(oldLocation);
		}
		@Override public void onCompleted() { }
		@Override public void onNext(final Position position) { Timber.v("updated position:" + position); }
	}
	static class DeleteLocationSubscriber extends DevUiSubscriber<Void> {
		@NonNull final Location oldLocation;
		public DeleteLocationSubscriber(@NonNull final Location oldLocation) {
			this.oldLocation = checkNotNull(oldLocation);
		}
		@Override public void onCompleted() { Timber.v("deleted location:" + oldLocation); }
		@Override public void onNext(@IgnoreArg final Void ignore) { }
	}
}
