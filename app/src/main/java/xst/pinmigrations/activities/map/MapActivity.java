package xst.pinmigrations.activities.map;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ToggleButton;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import xst.pinmigrations.R;
import xst.pinmigrations.activities.StandardActivity;
import xst.pinmigrations.app.IgnoreArg;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceGenerator;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.android_context.GoogleApiService;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.maps.MapsUtil;
import xst.pinmigrations.domain.paths.PathListResource;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.ui.UiUtil;
import xst.pinmigrations.ui.rx.DevUiSubscriber;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;
import static xst.pinmigrations.ui.UiUtil.SCREEN_ROTATE_NORMAL;

public class MapActivity extends StandardActivity
		implements OnMapReadyCallback, DialogInterface.OnCancelListener,
					View.OnClickListener, CompoundButton.OnCheckedChangeListener {
	/* DI services */
	@Inject PathListResource mPathListResource;
	@Inject GoogleApiService mGoogleApiService;
	@Inject UiServiceGenerator mUiServiceGenerator;
	@Inject PrefsManager mPrefsManager;
	@Inject RxJavaService mRxJavaService;
	@Inject AndroidService mAndroidService;

	@BindView(R.id.map_lockRotationBtn) ToggleButton mLockRotationBtn;
	@BindView(R.id.map_activityFrame) MapActivityFrame mMapActivityFrame;

	public static final String MAP_TYPE_TAG = "MapType";
	public static final int VIEW_ALL = 10, NEW = 11, EDIT = 12;
	int mMapType = VIEW_ALL;

	boolean googleApiAvail = false;
	boolean mMapReady = false;

	GMapFragment mMapFragment;
	FrameLayout mFrame;
	boolean gpsUpdateRejected = false;

	@Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_activity);

		getInjector().inject(this); // DI

		setUiService( mUiServiceGenerator.newUiService(this) );

		mFrame = (FrameLayout) findViewById(R.id.map_activityFrame);
		mFrame.setOnClickListener(this);

		final Bundle extras = getIntent().getExtras();
		int mapType = 0;
		if (null != extras) {
			mapType = extras.getInt(MAP_TYPE_TAG);
		}
		if (0 != mapType) { // zero is bundle default also
			mMapType = mapType;
		}

		if (handlePotentialGooglePlayServicesErrors()) {
			continueCreateActivity(); // have GooglePlayServices now
		}
	}

	//region Orientation
	class OrientationPrefSubscriber extends DevUiSubscriber<Integer> {
		@Override public void onNext(final Integer orientation) {
			switch (orientation) {
				case UiUtil.SCREEN_ROTATE_LANDSCAPE:
					UiUtil.lockToLandscape(MapActivity.this);
					mLockRotationBtn.setChecked(true);
					break;
				case UiUtil.SCREEN_ROTATE_PORTRAIT:
					UiUtil.lockToPortrait(MapActivity.this);
					mLockRotationBtn.setChecked(true);
					break;
				case SCREEN_ROTATE_NORMAL:
					mLockRotationBtn.setChecked(false);
					break;
			}

			mLockRotationBtn.setOnCheckedChangeListener(MapActivity.this);
			unsubscribe(); // dont re-exec
		}
	}
	@Override public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
		if (isChecked) {
			UiUtil.lockOrientation(this);
			final int orientationType = UiUtil.getSimpleCurrentOrientation(this);
			mPrefsManager.updateMapOrientationPref(orientationType);
		} else {
			UiUtil.unlockOrientation(this);
			mPrefsManager.updateMapOrientationPref(SCREEN_ROTATE_NORMAL);
		}
	}
	//endregion

	// check fragment, etc..
	void continueCreateActivity() {
		mFrame.setOnClickListener(null); // remove pestering-update click listener
		mFrame = null; // no longer needed

		mMapFragment = (GMapFragment) verifyNotNull( getFragmentManager().findFragmentById(R.id.gmap_fragment) );
		mMapFragment.setMapType(mMapType);

		mMapFragment.getMapAsync(this); // Obtain the MapFragment and get notified when the map is ready to be used.

		mUnbinder = ButterKnife.bind(this);

		// setup initial orientation from prefs
		mCompositeSubscription.add(
			mPrefsManager.getMapOrientationPref()
					.observeOn(mRxJavaService.androidMainScheduler())
					.subscribe(new OrientationPrefSubscriber())
		);
	}

	@Override public void onClick(final View v) { handleGpsClick(); }

	@Override public void destroy() {
		mLockRotationBtn.setOnCheckedChangeListener(null);

		if (null != mFrame) { // already gone if GPS is available
			mFrame.setOnClickListener(null);
			mFrame = null;
		}
	}

	@Override protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		switch(requestCode) {
			case GoogleApiService.UPDATE_GOOGLE_PLAY_SERVICES:
				handleGpsActivityResult(resultCode, data);
				break;
			default: break;
		}
	}

	@Override public void onMapReady(final GoogleMap googleMap) {
		mMapReady = true;

		final int infowindowBottomOffsetDp = MapsUtil.DEFAULT_MARKER_HEIGHT + MapsUtil.DEFAULT_INFOWINDOW_OFFSET;
		final int infowindowBottomOffsetPx = UiUtil.getPixelsFromDp(mAndroidService.getDeviceDensity(), infowindowBottomOffsetDp);
		mMapActivityFrame.init(googleMap, infowindowBottomOffsetPx);
	}

	//region Google Play Services setup

	@Override protected void onResume() {
		super.onResume();
		onResumeGpsCheck();
	}

	boolean handlePotentialGooglePlayServicesErrors() {
		if (mGoogleApiService.isAvailable()) {
			googleApiAvail = true;
			return true;
		}

		final int gpsStatus = mGoogleApiService.queryAvailability();

		if (GoogleApiService.isNetworkIssue(gpsStatus)) {
			checkNotNull( getUiService() ).showImportantMessage("There is a problem connecting with Google Play Services");
		} else if (GoogleApiService.isUpdateRequired(gpsStatus)) {
			mGoogleApiService.getUpdateRequiredErrorDialog(this, this).show();
		} else {
			checkNotNull( getUiService() ).showImportantMessage("Theres an error with Google Play Services on this device");
		}

		googleApiAvail = false;
		return false;
	}
	public void handleGpsActivityResult(final int resultCode, @IgnoreArg final Intent ignore) {
		if (resultCode == Activity.RESULT_OK) {
			if (mGoogleApiService.isAvailable()) {
				continueCreateActivity();
			}
		} else if (resultCode == Activity.RESULT_CANCELED) {
			gpsUpdateRejected = true;
			showGpsUpdateWarning();
		}
	}
	public void handleGpsClick() {
		if (!mMapReady) {
			if (handlePotentialGooglePlayServicesErrors()) { continueCreateActivity(); }
		} else {
			throw new AssertionError("listener should have been removed, play services is available and map is ready");
		}
	}
	/* dialog for updating Google Play services cancel listener */
	@Override public void onCancel(final DialogInterface dialog) { showGpsUpdateWarning(); }
	void showGpsUpdateWarning() {
		gpsUpdateRejected = true;
		checkNotNull( getUiService() ).showImportantMessage("Can't show Map without updating Google Play Services!\nTap anywhere to retry...");
	}
	void showGenericGpsWarning() {
		final UiService uiService = checkNotNull( getUiService() );
		uiService.showImportantMessage("There is a problem connecting with Google Play Services");
		uiService.showImportantMessage("Check your internet connection and then try updating Google Play Services by returning to this screen");
	}
	void onResumeGpsCheck() {
		if (!mMapReady && gpsUpdateRejected) {
			showGenericGpsWarning();
		}
	}
	//endregions
}
