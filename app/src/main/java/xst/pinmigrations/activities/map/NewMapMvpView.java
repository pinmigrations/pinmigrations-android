package xst.pinmigrations.activities.map;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowCloseListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.map.Contracts.NewMap;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.maps.EditableLocationPath;
import xst.pinmigrations.domain.maps.Location;
import xst.pinmigrations.domain.maps.ui.EditableInfoWindowAdapter;
import xst.pinmigrations.domain.positions.PositionsUtil;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;
import static xst.pinmigrations.domain.maps.MapsUtil.CURRENT_MARKER_TAG;

// TODO: determine if DI is needed
public class NewMapMvpView extends MapMvpViewBase implements NewMap.MvpView, Destroyable {
	// mBtn1 -> Add Location
	// mBtn2 (disabled)

	NewMapPresenter mPresenter;
	@NonNull final EditableLocationPath mLocationPath;

	MarkerOptions currentMarkerOptions;
	@Nullable Marker currentMarker;
	EditableInfoWindowAdapter mInfoWindowAdapter;

	public NewMapMvpView(@NonNull final GoogleMap map, @NonNull final View rootView, @NonNull final InjectorComponent injector) {
		super(map, rootView);
		injector.inject(this);
		mLocationPath = new EditableLocationPath( PositionsUtil.newGuid() );
		init();
	}
	@Override public void setPresenter(@NonNull final NewMap.Presenter presenter) {
		this.mPresenter = (NewMapPresenter) checkNotNull(presenter);
	}

	void addPlace() {
		if (null == currentMarker) {
			useUiService().showImportantMessage("Please select a location first");
		} else {
			final Location unsavedLocation = mLocationPath.addNewLocation( checkNotNull( currentMarker.getPosition() ) );
			currentMarker.remove();
			currentMarker = null;

			mMapsService.reRenderEditablePath(mLocationPath);
			mPresenter.saveNewLocation(unsavedLocation);
		}
	}
	void removeLocation(@NonNull final Location location) {
		final int deleteIndex = mLocationPath.locations.indexOf(location);

		if (deleteIndex >= 0) {
			final Location deleteLoc = mLocationPath.locations.remove(deleteIndex);
			deleteLoc.markForDeletion();
		} else {
			throw new AssertionError("invalid location");
		}
	}

	public void editDoneForLocation(@NonNull final Location location) {
		if ( !location.isMarkedForDeletion() ) {
			mInfoWindowAdapter.setEditable();
			mInfoWindowAdapter.setReOpening(true);
			location.hideInfoWindow();
			location.showInfoWindow();
		} else {
			mInfoWindowAdapter.setEditable();
			mInfoWindowAdapter.setReOpening(false);
			mMapsService.reRenderEditablePath(mLocationPath);
		}
	}

	//region nav
	void viewAll() { mPresenter.viewAll(); }
	//endregion

	//region UI setup
	@Override void setupButtons() {
		mBtn1.setText("Add Place");
		mBtn1.setOnClickListener(mClickListener);

		disableButton(mBtn2);

		mBtn3.setText("View All");
		mBtn3.setOnClickListener(mClickListener);

		disableButton(mBtn4);
	}
	@Override void setupMapListeners() {
		mMap.setOnMapClickListener(mapClickCurrentMarkerListener);
		mMap.setOnMarkerDragListener(markderDragListener);
		mInfoWindowAdapter = new EditableInfoWindowAdapter(mAndroidService.getLayoutInflater());
		mMap.setInfoWindowAdapter(mInfoWindowAdapter);
		mMap.setOnInfoWindowClickListener(infowindowClickListener);
		mMap.setOnInfoWindowCloseListener(infowindowCloseListener);

		currentMarkerOptions = verifyNotNull( mMapsService.getCurrentMarkerOptions() );
	}
	@Override void teardownData() { mLocationPath.destroy(); }
	@Override void teardownMapListeners() {
		super.teardownMapListeners();
		mInfoWindowAdapter.destroy();
		mInfoWindowAdapter = null;
	}

	protected final View.OnClickListener mClickListener = new View.OnClickListener() {
		@Override public void onClick(final View v) {
			switch( v.getId() ) {
				case R.id.map_btn1: // Add Place button
					addPlace();
					break;
				case R.id.map_btn2: throw new AssertionError("btn2 clicked, should be disabled on NewMap");
				case R.id.map_btn3: // View All button
					viewAll();
					break;
				case R.id.map_btn4: throw new AssertionError("btn4 clicked, should be disabled on NewMap");
			}
		}
	};
	protected final OnMapClickListener mapClickCurrentMarkerListener = new OnMapClickListener() {
		@Override public void onMapClick(final LatLng currentClickPosition) {
			if (null == currentMarker) { // removed after adding, etc..
				currentMarkerOptions.position(currentClickPosition);
				currentMarker = mMap.addMarker( currentMarkerOptions );
				currentMarker.setTag(CURRENT_MARKER_TAG);
			} else { // just moved from place to place
				currentMarker.setPosition( currentClickPosition );
			}
		}
	};
	protected final OnMarkerDragListener markderDragListener = new OnMarkerDragListener() {
		@Override public void onMarkerDragStart(final Marker marker) { }
		@Override public void onMarkerDrag(final Marker marker) { }
		@Override public void onMarkerDragEnd(final Marker marker) {
			final Object markerTag = checkNotNull( marker.getTag() );

			//noinspection ObjectEquality (object's purpose is equality-checking)
			if (CURRENT_MARKER_TAG == markerTag) { return; } // dont care about dragging current marker

			final Location location = (Location) markerTag;
			final LatLng newPosition = marker.getPosition();
			location.position.lat = newPosition.latitude;
			location.position.lng = newPosition.longitude;
			location.latLng = newPosition;

			mPresenter.updateLocation(location);

			mMapsService.reRenderEditablePath(mLocationPath);
		}
	};
	protected final OnInfoWindowClickListener infowindowClickListener = new OnInfoWindowClickListener() {
		@Override public void onInfoWindowClick(final Marker marker) {
			final Location location = (Location) checkNotNull( marker.getTag() );

			if ( !mInfoWindowAdapter.isEditing() ) {
				mInfoWindowAdapter.setEditing();
				mInfoWindowAdapter.setReOpening(true);
				marker.hideInfoWindow();
				marker.showInfoWindow();
				mPresenter.editDialogForLocation(location);
			}
		}
	};
	protected final OnInfoWindowCloseListener infowindowCloseListener = new OnInfoWindowCloseListener() {
		@Override public void onInfoWindowClose(final Marker marker) {
			if ( mInfoWindowAdapter.isReOpening() ) {
				mInfoWindowAdapter.setReOpening(false);
			} else if ( mInfoWindowAdapter.isEditing() ) {
				mInfoWindowAdapter.setEditable();
			}
		}
	};
	//endregion
}
