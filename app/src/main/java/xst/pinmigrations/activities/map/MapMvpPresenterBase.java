package xst.pinmigrations.activities.map;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

import xst.pinmigrations.ui.mvp.BasePresenter;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class MapMvpPresenterBase extends BasePresenter<MapActivity> {

	private final WeakReference<GMapFragment> fragmentRef;
	public final @Nullable GMapFragment getFragment() { return fragmentRef.get(); }

	public MapMvpPresenterBase(@NonNull final MapActivity activity, @NonNull GMapFragment fragment) {
		super(activity);
		this.fragmentRef = new WeakReference<GMapFragment>( checkNotNull(fragment) );
	}

	// invoked after nav changes, map activity done
	@CallSuper @Override public void destroy() {
		super.destroy();
		fragmentRef.clear();
	}
}
