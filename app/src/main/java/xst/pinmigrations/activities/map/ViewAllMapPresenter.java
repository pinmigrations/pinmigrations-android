package xst.pinmigrations.activities.map;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import timber.log.Timber;
import xst.pinmigrations.activities.map.Contracts.ViewAllMap;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.domain.paths.PathList;
import xst.pinmigrations.domain.paths.PathListResource;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.ui.rx.DevUiSubscriber;

import static com.google.common.base.Preconditions.checkNotNull;

public class ViewAllMapPresenter extends MapMvpPresenterBase implements ViewAllMap.Presenter {
	@Inject RxJavaService mRxJavaService;
	@Inject PathListResource mPathListResource;

	@NonNull final ViewAllMapMvpView mMapView;

	public ViewAllMapPresenter(@NonNull final MapActivity activity, @NonNull final ViewAllMapMvpView mapView,
							   @NonNull final InjectorComponent injector, @NonNull GMapFragment fragment) {
		super(activity, fragment);
		mMapView = checkNotNull(mapView);
		mMapView.setPresenter(this);
		checkNotNull(injector).inject(this);
		subscribeToLifecycle();
	}

	@CallSuper @Override public void destroy() {
		super.destroy();
		mMapView.destroy();
	}

	@Override public void onResume() {
		mCompositeSubscription.add(
				mPathListResource.getAllPaths()
						.observeOn( mRxJavaService.androidMainScheduler() )
						.subscribe(new PathListSubscriber())
		);
	}

	//region NAV
	public void editPath(@NonNull final String guid) {
		checkNotNull( getFragment() ).editPath(guid);
	}
	public void newPath() {
		checkNotNull( getFragment() ).newPath();
	}
	//endregion

	class PathListSubscriber extends DevUiSubscriber<PathList> {
		@Override public void onNext(final PathList pathList) {
			if (pathList.getPositionsCount() > 0) {
				mMapView.setPathList(pathList);
			} else {
				mMapView.showImportantMessage("Error: no data");
			}
		}
		@Override public void onError(final Throwable throwable) {
			if (!checkNotNull(mMapView.getUiService()).handlePotentialNetConnectError(throwable)) {
				mMapView.getUiService().showImportantMessage("an error occured while loading positions");
				Timber.e(throwable);
			}
		}
	}
}
