package xst.pinmigrations.activities.map;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import static com.google.common.base.Preconditions.checkNotNull;

/*
http://stackoverflow.com/a/15040761/1163940
 */

public class MapActivityFrame extends FrameLayout {
	protected GoogleMap mMap;
	protected int mBottomOffsetPixels;

	/**
	 * A currently selected marker
	 */
	protected Marker mMarker;

	/**
	 * Our custom view which is returned from either the InfoWindowAdapter.getInfoContents
	 * or InfoWindowAdapter.getInfoWindow
	 */
	protected View mInfoWindow;

	//region CTOR

	public MapActivityFrame(@NonNull final Context context) { this(context, null, 0); }
	public MapActivityFrame(@NonNull final Context context, @Nullable final AttributeSet attrs) { this(context, attrs, 0); }
	public MapActivityFrame(@NonNull final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
		super(checkNotNull(context), attrs, defStyleAttr);
	}
	//endregion


	/**
	 * Must be called before we can route the touch events
	 */
	public void init(@NonNull final GoogleMap map, final int bottomOffsetPixels) {
		mMap = map;
		mBottomOffsetPixels = bottomOffsetPixels;
	}

	/**
	 * Best to be called from either the InfoWindowAdapter.getInfoContents
	 * or InfoWindowAdapter.getInfoWindow.
	 */
	public void setMarkerWithInfoWindow(@NonNull final Marker marker, @NonNull final View infoWindow) {
		mMarker = checkNotNull(marker);
		mInfoWindow = checkNotNull(infoWindow);
	}

	@Override public boolean dispatchTouchEvent(final MotionEvent event) {
		boolean eventConsumed = false;

		if (mMarker != null && mMarker.isInfoWindowShown() && mMap != null && mInfoWindow != null) {
			// get a marker position on the screen
			final Point point = mMap.getProjection().toScreenLocation(mMarker.getPosition());

			// Make a copy of the MotionEvent and adjust it's location
			// so it is relative to the infoWindow left top corner
			final MotionEvent copyEv = MotionEvent.obtain(event);
			copyEv.offsetLocation(
					-point.x + (mInfoWindow.getWidth() / 2),
					-point.y + mInfoWindow.getHeight() + mBottomOffsetPixels);

			// Dispatch the adjusted MotionEvent to the infoWindow
			eventConsumed = mInfoWindow.dispatchTouchEvent(copyEv);
		}

		// If the infoWindow consumed the touch event, then just return true.
		// Otherwise pass this event to the super class and return it's result
		return eventConsumed || super.dispatchTouchEvent(event);
	}
}
