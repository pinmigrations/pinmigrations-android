package xst.pinmigrations.activities.map;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import xst.pinmigrations.R;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.maps.Location;

import static com.google.common.base.Preconditions.checkNotNull;

public class LocationDialogFragment extends DialogFragment implements Destroyable {
	public static final String DIALOG_FRAGMENT_TAG = "Maps::LocationDialog";

	@Inject AndroidService mAndroidService;

	@BindView(R.id.location_edit_dialog_description) EditText mDescriptionText;
	Unbinder mUnbinder;

	Location mEditLocation;
	@Nullable EditListener mEditListener;

	/** 1st in creation sequence */
	@Override public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final MapActivity activity = ((MapActivity) checkNotNull(getActivity()) );
		activity.getInjector().inject(this);
	}

	public void setListener(@NonNull final EditListener listener) { mEditListener = checkNotNull(listener); }
	public void setEditLocation(@NonNull final Location editLocation) { mEditLocation = checkNotNull(editLocation); }

	/** 2nd in creation sequence */
	@Override public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final Dialog dialog = new Dialog(getActivity(), getTheme());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setTitle("Location Edit Dialog"); // not visible, used in LayoutInspector
		return dialog;
	}

	// todo: fix race-condition here with setEditLocation()
	/** 3rd in creation sequence */
	@Override public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
									   final Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.map_location_edit_dialog, container);
		mUnbinder = ButterKnife.bind(this, rootView);

		mDescriptionText.setText( checkNotNull(mEditLocation).position.description );
		return rootView;
	}

	@OnClick(R.id.location_edit_dialog_save)
	public void saveLocation() {
		mEditLocation.updateDescription( mDescriptionText.getText().toString() );
		checkNotNull(mEditListener).updateLocation( mEditLocation );
		finish();
	}

	@OnClick(R.id.location_edit_dialog_delete)
	public void deleteLocation() {
		checkNotNull(mEditListener).deleteLocation(mEditLocation);
		finish();
	}

	protected void finish() {
		dismiss();
	}

	@Override public void destroy() {
		if (null != mEditLocation) {
			mEditLocation = null;
		}

		if (null != mUnbinder) {
			mUnbinder.unbind();
			mUnbinder = null;
		}
	}

	@Override public void onDismiss(final DialogInterface dialog) {
		super.onDismiss(dialog);
		checkNotNull(mEditListener).dialogDismissed(mEditLocation);
	}

	@Override public void onDestroy() {
		super.onDestroy();
		destroy();
	}

	public interface EditListener {
		void updateLocation(@NonNull Location location);
		void deleteLocation(@NonNull Location location);
		void dialogDismissed(@NonNull Location location);
	}
}
