package xst.pinmigrations.activities.map;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;

import butterknife.ButterKnife;
import timber.log.Timber;
import xst.pinmigrations.R;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;
import xst.pinmigrations.domain.maps.Location;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static xst.pinmigrations.activities.map.MapActivity.EDIT;
import static xst.pinmigrations.activities.map.MapActivity.NEW;
import static xst.pinmigrations.activities.map.MapActivity.VIEW_ALL;

public class GMapFragment extends MapFragment implements OnMapReadyCallback, Destroyable {
	@Nullable View mRootView;
	@Nullable GoogleMap mMap;

	int mMapType = VIEW_ALL;
	@NonNull String mEditGuid = "";

	@Nullable ViewAllMapPresenter mViewAllPresenter;
	@Nullable NewMapPresenter mNewPresenter;
	@Nullable NewMapMvpView mNewMvpView;
	@Nullable EditMapPresenter mEditPresenter;
	@Nullable EditMapMvpView mEditMvpView;

	@Override public void onCreate(@Nullable final Bundle bundle) {
		super.onCreate(bundle);
		this.getMapAsync(this);
	}

	// TODO: save mMapType/mEditGuid in onSaveInstanceState ?

	// map UI setup
	@Override public View onCreateView(final LayoutInflater inflater, final ViewGroup group, final Bundle bundle) {
		final View mapView = super.onCreateView(inflater, group, bundle);

		mRootView = inflater.inflate(R.layout.map_fragment, group, false);
		final FrameLayout mapFrame = (FrameLayout) mRootView.findViewById(R.id.map_fragmentFrame);

		// place at top of FrameLayout so it is behind buttons
		mapFrame.addView(mapView, 0);

		ButterKnife.bind(this, mRootView);

		return mRootView;
	}

	public void setMapType(final int mapType) {
		mMapType = mapType;
	}
	public void setEditGuid(@NonNull final String guid) {
		verify( !checkNotNull(guid).isEmpty(), "empty/null guid");
		mEditGuid = guid;
	}

	/* setup UI after map is ready */
	@Override public void onMapReady(final GoogleMap map) {
		mMap = checkNotNull(map);
		setupMvp(mMap);
	}

	//region MVP setup
	void setupMvp(@NonNull final GoogleMap map) {
		final MapActivity activity = (MapActivity) checkNotNull( getActivity() );
		final View rootView = checkNotNull(mRootView);
		final UiService uiService = checkNotNull( activity.getUiService() );
		final InjectorComponent injector = activity.getInjector();

		switch (mMapType) {
			case VIEW_ALL: setupViewAllMap(map, activity, rootView, uiService, injector); break;
			case NEW: setupNewMap(map, activity, rootView, uiService, injector); break;
			case EDIT: setupEditMap(map, activity, rootView, uiService, injector); break;
			default: throw new AssertionError("invalid map type");
		}
	}

	void setupViewAllMap(@NonNull final GoogleMap map, @NonNull final MapActivity activity,
						 @NonNull final View rootView, @NonNull final UiService uiService,
						 @NonNull final InjectorComponent injector) {
		final ViewAllMapMvpView viewAllMvpView = new ViewAllMapMvpView(map, rootView, injector);
		viewAllMvpView.setUiService( uiService );
		mViewAllPresenter = new ViewAllMapPresenter(activity, viewAllMvpView, injector, this);

	}
	// TODO: determine if this fragment needs to keep references to mvp view/presenter
	void setupNewMap(@NonNull final GoogleMap map, @NonNull final MapActivity activity,
					 @NonNull final View rootView, @NonNull final UiService uiService,
					 @NonNull final InjectorComponent injector) {
		mNewMvpView = new NewMapMvpView(map, rootView, injector);
		mNewMvpView.setUiService( uiService );
		mNewPresenter = new NewMapPresenter(activity, mNewMvpView, injector, this);
	}
	// TODO: determine if this fragment needs to keep references to mvp view/presenter
	void setupEditMap(@NonNull final GoogleMap map, @NonNull final MapActivity activity,
					  @NonNull final View rootView, @NonNull final UiService uiService,
					  @NonNull final InjectorComponent injector) {
		verify(!mEditGuid.isEmpty(), "edit-guid is empty");
		mEditMvpView = new EditMapMvpView(map, rootView, injector, mEditGuid);
		mEditMvpView.setUiService( uiService );
		mEditPresenter = new EditMapPresenter(activity, mEditMvpView, injector, this, mEditGuid);
	}
	//endregion

	//region NAV (presenter callbacks)

	// start edit-map MVP
	public void editPath(@NonNull final String guid) {
		teardownExistingPresenters();

		setEditGuid(guid);
		setMapType(EDIT);
		setupMvp( checkNotNull(mMap) );
	}
	// start new-map MVP
	public void newPath() {
		teardownExistingPresenters();
		setMapType(NEW);
		setupMvp( checkNotNull(mMap) );
	}
	// start view-all-map MVP
	public void viewAll() {
		teardownExistingPresenters();
		setMapType(VIEW_ALL);
		setupMvp( checkNotNull(mMap) );
	}
	protected void teardownExistingPresenters() {
		if (null != mViewAllPresenter) {
			mViewAllPresenter.destroy();
			mViewAllPresenter = null;
		}
		if (null != mNewPresenter) {
			mNewPresenter.destroy();
			mNewPresenter = null;
		}
		if (null != mEditPresenter) {
			mEditPresenter.destroy();
			mEditPresenter = null;
		}
	}
	//endregion

	public void editDialogForLocation(@NonNull final Location location,
									  @NonNull final LocationDialogFragment.EditListener listener) {
		final MapActivity activity = (MapActivity) checkNotNull( getActivity() );
		final FragmentManager fragmentManager = activity.getFragmentManager();

		final Fragment existingFragment = fragmentManager.findFragmentByTag(LocationDialogFragment.DIALOG_FRAGMENT_TAG);
		if (existingFragment != null) {
			Timber.d("removing existing dialog fragment");
			fragmentManager.beginTransaction().remove(existingFragment).commit(); // remove if one is already loaded
		}

		final LocationDialogFragment dialogFragment = new LocationDialogFragment();
		dialogFragment.setEditLocation( checkNotNull(location) );
		dialogFragment.setListener(listener);
		dialogFragment.show(fragmentManager, LocationDialogFragment.DIALOG_FRAGMENT_TAG);
	}

	@Override public void destroy() {
		mViewAllPresenter = null;
		mNewPresenter = null;
		mNewMvpView = null;
		mEditPresenter = null;
		mEditMvpView = null;
	}
}
