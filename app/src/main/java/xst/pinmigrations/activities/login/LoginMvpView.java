package xst.pinmigrations.activities.login;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import xst.pinmigrations.R;
import xst.pinmigrations.activities.login.LoginContract.MvpView;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;

import static com.google.common.base.Preconditions.checkNotNull;

public class LoginMvpView extends LinearLayout implements MvpView, Destroyable {

	@BindView(R.id.login_loginBtn) Button mLoginBtn;
	@BindView(R.id.login_logoutBtn) Button mLogoutBtn;
	@BindView(R.id.login_emailEdit) EditText mUserEmailEdit;
	@BindView(R.id.login_passwordEdit) EditText mUserPasswordEdit;
	protected @Nullable Unbinder mUnbinder;

	protected @Nullable LoginPresenter mLoginPresenter;
	protected @Nullable UiService mUiService;


	//region CTOR

	public LoginMvpView(@NonNull final Context context) { this(context, null, 0); }
	public LoginMvpView(@NonNull final Context context, @Nullable final AttributeSet attrs) { this(context, attrs, 0); }
	public LoginMvpView(@NonNull final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
		super(checkNotNull(context), attrs, defStyleAttr);
		init();
	}
	//endregion

	protected void init() {
		LayoutInflater.from(getContext()).inflate(R.layout.login_mvp_view, this, true);
		// bind after inflate!
		mUnbinder = ButterKnife.bind(this);
		bindClickListeners();
	}

	@Override public void destroy() {
		if (null != mLoginBtn) { mLoginBtn.setOnClickListener(null); }
		if (null != mLogoutBtn) { mLogoutBtn.setOnClickListener(null); }
		if (null != mUnbinder) { mUnbinder.unbind(); mUnbinder = null; }
		mUiService = null; // destroyed by activity
		mLoginPresenter = null; // destroyed by activity
	}


	/** update UI to reflect login state */
	public void showLoggedIn(final boolean loggedIn) {
		if (loggedIn) {
			mLoginBtn.setVisibility(View.GONE);
			mLogoutBtn.setVisibility(View.VISIBLE);

			mUserEmailEdit.setVisibility(View.GONE);
			mUserPasswordEdit.setVisibility(View.GONE);
		} else {
			mLoginBtn.setVisibility(View.VISIBLE);
			mLogoutBtn.setVisibility(View.GONE);

			mUserEmailEdit.setVisibility(View.VISIBLE);
			mUserPasswordEdit.setVisibility(View.VISIBLE);
		}

		invalidateViews();
	}

	void invalidateViews() {
		mLogoutBtn.invalidate();
		mLoginBtn.invalidate();
		mUserEmailEdit.invalidate();
		mUserPasswordEdit.invalidate();
		this.invalidate();
	}

	public void login() {
		final String userEmail = mUserEmailEdit.getText().toString();
		final String userPassword = mUserPasswordEdit.getText().toString();

		if (userEmail.length() == 0 || userPassword.length() == 0) {
			checkNotNull(mUiService).showImportantMessage("Please complete all fields");
			return;
		}

		// TODO: show spinner or something
		checkNotNull(mLoginPresenter).hideSoftKeyboard();
		checkNotNull(mLoginPresenter).login(userEmail, userPassword);
	}

	public void logout() {
		// TODO: show spinner or something
		checkNotNull(mLoginPresenter).logout();
	}

	//region View setup

	void bindClickListeners() {
		mLoginBtn.setOnClickListener( viewClickListener );
		mLogoutBtn.setOnClickListener( viewClickListener );
	}
	protected final View.OnClickListener viewClickListener = new View.OnClickListener() {
		@Override public void onClick(final View v) {
			switch (v.getId()) {
				case R.id.login_loginBtn: login(); break;
				case R.id.login_logoutBtn: logout(); break;
			}
		}
	};

	@Override public void setPresenter(@NonNull final LoginContract.Presenter presenter) {
		mLoginPresenter = (LoginPresenter) checkNotNull(presenter); // TODO: update after abstracting out methods into interface
	}

	@Override public void setUiService(@NonNull final UiService uiService) {
		mUiService = checkNotNull(uiService);
	}
	@Nullable @Override public UiService getUiService() { return mUiService; }

	@Override public void showQuickNotice(@NonNull final String text) {
		checkNotNull(mUiService).showQuickNotice(text);
	}
	@Override public void showImportantMessage(@NonNull final String text) {
		checkNotNull(mUiService).showImportantMessage(text);
	}
	//endregion
}
