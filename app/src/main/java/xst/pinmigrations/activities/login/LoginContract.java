package xst.pinmigrations.activities.login;
import xst.pinmigrations.ui.mvp.MvpPresenter;

public class LoginContract {
	interface MvpView extends xst.pinmigrations.ui.mvp.MvpView<Presenter> { }
	interface Presenter extends MvpPresenter<LoginActivity> { }
}
