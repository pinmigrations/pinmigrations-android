package xst.pinmigrations.activities.login;

import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.StandardActivity;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceGenerator;

import static com.google.common.base.Verify.verifyNotNull;

/** MVP activity for logging in / logging out
 * LoginPresenter
 * https://github.com/Syhids/android-architecture/tree/todo-mvp-fragmentless
 */
public class LoginActivity extends StandardActivity {
	@Nullable LoginPresenter mLoginPresenter;
	@Inject UiServiceGenerator mUiServiceGenerator;

	@Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getInjector().inject(this); // DI

		final boolean checkRemote = (null == savedInstanceState); // null on first load

		setContentView(R.layout.login_activity); // NOTE: this creates `LoginView`
		final LoginMvpView loginView = (LoginMvpView) verifyNotNull(findViewById(R.id.login_mvpView));

		setUiService(mUiServiceGenerator.newUiService(this));
		mLoginPresenter = new LoginPresenter(this, loginView, checkRemote);
	}

	@Override public void destroy() { /* no extra cleanup needed */ }
}
