package xst.pinmigrations.activities.login;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.login.Login;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.LoginResource;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.ui.mvp.BasePresenter;
import xst.pinmigrations.ui.rx.DevUiSubscriber;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class LoginPresenter extends BasePresenter<LoginActivity> implements LoginContract.Presenter, Destroyable {
	/* DI  */
	@Inject LoginResource mLoginResource;
	@Inject LoginManager mLoginManager;
	@Inject RxJavaService mRxJavaService;

	@NonNull final LoginMvpView mLoginView;

	volatile boolean checkRemote;
	protected final CompositeSubscription mCompositeSubscription = new CompositeSubscription();

	public LoginPresenter(@NonNull final LoginActivity activity, @NonNull final LoginMvpView loginView, final boolean checkRemote) {
		super( activity );
		activity.getInjector().inject(this); // DI

		this.checkRemote = checkRemote;

		mLoginView = checkNotNull(loginView);
		mLoginView.setPresenter(this);
		mLoginView.setUiService( checkNotNull(activity.getUiService()) );

		if (!checkRemote) {
			softLoginCheck();
		}

		subscribeToLifecycle();
	}

	@Override public void destroy() {
		super.destroy();
		mLoginView.destroy();
	}

	public void onResume() {
		if (checkRemote) {
			checkRemote = false;
			remoteLoginCheck();
		}
	}

	void softLoginCheck() {
		mCompositeSubscription.add(
			mLoginResource.softLoginCheck()
					.observeOn(mRxJavaService.androidMainScheduler())
					.subscribe( mCheckSignInSubscriber = new LoginActivityUpdateSubscriber() )
		);
	}
	void remoteLoginCheck() {
		mCompositeSubscription.add(
			mLoginResource.checkLoginStatus()
					.observeOn(mRxJavaService.androidMainScheduler())
					.subscribe( mCheckSignInSubscriber = new LoginActivityUpdateSubscriber() )
		);
	}

	public void login(@NonNull final String email, @NonNull final String password) {
		checkArgument( !checkNotNull(email).isEmpty(), "email cant be empty-string");
		checkArgument( !checkNotNull(password).isEmpty(), "password cant be empty-string");

		mLoginResource.login(email, password)
				.observeOn(mRxJavaService.androidMainScheduler())
				.subscribe(mLoginSubscriber = new LoginSubscriber());
	}

	public void logout() {
		mLoginResource.logout()
				.observeOn(mRxJavaService.androidMainScheduler())
				.subscribe(mLogoutSubscriber = new LogoutSubscriber());
	}

	Subscriber<Login> mCheckSignInSubscriber;
	Subscriber<Login> mLoginSubscriber;
	class LoginSubscriber extends LoginActivityUpdateSubscriber {
		@Override public void afterNext(final Login loginObj) {
			checkArgument(!loginObj.isUnauthed(), "tried logging out but login-state is now UNAUTHED");

			if (loginObj.isLoggedIn()) {
				mLoginView.showQuickNotice("Logged In");
			} else if (loginObj.loginAttemptFailed()) {
				mLoginView.showImportantMessage("Log In Failed! Check your email/pass");
			} else {
				throw new AssertionError("tried logging in and got invalid login-state");
			}
		}
	}
	Subscriber<Login> mLogoutSubscriber;
	class LogoutSubscriber extends LoginActivityUpdateSubscriber {
		@Override public void afterNext(final Login loginObj) {
			checkArgument(!loginObj.loginAttemptFailed(), "tried logging out but login-state is now LOGIN_FAILED");
			checkArgument(!loginObj.isLoggedIn(), "tried logging out but login-state is now LOGGED_IN");

			if (loginObj.isUnauthed()) {
				mLoginView.showQuickNotice("Logged Out");
			} else {
				throw new AssertionError("tried logging out and got invalid login-state");
			}
		}
	}

	// base class for check-sign-in, login, logout
	class LoginActivityUpdateSubscriber extends DevUiSubscriber<Login> {
		// final instead of @CallSuper, need to always update UI in same way
		@Override public final void onNext(final Login loginObj) {
			mLoginView.showLoggedIn( loginObj.isLoggedIn() );
			afterNext(loginObj);
		}

		public void afterNext(final Login loginObj) { }
		@Override public void onError(final Throwable throwable) {
			if (!checkNotNull(mLoginView.getUiService()).handlePotentialNetConnectError(throwable)) {
				super.onError(throwable);
			}
		}
	}
}
