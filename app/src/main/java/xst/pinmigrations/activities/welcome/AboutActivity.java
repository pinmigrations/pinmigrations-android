package xst.pinmigrations.activities.welcome;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import xst.pinmigrations.R;
import xst.pinmigrations.activities.StandardActivity;

public class AboutActivity extends StandardActivity {
	@BindView(R.id.about_intro_text) TextView mIntroTextView;
	@BindView(R.id.about_usage_text) TextView mUsageTextView;
	@BindView(R.id.about_more_info) TextView mMoreInfoTextView;

	@Override protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_activity);

		mUnbinder = ButterKnife.bind(this);


		final MovementMethod linkMovementMethod = LinkMovementMethod.getInstance();
		mIntroTextView.setMovementMethod(linkMovementMethod);
		mUsageTextView.setMovementMethod(linkMovementMethod);
		mMoreInfoTextView.setMovementMethod(linkMovementMethod);
	}

	@Override public void destroy() { }
}
