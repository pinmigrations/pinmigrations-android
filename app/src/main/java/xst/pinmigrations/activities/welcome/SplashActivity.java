package xst.pinmigrations.activities.welcome;

import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.ui.UiUtil;

public class SplashActivity extends BaseActivity {
	public static final int INIT_DELAY_MS = 500;
	@Override protected void onResume() {
		super.onResume();

		new Thread(new InitDelayRunnable(INIT_DELAY_MS)).start();
	}

	class InitDelayRunnable implements Runnable {
		final int delayMs;

		public InitDelayRunnable(final int delayMs) { this.delayMs = delayMs; }

		@Override public void run() {
			try {
				Thread.sleep(delayMs);
			} catch (final InterruptedException ignore) { }
			finally {
				UiUtil.startActivity(SplashActivity.this, WelcomeActivity.class);
				SplashActivity.this.finish(); // dont keep this on the backstack
			}
		}
	}
}
