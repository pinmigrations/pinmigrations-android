package xst.pinmigrations.activities.welcome;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;
import xst.pinmigrations.R;
import xst.pinmigrations.activities.StandardActivity;
import xst.pinmigrations.activities.admin.AdminActivity;
import xst.pinmigrations.activities.dev_debug.DevDebugActivity;
import xst.pinmigrations.activities.login.LoginActivity;
import xst.pinmigrations.activities.map.MapActivity;
import xst.pinmigrations.app.App;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceGenerator;
import xst.pinmigrations.domain.login.Login;
import xst.pinmigrations.domain.login.LoginResource;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.ui.UiUtil;
import xst.pinmigrations.ui.rx.DevUiSubscriber;

import static com.google.common.base.Preconditions.checkNotNull;


/** MVC activity that is first logical activity in app
 * + buttons to other activities in app
 * + determines login status to show login vs logout
 */
public class WelcomeActivity extends StandardActivity implements View.OnClickListener {
	@Inject LoginResource mLoginResource;
	@Inject RxJavaService mRxJavaService;
	@Inject UiServiceGenerator mUiServiceGenerator;

	@BindView(R.id.welcome_loginBtn) Button mLoginBtn;
	@BindView(R.id.welcome_logoutBtn) Button mLogoutBtn;
	@BindView(R.id.welcome_mainLayout) View mMainLayout;
	@BindView(R.id.welcome_devDebugBtn) Button mDevDebugBtn;
	@BindView(R.id.welcome_adminActBtn) Button mAdminBtn;


	@Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome_activity);

		getInjector().inject(this);
		mUnbinder = ButterKnife.bind(this);

		setUiService(mUiServiceGenerator.newUiService(this));

		if (App.isDebugBuild()) {
			mDevDebugBtn.setVisibility(View.VISIBLE);
		} else {
			mDevDebugBtn.setVisibility(View.INVISIBLE);
		}
	}


	@Override protected void onResume() {
		super.onResume();

		if (mLoginResource.isInitialized()) { checkSignInImmediate(); }
		else { checkSignInDelayed(); }
	}

	void checkSignInImmediate() {
		mCompositeSubscription.add(
				mLoginResource.softLoginCheck()
						.observeOn(mRxJavaService.androidMainScheduler())
						.subscribe(mLoginSubscriber = new LoginSubscriber())
		);
	}
	void checkSignInDelayed() {
		mCompositeSubscription.add(
				mRxJavaService.delayedExecutionObservable(mLoginResource.softLoginCheck(), 0.3, TimeUnit.SECONDS)
						.observeOn(mRxJavaService.androidMainScheduler())
						.subscribe(mLoginSubscriber = new LoginSubscriber())
		);
	}

	@OnClick({R.id.welcome_mapActBtn,
			R.id.welcome_loginBtn,
			R.id.welcome_logoutBtn,
			R.id.welcome_adminActBtn,
			R.id.welcome_aboutBtn,
			R.id.welcome_devDebugBtn})
	@Override public void onClick(final View v) {
		Class<? extends StandardActivity> targetActClass;
		switch (v.getId()) {
			case R.id.welcome_adminActBtn:
				targetActClass = AdminActivity.class;
				break;
			case R.id.welcome_mapActBtn:
				targetActClass = MapActivity.class;
				break;
			case R.id.welcome_loginBtn: // intentional fall-through
			case R.id.welcome_logoutBtn:
				targetActClass = LoginActivity.class;
				break;
			case R.id.welcome_devDebugBtn:
				targetActClass = DevDebugActivity.class;
				break;
			case R.id.welcome_aboutBtn:
				targetActClass = AboutActivity.class;
				break;
			default: return;
		}

		UiUtil.startActivity(this, targetActClass);
	}

	protected void setLoginStatus(final boolean loggedIn) {
		if (loggedIn) {
			mLoginBtn.setVisibility(View.GONE);
			mLogoutBtn.setVisibility(View.VISIBLE);

			mAdminBtn.setVisibility(View.VISIBLE);
		} else {
			mLoginBtn.setVisibility(View.VISIBLE);
			mLogoutBtn.setVisibility(View.GONE);

			mAdminBtn.setVisibility(View.GONE);
		}

		invalidateViews();
	}

	void invalidateViews() {
		mLogoutBtn.invalidate();
		mLoginBtn.invalidate();
		mMainLayout.invalidate();
	}

	Subscriber<Login> mLoginSubscriber;
	class LoginSubscriber extends DevUiSubscriber<Login> {
		@Override public void onNext(final Login login) {
			setLoginStatus(login.isLoggedIn());
		}
		@Override public void onError(final Throwable throwable) {
			if (throwable instanceof UnknownHostException) {
				checkNotNull(getUiService()).showImportantMessage("No Internet Connection!");
			} else {
				super.onError(throwable);
			}
		}
	}


	@Override public void destroy() { }
}
