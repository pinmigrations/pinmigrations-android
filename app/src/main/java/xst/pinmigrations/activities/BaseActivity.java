package xst.pinmigrations.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import xst.pinmigrations.app.App;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.ui.UiUtil;

import static com.google.common.base.Verify.verifyNotNull;

/** Base class for all Activities in this application
 *
 */
public abstract class BaseActivity extends AppCompatActivity {
	//region Activity facility

	public final @NonNull App getApp() { return (App) verifyNotNull( getApplication() ); }
	public final @NonNull InjectorComponent getInjector() { return getApp().getInjector(); }
	//endregion

	//region Activity methods

	public void hideSoftKeyboard() {
		UiUtil.hideSoftKeyboard(this);
	}

	protected String getSubclassName() {
		return this.getClass().getSimpleName();
	}
	//endregion

	//region disable Transitions

	@Override protected void onPause() {
		super.onPause();
		overridePendingTransition(0, 0);
	}
	//endregion
}
