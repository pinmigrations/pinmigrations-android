package xst.pinmigrations.activities.admin;

import xst.pinmigrations.ui.mvp.MvpPresenter;

public class AdminContract {
	interface MvpView extends xst.pinmigrations.ui.mvp.MvpView<Presenter> { }
	interface Presenter extends MvpPresenter<AdminActivity> { }
}
