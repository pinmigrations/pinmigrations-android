package xst.pinmigrations.activities.admin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.codecrafters.tableview.listeners.SwipeToRefreshListener;
import de.codecrafters.tableview.listeners.TableDataLongClickListener;
import xst.pinmigrations.R;
import xst.pinmigrations.activities.admin.AdminContract.Presenter;
import xst.pinmigrations.activities.map.MapActivity;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;
import xst.pinmigrations.domain.paths.PathList;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.ui.UiUtil;

import static com.google.common.base.Preconditions.checkNotNull;

public class AdminMvpView extends HorizontalScrollView implements AdminContract.MvpView, Destroyable {
	@BindView(R.id.admin_positionsTable) SortablePositionTableView mPositionsTable;
	@BindView(R.id.admin_viewMapBtn) Button mViewMapBtn;
	@BindView(R.id.admin_reloadRemoteBtn) Button mReloadRemoteBtn;
	@BindView(R.id.admin_loadSamplePositionsBtn) Button mLoadSamplesBtn;
	@BindView(R.id.admin_clearTableBtn) Button mClearTableBtn;
	protected @Nullable Unbinder mUnbinder;


	AdminPresenter mAdminPresenter;
	UiService mUiService;

	//region CTOR

	public AdminMvpView(@NonNull final Context context) { this(context, null, 0); }
	public AdminMvpView(@NonNull final Context context, @Nullable final AttributeSet attrs) { this(context, attrs, 0); }
	public AdminMvpView(@NonNull final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
		super(checkNotNull(context), attrs, defStyleAttr);
		init();
	}
	//endregion

	protected void init() {
		LayoutInflater.from(getContext()).inflate(R.layout.admin_mvp_view, this, true);
		mUnbinder = ButterKnife.bind(this);
	}

	/** initialization that requires an Activity ref */
	public void contextInit(@NonNull final AdminActivity activity) {
		mPositionsTable.setup(activity);
		bindClickListeners();
	}

	public void bindClickListeners() {
		mViewMapBtn.setOnClickListener(viewClickListener);
		mReloadRemoteBtn.setOnClickListener(viewClickListener);
		mLoadSamplesBtn.setOnClickListener(viewClickListener);
		mClearTableBtn.setOnClickListener(viewClickListener);
		mPositionsTable.addDataLongClickListener(tableRowClickListener);
		mPositionsTable.setSwipeToRefreshEnabled( true );
		mPositionsTable.setSwipeToRefreshListener( swipreRefreshListener );
	}
	protected final View.OnClickListener viewClickListener = new View.OnClickListener() {
		@Override public void onClick(final View v) {
			switch (v.getId()) {
				case R.id.admin_viewMapBtn:
					UiUtil.startActivity(mAdminPresenter.getActivity(), MapActivity.class);
					break;
				case R.id.admin_loadSamplePositionsBtn:
					mAdminPresenter.loadSamplePositions(false);
					break;
				case R.id.admin_clearTableBtn:
					mPositionsTable.clearTable();
					break;
				case R.id.admin_reloadRemoteBtn:
					mAdminPresenter.reloadRemotePositions(false);
					break;
				default:
			}
		}
	};
	protected final TableDataLongClickListener<Position> tableRowClickListener = new TableDataLongClickListener<Position>() {
		@Override public boolean onDataLongClicked(final int rowIndex, final Position clickedPosition) {
			mAdminPresenter.dialogForPosition(clickedPosition);
			return true; // event is consumed, dont propagate
		}
	};
	protected final SwipeToRefreshListener swipreRefreshListener = new SwipeToRefreshListener() {
		@Override public void onRefresh(final RefreshIndicator refreshIndicator) {
			refreshIndicator.hide();
			mAdminPresenter.reloadRemotePositions(true);
		}
	};

	@Override public void destroy() {
		mViewMapBtn.setOnClickListener( null );
		mReloadRemoteBtn.setOnClickListener( null );
		mLoadSamplesBtn.setOnClickListener( null );
		mClearTableBtn.setOnClickListener( null );

		mPositionsTable.removeDataLongClickListener(tableRowClickListener);
		mPositionsTable.destroy();

		if (null != mUnbinder) {
			mUnbinder.unbind();
			mUnbinder = null;
		}
	}

	public void setPathList(@NonNull final PathList pathList) {
		mPositionsTable.setPathList(pathList);
	}

	@Override public void setPresenter(@NonNull final Presenter presenter) {
		mAdminPresenter = (AdminPresenter) checkNotNull(presenter); // TODO: update after abstracting out methods into interface
	}

	@Override public void setUiService(@NonNull final UiService uiService) {
		mUiService = checkNotNull( uiService );
	}

	@Nullable @Override public UiService getUiService() {
		return mUiService;
	}

	@Override public void showQuickNotice(@NonNull final String text) {
		mUiService.showQuickNotice(text);
	}

	@Override public void showImportantMessage(@NonNull final String text) {
		mUiService.showImportantMessage(text);
	}
}
