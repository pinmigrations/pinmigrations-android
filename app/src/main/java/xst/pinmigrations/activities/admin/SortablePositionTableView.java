package xst.pinmigrations.activities.admin;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pushtorefresh.javac_warning_annotation.Warning;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import de.codecrafters.tableview.SortableTableView;
import de.codecrafters.tableview.TableDataAdapter;
import de.codecrafters.tableview.model.TableColumnDpWidthModel;
import de.codecrafters.tableview.model.TableColumnModel;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.paths.PathList;
import xst.pinmigrations.domain.paths.PathsUtil;
import xst.pinmigrations.domain.positions.Position;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;
import static xst.pinmigrations.domain.positions.PositionsUtil.POSITION_DESC_COMPARATOR;
import static xst.pinmigrations.domain.positions.PositionsUtil.POSITION_GUID_COMPARATOR;
import static xst.pinmigrations.domain.positions.PositionsUtil.POSITION_ID_COMPARATOR;
import static xst.pinmigrations.domain.positions.PositionsUtil.POSITION_LAT_COMPARATOR;
import static xst.pinmigrations.domain.positions.PositionsUtil.POSITION_LNG_COMPARATOR;
import static xst.pinmigrations.domain.positions.PositionsUtil.POSITION_ORD_COMPARATOR;

public class SortablePositionTableView extends SortableTableView<Position> implements Destroyable {
	public static final String[] TABLE_HEADER = {
			"id", "desc", "ord", "lat", "lng", "guid"
	};
	public static final int ID_COL = 0, DESC_COL = 1,  ORD_COL = 2, LAT_COL = 3, LNG_COL = 4, GUID_COL = 5;

	public static final int COLUMN_COUNT = 6;
	public static final int COLUMN_DEFAULT_WIDTH = 0; // to detect errors visually
	public static final int ID_COL_WIDTH_DP = 45;
	public static final int LAT_COL_WIDTH_DP = 90;
	public static final int LNG_COL_WIDTH_DP = 95;
	public static final int DESC_COL_WIDTH_DP = 150;
	public static final int GUID_COL_WIDTH_DP = 180;
	public static final int ORD_COL_WIDTH_DP = 40;

	public static final int CELL_PADDING_LEFT_DPI = 5, CELL_PADDING_TOP_DPI = 10,
			CELL_PADDING_RIGHT_DPI = 5, CELL_PADDING_BOTTOM_DPI = 10;

	public static final int HEADER_TEXT_SIZE_SP = 14;

	@Inject AndroidService mAndroidService;
	public float screenDensity = 1.5f; // sensible default

	private WeakReference<AdminActivity> activityRef;
	public final @Nullable AdminActivity getActivity() { return activityRef.get(); }

	//region CTOR
	public SortablePositionTableView(final Context context) { this(context, null, 0); }
	public SortablePositionTableView(final Context context, final AttributeSet attrs) { this(context, attrs, 0); }
	public SortablePositionTableView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
		super(checkNotNull(context), attrs, defStyleAttr);
	}
	//endregion

	@Override public void destroy() {
		activityRef.clear();

		setSwipeToRefreshEnabled( false );
		setSwipeToRefreshListener( null );
		clearTable();
		removeAllViews();

		mPathList.destroy();
	}

	final PathList mPathList = PathsUtil.emptyPathList();

	public void setup(final @NonNull AdminActivity activityRef) {
		final AdminActivity activity = checkNotNull(activityRef);
		this.activityRef = new WeakReference<>(activity);

		activity.getInjector().inject(this);

		setupDimensions();
		setupColumns(activity);
		setupTableHeader(activity);
		this.setSwipeToRefreshEnabled( true );
		this.setSaveEnabled( false );
	}

	void setupDimensions() {
		screenDensity = mAndroidService.getDisplayMetrics().density;
		// TODO: calc dimensions for TableHeader here (use `screenDensity`)
	}

	void setupColumns(@NonNull final AdminActivity activity) {
		this.setColumnCount(COLUMN_COUNT);

		final TableColumnDpWidthModel columnModel = new TableColumnDpWidthModel(activity, COLUMN_COUNT, COLUMN_DEFAULT_WIDTH);
		columnModel.setColumnWidth(ID_COL, ID_COL_WIDTH_DP);
		columnModel.setColumnWidth(LAT_COL, LAT_COL_WIDTH_DP);
		columnModel.setColumnWidth(LNG_COL, LNG_COL_WIDTH_DP);
		columnModel.setColumnWidth(DESC_COL, DESC_COL_WIDTH_DP);
		columnModel.setColumnWidth(GUID_COL, GUID_COL_WIDTH_DP);
		columnModel.setColumnWidth(ORD_COL, ORD_COL_WIDTH_DP);
		this.setColumnModel(columnModel);

		this.setColumnComparator(ID_COL, POSITION_ID_COMPARATOR);
		this.setColumnComparator(LAT_COL, POSITION_LAT_COMPARATOR);
		this.setColumnComparator(LNG_COL, POSITION_LNG_COMPARATOR);
		this.setColumnComparator(DESC_COL, POSITION_DESC_COMPARATOR);
		this.setColumnComparator(GUID_COL, POSITION_GUID_COMPARATOR);
		this.setColumnComparator(ORD_COL, POSITION_ORD_COMPARATOR);
	}

	@Warning("unscaled pixel values, calculate from DP using device-density")
	void setupTableHeader(@NonNull final AdminActivity activity) {
		final String[] TH = TABLE_HEADER;

		final SimpleTableHeaderAdapter headerAdapter =
				new SimpleTableHeaderAdapter(activity, TH[0], TH[1], TH[2], TH[3], TH[4], TH[5]);
		headerAdapter.setTextSize(HEADER_TEXT_SIZE_SP);
		// TODO: calc dimensions using device-density
		headerAdapter.setPaddingLeft(10); // px not dp!
		headerAdapter.setPaddingRight(10);  // px not dp!
		headerAdapter.setPaddingTop(15);  // px not dp!
		headerAdapter.setPaddingBottom(15);  // px not dp!

		this.setHeaderAdapter(headerAdapter);
	}

	/** clear table data-adapter */
	public void clearTable() {
		final TableDataAdapter<Position> dataAdapter = this.getDataAdapter();
		if (null != dataAdapter) { dataAdapter.clear(); }
		mPathList.clear();
	}

	public void setPathList(@NonNull final PathList pathList) {
		clearTable();
		mPathList.replacePaths( checkNotNull(pathList) );
		displayPathList(mPathList);
	}


	/** load PathList Positions into table. Flat because they are not visually grouped by GUID. */
	void displayPathList(@NonNull final PathList pathList) {
		if (0 == pathList.getPathsCount() || 0 == pathList.getPositionsCount()) {
			return;
		}

		this.setDataAdapter(
				new PositionsTableDataAdapter(getActivity(), getColumnModel(), PathsUtil.flattenPathList(pathList), screenDensity)
		);

		// force UI reload
		this.invalidate();
		this.setVisibility(View.GONE);
		this.setVisibility(View.VISIBLE);
	}


	//region DataAdapter

	public static class PositionsTableDataAdapter extends TableDataAdapter<Position> {
		protected int paddingLeftPx, paddingTopPx, paddingRightPx, paddingBottomPx;
		protected int textSizeSp = 14;
		protected int typeface = Typeface.NORMAL;
		protected int textColor = 0x99000000;

		protected PositionsTableDataAdapter(final Context ctx, final TableColumnModel columnModel,
											final List<Position> data, final float deviceDensity) {
			super(ctx, columnModel, data);
			paddingLeftPx = (int) (CELL_PADDING_LEFT_DPI * deviceDensity);
			paddingTopPx = (int) (CELL_PADDING_TOP_DPI * deviceDensity);
			paddingRightPx = (int) (CELL_PADDING_RIGHT_DPI * deviceDensity);
			paddingBottomPx = (int) (CELL_PADDING_BOTTOM_DPI * deviceDensity);
		}

		@Override public View getCellView(final int rowIndex, final int columnIndex, final ViewGroup group) {
			final TextView textView = new TextView(getContext());
			textView.setPadding(paddingLeftPx, paddingTopPx, paddingRightPx, paddingBottomPx); // px not dp!
			textView.setTypeface(textView.getTypeface(), typeface);
			textView.setTextSize(textSizeSp); // sp, density already factored here
			textView.setTextColor(textColor);
			textView.setSingleLine();
			textView.setEllipsize(TextUtils.TruncateAt.END);
			textView.setText( getTextCellText(rowIndex, columnIndex) );

			return textView;
		}

		@NonNull String getTextCellText(final int rowIndex, final int columnIndex) {
			final Position p = verifyNotNull( getItem(rowIndex) );
			switch(columnIndex) {
				case ID_COL: return p.id + "";
				case LAT_COL: return p.lat + "";
				case LNG_COL: return p.lng + "";
				case DESC_COL: return p.description;
				case GUID_COL: return p.guid;
				case ORD_COL: return p.ord + "";
				default: throw new AssertionError("invalid column index: " + columnIndex);
			}
		}
	}
	//endregion
}
