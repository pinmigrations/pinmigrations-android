package xst.pinmigrations.activities.admin;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.paths.PathList;
import xst.pinmigrations.domain.paths.PathListResource;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.ui.mvp.BasePresenter;
import xst.pinmigrations.ui.rx.DevUiSubscriber;

import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.activities.admin.PositionDialogFragment.POSITION_ID_TAG;
import static xst.pinmigrations.activities.admin.PositionDialogFragment.POSITION_IS_SAMPLE_TAG;

public class AdminPresenter extends BasePresenter<AdminActivity> implements AdminContract.Presenter, Destroyable {
	@Inject PathListResource mPathListResource;
	@Inject RxJavaService mRxJavaService;

	@NonNull final CompositeSubscription mCompositeSubscription = new CompositeSubscription();

	@NonNull final AdminMvpView mAdminView;

	boolean loadedSamples = false;

	public AdminPresenter(@NonNull final AdminActivity activity, @NonNull final AdminMvpView adminView) {
		super(activity);
		activity.getInjector().inject(this);

		mAdminView = checkNotNull(adminView);
		mAdminView.setPresenter(this);
		mAdminView.setUiService( checkNotNull(activity.getUiService()) );
		mAdminView.contextInit(activity);

		subscribeToLifecycle();
	}

	@Override public void destroy() {
		destroyDialogFragment();
		mAdminView.destroy();
	}

	@Override public void onResume() {
		if (loadedSamples) {
			loadSamplePositions(false);
		} else {
			softLoadRemotePositions(false);
		}
	}

	public void dialogForPosition(final @NonNull Position positionForDialog) {
		final Position position = checkNotNull(positionForDialog);
		final AdminActivity activity = (AdminActivity) getActivity();
		if (null != activity) {
			startDialog(activity, position);
		}
	}

	protected void startDialog(@NonNull final AdminActivity activity, final @NonNull Position position) {
		final FragmentManager fragmentManager = activity.getFragmentManager();
		final Fragment existingFragment = fragmentManager.findFragmentByTag(PositionDialogFragment.DIALOG_FRAGMENT_TAG);
		if (existingFragment != null) {
			fragmentManager.beginTransaction().remove(existingFragment).commit(); // remove if one is already loaded
		}

		final PositionDialogFragment dialogFragment = new PositionDialogFragment();
		final Bundle args = new Bundle();
		args.putLong(POSITION_ID_TAG, position.id);
		args.putBoolean(POSITION_IS_SAMPLE_TAG, loadedSamples);
		dialogFragment.setArguments(args);
		dialogFragment.show(fragmentManager, PositionDialogFragment.DIALOG_FRAGMENT_TAG);
	}

	void destroyDialogFragment() {
		final Activity activity = getActivity();
		PositionDialogFragment fragment = null;
		if (null != activity) {
			fragment = (PositionDialogFragment) activity.getFragmentManager().findFragmentByTag(PositionDialogFragment.DIALOG_FRAGMENT_TAG);
		}

		if (null != fragment) {
			fragment.destroy();
		}
	}

	//region Data Loading

	void softLoadRemotePositions(final boolean notify) {
		loadedSamples = false;
		mCompositeSubscription.add( // get LifeCycle notifications
				mPathListResource.softGetAllPaths()
					.observeOn( mRxJavaService.androidMainScheduler() )
					.subscribe(new PathListSubscriber(notify))
		);
	}
	void reloadRemotePositions(final boolean notify) {
		loadedSamples = false;
		mCompositeSubscription.add(
			mPathListResource.getAllPaths()
				.observeOn( mRxJavaService.androidMainScheduler() )
				.subscribe(new PathListSubscriber(notify))
		);
	}
	/** load local sample set (from APK-assets) */
	void loadSamplePositions(final boolean notify) {
		loadedSamples = true;
		mCompositeSubscription.add(
			mPathListResource.getSamplePathList()
				.observeOn( mRxJavaService.androidMainScheduler() )
				.subscribe( new PathListSubscriber(notify) )
		);
	}
	class PathListSubscriber extends DevUiSubscriber<PathList> {
		boolean notify = true;
		public PathListSubscriber(final boolean notify) { this.notify = notify; }
		public PathListSubscriber() { notify = true; }
		@Override public void onNext(final PathList pathList) {
			if (pathList.getPositionsCount() > 0) {
				mAdminView.setPathList(pathList);

				if (notify) { mAdminView.showQuickNotice("Position Data Loaded"); }
			} else {
				mAdminView.showImportantMessage("Error: no data");
			}
		}
		@Override public void onError(final Throwable throwable) {
			if (!checkNotNull(mAdminView.getUiService()).handlePotentialNetConnectError(throwable)) {
				mAdminView.getUiService().showImportantMessage("an error occured while loading positions");
				Timber.e(throwable);
			}
		}
	}
	//endregion
}
