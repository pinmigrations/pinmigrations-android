package xst.pinmigrations.activities.admin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.TextView;

import javax.inject.Inject;
import javax.security.auth.login.LoginException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.subscriptions.CompositeSubscription;
import xst.pinmigrations.R;
import xst.pinmigrations.activities.StandardActivity;
import xst.pinmigrations.activities.login.LoginActivity;
import xst.pinmigrations.domain.Destroyable;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.positions.PositionsResource;
import xst.pinmigrations.domain.positions.PositionsSamplesResource;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.ui.UiUtil;
import xst.pinmigrations.ui.rx.DevUiSubscriber;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

public class PositionDialogFragment extends DialogFragment implements Destroyable, View.OnClickListener {
	public static final String DIALOG_FRAGMENT_TAG = "Admin::PositionDialog", POSITION_ID_TAG = "PositionId",
			POSITION_IS_SAMPLE_TAG = "PositionType";

	@Inject PositionsResource mPositionsResource;
	@Inject PositionsSamplesResource mPositionsSamplesResource;
	@Inject RxJavaService mRxJavaService;
	@Inject AndroidService mAndroidService;

	@BindView(R.id.admin_dialog_id_view) TextView mIdView;
	@BindView(R.id.admin_dialog_desc_view) TextView mDescView;
	@BindView(R.id.admin_dialog_lat_view) TextView mLatView;
	@BindView(R.id.admin_dialog_lng_view) TextView mLngView;
	@BindView(R.id.admin_dialog_guid_view) TextView mGuidView;
	@BindView(R.id.admin_dialog_ord_view) TextView mOrdView;
	Unbinder mUnbinder;

	long positionId;
	boolean loadedSample = false;

	int dialogWidthPx, dialogHeightPx, dialogWideWidthPx, dialogWideHeightPx, smallestScreenWidthDp;
	public static final int VERTICAL_MARGIN_DPI = 65, VERTICAL_MARGIN_WIDE_DPI = 23, HORIZONTAL_MARGIN_WIDE_DPI = 28;

	//region initialization

	/** 1st in creation sequence */
	@Override public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((StandardActivity) getActivity()).getInjector().inject(this);

		setupDimensions();

		positionId = checkNotNull(getArguments()).getLong(POSITION_ID_TAG);
		loadedSample = checkNotNull(getArguments()).getBoolean(POSITION_IS_SAMPLE_TAG);

		verify(positionId != 0); // 0 is default get-long val
	}

	/** 2nd in creation sequence */
	@Override public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final Dialog dialog = new Dialog(getActivity(), getTheme());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setTitle("Position Dialog"); // not visible, used in LayoutInspector

		final Dimensions dimens = getDimensions();
		checkNotNull( dialog.getWindow() ).getDecorView().setLayoutParams(new LayoutParams(dimens.width, dimens.height));

		return dialog;
	}

	/** 3rd in creation sequence */
	@Override public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
									   final Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.admin_position_dialog, container);
		mUnbinder = ButterKnife.bind(this, rootView);

		mIdView.setText(positionId + "");
		loadPosition();

		return rootView;
	}

	/** 4th in creation sequence */
	@Override public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		final Dialog dialog = getDialog();
		final Dimensions dimens = getDimensions();

		checkNotNull( dialog.getWindow() ).setLayout(dimens.width, dimens.height);
	}
	//endregion

	//region cleanup
	@Override public void onDestroy() {
		super.onDestroy();
		destroy();
	}

	@Override public void destroy() {
		if (mUnbinder != null) {
			mUnbinder.unbind();
			mUnbinder = null;
		}

		mCompositeSubscription.unsubscribe();
	}
	//endregion

	@OnClick({R.id.admin_dialog_edit, R.id.admin_dialog_delete})
	@Override public void onClick(final View v) {
		switch(v.getId()) {
			case R.id.admin_dialog_edit:
				editPosition();
				break;
			case R.id.admin_dialog_delete:
				confirmDeletePosition();
				break;
			default:
				throw new AssertionError("invalid click target");
		}
	}

	void loadPosition() {
		if (loadedSample) {
			mCompositeSubscription.add(
					mPositionsSamplesResource.getSamplePosition(positionId)
							.observeOn(mRxJavaService.androidMainScheduler())
							.subscribe(new PositionSubscriber())
			);
		} else {
			mCompositeSubscription.add(
				mPositionsResource.softGetPosition(positionId)
						.observeOn(mRxJavaService.androidMainScheduler())
						.subscribe(new PositionSubscriber())
			);
		}
	}

	void editPosition() {
		final Activity containingActivity = getActivity();
		final Intent intent = new Intent(containingActivity, EditPositionActivity.class);
		final Bundle extras = new Bundle();
		extras.putLong(POSITION_ID_TAG, positionId);
		extras.putBoolean(POSITION_IS_SAMPLE_TAG, loadedSample);
		intent.putExtras(extras);

		UiUtil.startActivity(containingActivity, intent);
		dismiss();
	}

	void confirmDeletePosition() {
		new AlertDialog.Builder(getActivity()).setMessage("Delete selected Position?")
				// .setTitle("")
				.setPositiveButton("Yes", deleteConfirmListener)
				.setNegativeButton("No", null)
				.show();

		this.dismiss();
	}
	protected final DialogInterface.OnClickListener deleteConfirmListener = new DialogInterface.OnClickListener() {
		@Override public void onClick(final DialogInterface dialog, final int which) {
			deletePosition();
		}
	};
	void deletePosition() {
		if (loadedSample) {
			mCompositeSubscription.add(
				mPositionsSamplesResource
					.delete(positionId)
					.observeOn(mRxJavaService.androidMainScheduler())
					.subscribe(new PositionDeleteSubscriber())
			);
		} else {
			try {
				mCompositeSubscription.add(
						mPositionsResource
								.delete(positionId)
								.observeOn(mRxJavaService.androidMainScheduler())
								.subscribe(new PositionDeleteSubscriber())
				);
			} catch(LoginException exc) {
				final AdminActivity activity = getAdminActivity();
				checkNotNull(activity.getUiService()).showImportantMessage("Not logged in, redirecting to Login"); //!error uiservice null
				UiUtil.startActivity(activity, LoginActivity.class);
				// note: dialog not dismissed so they user can continue after logging in
			}
		}
	}

	void setupDimensions() {
		smallestScreenWidthDp = mAndroidService.getSmallestScreenWidthDp();

		final float density = mAndroidService.getDeviceDensity();
		final Resources res = mAndroidService.getResources();

		dialogWidthPx = res.getDimensionPixelSize(R.dimen.position_dialog_width);
		dialogHeightPx = res.getDimensionPixelSize(R.dimen.position_dialog_height) + (int)(VERTICAL_MARGIN_DPI * density);
		dialogWideWidthPx = res.getDimensionPixelSize(R.dimen.position_dialog_wide_width) + (int)(VERTICAL_MARGIN_WIDE_DPI * density);
		dialogWideHeightPx = res.getDimensionPixelSize(R.dimen.position_dialog_wide_height) + (int)(HORIZONTAL_MARGIN_WIDE_DPI * density);
	}

	boolean useWideLayout() {
		return smallestScreenWidthDp >= UiUtil.APP_TABLET_SW_WIDTH || UiUtil.isLandscape(getActivity());
	}

	/** returns appropriate dimensions for the dialog overall width/height */
	Dimensions getDimensions() {
		final Dimensions dimens = new Dimensions();
		if ( useWideLayout() ) {
			dimens.height = dialogWideHeightPx;
			dimens.width = dialogWideWidthPx;
		} else {
			dimens.height = dialogHeightPx;
			dimens.width = dialogWidthPx;
		}
		return dimens;
	}

	protected final CompositeSubscription mCompositeSubscription = new CompositeSubscription();
	class PositionSubscriber extends DevUiSubscriber<Position> {
		@Override public void onNext(final Position position) {
			mIdView.setText(position.id + "");
			mDescView.setText(position.description);
			mLatView.setText(position.lat + "");
			mLngView.setText(position.lng + "");
			mGuidView.setText(position.guid);
			mOrdView.setText(position.ord + "");
		}
	}
	class PositionDeleteSubscriber extends DevUiSubscriber<Void> {
		@Override public void onNext(final Void nothing) {/* Void, so no onNext */}
		@Override public void onCompleted() {
			final UiService uiService = getAdminActivity().getUiService();
			if (null != uiService) { uiService.showQuickNotice("Position Deleted"); }
			PositionDialogFragment.this.dismiss();
		}
	}

	static class Dimensions { public int height, width; }
	protected AdminActivity getAdminActivity() {
		return (AdminActivity) getActivity();
	}
}
