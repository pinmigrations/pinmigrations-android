package xst.pinmigrations.activities.admin;

import android.os.Bundle;

import javax.inject.Inject;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.StandardActivity;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceGenerator;

import static com.google.common.base.Verify.verifyNotNull;

public class AdminActivity extends StandardActivity {
	/* DI services */
	@Inject UiServiceGenerator mUiServiceGenerator;

	@Override protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getInjector().inject(this); // DI

		// UI
		setContentView(R.layout.admin_activity);
		final AdminMvpView adminView = (AdminMvpView) verifyNotNull(findViewById(R.id.admin_mvpView));

		setUiService(mUiServiceGenerator.newUiService(this));
		//noinspection ResultOfObjectAllocationIgnored (no ref needed)
		new AdminPresenter(this, adminView);
	}

	@Override public void destroy() { /* no extra cleanup needed */ }
}
