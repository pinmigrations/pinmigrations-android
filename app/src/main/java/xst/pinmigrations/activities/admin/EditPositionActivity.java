package xst.pinmigrations.activities.admin;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;

import com.google.common.base.Optional;

import javax.inject.Inject;
import javax.security.auth.login.LoginException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import xst.pinmigrations.R;
import xst.pinmigrations.activities.StandardActivity;
import xst.pinmigrations.activities.login.LoginActivity;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceGenerator;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.positions.PositionsResource;
import xst.pinmigrations.domain.positions.PositionsSamplesResource;
import xst.pinmigrations.domain.positions.PositionsUtil;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.ui.UiUtil;
import xst.pinmigrations.ui.rx.DevUiSubscriber;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static xst.pinmigrations.domain.positions.Position.MAX_LAT;
import static xst.pinmigrations.domain.positions.Position.MAX_LNG;
import static xst.pinmigrations.domain.positions.Position.MIN_LAT;
import static xst.pinmigrations.domain.positions.Position.MIN_LNG;

public class EditPositionActivity extends StandardActivity implements View.OnClickListener {
	public static final String POSITION_ID_TAG = "PositionId";
	public static final String POSITION_IS_SAMPLE_TAG = "PositionType";

	@Inject PositionsResource mPositionsResource;
	@Inject PositionsSamplesResource mPositionsSamplesResource;
	@Inject RxJavaService mRxJavaService;
	@Inject UiServiceGenerator mUiServiceGenerator;

	@BindView(R.id.edit_position_id_text) EditText mIdEdit;
	@BindView(R.id.edit_position_desc_text) EditText mDescEdit;
	@BindView(R.id.edit_position_ord_text) EditText mOrdEdit;
	@BindView(R.id.edit_position_lat_text) EditText mLatEdit;
	@BindView(R.id.edit_position_lng_text) EditText mLngEdit;
	@BindView(R.id.edit_position_guid_text) EditText mGuidEdit;
	Unbinder mUnbinder;

	long positionId;
	boolean loadedSample = false;

	@Override protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_position_activity);

		mUnbinder = ButterKnife.bind(this);
		getInjector().inject(this);

		final Bundle extras = checkNotNull( getIntent().getExtras() );

		setUiService(mUiServiceGenerator.newUiService(this));

		positionId = extras.getLong(POSITION_ID_TAG);
		loadedSample = extras.getBoolean(POSITION_IS_SAMPLE_TAG);

		verify(positionId != 0); // 0 is default get-long val
	}

	@Override protected void onResume() {
		super.onResume();

		loadPosition();
	}

	@Override public void destroy() { /* nothing manual necessary */ }

	@OnClick({R.id.edit_pos_save})
	@Override public void onClick(final View v) {
		if (R.id.edit_pos_save == v.getId()) {
			savePosition();
		} else { throw new AssertionError("invalid click source"); }
	}

	void loadPosition() {
		if (loadedSample) {
			mPositionsSamplesResource.getSamplePosition(positionId)
					.observeOn(mRxJavaService.androidMainScheduler())
					.subscribe(new PositionSubscriber());
		} else {
			mPositionsResource.softGetPosition(positionId)
					.observeOn(mRxJavaService.androidMainScheduler())
					.subscribe(new PositionSubscriber());
		}
	}

	/* reads from UI fields and builds Position object.
	 	if invalid, user is notified, and return value is Optional.absent
	 */
	@NonNull Optional<Position> getPositionData() {
		final UiService uiService = checkNotNull(getUiService());
		final Position position = new Position(positionId); // id passed from Intent

		// description
		position.description = mDescEdit.getText().toString().trim();

		// ord
		try {
			position.ord = Integer.parseInt( mOrdEdit.getText().toString().trim() );
		} catch (final NumberFormatException exc) {
			uiService.showImportantMessage("ORD value is invalid, must be a whole number (integer)");
			return Optional.absent();
		}

		// lat
		try {
			position.lat = Double.parseDouble( mLatEdit.getText().toString().trim() );
		} catch (final NumberFormatException exc) {
			uiService.showImportantMessage("LAT value is invalid, must be a number (probably but not necessarily decimal)");
			return Optional.absent();
		}
		if (position.lat > MAX_LAT || position.lat < MIN_LNG) {
			uiService.showImportantMessage("LNG value is invalid, must be between " + MIN_LAT + " and " + MAX_LAT);
			return Optional.absent();
		}

		// lng
		try {
			position.lng = Double.parseDouble( mLngEdit.getText().toString().trim() );
		} catch (final NumberFormatException exc) {
			uiService.showImportantMessage("LNG value is invalid, must be a number (probably but not necessarily decimal)");
			return Optional.absent();
		}
		if (position.lng > MAX_LAT || position.lng < MIN_LNG) {
			uiService.showImportantMessage("LNG value is invalid, must be between " + MIN_LNG + " and " + MAX_LNG);
			return Optional.absent();
		}

		// TODO: offer way to generate a new GUID
		// guid
		position.guid = mGuidEdit.getText().toString().trim();
		if ( !PositionsUtil.isValidGuid(position.guid) ) {
			uiService.showImportantMessage("GUID value is invalid");
			return Optional.absent();
		}

		return Optional.of(position);
	}

	void savePosition() {
		final Optional<Position> posOpt = getPositionData();
		if ( posOpt.isPresent() ) {
			final Position pos = posOpt.get();

			if (loadedSample) {
				mCompositeSubscription.add(
						mPositionsSamplesResource
								.update(pos)
								.observeOn(mRxJavaService.androidMainScheduler())
								.subscribe(new SaveSubscriber())
				);
			} else {
				try {
					mCompositeSubscription.add(
							mPositionsResource
									.update(pos)
									.observeOn(mRxJavaService.androidMainScheduler())
									.subscribe(new SaveSubscriber())
					);
				} catch (final LoginException exc) {
					checkNotNull(getUiService()).showImportantMessage("Not logged in, redirecting to Login");
					UiUtil.startActivity(this, LoginActivity.class);
				}
			}
		} // else user entered invalid input, dont save
	}

	class PositionSubscriber extends DevUiSubscriber<Position> {
		@Override public void onNext(final Position position) {
			mIdEdit.setText(position.id + "");
			mDescEdit.setText(position.description);
			mLatEdit.setText(position.lat + "");
			mLngEdit.setText(position.lng + "");
			mGuidEdit.setText(position.guid);
			mOrdEdit.setText(position.ord + "");
		}
	}

	class SaveSubscriber extends DevUiSubscriber<Position> {
		@Override public void onNext(final Position position) {
			checkNotNull(getUiService()).showQuickNotice("saved");
			EditPositionActivity.this.finish();
		}
	}
}
