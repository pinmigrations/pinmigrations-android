package xst.pinmigrations.activities.dev_debug.screen_rotation;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.ui.UiUtil;


/**
 * http://stackoverflow.com/questions/2366706/how-to-lock-orientation-during-runtime
 */
public class ScreenRotationActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {
	public static final String TAG_ACT_SCREEN_ROTATE = "SCREEN_ROTATE::StartType";

	@Override protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen_rotation_activity);

		final ToggleButton screenRotateToggle = (ToggleButton) findViewById(R.id.map_lockRotationBtn);

		// if instance state was saved, always start in normal mode
		// actual device orientation will dictate layout
		@SuppressWarnings("VariableNotUsedInsideIf")
		final int startType = (null == savedInstanceState)
				? getIntent().getExtras().getInt(TAG_ACT_SCREEN_ROTATE)
				: UiUtil.SCREEN_ROTATE_NORMAL;

		switch (startType) {
			case UiUtil.SCREEN_ROTATE_LANDSCAPE:
				screenRotateToggle.setChecked(true);
				UiUtil.lockToLandscape(this);
				break;

			case UiUtil.SCREEN_ROTATE_PORTRAIT:
				screenRotateToggle.setChecked(true);
				UiUtil.lockToPortrait(this);
				break;
			case UiUtil.SCREEN_ROTATE_NORMAL:
			default:
				// do nothing
				break;
		}

		// set listener after (potentially) setting toggle checked/un-checked
		// otherwise setChecked prematurely triggers listener
		screenRotateToggle.setOnCheckedChangeListener(this);
	}

	// bundle will be passed to `onCreate` if the process is killed and restarted.
	@Override public void onSaveInstanceState(final Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);

		savedInstanceState.putInt(TAG_ACT_SCREEN_ROTATE, UiUtil.SCREEN_ROTATE_NORMAL);
	}

	/* ToggleButton listener */
	@Override public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
		if (isChecked) {
			UiUtil.lockOrientation(this);
		} else {
			UiUtil.unlockOrientation(this);
		}
	}
}
