package xst.pinmigrations.activities.dev_debug;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.StandardActivity;
import xst.pinmigrations.activities.dev_debug.manual_login.CsrfActivity;
import xst.pinmigrations.activities.dev_debug.screen_rotation.ScreenRotationActivity;
import xst.pinmigrations.ui.UiUtil;

import static xst.pinmigrations.ui.UiUtil.SCREEN_ROTATE_LANDSCAPE;
import static xst.pinmigrations.ui.UiUtil.SCREEN_ROTATE_NORMAL;
import static xst.pinmigrations.ui.UiUtil.SCREEN_ROTATE_PORTRAIT;
import static xst.pinmigrations.activities.dev_debug.screen_rotation.ScreenRotationActivity.TAG_ACT_SCREEN_ROTATE;

@SuppressWarnings("ALL") // suppress IntelliJ inspections
public final class DevDebugActivity extends StandardActivity implements View.OnClickListener {
	// UI
	GridLayout mGridLayout;

	@Override
	protected final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// UI
		setContentView(R.layout.dev_debug_activity);
		mGridLayout = (GridLayout) findViewById(R.id.activity_dev_debug);
	}

	@Override
	public final void destroy() { /* dont need anything probably */ }

	@Override public final void onClick(final View v) {
		switch (v.getId()) {
			/* CSRF activity */
			case R.id.ddCsrfAct: startCsrfActivity(); break;

			/* screen rotation */
			case R.id.ddScreenRotationActNormal: startScreenRotationActivity(SCREEN_ROTATE_NORMAL); break;
			case R.id.ddScreenRotationActPortrait: startScreenRotationActivity(SCREEN_ROTATE_PORTRAIT); break;
			case R.id.ddScreenRotationActLandscape: startScreenRotationActivity(SCREEN_ROTATE_LANDSCAPE); break;

			default: return;
		}
	}

	@Override
	public final void onConfigurationChanged(final Configuration newConfig) {
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			UiUtil.changeGridColumnCount(mGridLayout, 4);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			UiUtil.changeGridColumnCount(mGridLayout, 2);
		}

		super.onConfigurationChanged(newConfig);
	}

	//region Manual Login
	public final void startCsrfActivity() {
		UiUtil.startActivity(this, CsrfActivity.class);
	}
	//endregion

	//region Screen Rotate Activity
	public final void startScreenRotationActivity(final int startType) {
		final Intent intent = new Intent(this, ScreenRotationActivity.class);
		final Bundle extras = new Bundle();
		extras.putInt(TAG_ACT_SCREEN_ROTATE, startType);
		intent.putExtras(extras);

		UiUtil.startActivity(this, intent);
	}
	//endregion
}
