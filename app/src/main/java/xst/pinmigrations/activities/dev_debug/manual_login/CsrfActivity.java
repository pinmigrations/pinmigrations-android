package xst.pinmigrations.activities.dev_debug.manual_login;

import android.os.Bundle;
import android.view.View;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.StandardActivity;
import xst.pinmigrations.ui.UiUtil;

public class CsrfActivity extends StandardActivity implements View.OnClickListener {
	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.csrf_activity);

		UiUtil.lockToPortrait(this);

		finish();
	}

	@Override
	public void destroy() { /* dont need any cleanup */ }

	@SuppressWarnings("OverlyComplexMethod") // theres lots of buttons in this dev-mode only activity
	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
			case R.id.ddLogin: /* do nothing */ break;
			case R.id.ddCheckLogin: /* do nothing */ break;
			case R.id.ddLogout: /* do nothing */ break;
			case R.id.ddClearCookies: /* do nothing */ break;

			case R.id.ddPublicGet: /* do nothing */ break;
			case R.id.ddPublicPost: /* do nothing */ break;
			case R.id.ddCsrfPost: /* do nothing */ break;
			case R.id.ddAuthedGet: /* do nothing */ break;
			case R.id.ddAuthedPost: /* do nothing */ break;

			case R.id.ddPrintCookies: /* do nothing */ break;
			case R.id.ddPrintLogin: /* do nothing */ break;
		}
	}
}
