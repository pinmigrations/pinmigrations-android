package xst.pinmigrations.test.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.BaseActivity;

/**
 * Context for some Tests (Robolectric / AndroidTest)
 */
public final class BlankActivity extends BaseActivity {
	@Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blank_activity);
	}
}
