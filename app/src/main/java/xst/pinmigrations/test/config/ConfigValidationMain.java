package xst.pinmigrations.test.config;

import java.util.function.Consumer;

import timber.log.Timber;

public class ConfigValidationMain {

	/** NOTE: invoking these methods from `androidTest` sometimes requires a `fullCleanBuild`
	 * causes IllegalAccessError: Class ref in pre-verified class resolved to unexpected
	 */
	public static Consumer<String> getMethodReference() {
		return System.out::println;
	}
	public static Consumer<String> getLambda() {
		return (s) -> { };
	}

	public static void usesLambdasInternally() {
		Consumer<String> fnRef = Timber::d;
		fnRef.accept("AWESOME");
	}
	public static void usesMethodReferenceInternally() {
		Consumer<String> fnRef = (String s) -> { Timber.d(s); };
		fnRef.accept("AWESOME");
	}
}
