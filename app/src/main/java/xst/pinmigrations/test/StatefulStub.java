package xst.pinmigrations.test;

public interface StatefulStub {
	void reset();
}
