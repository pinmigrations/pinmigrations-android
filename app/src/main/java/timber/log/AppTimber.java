package timber.log;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import xst.pinmigrations.app.App;
import xst.pinmigrations.app.logging.TestLog;
import xst.pinmigrations.app.IgnoreArg;

/**
 * Timber.Tree implementation that unifies logging invocation (just use Timber.*)
 * across environments (production device / unit-tests / instrumented)
 * and sets application-specific log format before forwarding to androids logging framework
 * unfortunately the only way to get the desired functionality without forking Timber
 * is to subclass within its package-space
 */
public class AppTimber extends Timber.Tree {
	private static final String APP_TAG = "xst.PinMigrations";
	private static final int MAX_LOG_LENGTH = 4000;

	// TODO: determine if Timber forwards `throwable` into this invocation or is just used internally

	// has to exec quickly, thus the short-circuits
	// ignore tag param: always use APP_TAG
	@SuppressWarnings("MethodWithMultipleReturnPoints")
	@Override
	protected void log(final int priority, @IgnoreArg String tag,
					   final String message, final Throwable throwable) {
		if (App.loggingDisabled()) { return; }

		// Preconditions
		if (null == message) { throw new AssertionError("null string from Timber lib @ AppTimber.log"); }

		final int messageLength = message.length();
		if (messageLength < MAX_LOG_LENGTH) {
			_log(priority, message);
			return;
		}

		// Split by line, then ensure each line can fit into Log's maximum length.
		for (int i = 0; i < messageLength; i++) {

			int newline = message.indexOf('\n', i);
			newline = newline != -1 ? newline : messageLength;
			do {
				final int end = Math.min(newline, i + MAX_LOG_LENGTH);
				_log(priority, message.substring(i, end));

				i = end;
			} while (i < newline);
		}
	}

	@Override final String getTag() { return APP_TAG; }

	//region piping/forwarding
	private void _log(final int priority, @NonNull final String part) {
		if (App.unitTestExec()) {
			if (null == testLogger) {
				System.out.print("// TEST-LOGGER NOT SET // ");
				System.out.println(part);
			} else {
				testLogger.println(priority, part);
			}
		} else if (Log.ASSERT == priority) {
			Log.wtf(APP_TAG, part);
		} else {
			Log.println(priority, APP_TAG, part);
		}
	}
	//endregion


	//region TestLogger
	@Nullable private static TestLog testLogger;
	public static void setTestLogger(@NonNull final TestLog logger) {
		testLogger = logger;
	}
	//endregion
}
