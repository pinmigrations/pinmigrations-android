package xst.pinmigrations.app;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.test.runner.AndroidJUnitRunner;

/**
 * use AT_App instead of App for instrumented device tests
 * this class must be specified in build.gradle under setting `testInstrumentationRunner`
 * AS/IntelliJ run configurations typically pick this up from build script fine (verify XML if needed)
 * this class is ultimately passed as argument to `adb am` command (either by AndroidStudio or gradle, etc..)
 */
public class AT_JUnitRunner extends AndroidJUnitRunner {
	public AT_JUnitRunner() {
		System.out.println("AT_JUnitRunner ctor");
	}

	/** creates single application instance for all device tests */
	@Override public Application newApplication(final ClassLoader cl, final String className, final Context context)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {

		System.out.println("AT_JUnitRunner.newApplication");

		return super.newApplication(cl, AT_App.class.getCanonicalName(), context);
	}

	/** cleanup after ALL tests */
	@Override @CallSuper
	public void finish(int resultCode, final Bundle results) {
		super.finish(resultCode, results);
	}
}
