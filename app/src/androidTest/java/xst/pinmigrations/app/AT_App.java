package xst.pinmigrations.app;

import android.support.annotation.NonNull;

import com.squareup.leakcanary.RefWatcher;

import timber.log.AppTimber;
import timber.log.Timber;
import xst.pinmigrations.app.di.AT_AppComponent;
import xst.pinmigrations.app.di.AT_AppModule;
import xst.pinmigrations.app.di.DaggerAT_AppComponent;
import xst.pinmigrations.app.di.DaggerAT_BaseComponent;

public class AT_App extends App {

	public static final boolean AT_AppDebug = false;

	// re-used across device-tests
	@NonNull final AT_AppComponent mAppComponent;
	public AT_App() {
		setupAndroidTestLogging();

		mAppComponent = DaggerAT_AppComponent.builder().aT_AppModule(new AT_AppModule(this)).build();
	}

	void setupAndroidTestLogging() {
		setExecModeDeviceTests();
		if (0 == Timber.treeCount()) {
			Timber.plant(new AppTimber());
		}
	}


	@Override public void onCreate() {
		setupAndroidTestLogging();  // just in case

		super.onCreate();

		Thread.setDefaultUncaughtExceptionHandler(new TestsUncaughtExceptionHandler());

		setupAppDepGraph();
	}

	public void setupAppDepGraph() {
		// directly override the field in `App`
		// GC/re-create for each device-tests
		mBaseComponent = DaggerAT_BaseComponent.builder().aT_AppComponent(mAppComponent).build();
	}

	@Override protected void initializeLogin() { /* do nothing, done manually in tests */ }

	/** may need more clearing... */
	public void resetAppForNextTest() {
		setupAppDepGraph();
	}

	//region LeakCanary

	/** leak canary will slow down device tests, disable */
	protected RefWatcher installLeakCanary(final App app) {
		return RefWatcher.DISABLED;
	}
	//endregion
}
