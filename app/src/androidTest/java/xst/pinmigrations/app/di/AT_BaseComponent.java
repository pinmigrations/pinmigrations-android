package xst.pinmigrations.app.di;

import dagger.Component;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceSpyGenerator;
import xst.pinmigrations.domain.android_context.TGoogleApiService;
import xst.pinmigrations.domain.api.StubApiGenerator;
import xst.pinmigrations.domain.login.TLoginManager;

/*
 */
@AppScope
@Component(dependencies = AT_AppComponent.class,
		modules = { AT_ServicesModule.class, AT_AndroidContextModule.class, AT_ResourcesModule.class })
public interface AT_BaseComponent extends BaseComponent {
	TLoginManager getTLoginManager();
	StubApiGenerator getStubApiGenerator();
	UiServiceSpyGenerator getUiServiceSpyGenerator();

	TGoogleApiService getTGoogleApiService();
}
