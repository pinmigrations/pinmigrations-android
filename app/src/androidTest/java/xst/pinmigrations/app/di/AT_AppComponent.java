package xst.pinmigrations.app.di;

import android.app.Instrumentation;
import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import xst.pinmigrations.app.AT_App;

@Singleton
@Component(modules = AT_AppModule.class)
public interface AT_AppComponent extends AppComponent {
	AT_App getATApplication();

	@Named("INSTRUMENTATION_CONTEXT") Context getInstrumentationContext();
	@Named("APP_CONTEXT") Context getAppContext();
	Instrumentation getInstrumentation();
}
