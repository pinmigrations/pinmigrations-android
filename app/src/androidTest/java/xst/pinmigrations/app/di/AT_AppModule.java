package xst.pinmigrations.app.di;

import android.app.Instrumentation;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import xst.pinmigrations.app.AT_App;
import xst.pinmigrations.app.App;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * NOTE: static initializers only run once when JVM is started for test-suite
 * Test*Module and Test*Component are re-instantiated for every test
 *
 * https://github.com/robolectric/robolectric/issues/595
 */
@Module
public class AT_AppModule {
	private final AT_App mApp;

	public AT_AppModule(@NonNull final AT_App app) {
		mApp = checkNotNull(app);
	}

	@Singleton @Provides
	public App provideApp() {
		return mApp;
	}

	@Singleton @Provides
	public AT_App provideATApp() {
		return mApp;
	}

	/** allows access for ping before app is initialized */
	public static final HttpUrl BASE_URL = HttpUrl.parse("http://pinmigrations.herokuapp.com");

	@Singleton @Provides @Named("BASE_URL")
	public HttpUrl provideBaseUrl() {
		return BASE_URL;
	}

	/** different than main so tests dont clobber its data */
	public static final String AT_SHARED_PREFS = "AT-SharedPreferences";

	@Singleton @Provides @Named("PREFS_FILE_NAME")
	public String providePrefsFileName() {
		return AT_SHARED_PREFS;
	}


	@Singleton @Provides @Named("INSTRUMENTATION_CONTEXT")
	public Context provideInstrumentationContext() {
		return InstrumentationRegistry.getTargetContext();
	}

	@Singleton @Provides @Named("APP_CONTEXT")
	public Context provideAppContext(@Named("INSTRUMENTATION_CONTEXT") final Context instrumentationCtx) {
		return instrumentationCtx.getApplicationContext();
	}

	@Singleton @Provides
	public Instrumentation provideInstrumentation() {
		return InstrumentationRegistry.getInstrumentation();
	}
}
