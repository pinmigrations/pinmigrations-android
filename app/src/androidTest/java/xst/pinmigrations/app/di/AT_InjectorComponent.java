package xst.pinmigrations.app.di;

import dagger.Component;
import xst.pinmigrations.activities.login.LoginActivityDeviceTest;
import xst.pinmigrations.activities.welcome.WelcomeActivityDeviceTest;
import xst.pinmigrations.app.di.scopes.ActivityScope;

@ActivityScope
@Component(dependencies = AT_BaseComponent.class)
public interface AT_InjectorComponent extends InjectorComponent {

	/** Tests */
	void inject(WelcomeActivityDeviceTest test);
	void inject(LoginActivityDeviceTest test);
}
