package xst.pinmigrations.app.di;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import xst.pinmigrations.app.App;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.TLogin;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.domain.rx.TRxJavaService;

/*
	provides services w/o deps or only on other services, not on Activity/Context
 */
@Module
public class AT_ServicesModule {
	@AppScope @Provides
	public TLogin provideInitialLoginState() {
		return new TLogin();
	}

	@AppScope @Provides
	public TLoginManager provideTLoginManager(final TLogin initialLoginState, final PrefsManager prefsManager,
											  final App app) {
		return new TLoginManager(initialLoginState, prefsManager, app);
	}
	@AppScope @Provides
	public LoginManager provideLoginManager(final TLoginManager tLoginManager) {
		return tLoginManager;
	}

	@AppScope @Provides
	public NetworkService provideNetworkService(final LoginManager loginManager, @Named("BASE_URL") final HttpUrl rootUrl) {
		return new NetworkService(rootUrl, loginManager);
	}

	@AppScope @Provides
	public RxJavaService provideRxJavaService() {
		return TRxJavaService.newServiceDefault();
	}
}
