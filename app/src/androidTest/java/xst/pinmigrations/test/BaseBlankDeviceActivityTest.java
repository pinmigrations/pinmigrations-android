package xst.pinmigrations.test;

import org.junit.Rule;

import xst.pinmigrations.test.activities.BlankActivity;

/**
 * base class for misc tests on devices
 */
public class BaseBlankDeviceActivityTest extends BaseDeviceTest {
	// NOTE: must be public
	@Rule public final UnlaunchedActivityTestRule<BlankActivity> // dont need activity at all
			mActivityRule = new UnlaunchedActivityTestRule<>(BlankActivity.class);

}
