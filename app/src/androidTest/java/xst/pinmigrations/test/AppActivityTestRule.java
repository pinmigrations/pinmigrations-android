package xst.pinmigrations.test;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import java.util.ArrayList;

import timber.log.Timber;
import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.app.AT_App;

import static com.google.common.base.Preconditions.checkNotNull;

/** ActivityTestRule that lets test perform setup before activity is launched
 * https://jabknowsnothing.wordpress.com/2015/11/05/activitytestrule-espressos-test-lifecycle/
 *
 */
public abstract class AppActivityTestRule<A extends BaseActivity> extends ActivityTestRule<A> {
	final protected @NonNull AT_App app;
	@Nullable volatile A activity;

	public AppActivityTestRule(final Class<A> activityClass, final boolean initialTouchMode, final boolean launchActivity) {
		super(activityClass, initialTouchMode, launchActivity);
		app = getApp();
	}

	//region dont use
	private AppActivityTestRule(final Class<A> activityClass) { super(activityClass); app = getApp(); }
	private AppActivityTestRule(final Class<A> activityClass, final boolean initialTouchMode) {
		super(activityClass, initialTouchMode); app = getApp(); }
	//endregion

	//region Launch

	/** launch activity without intent (eg: calls onCreate(null) ) */

	final public A launchActivityNoIntent() { return activity = super.launchActivity(null); }
	@Override final public A launchActivity(@Nullable final Intent startIntent) { return activity = super.launchActivity(startIntent); }
	//endregion


	//region Before Launch

	private Runnable beforeLaunchActivityRunnable;
	public final void setbeforeLaunchActivityRunnable(@NonNull final Runnable runnable) {
		this.beforeLaunchActivityRunnable = checkNotNull(runnable);
	}

	private Runnable beforeLaunchInstrumentationRunnable;
	public final void setbeforeLaunchInstrumentationRunnable(@NonNull final Runnable runnable) {
		this.beforeLaunchInstrumentationRunnable = checkNotNull(runnable);
	}

	@Override protected final void beforeActivityLaunched() {
		Timber.d("before activity launched");

		if (null != beforeLaunchInstrumentationRunnable) {
			Timber.d("exec before-launch instr runnable");
			beforeLaunchInstrumentationRunnable.run();
		}

		if (null != beforeLaunchActivityRunnable) {
			Timber.d("exec before-launch instr runnable");
			checkNotNull(activity).runOnUiThread(beforeLaunchActivityRunnable);
		}

		onBeforeActivityLaunched();
	}
	abstract void onBeforeActivityLaunched(); // for subclasses
	//endregion


	//region After Launch
	private  Runnable afterLaunchActivityRunnable;
	public final void setAfterLaunchActivityRunnable(@NonNull final Runnable runnable) {
		this.afterLaunchActivityRunnable = checkNotNull(runnable);
	}

	private Runnable afterLaunchInstrumentationRunnable;
	public final void setAfterLaunchInstrumentationRunnable(@NonNull final Runnable runnable) {
		this.afterLaunchInstrumentationRunnable = checkNotNull(runnable);
	}

	@Override protected final void afterActivityLaunched() {
		activity = checkNotNull( getActivity() ); // super-class doesnt hasn't completed `launchActivity` when this is invoked

		if (null != afterLaunchInstrumentationRunnable) {
			afterLaunchInstrumentationRunnable.run();
		}

		if (null != afterLaunchActivityRunnable) {
			//noinspection ConstantConditions (already checked for null above)
			activity.runOnUiThread(afterLaunchActivityRunnable);
		}

		//noinspection ConstantConditions (already checked for null above)
		onAfterActivityLaunched(activity);
	}

	abstract void onAfterActivityLaunched(@NonNull final A activity); // for subclasses
	//endregion


	//region After Finish

	/** reset stubs */
	@Override final protected void afterActivityFinished() {
		final ArrayList<StatefulStub> stubs = app.getStubs();

		for (final StatefulStub stub: stubs) { stub.reset(); }
	}

	abstract void onAfterActivityFinished(); // for subclasses
	//endregion

	private static AT_App getApp()  {
		return (AT_App) InstrumentationRegistry.getTargetContext().getApplicationContext();
	}
}
