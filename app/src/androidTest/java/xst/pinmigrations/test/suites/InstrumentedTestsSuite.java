package xst.pinmigrations.test.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import xst.pinmigrations.activities.login.LoginActivityDeviceTest;
import xst.pinmigrations.activities.welcome.WelcomeActivityDeviceTest;
import xst.pinmigrations.test.config.ConfigValidationAndroidTest;

/**
 * Instrumented Application tests
 * NOTE: use the AndroidStudio run-configuration in project, gradle isnt configured to run this properly for some reason
 * NOTE: avoid frequently running `InstrumentedBasicFunctionsSuite`
 */
@RunWith( Suite.class )
@Suite.SuiteClasses({
		ConfigValidationAndroidTest.class,
		WelcomeActivityDeviceTest.class,
		LoginActivityDeviceTest.class
})
public class InstrumentedTestsSuite { }
