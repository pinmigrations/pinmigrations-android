package xst.pinmigrations.test.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * NOTE: use `InstrumentedTestsSuite` by default
 * NOTE: avoid frequently running this (because of `InstrumentedBasicFunctionsSuite`)
 */
@RunWith( Suite.class )
@Suite.SuiteClasses({
	InstrumentedTestsSuite.class,
	InstrumentedBasicFunctionsSuite.class
})
public class AllInstrumentedValidationsSuite { }
