package xst.pinmigrations.test.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import xst.pinmigrations.domain.android_context.AssetsReadDeviceTest;
import xst.pinmigrations.test.basic_functions.WebServiceConnectivityTest;
import xst.pinmigrations.test.basic_functions.WebSocketConnectivityTest;
import xst.pinmigrations.test.config.ConfigValidationAndroidTest;

/**
 * validates device hardware to software compat (file/web/web-sockets)
 * validates test configuration (compile/link/exec, no errors)
 * NOTE: dont run this test on every build, just when testing new device or device features
 */
@RunWith( Suite.class )
@Suite.SuiteClasses({
		AssetsReadDeviceTest.class,
		WebServiceConnectivityTest.class,
		WebSocketConnectivityTest.class,
		ConfigValidationAndroidTest.class
})
public class InstrumentedBasicFunctionsSuite { }
