package xst.pinmigrations.test;

import android.support.annotation.NonNull;

import xst.pinmigrations.activities.BaseActivity;

import static com.google.common.base.Preconditions.checkNotNull;

/** ActivityTestRule that allows performing setup before activity is launched
 * https://jabknowsnothing.wordpress.com/2015/11/05/activitytestrule-espressos-test-lifecycle/
 *
 * Execution Order:
 * 	@Before methods in TestCase
 * 	test-method -> activityRule.launchActivity
 * 	super.beforeActivityLaunched() -> onBeforeActivityLaunched
 * 	Activity CREATE/START/RESUME
 * 	super.afterActivityLaunched() -> onAfterActivityLaunched
 * 	Activity PAUSE/STOP
 * 	@Test method in TestCase
 * 	@After methods in TestCase
 * 	afterActivityFinished()
 */
public class UnlaunchedActivityTestRule<A extends BaseActivity> extends AppActivityTestRule<A> {
	/**
	 * NOTE: use `testRule.launchActivity(null);` to launch activity w default intent
	 */
	public UnlaunchedActivityTestRule(@NonNull final Class<A> activityClass) {
		super(checkNotNull(activityClass),
				false,    // initialTouchMode (default)
				false);  // launchActivity (default=true) False so we set up before activity launch
	}

	@Override void onBeforeActivityLaunched() { }
	@Override void onAfterActivityLaunched(@NonNull final A activity) { }

	@Override void onAfterActivityFinished() { }
}
