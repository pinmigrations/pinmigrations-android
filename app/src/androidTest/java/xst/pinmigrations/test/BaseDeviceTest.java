package xst.pinmigrations.test;

import android.app.Instrumentation;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.UiDevice;

import org.assertj.core.api.Fail;

import xst.pinmigrations.activities.BaseActivity;
import xst.pinmigrations.app.AT_App;
import xst.pinmigrations.app.App;
import xst.pinmigrations.app.di.AT_BaseComponent;
import xst.pinmigrations.app.di.AT_InjectorComponent;
import xst.pinmigrations.app.di.DaggerAT_InjectorComponent;
import xst.pinmigrations.domain.network.AT_NetworkUtil;

import static com.google.common.base.Verify.verifyNotNull;

public abstract class BaseDeviceTest extends BaseTest {
	static {
		App.setExecModeDeviceTests();

		// NOTE: enable when running *BasicFunctions tests, not necessary for regular instrumented tests
		if (AT_App.AT_AppDebug) {
			AT_NetworkUtil.pingRemoteBlocking(); // ping server so basic functions tests can run
		}
	}


	@NonNull final protected Context instrumentationCtx;
	@NonNull final protected AT_App app;
	@NonNull final protected Instrumentation instrumentation;
	@NonNull final protected UiDevice uiDevice;

	public BaseDeviceTest() {
		instrumentation = verifyNotNull( InstrumentationRegistry.getInstrumentation() );
		instrumentationCtx = verifyNotNull( InstrumentationRegistry.getTargetContext() );
		app = (AT_App) verifyNotNull( instrumentationCtx.getApplicationContext() );
		uiDevice = verifyNotNull( UiDevice.getInstance(instrumentation) );
	}

	public AT_InjectorComponent getInjector() {
		return DaggerAT_InjectorComponent.builder()
				.aT_BaseComponent( (AT_BaseComponent) app.getBaseComponent() ).build();
	}

	/** needed for Espresso tests */
	protected void wakeUpDevice() {
		final Point[] coordinates = new Point[4];
		coordinates[0] = new Point(248, 1520);
		coordinates[1] = new Point(248, 929);
		coordinates[2] = new Point(796, 1520);
		coordinates[3] = new Point(796, 929);
		try {
			if (!uiDevice.isScreenOn()) {
				uiDevice.wakeUp();
				uiDevice.swipe(coordinates, 10);
			}
		} catch (final RemoteException remoteException) {
			Fail.fail("could not wake up device", remoteException);
		}
	}

	protected void rotateScreen(final BaseActivity activity) {
		final int orientation = instrumentationCtx.getResources().getConfiguration().orientation;
		activity.setRequestedOrientation(
				(orientation == Configuration.ORIENTATION_PORTRAIT) ?
						ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
}
