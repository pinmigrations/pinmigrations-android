package xst.pinmigrations.test.basic_functions;

import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.atomic.AtomicInteger;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocketListener;
import timber.log.Timber;
import xst.pinmigrations.test.BaseBlankDeviceActivityTest;
import xst.pinmigrations.domain.network.AT_NetworkUtil;
import xst.pinmigrations.app.util.TAsyncUtil;
import xst.pinmigrations.test.websockets.AT_WebSocketConnectivityListener;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(AndroidJUnit4.class)
public class WebSocketConnectivityTest extends BaseBlankDeviceActivityTest {
	/**
	 * NOTE: this test fails when run with `connected*AndroidTest` with
	 * NoClassDefFoundError: ... WebSocketConnectivityTest$1
	 * but executes correctly when run with suite classes
	 */
	@LargeTest
	@Test
	public void shouldWebSocketConnectToFeed() throws Throwable {
		// GIVEN
		// one to wait for remote WS response
		// the second to signal that the WS thread completed
		final AtomicInteger counter = new AtomicInteger(2);

		final Throwable[] asyncThrowable = { null }; // synchronize on array to return errors to test thread
		final String[] responseText = { null };  // synchronize on array to return text to test thread

		final WebSocketListener wsl = new AT_WebSocketConnectivityListener(counter, responseText);
		final OkHttpClient client = AT_NetworkUtil.getHttpClient(30, SECONDS);

		final Request request = new Request.Builder()
				.url("ws://pinmigrations.herokuapp.com/feed")
				.build();


		@SuppressWarnings("AnonymousInnerClassMayBeStatic")
		final Thread t = new Thread(new Runnable() {
			@Override public void run() {
				try {
					Timber.d("opening websocket...");
					client.newWebSocket(request, wsl);

					// give webservice time to respond
					TAsyncUtil.asyncWaitStep(counter, 1, 30_000);
				} catch (final Throwable throwable){
					asyncThrowable[0] = throwable; // not logging/failing here yet
				} finally {
					// Trigger shutdown of the dispatcher's executor so this process can exit cleanly.
					client.dispatcher().executorService().shutdown();
				}
			}
		});


		// WHEN
		t.start();
		// wait until test completes
		TAsyncUtil.asyncCountdown(counter, 30_000);
		t.interrupt();


		// THEN
		// get any async error, including exceptions and assertationErrors
		if ( asyncThrowable[0] != null ) {
			fail(asyncThrowable[0]);
		}

		assertThat(responseText[0]).as("response-text").isNotNull();
	}
}
