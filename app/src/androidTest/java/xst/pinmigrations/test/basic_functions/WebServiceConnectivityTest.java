package xst.pinmigrations.test.basic_functions;

import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import xst.pinmigrations.test.BaseBlankDeviceActivityTest;
import xst.pinmigrations.domain.network.AT_NetworkUtil;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(AndroidJUnit4.class)
public class WebServiceConnectivityTest extends BaseBlankDeviceActivityTest {
	@LargeTest
	@Test
	public void shouldConnectToFeed() throws IOException {
		// GIVEN
		final OkHttpClient client = AT_NetworkUtil.getHttpClient(30, SECONDS);
		final Request request = new Request.Builder()
				.url("http://pinmigrations.herokuapp.com/feed")
				.build();


		// WHEN
		final Response response = client.newCall(request).execute();


		// THEN
		assertThat(response.code()).as("response.code").isEqualTo(200);
	}
}
