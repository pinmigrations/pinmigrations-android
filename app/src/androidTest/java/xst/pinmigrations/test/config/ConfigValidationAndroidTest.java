package xst.pinmigrations.test.config;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.function.Consumer;

import timber.log.Timber;
import xst.pinmigrations.test.BaseBlankDeviceActivityTest;

import static org.assertj.core.api.Assertions.assertThat;

/** NOTE: ConfigValidationTestCommon from `testCommon` source set */



/**
 * assertions on test environment!
 */
@RunWith(AndroidJUnit4.class)
public class ConfigValidationAndroidTest extends BaseBlankDeviceActivityTest {
	@Test
	public void shouldCompileAndLinkTestCommon() {
		Timber.d( ConfigValidationTestCommon.validateConfig() );

		assertThat( ConfigValidationTestCommon.validateConfig() ).isEqualTo("AWESOME");
	}

	@Test
	public void testCommonShouldLinkWithTestOnlyLibs() {
		final String str = "APACHE-COMMONS-LANG: random: " +
				ConfigValidationTestCommon.tryUsingApacheCommons();

		Timber.d(str);
		assertThat( str ).isNotEmpty();
	}

	/** leaving for future upgrades to build env */
	// @Test
	public void shouldLinkWithMainCodeWithMethodReferences() {
		Consumer<String> fnRef = ConfigValidationMain.getMethodReference();
		Timber.d("AWESOME!!");
	}

	/** leaving for future upgrades to build env */
	// @Test
	public void shouldLinkWithMainCodeWithLambdas() {
		Consumer<String> fnRef = ConfigValidationMain.getLambda();
		Timber.d("AWESOME!!");
	}

	/** leaving for future upgrades to build env */
	// @Test
	public void shouldLinkWithTestCommonCodeWithMethodReferences() {
		Consumer<String> fnRef = ConfigValidationTestCommon.getMethodReference();
		Timber.d("AWESOME!!");
	}

	/** leaving for future upgrades to build env */
	// @Test
	public void shouldLinkWithTestCommonCodeWithLambdas() {
		Consumer<String> fnRef = ConfigValidationTestCommon.getLambda();
		Timber.d("AWESOME!!");
	}

	/** leaving for future upgrades to build env */
	// @Test
	public void shouldLinkWithMainCodeThatUsesMethodReferencesInternally() {
		ConfigValidationMain.usesMethodReferenceInternally();
		Timber.d("AWESOME!!");
	}

	/** leaving for future upgrades to build env */
	// @Test
	public void shouldLinkWithMainCodeThatUsesLambdasInternally() {
		ConfigValidationMain.usesLambdasInternally();
		Timber.d("AWESOME!!");
	}
}