package xst.pinmigrations.test;

import android.support.annotation.NonNull;

import xst.pinmigrations.activities.BaseActivity;

import static com.google.common.base.Preconditions.checkNotNull;

/** ActivityTestRule that sets up and launches activity (exactly as default ActivityTestRule)
 * the main purpose of this class is to make test-rule behavior known and explicit
 * https://jabknowsnothing.wordpress.com/2015/11/05/activitytestrule-espressos-test-lifecycle/
 *
 * Execution Order:
 * 	super.beforeActivityLaunched() -> onBeforeActivityLaunched
 * 	Activity CREATE/START/RESUME
 * 	super.afterActivityLaunched() -> onAfterActivityLaunched
 * 	@Before methods in TestCase
 * 	@Test method in TestCase
 * 	@After methods in TestCase
 * 	Activity PAUSE
 * 	afterActivityFinished()
 */
public class LaunchedActivityTestRule<A extends BaseActivity> extends AppActivityTestRule<A> {
	/**
	 * NOTE: use `testRule.launchActivity(null);` to launch activity w default intent
	 */
	public LaunchedActivityTestRule(@NonNull final Class<A> activityClass) {
		super(checkNotNull(activityClass),
				false,    // initialTouchMode (default)
				true);  // launchActivity (default)
	}

	@Override void onBeforeActivityLaunched() { }
	@Override void onAfterActivityLaunched(@NonNull final A activity) { }
	@Override void onAfterActivityFinished() { }
}
