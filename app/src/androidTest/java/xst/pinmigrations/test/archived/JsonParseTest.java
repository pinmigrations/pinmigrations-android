package xst.pinmigrations.test.archived;

/*
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;


// NOTE: removing Json.simple dependency from project
// if needed again use
// compile 'com.googlecode.json-simple:json-simple:1.1.1'
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import timber.log.Timber;
import xst.pinmigrations.activities.positionslist.PositionsListActivity;
import xst.pinmigrations.domain.Position;
import xst.pinmigrations.util.FileUtil;
*/

/**
 * Basic TDD Test for JSON Parsing using Json.simple
 * test code is the SUT (system under test), so not useful to re-use
 * Json.simple library is being removed from the project so this will no longer compile also
 * Keep as an example only.
 */

/*
@RunWith(AndroidJUnit4.class)
public class JsonParseTest extends ActivityInstrumentationTestCase2<PositionsListActivity> {

	PositionsListActivity mActivity = null;
	static String exampleJsonString = null;

	public JsonParseTest() {
		super(PositionsListActivity.class);
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();

		injectInstrumentation(InstrumentationRegistry.getInstrumentation());
		mActivity = getActivity();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	private static String getJson(Context ctx) throws IOException {
		InputStream is = FileUtil.getAssetFile(ctx, "example-positions.json");
		return FileUtil.readFile(is);
	}

	private static boolean ensureTestDataIsReady(Context ctx) {
		if (null == exampleJsonString) {
			try {
				exampleJsonString = getJson(ctx);
			} catch (IOException e) {
				Timber.e(e);
				return false;
			}
			return null != exampleJsonString;
		} else {
			return true;
		}
	}

	public static final double MAX_DELTA_DOUBLE_COMPARISONS = 1e-15;


	@Test
	public void shouldParseJsonString() {
		if (ensureTestDataIsReady(mActivity)) {

			JSONParser parser = new JSONParser();
			try {
				Object obj = parser.parse(exampleJsonString);
				JSONArray jsonArray = (JSONArray) obj;


				Iterator iter = jsonArray.iterator();
				JSONObject posJson;
				Position position;
				int numPosParsed = 0;
				while (iter.hasNext()) {
					posJson = (JSONObject) iter.next();
					position = new Position(posJson);

					if (0 == numPosParsed) {
						assertEquals(position.id, 58);
						assertEquals(position.lat, 20.61222, MAX_DELTA_DOUBLE_COMPARISONS);
						assertEquals(position.lng, -103.3374, MAX_DELTA_DOUBLE_COMPARISONS);
						assertEquals(position.description, "Julio arana");
						assertEquals(position.guid, "");
						assertEquals(position.ord, 0);
					}

					if (49 == numPosParsed) {
						assertEquals(position.id, 72);
						assertEquals(position.lat, 48.16609, MAX_DELTA_DOUBLE_COMPARISONS);
						assertEquals(position.lng, 66.88477, MAX_DELTA_DOUBLE_COMPARISONS);
						assertEquals(position.description, "");
						assertEquals(position.guid, "ea4863f2-0234-4d5f-a9b9-174a6b3d98d0");
						assertEquals(position.ord, 0);
					}

					numPosParsed += 1;
				}

				assertEquals(numPosParsed, 50);
			} catch (ParseException e) {
				logAndFail("parse exception");
			}
		} else {
			logAndFail("example json string unavailable");
		}
	}


	private void logAndFail(String msg) {
		Timber.d(msg);
		fail(msg);
	}
}
*/