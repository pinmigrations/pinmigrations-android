package xst.pinmigrations.activities.map;

import org.junit.Rule;
import org.junit.Test;

import timber.log.Timber;
import xst.pinmigrations.R;
import xst.pinmigrations.app.util.condition_watcher.ConditionWatcher;
import xst.pinmigrations.test.BaseEspressoTest;
import xst.pinmigrations.test.UnlaunchedActivityTestRule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.assertj.core.api.Assertions.assertThat;

public class MapActivityDeviceTest extends BaseEspressoTest {
	public static final String MAP_FQN = "com.google.android.gms.maps.GoogleMap";


	// NOTE: must be public
	@Rule public final UnlaunchedActivityTestRule<MapActivity>
			activityRule = new UnlaunchedActivityTestRule<>(MapActivity.class);

	MapActivity mapActivity;
	GMapFragment mapFragment;

	@Test public void shouldLoadMapAndMapUi() throws Exception {
		// GIVEN


		// WHEN
		mapActivity = activityRule.launchActivityNoIntent();
		ConditionWatcher.waitForCondition(mapReady, 250, MILLISECONDS, 7, SECONDS);
		mapFragment = mapActivity.mMapFragment;


		// THEN
		onView(withId(R.id.map_btn1)).check(matches(isDisplayed()));
		onView(withId(R.id.map_btn2)).check(matches(isDisplayed()));
		onView(withId(R.id.map_lockRotationBtn)).check(matches(isDisplayed()));

		assertThat( mapActivity.mMapReady ).isTrue();
		assertThat( mapFragment ).isNotNull();
		assertThat( mapFragment.mMap ).isNotNull();

		// different implementation class in view hierarchy
		// final UiObject map = uiDevice.findObject(new UiSelector().className(MAP_FQN));
		// assertThat( map.exists() ).isTrue();
	}

	final ConditionWatcher.Condition mapReady = new ConditionWatcher.Condition() {
		@Override public boolean checkCondition() {
			final GMapFragment fragment = mapActivity.mMapFragment;
			if (null == fragment) { return false; }
			else { return null != fragment.mMap; }
		}
		@Override public void failLog() {
			final GMapFragment fragment = mapActivity.mMapFragment;
			if (null == fragment) { Timber.d("map fragment is null"); }
			if (null != fragment && null == fragment.mMap) { Timber.d("map is null"); }
		}
	};
}
