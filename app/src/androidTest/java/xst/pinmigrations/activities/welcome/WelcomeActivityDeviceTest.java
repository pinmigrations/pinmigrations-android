package xst.pinmigrations.activities.welcome;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.inject.Inject;

import timber.log.Timber;
import xst.pinmigrations.R;
import xst.pinmigrations.app.util.condition_watcher.ConditionWatcher;
import xst.pinmigrations.domain.api.StubApiGenerator;
import xst.pinmigrations.domain.login.AT_LoginUtil;
import xst.pinmigrations.domain.login.LoginApi;
import xst.pinmigrations.domain.login.LoginApiStub;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.test.BaseEspressoTest;
import xst.pinmigrations.test.UnlaunchedActivityTestRule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

public class WelcomeActivityDeviceTest extends BaseEspressoTest {
	// NOTE: must be public
	@Rule public final UnlaunchedActivityTestRule<WelcomeActivity>
			activityRule = new UnlaunchedActivityTestRule<>(WelcomeActivity.class);

	WelcomeActivity welcomeActivity;

	@Inject StubApiGenerator stubApiGenerator;
	@Inject TLoginManager loginManager;
	LoginApiStub loginApiStub;

	@Before public void setUp() throws Exception {
		getInjector().inject(this);
		loginApiStub = (LoginApiStub) stubApiGenerator.createService(LoginApi.class);
	}

	@Test public void shouldShowLoginButtonIfNotSignedIn() throws Exception {
		// GIVEN
		AT_LoginUtil.setupStubsForCheckSignInFalse(stubApiGenerator, loginManager);

		// WHEN
		welcomeActivity = activityRule.launchActivityNoIntent();
		ConditionWatcher.waitForCondition(new WelcomeLoginCheckCompleted());

		// THEN
		onView(withId(R.id.welcome_loginBtn)).check(matches(isDisplayed()));
		onView(withId(R.id.welcome_logoutBtn)).check(matches(not(isDisplayed())));
		org.assertj.core.api.Assertions.
				assertThat( loginApiStub.getCheckSigninStatusParams() ).hasSize(1);
	}


	@Test public void shouldShowLogoutButtonIfSignedIn() throws Exception {
		// GIVEN
		AT_LoginUtil.setupStubsForCheckSignInTrue(stubApiGenerator, loginManager);

		// WHEN
		welcomeActivity = activityRule.launchActivityNoIntent();
		ConditionWatcher.waitForCondition(new WelcomeLoginCheckCompleted());

		// THEN
		onView(withId(R.id.welcome_loginBtn)).check(matches(not(isDisplayed())));
		onView(withId(R.id.welcome_logoutBtn)).check(matches(isDisplayed()));
		org.assertj.core.api.Assertions.
				assertThat( loginApiStub.getCheckSigninStatusParams() ).hasSize(1);
	}

	public class WelcomeLoginCheckCompleted extends ConditionWatcher.Condition {
		@Override public boolean checkCondition() {
			final boolean loginApiCompleted = loginApiStub.stubbedMethodsCompleted();
			final boolean loginSubscriberCompleted = welcomeActivity.mLoginSubscriber.isUnsubscribed();
			// need to check both
			// only loginApiCompleted might be too soon, UI may not be updated
			// only loginSubscriberCompleted might be too soon, will be unsubscribed before loginApi is invoked
			return loginApiCompleted && loginSubscriberCompleted;
		}

		@Override public void failLog() {
			if (!loginApiStub.stubbedMethodsCompleted()) { Timber.d("stubbed methods not complete"); }
			if (!welcomeActivity.mLoginSubscriber.isUnsubscribed()) { Timber.d("LoginSubscriber still subscribed"); }
		}
	}
}
