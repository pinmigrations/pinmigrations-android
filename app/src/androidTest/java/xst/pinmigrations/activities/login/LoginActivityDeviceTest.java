package xst.pinmigrations.activities.login;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import timber.log.Timber;
import xst.pinmigrations.R;
import xst.pinmigrations.app.util.condition_watcher.ConditionWatcher;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceSpy;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceSpyGenerator;
import xst.pinmigrations.domain.api.StubApiGenerator;
import xst.pinmigrations.domain.login.AT_LoginUtil;
import xst.pinmigrations.domain.login.LoginApi;
import xst.pinmigrations.domain.login.LoginApiStub;
import xst.pinmigrations.domain.login.LoginApiStub.LoginParams;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.test.BaseEspressoTest;
import xst.pinmigrations.test.UnlaunchedActivityTestRule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.not;
import static xst.pinmigrations.domain.login.TLogin.TEST_EMAIL;
import static xst.pinmigrations.domain.login.TLogin.TEST_PASSWORD;

@RunWith(AndroidJUnit4.class)
public class LoginActivityDeviceTest extends BaseEspressoTest {
	// NOTE: must be public
	@Rule public final UnlaunchedActivityTestRule<LoginActivity>
			activityRule = new UnlaunchedActivityTestRule<>(LoginActivity.class);

	@Inject StubApiGenerator stubApiGenerator;
	@Inject TLoginManager loginManager;
	@Inject UiServiceSpyGenerator uiServiceSpyGenerator;
	LoginApiStub loginApiStub;

	LoginActivity loginActivity;
	LoginPresenter loginPresenter;
	UiServiceSpy uiServiceSpy;

	@Before public void setUp() throws Exception {
		getInjector().inject(this);
		loginApiStub = (LoginApiStub) stubApiGenerator.createService(LoginApi.class);
	}

	@Test public void shouldLoadProperlyWhenLoggedIn() throws Exception {
		// GIVEN
		AT_LoginUtil.setupStubsForCheckSignInTrue(stubApiGenerator, loginManager);

		// WHEN
		loginActivity = activityRule.launchActivityNoIntent();
		loginPresenter = loginActivity.mLoginPresenter;
		ConditionWatcher.waitForCondition(checkSigninCompleted);

		// THEN
		onView(withId(R.id.login_loginBtn)).check(matches(not(isDisplayed())));
		onView(withId(R.id.login_logoutBtn)).check(matches(isDisplayed()));
		assertThat( loginApiStub.getCheckSigninStatusParams() ).hasSize(1);
	}

	@Test public void shouldLoadProperlyWhenLoggedOut() throws Exception {
		// GIVEN
		AT_LoginUtil.setupStubsForCheckSignInFalse(stubApiGenerator, loginManager);

		// WHEN
		loginActivity = activityRule.launchActivityNoIntent();
		loginPresenter = loginActivity.mLoginPresenter;
		ConditionWatcher.waitForCondition(checkSigninCompleted);

		// THEN
		onView(withId(R.id.login_loginBtn)).check(matches(isDisplayed()));
		onView(withId(R.id.login_logoutBtn)).check(matches(not(isDisplayed())));
		assertThat( loginApiStub.getCheckSigninStatusParams() ).hasSize(1);
	}

	@Test public void clickingLoginButton_shouldLogin() throws Exception {
		// GIVEN
		AT_LoginUtil.setupStubsForCheckSignInFalse(stubApiGenerator, loginManager);
		loginActivity = activityRule.launchActivityNoIntent();
		loginPresenter = loginActivity.mLoginPresenter;
		ConditionWatcher.waitForCondition(checkSigninCompleted);

		AT_LoginUtil.setupStubsForLoginSuccess(stubApiGenerator, loginManager, TEST_EMAIL, TEST_PASSWORD);

		// WHEN
		onView(withId(R.id.login_emailEdit)).perform(replaceText(TEST_EMAIL));
		onView(withId(R.id.login_passwordEdit)).perform(replaceText(TEST_PASSWORD));
		onView(withId(R.id.login_loginBtn)).perform(click());

		ConditionWatcher.waitForCondition(loginCompleted, 200, MILLISECONDS, 9, SECONDS); // TODO: test times-out waiting here

		// THEN
		final LoginParams params = loginApiStub.getLoginParams().get(0);
		assertThat( params.email ).isEqualTo(TEST_EMAIL);
		assertThat( params.password ).isEqualTo(TEST_PASSWORD);

		assertThat(loginManager.isAuthed()).isTrue();
	}

	@Test public void clickingLogoutButton_shouldLogout() throws Exception {
		// GIVEN
		AT_LoginUtil.setupStubsForCheckSignInTrue(stubApiGenerator, loginManager);
		loginActivity = activityRule.launchActivityNoIntent();
		loginPresenter = loginActivity.mLoginPresenter;
		ConditionWatcher.waitForCondition(checkSigninCompleted);

		AT_LoginUtil.setupStubsForLogout(stubApiGenerator, loginManager, TEST_EMAIL, TEST_PASSWORD);

		// WHEN
		onView(withId(R.id.login_logoutBtn)).perform(click());
		ConditionWatcher.waitForCondition(logoutCompleted);

		// THEN
		assertThat(loginManager.isUnauthed()).isTrue();
	}

	@Test public void clickingLoginButtonWithoutCredentials_shouldShowMessageOnly() throws Exception {
		// GIVEN
		AT_LoginUtil.setupStubsForCheckSignInFalse(stubApiGenerator, loginManager);
		loginActivity = activityRule.launchActivityNoIntent();
		loginPresenter = loginActivity.mLoginPresenter;
		ConditionWatcher.waitForCondition(checkSigninCompleted);

		uiServiceSpy = uiServiceSpyGenerator.getGeneratedUiService(LoginActivity.class);

		// WHEN
		onView(withId(R.id.login_emailEdit)).perform(clearText());
		onView(withId(R.id.login_passwordEdit)).perform(clearText());
		onView(withId(R.id.login_loginBtn)).perform(click());

		// THEN
		assertThat(uiServiceSpy.getImportantMessages()).hasSize(1); // in case message changes, fail at the right spot
		assertThat(uiServiceSpy.getImportantMessages().get(0)).isEqualTo("Please complete all fields");
		assertThat(loginManager.isUnauthed()).isTrue();
		assertThat(loginApiStub.getLoginParams()).hasSize(0); // no attempts (note: `.login()` would have caused error)
	}


	//region ConditionWatcher

	final ConditionWatcher.Condition checkSigninCompleted = new ConditionWatcher.Condition() {
		@Override public boolean checkCondition() {
			return loginApiStub.checkSigninStatusCompleted()
					&& loginPresenter.mCheckSignInSubscriber.isUnsubscribed();
		}

		@Override public void failLog() {
			if (!loginApiStub.checkSigninStatusCompleted()) { Timber.d("LoginApi.checkSigninStatus not complete"); }
			if (!loginPresenter.mCheckSignInSubscriber.isUnsubscribed()) { Timber.d("CheckSigninSubscriber still subscribed"); }
		}
	};
	final ConditionWatcher.Condition loginCompleted = new ConditionWatcher.Condition() {
		@Override public boolean checkCondition() {
			return loginApiStub.checkSigninStatusCompleted()
					&& loginApiStub.loginCompleted()
					&& loginPresenter.mLoginSubscriber.isUnsubscribed();
		}
		@Override public void failLog() {
			if (!loginApiStub.checkSigninStatusCompleted()) { Timber.d("LoginApi.checkSignIn not complete"); }
			if (!loginApiStub.loginCompleted()) { Timber.d("LoginApi.login not complete"); }
			if (!loginPresenter.mLoginSubscriber.isUnsubscribed()) { Timber.d("LoginSubscriber still subscribed"); }
		}
	};
	final ConditionWatcher.Condition logoutCompleted = new ConditionWatcher.Condition() {
		@Override public boolean checkCondition() {
			return loginApiStub.checkSigninStatusCompleted()
					&& loginApiStub.logoutCompleted()
					&& loginPresenter.mLogoutSubscriber.isUnsubscribed();
		}
		@Override public void failLog() {
			if (!loginApiStub.checkSigninStatusCompleted()) { Timber.d("LoginApi.checkSignIn not complete"); }
			if (!loginApiStub.logoutCompleted()) { Timber.d("LoginApi.logout not complete"); }
			if (!loginPresenter.mLogoutSubscriber.isUnsubscribed()) { Timber.d("LogoutSubscriber still subscribed"); }
		}
	};
	//endregion
}
