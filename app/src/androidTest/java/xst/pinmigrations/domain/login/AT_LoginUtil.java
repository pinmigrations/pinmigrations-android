package xst.pinmigrations.domain.login;


import android.support.annotation.NonNull;

import xst.pinmigrations.domain.api.StubApiGenerator;
import xst.pinmigrations.domain.login.TLogin.LoginInterceptObservable;
import xst.pinmigrations.domain.login.json.TSignedInResponse;

public final class AT_LoginUtil {

	/** sets LOGGED_IN in LoginManager, stubs LoginApiStub to return TSignedInResponse w true */
	public static void setupStubsForCheckSignInTrue(@NonNull final StubApiGenerator stubApiGenerator,
									 @NonNull final TLoginManager loginManager) {
		loginManager.setLoginObj( TLogin.newTestAuthed() );

		// creates or retrieves it
		final LoginApiStub loginApiStub = (LoginApiStub) stubApiGenerator.createService(LoginApi.class);
		loginApiStub.putCheckSigninStatus( TSignedInResponse.newSignedInTrueObservable() );
	}


	/** sets LOGGED_IN in LoginManager, stubs LoginApiStub to return TSignedInResponse w false */
	public static void setupStubsForCheckSignInFalse(@NonNull final StubApiGenerator stubApiGenerator,
													 @NonNull final TLoginManager loginManager) {
		loginManager.setLoginObj( TLogin.newPristineUnauthed() );

		// creates or retrieves it
		final LoginApiStub loginApiStub = (LoginApiStub) stubApiGenerator.createService(LoginApi.class);
		loginApiStub.putCheckSigninStatus( TSignedInResponse.newSignedInFalseObservable() );
	}

	/** stubs LoginApiStub to update LoginManager on logout */
	public static void setupStubsForLogout(@NonNull final StubApiGenerator stubApiGenerator,
										   @NonNull final TLoginManager loginManager,
												 @NonNull final String email, @NonNull final String password) {
		final LoginApiStub loginApiStub = (LoginApiStub) stubApiGenerator.createService(LoginApi.class);
		loginApiStub.putLogout( LoginInterceptObservable.create(loginManager,
				TLogin.newLogoutUnauthed(email, password) ));
	}

	/**
	 * LoginManager, stubs LoginApiStub to return TSignedInResponse w true
	 * NOTE: an invocation of LoginResource.login forces a LoginApi.checkSignInStatus,
	 * so have to add another a stub response for checkSignInStatus also
	 */
	public static void setupStubsForLoginSuccess(@NonNull final StubApiGenerator stubApiGenerator,
												 @NonNull final TLoginManager loginManager,
												@NonNull final String email, @NonNull final String password) {


		final LoginApiStub loginApiStub = (LoginApiStub) stubApiGenerator.createService(LoginApi.class);
		// NOTE: an invocation of LoginResource.login forces a LoginApi.checkSignInStatus
		loginApiStub.putCheckSigninStatus( TSignedInResponse.newSignedInFalseObservable() );
		loginApiStub.putLogin( LoginInterceptObservable.create(loginManager,
				TLogin.newAuthed(email, password) ));
	}
}
