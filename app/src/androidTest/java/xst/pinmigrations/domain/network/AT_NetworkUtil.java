package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import timber.log.Timber;
import xst.pinmigrations.app.di.AT_AppModule;
import xst.pinmigrations.app.util.MainUtil;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static xst.pinmigrations.domain.login.DefaultHeadersInterceptor.DNT_HDR_VAL;
import static xst.pinmigrations.domain.login.DefaultHeadersInterceptor.DNT_HEADER;
import static xst.pinmigrations.domain.network.NetworkService.PLATFORM_HEADER_KEY;
import static xst.pinmigrations.domain.network.NetworkService.REQUESTED_WITH_HEADER_KEY;
import static xst.pinmigrations.domain.network.NetworkService.REQUESTED_WITH_HEADER_VAL;
import static xst.pinmigrations.domain.network.NetworkService.platformHdrVal;

public class AT_NetworkUtil {
	static final int MISC_CLIENT_TIMEOUT_SECONDS = 7;



	/** configure a misc/util HttpClient, no auth, no logging */
	public static OkHttpClient getHttpClient(final double timeout, @NonNull final TimeUnit timeUnit) {
		final int msTimeout = MainUtil.getMs(timeout, timeUnit);
		return new OkHttpClient.Builder()
				.connectTimeout(msTimeout, MILLISECONDS)
				.readTimeout(msTimeout, MILLISECONDS)
				.writeTimeout(msTimeout, MILLISECONDS)
				.followRedirects(true)
				.build();
	}

	public static void pingRemoteBlocking() {
		pingRemoteBlocking(MISC_CLIENT_TIMEOUT_SECONDS, SECONDS);
	}

	public static void pingRemoteBlocking(final double maxWait, @NonNull final TimeUnit timeUnit) {
		final int msTimeout = MainUtil.getMs(maxWait, timeUnit);

		final OkHttpClient client = getHttpClient(msTimeout, MILLISECONDS);
		final Request pingRequest = newPingRequestObj();

		// BLOCK!!!
		try {
			Timber.i("ping remote before android-tests");
			client.newCall(pingRequest).execute();
			Timber.i("remote responded to ping before timeout!");
		} catch (final IOException exc) {
			if (exc instanceof SocketTimeoutException) {
				Timber.i("ping socket timeout with length: " + msTimeout + "ms");
			} else {
				throw new RuntimeException("connect error trying to ping", exc);
			}
		}
	}

	public static final HttpUrl PROD_PING_URL = AT_AppModule.BASE_URL.resolve("/ping");
	static Request newPingRequestObj() {
		return new Request.Builder()
				.url(PROD_PING_URL)
				.cacheControl(CacheControl.FORCE_NETWORK)
				// add headers manually to skip creating `DefaultHeadersInterceptor`
				.addHeader(REQUESTED_WITH_HEADER_KEY, REQUESTED_WITH_HEADER_VAL)
				.addHeader(PLATFORM_HEADER_KEY, platformHdrVal)
				.addHeader(DNT_HEADER, DNT_HDR_VAL)
				.build();
	}
}
