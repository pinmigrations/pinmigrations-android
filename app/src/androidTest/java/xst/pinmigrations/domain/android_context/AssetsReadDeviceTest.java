package xst.pinmigrations.domain.android_context;

import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;

import xst.pinmigrations.test.BaseBlankDeviceActivityTest;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(AndroidJUnit4.class)
public class AssetsReadDeviceTest extends BaseBlankDeviceActivityTest {

	AndroidService androidService;
	@Before public void setupAssetService() {
		androidService = new AndroidService(instrumentationCtx);
	}

	@LargeTest @Test public void shouldOpenFileAsInputStream() throws IOException {
		final InputStream is = androidService.getAssetFile("example-positions.json");

		assertThat(is).isNotNull();
	}

	public final static String JSON_START = "[{\"id\":58,";
	public final static String JSON_END = "\"ord\":0}]";

	@LargeTest @Test public void shouldReadInputStream() throws IOException {
		final String json = androidService.getAssetFileAsString("example-positions.json");

		assertThat(json).isNotNull();
		assertThat(json).isNotNull();
		assertThat(json.length()).as("json.length").isEqualTo(5586);

		final String jsonReadStart = json.substring(0, JSON_START.length());
		assertThat(jsonReadStart).as("jsonReadStart").isEqualTo(JSON_START);

		final String jsonReadEnd = json.substring(json.length() - JSON_END.length());
		assertThat(jsonReadEnd).as("jsonReadEnd").isEqualTo(JSON_END);
	}
}