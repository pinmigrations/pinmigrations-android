package xst.pinmigrations.app;

import android.os.StrictMode;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

public class DebugApp extends App {
	// set Strict-Mode to detect all and log
	static {
		if ( normalExec() ) { // dont enable strict policies for tests
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
					.detectAll()
					.penaltyLog()
					.build());

			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
					.detectAll()
					.penaltyLog()
					.build());
		}
	}

	/**
	 * Links:
	 * + https://github.com/square/leakcanary/wiki/Customizing-LeakCanary
	 * + https://github.com/square/leakcanary/blob/master/leakcanary-android/src/main/java/com/squareup/leakcanary/AndroidRefWatcherBuilder.java
	 * + https://github.com/square/leakcanary/blob/master/leakcanary-android/src/main/java/com/squareup/leakcanary/ActivityRefWatcher.java
	 */
	@SuppressWarnings("MethodWithMultipleReturnPoints") // muliple returns are cleaner for such a simple method
	@Override
	protected RefWatcher installLeakCanary(final App app) {
		if (deviceTestExec() || unitTestExec()) { return RefWatcher.DISABLED; }

		return sDisableLeakCanary ? RefWatcher.DISABLED : LeakCanary.refWatcher(app)
				.maxStoredHeapDumps(100)
				.buildAndInstall();
	}
}
