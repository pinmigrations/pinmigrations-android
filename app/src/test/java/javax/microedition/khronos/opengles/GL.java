package javax.microedition.khronos.opengles;

/**
 * fix for Robolectric 3.3 on SDK 23
 */
// https://github.com/robolectric/robolectric/issues/1932
public interface GL {
}
