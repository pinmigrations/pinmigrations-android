package xst.pinmigrations.implementation.java;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class Java8Demo {
	public static class Fns {
		void m1() { }
		void m2(String s) { }

		String m3() { return ""; }
		String m4(String s) { return s; }

		static void m5() { }
		static void m6(String s) { }

		boolean m7() { return false; }
		boolean m8(String s) { return true; }

		String m9(String s1, String s2) { return s1 + s2; }
		Integer m10(String s, boolean b) { return null; }
	}

//	@Test
	public void functionalInterfacesAndMethodReferences() {
		Fns fnsObject = new Fns();

		Runnable f1 = fnsObject::m1; // no param, no return type
		Consumer<String> f2 = fnsObject::m2; // takes one param, no return
		Supplier<String> f3 = fnsObject::m3; // returns val, no param

		Function<String, String> f4 = fnsObject::m4; // 1 param, 1 return
		UnaryOperator<String> f4_a = fnsObject::m4; // special case if param & return identical

		Runnable f5 = Fns::m5; // prefix class-name for static methods
		Consumer<String> f6 = Fns::m6; // prefix class-name for static methods

		BooleanSupplier f7 = fnsObject::m7; // like predicate but no input/param

		Predicate<String> f8 = fnsObject::m8; // special case if return value is boolean
		Function<String, Boolean> f8_a = fnsObject::m8;

		BiFunction<String, String, String> f9 = fnsObject::m9;
		BinaryOperator<String> f9_a = fnsObject::m9; //special case if all 3 types are identical

		BiFunction<String, Boolean, Integer> f10 = fnsObject::m10; // just showing gen-params order
	}
}
