package xst.pinmigrations.implementation.java;

import org.junit.Test;

import timber.log.Timber;
import xst.pinmigrations.test.BaseUnitTest;

public class OopDemo extends BaseUnitTest {

	// region OOP-1
	public static class Outer_1 {
		String[] data;

		public Outer_1() {
			innerAnon.run(); // will see null
			data = new String[10];
			innerAnon.run(); // will see the allocated array
		}

		// anonymous inner (non-static) class
		// has access to outer instance members (use package-private to avoid synthetic methods)
		Runnable innerAnon = new Runnable() {
			@Override public void run() {
				Timber.d("data=" + data);
			}
		};
	}

	// @Test
	public void oop_1() {
		Outer_1 outer = new Outer_1();
	}
	//endregion


	// region OOP-2

	/** same as OOP-1 but more verbose. use if multiple objects of the inner type are needed */
	public static class Outer_2 {
		String[] data;

		public Outer_2() {
			innerAnon.run(); // will see null
			data = new String[10];
			innerAnon.run(); // will see the allocated array
		}
		InnerRunnable innerAnon = new InnerRunnable();

		// anonymous inner (non-static) class
		// has access to outer instance members (use package-private to avoid synthetic methods)
		class InnerRunnable implements Runnable {
			@Override public void run() {
				Timber.d("data=" + data);
			}
		}
	}

	// @Test
	public void oop_2() {
		Outer_2 outer = new Outer_2();
	}
	//endregion
}
