package xst.pinmigrations.implementation.retrofit;

import com.github.aurae.retrofit2.LoganSquareConverterFactory;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.schedulers.Schedulers;
import timber.log.Timber;
import xst.pinmigrations.domain.positions.json.PositionJson;
import xst.pinmigrations.test.BaseUnitTest;

import static xst.pinmigrations.app.util.TCommonUtil.*;

public class RetrofitDemo extends BaseUnitTest {

	// @Test
	public void createApiDemo() throws InterruptedException {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(PositionsApiDemo.BASE_URL)
				.client( new OkHttpClient() ) // optional, auto-creates if not passed in
				.addConverterFactory(LoganSquareConverterFactory.create())
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.build();

		final PositionsApiDemo api = retrofit.create(PositionsApiDemo.class);

		// Each Call from the created `Api` can make a single synchronous or asynchronous HTTP request to the remote webserver.
		Observable<List<PositionJson>> positions =
				api.allPositions()
				.subscribeOn(Schedulers.io());

		// TODO: demo fn for `Observable.from` flattening behavior
		// another demo combining with `flatMap` for final result
		// lastly the `flatMapIterable` shortcut
		positions
			.flatMap(list -> Observable.from(list))
			.subscribe(pos -> {
				Timber.d("pos: " + pos);
			});
		Timber.d("subscribed");

		messageThenWait("wait for response", 5000);
	}
}
