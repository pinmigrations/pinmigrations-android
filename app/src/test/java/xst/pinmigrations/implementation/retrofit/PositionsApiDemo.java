package xst.pinmigrations.implementation.retrofit;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;
import xst.pinmigrations.domain.positions.json.PositionJson;

/*
https://github.com/square/retrofit/wiki/Retrofit-Tutorials
 */
public interface PositionsApiDemo {
	String BASE_URL = "http://pinmigrations.herokuapp.com";

	@GET(BASE_URL + "/feed")
	Observable<List<PositionJson>> allPositions();

	@GET(BASE_URL + "/ping")
	Observable<String> ping();

	// edit

	// update

	// delete

	// new
}
