package xst.pinmigrations.implementation.rx;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import timber.log.Timber;
import xst.pinmigrations.app.util.TCommonUtil;

public class RxJava1_Demo_Util {
	//region CustomObservables (use w Observable.create)

	/**
	 * Observable Contract
	 * onNext, onError and onCompleted must be invoked sequentially
	 * can't be called concurrently and have to be serialized
	 * expected pattern of method calls on Observer:
	 * 		onNext* (onError | onCompleted)?
	 * https://github.com/ReactiveX/RxJava/wiki/Implementing-custom-operators-(draft)#observable-protocol
	 */


	/** COLD: SimpleObservable immediately invokes onNext/onComplete (on subscription) */
	public static class ConstStringSingleObservable implements Observable.OnSubscribe<String> {
		final String returnValue;
		public ConstStringSingleObservable(final String returnValue) { this.returnValue = returnValue; }
		@Override public void call(final Subscriber<? super String> subscriber) {
			subscriber.onNext(returnValue);
			subscriber.onCompleted();
		}
	}

	/** COLD: SimpleObservable immediately invokes onNext/onComplete (on subscription) */
	public static class ImmediateTrackingObservable implements Observable.OnSubscribe<String> {
		final String CLASS_NAME = this.getClass().getSimpleName(); // these are frequently renamed and also copy/pasted
		final AtomicInteger invokes = new AtomicInteger(0);
		@Override
		public void call(final Subscriber<? super String> subscriber) {
			Timber.d("call " + CLASS_NAME + "#" + hashCode()
					+ " total-invokes=" + invokes.incrementAndGet()
					+ " on thread: " + TCommonUtil.threadName());
			subscriber.onNext(STRING_DATA);
			subscriber.onCompleted();
			Timber.d(CLASS_NAME + " done (already invoked onNext/onComplete)");
		}
	}

	/** COLD: uses Thread.sleep from ctor to simulate service latency, blocks calling thread */
	public static class BlockingTrackingObservable implements Observable.OnSubscribe<String> {
		final String CLASS_NAME = this.getClass().getSimpleName(); // these are frequently renamed and also copy/pasted
		final int DELAY;
		public BlockingTrackingObservable(int delay) { DELAY = delay; }

		final AtomicInteger invokes = new AtomicInteger(0);
		@Override
		public void call(final Subscriber<? super String> subscriber) {
			Timber.d("call " + CLASS_NAME + "#" + hashCode()
					+ " total-invokes=" + invokes.incrementAndGet()
					+ " on thread: " + TCommonUtil.threadName());
			try {
				Thread.sleep(DELAY);
			} catch (InterruptedException exc) {
				// this is thrown if the subscriber unsubscribes (why?)
				Timber.e(CLASS_NAME + " interrupted");

				subscriber.onError(exc);

				// early return necessary!
				// if this indicates the observable sequence is invalid
				return;
			}

			Timber.d(CLASS_NAME + " done sleeping on thread: " + TCommonUtil.threadName());
			subscriber.onNext(STRING_DATA);
			subscriber.onCompleted();
			Timber.d(CLASS_NAME + " done (already invoked onNext/onComplete)");
		}
	}

	/** COLD: deliberately omits `onComplete` to demo error states */
	public static class NoCompleteTrackingObservable implements Observable.OnSubscribe<String> {
		final AtomicInteger invokes = new AtomicInteger(0);
		@Override
		public void call(final Subscriber<? super String> subscriber) {
			Timber.d("call NoCompleteObservable " + hashCode()
					+ " total-invokes=" + invokes.incrementAndGet()
					+ " on thread: " + TCommonUtil.threadName());
			subscriber.onNext(STRING_DATA);
			// NOTE: not invoking `onComplete()` !
			Timber.d("NoCompleteObservable done (already invoked onNext/onComplete)");
		}
	}

	/** Doesnt log so it can be hooked into with RxJavaHooks */
	public static class NonLoggingObservable implements Observable.OnSubscribe<String> {
		@Override public void call(final Subscriber<? super String> subscriber) {
			while (!subscriber.isUnsubscribed()) {
				subscriber.onNext(RandomStringUtils.randomAlphanumeric(15));

				try {
					Thread.sleep(100 + rand.nextInt(200));
				} catch (InterruptedException e) {
					subscriber.onError(e);
					subscriber.unsubscribe();
					return;
				}
			}
		}
	}
	public static class NonLoggingRandomErrorObservable implements Observable.OnSubscribe<String> {
		@Override public void call(final Subscriber<? super String> subscriber) {
			while (!subscriber.isUnsubscribed()) {
				subscriber.onNext(RandomStringUtils.randomAlphanumeric(15));

				boolean causeError = rand.nextDouble() < 0.1;
				if (causeError) { throw new RuntimeException("random error"); }

				try {
					Thread.sleep(rand.nextInt(200));
				} catch (InterruptedException e) {
					subscriber.onError(e);
					subscriber.unsubscribe();
					return;
				}
			}
		}
	}
	//endregion

	// region Subscribers / Observers (use with Observable.subscribe)

	// NOTE: should have made these all Generic!
	public static class PrintThreadSubscriber extends Subscriber<String> {
		final String name;
		public PrintThreadSubscriber(final String name) { this.name = name; }
		@Override public void onStart() { Timber.d(name + " subscribed"); }
		@Override public void onCompleted() { Timber.d(name + " complete"); }
		@Override public void onNext(final String s) { Timber.d(name + " " +  s + " on thread: " + TCommonUtil.threadName()); }
		@Override public void onError(final Throwable throwable) {
			Timber.e(name + " error on thread: " + TCommonUtil.threadName() + "\n" + throwable.toString());
		}
	}
	public static class PrintThreadObserver implements Observer<String> {
		final String name;
		public PrintThreadObserver(final String name) { this.name = name; }
		@Override public void onCompleted() { Timber.d(name + " complete"); }
		@Override public void onNext(final String s) { Timber.d(name + " " +  s +  " on thread: " + TCommonUtil.threadName()); }
		@Override public void onError(final Throwable throwable) {
			Timber.e(name + " error on thread: " + TCommonUtil.threadName() + "\n" + throwable.toString());
		}
	}
	public static class PrintObserver implements Observer<String> {
		final String name;
		public PrintObserver(final String name) { this.name = name; }
		@Override public void onCompleted() { Timber.d(name + " complete"); }
		@Override public void onNext(final String s) { Timber.d(name + " " + s); }
		@Override public void onError(final Throwable throwable) { Timber.e(name + " error: " + "\n" + throwable.toString()); }
	}
	public static class PrintSubscriber extends Subscriber<String> {
		final String name;
		public PrintSubscriber(final String name) { this.name = name; }
		@Override public void onStart() { Timber.d(name + " subscribed"); }
		@Override public void onCompleted() { Timber.d(name + " complete"); }
		@Override public void onNext(final String s) { Timber.d(name + " " + s); }
		@Override public void onError(final Throwable throwable) { Timber.e(name + " error: " + "\n" + throwable.toString()); }
	}
	public static class PrintSubscriberGeneric<T> extends Subscriber<T> {
		final String name;
		public PrintSubscriberGeneric(final String name) { this.name = name; }
		@Override public void onStart() { Timber.d(name + " subscribed"); }
		@Override public void onCompleted() { Timber.d(name + " complete"); }
		@Override public void onNext(final T obj) { Timber.d(name + " " + obj.toString()); }
		@Override public void onError(final Throwable throwable) { Timber.e(name + " error: " + "\n" + throwable.toString()); }
	}
	public static class PrintNumSubscriber extends Subscriber<Long> {
		final String name;
		public PrintNumSubscriber(final String name) { this.name = name; }
		@Override public void onStart() { Timber.d(name + " subscribed"); }
		@Override public void onCompleted() { Timber.d(name + " complete"); }
		@Override public void onNext(final Long num) { Timber.d(name + " " + num); }
		@Override public void onError(final Throwable throwable) { Timber.e(name + " error: " + "\n" + throwable.toString()); }
	}
	// endregion

	// region Subscribing Actions
	public static class PrintAction<T> implements Action1<T> {
		@Override public void call(final T obj) { Timber.d(obj.toString()); }
	}
	public static class NoOpAction<T> implements Action1<T> {
		@Override public void call(final T t) { }
	}
	// endregion

	//region Callable (use w Observable.fromCallable) all COLD by definition

	/** COLD: implements Callable<String> */
	public static class SimpleCallable implements Callable<String> {
		final String CLASS_NAME = this.getClass().getSimpleName(); // these are frequently renamed and also copy/pasted

		final AtomicInteger invokes = new AtomicInteger(0);
		@Override public String call() throws Exception {
			Timber.d("call " + CLASS_NAME + "#" + hashCode()
					+ " total-invokes=" + invokes.incrementAndGet()
					+ " on thread: " + TCommonUtil.threadName());
			return STRING_DATA;
		}
	}
	/** COLD: implements Callable<String> */
	public static class SleepDelayCallable implements Callable<String> {
		final String CLASS_NAME = this.getClass().getSimpleName(); // these are frequently renamed and also copy/pasted
		final AtomicInteger invokes = new AtomicInteger(0);
		final int DELAY;
		public SleepDelayCallable(int delay) { DELAY = delay; }
		@Override public String call() throws Exception {
			Timber.d("call " + CLASS_NAME + "#" + hashCode()
					+ " total-invokes=" + invokes.incrementAndGet()
					+ " on thread: " + TCommonUtil.threadName());

			Thread.sleep(DELAY);
			Timber.d(CLASS_NAME + " done sleeping on thread: " + TCommonUtil.threadName());
			return STRING_DATA;
		}
	}
	public static class SleepDelayNumCallable implements Callable<Long> {
		final String CLASS_NAME = this.getClass().getSimpleName(); // these are frequently renamed and also copy/pasted
		final int DELAY;
		public SleepDelayNumCallable(int delay) { DELAY = delay; }
		@Override public Long call() throws Exception {
			Timber.d("call " + CLASS_NAME + "#" + hashCode());

			Thread.sleep(DELAY);
			final long val = rand.nextLong();
			Timber.d(CLASS_NAME + " done sleeping returning: " + val);
			return val;
		}
	}
	//endregion

	//region Func0< Observable<T> > (use w Observable.defer) all COLD by definition

	/** COLD: implements Func0<Observable<String>> by returning Observable.just("") */
	public static class Func0_ObsJust implements Func0<Observable<String>> {
		final String CLASS_NAME = this.getClass().getSimpleName(); // these are frequently renamed and also copy/pasted
		final AtomicInteger invokes = new AtomicInteger(0);
		@Override public Observable<String> call() {
			Timber.d("call " + CLASS_NAME + "#" + hashCode()
					+ " total-invokes=" + invokes.incrementAndGet()
					+ " on thread: " + TCommonUtil.threadName());

			return Observable.just(STRING_DATA);
		}
	}

	/** COLD: implements Func0<Observable<String>> by returning a new instance of SimpleCallable */
	public static class Func0_ObsSimpleCallable implements Func0<Observable<String>> {
		final String CLASS_NAME = this.getClass().getSimpleName(); // these are frequently renamed and also copy/pasted
		final AtomicInteger invokes = new AtomicInteger(0);
		@Override public Observable<String> call() {
			Timber.d("call " + CLASS_NAME + "#" + hashCode()
					+ " total-invokes=" + invokes.incrementAndGet()
					+ " on thread: " + TCommonUtil.threadName());

			// NOTE: the call here to new
			// each invocation creates a new instance SimpleCallable
			// this could be cached internally
			return Observable.fromCallable(new SimpleCallable());
		}
	}
	//endregion

	//region Util-Observables Factory
	/** COLD: uses Thread.sleep from ctor to simulate service latency
	 * is async because it schedules itself on a new thread
	 * assuming that its internals are uninteresting and will instead be used in composition
	 * COLD because doesnt emit its data until subscribed (note: Observable.fromCallable)
	 */
	public static Observable<String> createAsyncRndStrObservable(int serviceDelay) {
		// NOTE: static methods generates static lambda methods (so no `this` available)
		return Observable.fromCallable( () -> {
			final int id = rand.nextInt(); // to associate logging with an invocation
			Timber.d("AsyncObservable(" + id + ") begin, sleep: " + serviceDelay + "ms");
			Thread.sleep(serviceDelay);
			final String str = RandomStringUtils.randomAlphabetic(10);
			Timber.d("AsyncObservable(" + id + ") returning: " + str);
			return str;
		}).subscribeOn(Schedulers.newThread()).cache();
	}

	/** COLD: same as `createAsyncRndStrObservable` but returns Long
	 * use in composition, to demo composing different types
	 */
	public static Observable<Long> createAsyncRndNumObservable(int serviceDelay) {
		// NOTE: static methods generates static lambda methods (so no `this` available)
		return Observable.fromCallable( () -> {
			final long id = rand.nextInt(); // to associate logging with an invocation
			Timber.d("AsyncObservable(" + id + ") begin, sleep: " + serviceDelay + "ms");
			Thread.sleep(serviceDelay);
			Timber.d("AsyncObservable(" + id + ") returning: " + id);
			return id;
		}).subscribeOn(Schedulers.newThread()).cache();
	}

	public static Observable<Long> hotInterval(int freq, TimeUnit timeUnit) {
		final ConnectableObservable<Long> intervalObs = Observable
				.interval(freq, timeUnit)
				.publish(); // ConnectableObservable so any subscribers get a single sequence
		intervalObs.connect();
		intervalObs.subscribe(); // no-op subscribe so it starts emitting (becomes HOT)
		return intervalObs;
	}
	//endregion

	//region Convertors (use with Transformers, eg: Observable.map, Observable.lift)
	public static class LongToStringConverterWithLogging implements Func1<Long, String> {
		@Override public String call(final Long aLong) {
			Timber.d("LongToStringConverter " + hashCode()
					+ " converting " + aLong
					+ " on thread " + TCommonUtil.threadName());
			return aLong.toString();
		}
	}
	public static class LongToStringConverter implements Func1<Long, String> {
		@Override public String call(final Long aLong) { return aLong.toString(); }
	}
	//endregion

	//region Factory / Service Patterns

	/**
	 * example usage: pinging the server to wake it up
	 */
	public static class BackgroundJobService {
		// public service method (not final so it can be over-ridden if needed)
		public void initiateTask() throws InterruptedException {
			// NOTE: `theTask` should be a static final field if possible
			// test method would look very similar to this
			Observable.fromCallable(new Task())
					.subscribeOn(Schedulers.newThread())
					.subscribe(); // no-op subscribe initiates execution
		}

		// package-private and indirection for testability
		class Task implements Callable<Object> { // need to have 1 generic parameter
			public Task(/* pass params if needed*/) {}
			@Override public Object call() throws Exception {
				// slow disk/network code here ...
				// already know its on its own thread
				return null; // this is never used
			}
		}
	}

	/** Service method for blocking operations like network / disk
	 * notice that invocation requires no knowledge of underlying implementation
	 * it is asynchronous and rx-compatible by default
	 * to test interactively use DelayableCachingService
	 * its functionally equivalent (only adds a configurable delay to simulate blocking)
	 */
	public static class CachingService {
		public Observable<String> getString() {
			return Observable.fromCallable(getStringCallable)
					.subscribeOn(Schedulers.io()) // on IO scheduler since it is blocking
					.cache(); // prevent subscribers from retrigerring w/o deliberately invoking the service
					// also allows the blocking operation to complete so it can be cached, even if Observable is unsubscribed
		}
		String data;
		// this is re-usable so no need to re-create for every invocation of `getString` method
		// also should be private since it wont be used anywhere else
		private final Callable<String> getStringCallable = () -> {
			// NOTE: this isn't real cache logic, implement something real
			if (null == data) {
				// blocking operation here like network/disk-access
				data = RandomStringUtils.randomAlphabetic(10);
			}

			return data;
		};
	}
	//endregion

	//region Util
	public static final String STRING_DATA = "STRING-DATA";
	public static final void subscribersInitWait(final int delay) throws InterruptedException {
		Timber.d("test sleep to allow subscriptions to init " + delay + "ms");
		Thread.sleep(delay);
	}
	public static final void printSubscriptionStatus(final String name, final Subscription sub) {
		Timber.d(name + ".unsubscribed? " + sub.isUnsubscribed());
	}
	public static final Random rand = new Random(System.currentTimeMillis());
	//endregion
}
