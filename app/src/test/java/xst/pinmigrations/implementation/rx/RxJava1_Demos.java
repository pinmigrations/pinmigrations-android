package xst.pinmigrations.implementation.rx;

import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.observables.BlockingObservable;
import rx.observables.ConnectableObservable;
import rx.observers.TestSubscriber;
import rx.plugins.RxJavaHooks;
import rx.schedulers.Schedulers;
import rx.schedulers.TestScheduler;
import rx.subjects.AsyncSubject;
import rx.subjects.PublishSubject;
import timber.log.Timber;
import xst.pinmigrations.app.logging.TestLogger;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static xst.pinmigrations.app.util.TCommonUtil.endTestWait;
import static xst.pinmigrations.app.util.TCommonUtil.messageThenWait;
import static xst.pinmigrations.app.util.TCommonUtil.threadName;
import static xst.pinmigrations.implementation.rx.RxJava1_Demo_Util.*;

@SuppressWarnings("all")
/**
 * These tests are demos of fundamental Rx operators, behaviors, etc..
 * NOTE: disable all tests after usage as these are deliberaly slow and will delay the real test-suite
 * eg: search replace @Test with //@Test to allow easy re-enable
 */
public class RxJava1_Demos extends BaseUnitTest {

	//region Just

	/** `Observable.just` creates an observable out of multiple/single objects
	 */
	//@Test
	public void just1() throws Exception {
		// given
		Observable<String> obs1 = Observable.just("hello");
		PrintObserver obv1 = new PrintObserver("obv1");
		PrintObserver obv2 = new PrintObserver("obv2");

		// when
		Timber.d("subscribing obv1");
		Subscription sbt1 = obs1.subscribe(obv1);
		Timber.d("subscribing sbr2");
		Subscription sbt2 = obs1.subscribe(obv2);

		// then
		endTestWait(100);
		printSubscriptionStatus("sbt1", sbt1); // true
		printSubscriptionStatus("sbt2", sbt2); // true
	}

	/**
	 * observer/subscribers operate on entire sequence before next observer/subscriber
	 * NOTE: using Subscriber facilitates MGMT, because a separate Subscription object doesnt need to be kept
	 */
	//@Test
	public void just2() throws Exception {
		// given
		Observable<String> obs2 = Observable.just("hello", "world");
		PrintSubscriber sbr1 = new PrintSubscriber("sbr1");
		PrintSubscriber sbr2 = new PrintSubscriber("sbr2");

		// when
		obs2.subscribe(sbr1);
		obs2.subscribe(sbr2);

		// then
		endTestWait(100);
		// these are automatically unsubscribed by the implicit call to `onComplete` in `.just`
		printSubscriptionStatus("sbr1", sbr1); // true
		printSubscriptionStatus("sbr2", sbr2); // true
	}

	/** `Observable.just` captures and emits the value passed at creation
	 * it doesnt access/evaluate the variable
	 */
	//@Test
	public void justCapture() throws Exception {
		// given
		String scopeCaptureNotEval = "STR-1";

		Observable<String> obs2 = Observable.just(scopeCaptureNotEval);
		PrintObserver obv1 = new PrintObserver("obv1");
		PrintObserver obv2 = new PrintObserver("obv2");

		// when
		scopeCaptureNotEval = "STR-2";
		Timber.d("set scopeCaptureNotEval to " + scopeCaptureNotEval);
		Timber.d("subscribing obv1");
		obs2.subscribe(obv1);
		Timber.d("subscribing obv2");
		obs2.subscribe(obv2);

		// then
		// see output
		endTestWait(100);
	}
	//endregion


	//region FromCallable

	/** `Observable.fromCallable` is re-invoked for each observer/subscriber
	 *  execution begins upon subscription, not on creation (ColdObservable)
	 *  execution on same thread if unspecified
	 *  same Callable object is used for all subscriptions
	 */
	//@Test
	public void fromCallable() throws Exception {
		// given
		Observable<String> obs = Observable.fromCallable(new SimpleCallable());

		// when
		messageThenWait("Observable.fromCallable already created", 250);

		obs.subscribe(new PrintSubscriber("sbr1"));
		obs.subscribe(new PrintSubscriber("sbr2"));

		// then
		// see output
		endTestWait(100);
	}

	/** `Observable.fromCallable` is re-invoked but only completes if still subscribed
	 *  new thread for each invocation if scheduler is newThread / IO / Computation
	 *  same Callable object is used for all invocations (even on different threads!)
	 *  NOTE: ensure thread safety if using multiple subscribers
	 */
	//@Test
	public void fromCallableDelay() throws Exception {
		// given
		final int CALLABLE_DELAY = 100;
		Observable<String> delayedObs =
				Observable.fromCallable(new SleepDelayCallable(CALLABLE_DELAY))
						.subscribeOn(Schedulers.computation());

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		delayedObs.subscribe(sbr1);
		delayedObs.subscribe(sbr2);

		Timber.d("unsubscribing sbr1");
		sbr1.unsubscribe();
		// NOTE: the missing output for `sbr1`
		// result isnt simply discarded, execution never reaches logging of "callable done"

		Timber.d("sbr2 should still subscribed");
		Timber.d("sbr2.unsubscribed? " + sbr2.isUnsubscribed()); // false

		// then
		// see output
		final int FINISH_ALLOWANCE = CALLABLE_DELAY + 250;
		endTestWait(FINISH_ALLOWANCE);
	}

	/** `TestSubscriber` side effects on chain
	 * same as `//@Test fromCallableDelay` but adding `rx.observers.TestSubscriber`
	 * TestSubscriber also triggers another invocation
	 */
	//@Test
	public void fromCallableTestSubscriberSideEffects() throws Exception {
		// need finer time because there is more overlap in thread execution
		TestLogger.setFineTime();

		final int CALLABLE_DELAY = 500;
		final int OBSERVERS_INIT_ALLOW = 250;
		Observable<String> delayedObs =
				Observable.fromCallable(new SleepDelayCallable(CALLABLE_DELAY))
						.subscribeOn(Schedulers.io());

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");
		// official Rx testing util
		TestSubscriber<String> testSubscriber = new TestSubscriber<>();


		// when
		delayedObs.subscribe(sbr1);
		delayedObs.subscribe(sbr2);
		Timber.d("subscribing TestSubscriber");
		delayedObs.subscribe(testSubscriber);

		subscribersInitWait(OBSERVERS_INIT_ALLOW);


		Timber.d("unsubscribing sbr1");
		sbr1.unsubscribe();
		Timber.d("sbr2 should still subscribed");
		Timber.d("sbr2.unsubscribed? " + sbr2.isUnsubscribed()); // false
		Timber.d("TestSubscriber should still subscribed");
		Timber.d("TestSubscriber.unsubscribed? " + testSubscriber.isUnsubscribed()); // false

		final int FINISH_ALLOWANCE = CALLABLE_DELAY + 500;
		messageThenWait("test sleep to allow other threads to complete", FINISH_ALLOWANCE);

		// then
		Timber.d("test done sleeping");
		printSubscriptionStatus("sbr1", sbr1); // true
		printSubscriptionStatus("sbr2", sbr2); // true

		// cleanup
		TestLogger.resetTimeFormatter();
	}

	/** `Observable.fromCallable(...).cache()`
	 *  callable is not re-invoked because of `.cache()`
	 *  NOTE: sbr2 observes on main thread since the result is already cached
	 */
	//@Test
	public void fromCallableCached() throws Exception {
		// given
		Observable<String> cachingObs =
				Observable.fromCallable(new SimpleCallable())
						.subscribeOn(Schedulers.computation())
						.cache();

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		cachingObs.subscribe(sbr1);
		cachingObs.subscribe(sbr2);

		// then
		// see output
		endTestWait(100);
	}

	/** `Observable.fromCallable(...).cache(); unsubscribe(); subscribe();`
	 * Observable executes until completion even though no subscribers are attached
	 * in reality `.cache` returns an Observable with a
	 * hidden `ConnectableObservable` that remains subscribed until completion
	 * https://github.com/Froussios/Intro-To-RxJava/blob/master/Part%203%20-%20Taming%20the%20sequence/6.%20Hot%20and%20Cold%20observables.md#cache
	 */
	//@Test
	public void fromCallableCachedResubscribe() throws Exception {
		// given
		final int CALLABLE_DELAY = 1500;
		Observable<String> cachingDelayedObs =
				Observable.fromCallable(new SleepDelayCallable(CALLABLE_DELAY))
						.subscribeOn(Schedulers.io())
						.cache();

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");


		// when
		cachingDelayedObs.subscribe(sbr1);

		subscribersInitWait(250);

		Timber.d("unsubscribing sbr1");
		sbr1.unsubscribe();

		messageThenWait("allow observable to complete (without subscribers)", CALLABLE_DELAY + 500);

		Timber.d("re-subscribing with (new) sbr2");
		cachingDelayedObs.subscribe(sbr2);

		// then
		// see output
		endTestWait(300);

		printSubscriptionStatus("sbt1", sbr1); // true
		printSubscriptionStatus("sbt2", sbr2); // true
	}
	//endregion


	//region Defer

	/** `Observable.defer( () -> just(...) )`
	 *  single Func0Just instance, re-invoked for each subscription
	 *  execution of Func0Just occurs on subscription
	 */
	//@Test
	public void deferFunc0_ObsJust() throws Exception {
		// given
		Observable<String> obs = Observable.defer(new Func0_ObsJust());
		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		messageThenWait("Observable.defer already created", 250);

		obs.subscribe(sbr1);
		obs.subscribe(sbr2);

		// then
		endTestWait(250);
	}

	/** `Observable.defer( Observable.fromCallable( () -> { ...; return String; } ))`
	 *  single deferFunc0SimpleCallable instance, re-invoked for each subscription
	 *  multiple SimpleCallable instances
	 *  memoization not implemented within deferFunc0SimpleCallable but can be done easily
	 */
	//@Test
	public void deferFunc0_ObsSimpleCallable() throws Exception {
		// given
		Observable<String> obs = Observable.defer(new Func0_ObsSimpleCallable());
		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		messageThenWait("Observable.defer already created", 250);

		obs.subscribe(sbr1);
		obs.subscribe(sbr2);

		// then
		endTestWait(250);
		printSubscriptionStatus("sbr1", sbr1); // true
		printSubscriptionStatus("sbr2", sbr2); // true
	}
	//endregion


	//region Custom Observables

	/**
	 * `cache()` causes a single execution, despite multiple subscribers
	 *  execution begins on subscription, not on creation (ColdObservable)
	 *  not shown: this also works if the observable was slow and blocking
	 */
	//@Test
	public void withImmediateObservableAndCache() throws Exception {
		// creae async Observable by scheduling BlockingObservable on IO
		Observable<String> cachingObs =
				Observable.unsafeCreate(new ImmediateTrackingObservable())
						.cache(); // results in a single execution for all subscribers

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		messageThenWait("Observable already created", 250);

		cachingObs.subscribe(sbr1);
		cachingObs.subscribe(sbr2);

		// then
		endTestWait(250);
	}

	/** Observable.cache().delay()
	 * `cache()` causes a single execution
	 * `delay` delays execution of subscriber methods (onNext et. all)
	 * even though observable completes immediately, (blocking, but returns value directly)
	 * subscribers dont exec until after the delay from `delay()`
	 */
	//@Test
	public void withImmediateObservableAndCacheAndDelay() throws Exception {
		final int CHAIN_DELAY = 500;
		TestScheduler testScheduler = new TestScheduler();
		Observable<String> delayedCachingObs =
				Observable.unsafeCreate(new ImmediateTrackingObservable())
						.cache()
						.delay(CHAIN_DELAY, MILLISECONDS, testScheduler);

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		delayedCachingObs.subscribe(sbr1);
		delayedCachingObs.subscribe(sbr2);

		Timber.d("test-scheduler advancing time");
		testScheduler.advanceTimeBy(CHAIN_DELAY, MILLISECONDS);


		// then
		endTestWait(250);
	}

	/** Slow, BlockingObservable made async by scheduling on IO
	 * each subscription causes a new invocation (because `.cache()` is ommitted)
	 * does a Thread.sleep to simulate service-delay, invokes onError when interrupted (by unsubscription)
	 */
	//@Test
	public void withSlowBlockingObservableMadeAsync() throws Exception {
		final int OBSERVABLE_DELAY = 500;
		final int OBSERVERS_INIT_ALLOW = 250;
		Observable<String> delayedObs =
				Observable.unsafeCreate(new BlockingTrackingObservable(OBSERVABLE_DELAY))
					.subscribeOn(Schedulers.io());

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		delayedObs.subscribe(sbr1);
		delayedObs.subscribe(sbr2);

		subscribersInitWait(OBSERVERS_INIT_ALLOW);

		Timber.d("unsubscribing sbr1");
		sbr1.unsubscribe();
		Timber.d("sbr2 should still subscribed");
		printSubscriptionStatus("sbr2", sbr2); // false


		final int FINISH_ALLOWANCE = OBSERVABLE_DELAY + 500;
		endTestWait(FINISH_ALLOWANCE);
	}


	/** Faster, BlockingObservable made async by scheduling on IO
	 * sbr1 subscribes before Observable has completed its blocking op
	 * Observable completes, sbr1 receives data
	 * sbr2 and sbr3 subscribe and see data immediately (becaue of `.cache()`)
	 */
	//@Test
	public void withFasterAsyncObservableCached() throws Exception {
		final int OBSERVABLE_DELAY = 100;
		Observable<String> delayedObs =
				Observable.unsafeCreate(new BlockingTrackingObservable(OBSERVABLE_DELAY))
						.subscribeOn(Schedulers.io())
						.cache();

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");
		PrintThreadSubscriber sbr3 = new PrintThreadSubscriber("sbr3");
		final int OBSERVER_HEADSTART = 250;

		// when
		delayedObs.subscribe(sbr1);

		messageThenWait("only sbr1 subscribed for now.", OBSERVER_HEADSTART);

		delayedObs.subscribe(sbr2);
		delayedObs.subscribe(sbr3);


		endTestWait(100);

		// then
		// see output
		// cleanup
		sbr1.unsubscribe();
		sbr2.unsubscribe();
		sbr3.unsubscribe();
	}

	/** NoCompleteObservable - it doesnt invoke `onComplete`
	 * subscribers are still subscribed upon its "completion"
	 */
	//@Test
	public void withNoCompleteObservable() throws Exception {
		Observable<String> noCompleteObs =
				Observable.unsafeCreate(new NoCompleteTrackingObservable())
						.subscribeOn(Schedulers.io());

		PrintSubscriber sbr1 = new PrintSubscriber("sbr1");
		PrintSubscriber sbr2 = new PrintSubscriber("sbr2");

		// when
		noCompleteObs.subscribe(sbr1);
		noCompleteObs.subscribe(sbr2);

		final int FINISH_ALLOWANCE = 250;
		endTestWait(FINISH_ALLOWANCE);

		// then
		printSubscriptionStatus("sbr1", sbr1);
		printSubscriptionStatus("sbr2", sbr2);
	}
	//endregion


	//region Hot observables

	/**
	 * hotInterval uses Observable.interval().publish(); connect(); subscribe( no-op );
	 * to create and start a single sequence
	 */
	//@Test
	public void deliberatelyMisuseHotObservable() throws Exception {
		Observable<Long> hotIntervalObs = hotInterval(100, MILLISECONDS);
		PrintNumSubscriber sbr1 = new PrintNumSubscriber("sbr1");

		messageThenWait("let hot obs exec", 300);

		hotIntervalObs.subscribe(sbr1);

		messageThenWait("allow exec", 250);
		sbr1.unsubscribe();
	}
	//endregion


	//region RxJavaHooks / Testing

	/** setup RxJavaHooks before some test
	 * https://github.com/ReactiveX/RxJava/wiki/Plugins
	 */
	//@Test
	public void hookIntoObservableCreation() throws Exception {
		// setup RxJavaHooks
		RxJavaHooks.setOnObservableCreate( (observable) -> {
			Timber.d("creating " + observable.getClass() + "#" + observable.hashCode());
			return observable;
		});
		RxJavaHooks.setOnObservableStart((observable, onSubscribe) -> {
			Timber.d("starting " + onSubscribe.getClass() + " Observable#" + observable.hashCode());
			return onSubscribe;
		});
		RxJavaHooks.setOnObservableReturn(subscription -> {
			Timber.d("before return subscription " + subscription.hashCode());
			return subscription;
		});


		Observable<String> blackBoxObs =
				Observable.unsafeCreate(new NonLoggingObservable())
						.subscribeOn(Schedulers.newThread());
		blackBoxObs.subscribe(new PrintObserver("obv1"));


		messageThenWait("allow exec", 1500);


		// cleanup
		RxJavaHooks.reset();
	}

	/** setup RxJavaHooks before some test
	 * https://github.com/ReactiveX/RxJava/wiki/Plugins
	 */
	//@Test
	public void setTestSchedulerAndGlobalErrorHandler() throws Exception {
		// setup RxJavaHooks

		TestScheduler testScheduler = new TestScheduler();
		// when Schedulers.newThread() is invoked, return the TestScheduler
		RxJavaHooks.setOnNewThreadScheduler( (scheduler) -> testScheduler );

		// set global error handler (uncaught only!) checked exceptions trigger regular error handling
		RxJavaHooks.setOnError( (throwable) -> {
			final StringWriter sw = new StringWriter();
			throwable.printStackTrace(new PrintWriter(sw));
			Timber.d(sw.toString());
		});

		Observable<String> blackBoxObs =
				Observable.unsafeCreate(new NonLoggingRandomErrorObservable())
						.subscribeOn(Schedulers.newThread());
		blackBoxObs.subscribe(new PrintObserver("obv1"));

		messageThenWait("allow exec", 300);
		// NOTE: none should occur

		testScheduler.advanceTimeBy(1500, MILLISECONDS);

		// cleanup
		RxJavaHooks.clearAssemblyTracking();
		RxJavaHooks.reset();
	}

	/** assertions on Observable (using TestSubscriber) */
	//@Test
	public void assertionsOnObservable() throws Exception {
		// GIVEN
		Observable<String> obs =
				Observable.unsafeCreate(new ConstStringSingleObservable(STRING_DATA))
						.subscribeOn(Schedulers.newThread());

		TestSubscriber<String> testSubscriber = new TestSubscriber<>();

		// WHEN
		Timber.d("subscribe TestSubscriber before obv1");
		obs.subscribe(testSubscriber);
		obs.subscribe(new PrintObserver("obv1"));

		testSubscriber.awaitTerminalEventAndUnsubscribeOnTimeout(100, MILLISECONDS);

		// THEN
		// different types of assertions possible

		// successful completion
		testSubscriber.assertCompleted();

		// assert error / no-error
		testSubscriber.assertNoErrors();
//		testSubscriber.assertError(IOException.class);

		// total values in stream
		testSubscriber.assertValueCount(1);

		// some value was seen (and only this value?)
		testSubscriber.assertValue(STRING_DATA);

		// assert each T from onNext (var-args method)
		testSubscriber.assertValues(STRING_DATA);

		// assert each in List<T> from onNext
		List<String> compareList = new ArrayList<String>();
		compareList.add(STRING_DATA);
		testSubscriber.assertReceivedOnNext(compareList);

		// extract List<T> from onNext stream
		List<String> streamStrings = testSubscriber.getOnNextEvents();
		assertThat(streamStrings).hasSize(1);
		String str = streamStrings.get(0);
		assertThat(str).isEqualTo(STRING_DATA);

		// where it executed
		Thread thread = testSubscriber.getLastSeenThread();
		Timber.d("exec'd on " + thread.getName());
	}


	/** `TestSubscriber` side effects on chain when sole subscriber
	 */
	//@Test
	public void testSubscriberAloneSideEffects() throws Exception {
		// need finer time because there is more overlap in thread execution
		final int CALLABLE_DELAY = 500;
		Observable<String> delayedObs = // NOTE: is async because of scheduling
				Observable.unsafeCreate(new BlockingTrackingObservable(CALLABLE_DELAY))
						.subscribeOn(Schedulers.io());

		// official Rx testing util
		TestSubscriber<String> testSubscriber = new TestSubscriber<>();

		// when
		Timber.d("subscribing TestSubscriber");
		delayedObs.subscribe(testSubscriber);

		messageThenWait("allow exec", 500);
		Timber.d("TestSubscriber should still subscribed");
		Timber.d("TestSubscriber.unsubscribed? " + testSubscriber.isUnsubscribed()); // false
		final int FINISH_ALLOWANCE = CALLABLE_DELAY + 500;
		endTestWait(500);

		Timber.d("unsubscribing TestSubscriber");
		testSubscriber.unsubscribe();

		// then
		Timber.d("test done sleeping");
		printSubscriptionStatus("testSubscriber", testSubscriber); // true
		Timber.d("start assertions, no more output if all pass");

		// assertions

	}
	//endregion


	//region concat

	/** concat different typed Observables by using Subscriber<Object>
	 * demonstrates execution sequence with delay
	 * NOTE: that the `obs2` doesnt begin execution until `obs1` completes
	 * `obs1` does begin as soon as `combinedObs` is subscribed however
	 * `sbr` views output from `obs1` as soon as its ready, not before `obs2`
	 * execution is concurrent and asynchronous
	 * Observable<Object> is valid but less useful than first mapping to an appropriate type
	 */
	//@Test
	public void concatWithGenericSubscriberInterleaves() throws Exception {
		// given
		Observable<String> obs1 = createAsyncRndStrObservable(500);
		Observable<Long> obs2 = createAsyncRndNumObservable(1);
		Observable<Object> combinedObs = Observable.concat(obs1, obs2);

		// when
		PrintSubscriberGeneric<Object> sbr = new PrintSubscriberGeneric<>("sbr");
		// observables dont execute until subscription as demo'd elsewhere
		combinedObs.subscribe(sbr);

		// then see output
		endTestWait(1000);
	}

	/** concat different typed Observables by mapping to the necessary type
	 * NOTE: concat causes sequential execution of source observables. @see merge
	 * `cache` prevents multiple execution of obs1/obs2 but
	 * it does not pause the flow of events downward to sbr1/sbr2
	 * that execution is still concurrent / async
	 */
	//@Test
	public void concatWithMappingAndCache() throws Exception {
		//given
		Observable<String> obs1 = createAsyncRndStrObservable(250); // uses new thread
		Observable<String> obs2 = createAsyncRndNumObservable(1) // also uses new thread but doesnt start until `obs1` completes
				.map(new LongToStringConverter());
		Observable<String> combinedObsCaching =
				Observable.concat(obs1, obs2)
						.cache(); // again, store items but dont re-execute their creation

		// when
		combinedObsCaching.subscribe(new PrintSubscriber("sbr1")); // observables dont execute until subscription

		messageThenWait("let chain complete with sbr1", 500);

		Timber.d("subscribing sbr2");
		combinedObsCaching.subscribe(new PrintSubscriber("sbr2"));

		// then see output
		endTestWait(1000);
	}
	//endregion


	//region Merge

	/** Observable.merge (asyncObs1, asyncObs2)
	 * merge allows obs1/obs2 to execute concurrently and sbr to see values as they are ready
	 */
	//@Test
	public void mergeAsync() throws Exception {
		//given
		Observable<Long> obs1 = createAsyncRndNumObservable(300);
		Observable<Long> obs2 = createAsyncRndNumObservable(30);
		Observable<Long> mergedObs = Observable.merge(obs1, obs2);

		// when
		mergedObs.subscribe(new PrintNumSubscriber("sbr1")); // observables dont execute until subscription

		// then see output
		endTestWait(750);
	}

	/** Observable.merge (obs1, obs2, obs3)
	 * merge allows obs1/obs2/obs3 to execute and produce concurrently
	 * toBlocking doesnt force sequential execution (like `concat` instead of `merge` would)
	 * toBlocking makes `mergedObs` and thus `sbr` block the test-thread
	 * the end of the test `endTestWait` is reached after `mergedObs` completes (all obs1/obs2/obs3 and sbr)
	 */
	//@Test
	public void mergeAsyncAndBlock() throws Exception {
		//given
		Observable<Long> obs1 = createAsyncRndNumObservable(250);
		Observable<Long> obs2 = createAsyncRndNumObservable(125);
		Observable<Long> obs3 = createAsyncRndNumObservable(20);
		BlockingObservable<Long> mergedObs = Observable.merge(obs1, obs2, obs3).toBlocking();

		// when
		mergedObs.subscribe(new PrintNumSubscriber("sbr")); // observables dont execute until subscription

		// then see output
		endTestWait(500);
	}
	//endregion


	//region Threads

	/** single `Observable(...)` item with multiple subscribers subscribing/observing in different thread contexts */
	//@Test
	public void observableSingleWithSubscribersOnDiffThreads() throws Exception {
		// given
		Observable<String> delayedObs = // Observable.fromCallable( () -> { return String; } );
				Observable.fromCallable(new SimpleCallable());

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		delayedObs
			.subscribeOn(Schedulers.io())
			.observeOn(Schedulers.newThread())
			.subscribe(sbr1);

		subscribersInitWait(150);

		delayedObs
			.subscribeOn(Schedulers.computation())
			.observeOn(Schedulers.io())
			.subscribe(sbr2);

		// then
		// see output
		endTestWait(300);
	}

	/**
	 * Single observable (and single item)
	 * processed in turn my 3 different operators each scheduled on IO
	 * since thread-context (IO) is identical, only a single thread was used throughout the chain
	 */
	//@Test
	public void observableWithMultipleScheduledOperators() throws Exception {
		// GIVEN
		Observable<String> ioCachedObs = // just returns STRING_DATA
				Observable.fromCallable(new SimpleCallable())
					.subscribeOn(Schedulers.io())
					.cache();

		Random rand = new Random();

		Observable<String> step2Obs = ioCachedObs.map((final String str) -> {
			try { Thread.sleep( rand.nextInt(200) + 100 ); } catch (InterruptedException e) {}

			Timber.d(threadName());
			return str.toLowerCase();
		}).subscribeOn(Schedulers.io())
				.cache();

		Observable<String> step3Obs = step2Obs.map((final String str) -> {
			try { Thread.sleep( rand.nextInt(200) + 100 ); } catch (InterruptedException e) {}

			Timber.d(threadName());
			final int STR_MID = str.length() / 2;
			return str.substring( STR_MID ) + str.substring(0, STR_MID);
		}).subscribeOn(Schedulers.io())
				.cache();

		Observable<String> step4Obs = step3Obs.map((final String str) -> {
			try { Thread.sleep( rand.nextInt(200) + 100 ); } catch (InterruptedException e) {}

			Timber.d(threadName());
			return str.concat(str);
		}).subscribeOn(Schedulers.io())
				.cache();

		TestSubscriber<String> testSubscriber = new TestSubscriber<>();
		PrintSubscriber sbr = new PrintSubscriber("sbr");

		// when
		step4Obs.subscribe(testSubscriber);
		step4Obs.subscribe(sbr);


		subscribersInitWait(150);
		testSubscriber.awaitTerminalEventAndUnsubscribeOnTimeout(1000, MILLISECONDS);

		// then
		// see output
		endTestWait(50);
	}
	//endregion

	//region Delay

	//@Test
	public void delayThenCacheObservable() throws Exception {
		// given
		Observable<String> delayedObs = // Observable.fromCallable( () -> { return String; } );
				Observable.fromCallable(new SimpleCallable());

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		delayedObs
				.subscribeOn(Schedulers.io())
				.observeOn(Schedulers.newThread())
				.subscribe(sbr1);

		subscribersInitWait(150);

		delayedObs
				.subscribeOn(Schedulers.computation())
				.observeOn(Schedulers.io())
				.subscribe(sbr2);

		// then
		// see output
		endTestWait(300);
	}

	@Test public void justObsDelayed() throws Exception {
		// given
		Observable<Long> obs = Observable.just(0L).delay(300, TimeUnit.MILLISECONDS);

		PrintNumSubscriber sbr1 = new PrintNumSubscriber("sbr1");

		// when
		obs.subscribeOn(Schedulers.io())
			.observeOn(Schedulers.newThread())
			.subscribe(sbr1);

		subscribersInitWait(150);

		// then
		// see output
		endTestWait(300);
	}

	@Test public void justObsDelayedByZipWithTimer() throws Exception {
		// given
		final RxJavaService service = new RxJavaService();
		final Observable<String> originalObs = Observable.just("data");
		final Observable<String> timerDelayedObs = service.delayedReturnObservable(originalObs, 300, MILLISECONDS);
		final PrintSubscriber sbr1 = new PrintSubscriber("sbr1");

		// when
		timerDelayedObs.subscribeOn(Schedulers.io())
				.observeOn(Schedulers.newThread())
				.subscribe(sbr1);

		subscribersInitWait(150);

		// then
		// see output
		endTestWait(300);
	}
	//endregion


	//region Interval

	/**
	 * Observable.interval is COLD (produces when subscribed)
	 * NOTE: `.cache()` and `.publish()` alone dont make `.interval()` hot
	 */

	/** single Observable.interval with multiple subscribers creates distinct chains
	 * sbr2 sees values during same intervals but the values are out of sync with sbr1
	 * because it is subscribed later
	 * each chain has its own instance of LongToStringConverterWithLogging
	 * but it can be re-used if necessary
	 * NOTE: Observable.interval is scheduled on Computation thread-pool by default
	 */
	//@Test
	public void intervalObservableMultipleDistinctChains() throws Exception {
		// http://reactivex.io/RxJava/1.x/javadoc/rx/Observable.html#interval(long,%20java.util.concurrent.TimeUnit)
		final Observable<Long> intervalObs = Observable
				.interval(250, MILLISECONDS)
				.subscribeOn(Schedulers.io()); // NOTE: this has no effect;

		PrintThreadSubscriber sbr1 = new PrintThreadSubscriber("sbr1");
		PrintThreadSubscriber sbr2 = new PrintThreadSubscriber("sbr2");

		// when
		Timber.d("subscribing sbr1. observeOn new-thread (src-observable and converter on default thread)");
		intervalObs
				.subscribeOn(Schedulers.io()) // NOTE: also no effect
				.map(new LongToStringConverterWithLogging())
				.subscribeOn(Schedulers.io()) // NOTE: also no effect
				.observeOn(Schedulers.newThread()) // this does have effect
				.subscribe(sbr1);

		messageThenWait("let obv1 observe alone", 625);

		intervalObs
				.observeOn(Schedulers.io()) // this does have effect
				.map(new LongToStringConverterWithLogging())
				.subscribe(sbr2);

		messageThenWait("let both exec", 1250);


		Timber.d("unsubscribing sbr2");
		sbr2.unsubscribe();

		messageThenWait("allow exec", 750);

		Timber.d("unsubscribing sbr1");
		sbr1.unsubscribe();

		endTestWait(250);
	}

	/** single Observable.interval with multiple subscribers creates distinct chains
	 * a new observable is created from the first by applying `.cache()`
	 * subsequent subscribers on the caching Observable immediately receive all prior emissions (sbr3)
	 */
	//@Test
	public void intervalObservableWithCaching() throws Exception {
		// http://reactivex.io/RxJava/1.x/javadoc/rx/Observable.html#interval(long,%20java.util.concurrent.TimeUnit)
		final Observable<Long> intervalObs = Observable.interval(250, MILLISECONDS);

		PrintSubscriber sbr1 = new PrintSubscriber("<sbr1>");
		PrintSubscriber sbr2 = new PrintSubscriber("<sbr2>");
		PrintSubscriber sbr3 = new PrintSubscriber("<sbr3>");
		// simple and thread-safe, so reuse is ok
		final LongToStringConverter converter = new LongToStringConverter();

		// when
		intervalObs.map(converter).subscribe(sbr1);
		messageThenWait("let obv1 observe alone.", 625);

		final Observable<Long> cachedIntervalObs = intervalObs.cache();
		Timber.d("created caching interval-Observable from original");

		Timber.d("subscribing sbr2 on new caching interval-Observable");
		cachedIntervalObs
				.map(converter)
				.subscribe(sbr2);

		messageThenWait("let both exec.", 1250);

		Timber.d("subscribing sbr3 on same caching interval-Observable");
		cachedIntervalObs
				.map(converter)
				.subscribe(sbr3);

		messageThenWait("let all 3 exec.", 1250);

		Timber.d("unsubscribing sbr2");
		sbr2.unsubscribe();

		messageThenWait("allow exec", 750);

		Timber.d("unsubscribing sbr3");
		sbr3.unsubscribe();

		messageThenWait("allow exec", 300);

		Timber.d("unsubscribing sbr1");
		sbr1.unsubscribe();

		endTestWait(250);
	}
	//endregion


	//region PublishSubject

	/** single Observable.interval with PublishSubject
	 * sourceObservable (interval) isnt subscribed to until sbr1 subscribes to publishSubject
	 * subscribers on PublishSubject view the sequence in its current state (no replay)
	 * NOTE: this isnt explicitly shown, but there is a single subscription to intervalObs,
	 * the one from publishSubject. it isnt re-executed for each sbr1,sbr2,sbr3 as shown previously
	 */
	//@Test
	public void intervalObservableWithPublishSubject() throws Exception {
		final Observable<Long> intervalObs = Observable.interval(100, MILLISECONDS);
		final PublishSubject<Long> publishSubject = PublishSubject.create();

		PrintNumSubscriber sbr1 = new PrintNumSubscriber("sbr1");
		PrintNumSubscriber sbr2 = new PrintNumSubscriber("sbr2");
		PrintNumSubscriber sbr3 = new PrintNumSubscriber("sbr3");

		// when
		messageThenWait("subscribing PublishSubject", 250);
		intervalObs.subscribe(publishSubject);

		publishSubject.subscribe(sbr1);


		messageThenWait("let sbr1 observe alone", 350);

		publishSubject.subscribe(sbr2);

		messageThenWait("let both exec", 350);

		Timber.d("unsubscribing sbr2");
		sbr2.unsubscribe();

		publishSubject.subscribe(sbr3);

		messageThenWait("allow exec", 250);

		Timber.d("unsubscribing sbr1 / sbr3");
		sbr1.unsubscribe();
		sbr3.unsubscribe();

		endTestWait(250);
	}


	/** single Observable.interval.cache() then PublishSubject
	 * notice that .cache() doesnt start the interval stream, but PublishSubject with sbr1 attached does
	 */
	//@Test
	public void intervalObservableCachedThenPublishSubject() throws Exception {
		final Observable<Long> intervalObs = Observable.interval(100, MILLISECONDS);
		final PublishSubject<Long> publishSubject = PublishSubject.create();
		PrintNumSubscriber sbr1 = new PrintNumSubscriber("sbr1");
		publishSubject.subscribe(sbr1);
		Timber.d("PublishSubject has sbr1 pre-subscribed, but is itself not connected");

		// when
		Timber.d("create cachedObs");
		Observable<Long> cachedIntervalObs = intervalObs.cache();
		messageThenWait("allow cache exec", 350);

		Timber.d("subscribing PublishSubject");
		Subscription upstreamSubt = cachedIntervalObs.subscribe(publishSubject);

		messageThenWait("allow exec", 350);

		Timber.d("unsubscribing PublishSubject");
		upstreamSubt.unsubscribe();

		messageThenWait("allow exec", 250);

		Timber.d("unsubscribing sbr1");
		sbr1.unsubscribe();

		endTestWait(250);
	}
	//endregion


	//region ConnectableObservable (Observable.publish)
	/**
	 *  ConnectableObservable is similar to PublishSubject, but allows setting up Subscribers before-hand
	 */

	/** single Observable.interval.publish yields a ConnectableObservable
	 *  both sbr1/sbr2 are subscribed before triggering intervalObs with `.connect`
	 */
	//@Test
	public void syncSubscribersWithConnectableObservableOnIntervalObs() throws Exception {
		// given
		final Observable<Long> intervalObs = Observable.interval(100, MILLISECONDS);
		final ConnectableObservable<Long> connectableObs = intervalObs.publish();

		PrintNumSubscriber sbr1 = new PrintNumSubscriber("sbr1");
		PrintNumSubscriber sbr2 = new PrintNumSubscriber("sbr2");


		// when
		connectableObs.subscribe(sbr1);
		connectableObs.subscribe(sbr2);
		messageThenWait("both subscribed and ready", 250);
		Timber.d("connecting ConnectableObservable to intervalObs");
		Subscription upstreamSubscription = connectableObs.connect();

		messageThenWait("let both exec", 250);


		Timber.d("disconnecting ConnectableObservable (upstream.unsubcribe)");
		upstreamSubscription.unsubscribe();

		sbr1.unsubscribe();
		sbr2.unsubscribe();
		endTestWait(150);
	}


	/** single Observable.interval.publish yields a ConnectableObservable
	 * sourceObservable (interval) doesnt produce any until sbr1 subscribes to publishSubject
	 * because ConnectableObservable hasn't requested any? anyway ok
	 * sbr1/sbr2 receive values as they are produced from then on
	 * sbr1/sbr2 can be disconnected from the entire stream at any time (removed sbr2 below)
	 * disconnecting ConnectableObservable from `intervalObs` (using `upstreamSubscription`)
	 * terminates the entire stream permanently, re-connecting has no effect
	 */
	//@Test
	public void connectableObservableLimitations() throws Exception {
		// given
		final Observable<Long> intervalObs = Observable.interval(100, MILLISECONDS);
		final ConnectableObservable<Long> connectableObs = intervalObs.publish();

		PrintNumSubscriber sbr1 = new PrintNumSubscriber("sbr1");
		PrintNumSubscriber sbr2 = new PrintNumSubscriber("sbr2");


		// when
		messageThenWait("connecting ConnectableObservable to intervalObs", 250);
		// NOTE: intervalObs doesnt produce anything when connected because none are connected to ConnectableObservable
		Subscription upstreamSubscription = connectableObs.connect();
		printSubscriptionStatus("upstreamSubscription", upstreamSubscription);

		connectableObs.subscribe(sbr1);
		messageThenWait("let sbr1 observe alone", 350);

		connectableObs.subscribe(sbr2);
		messageThenWait("let both exec", 250);


		Timber.d("unsubscribing sbr2");
		sbr2.unsubscribe();

		messageThenWait("allow exec", 250);

		Timber.d("disconnecting ConnectableObservable (upstream.unsubcribe)");
		upstreamSubscription.unsubscribe();

		printSubscriptionStatus("upstreamSubscription", upstreamSubscription);
		printSubscriptionStatus("sbr1", sbr1);
		printSubscriptionStatus("sbr2", sbr2);
		messageThenWait("allow exec", 250);


		Timber.d("reconnecting ConnectableObservable to intervalObs (no effect)");
		connectableObs.connect();
		// NOTE: nothing here (doesnt restart `intervalObs`)

		messageThenWait("allow exec", 350);


		Timber.d("disconnect all");
		sbr1.unsubscribe();
		sbr2.unsubscribe();
		upstreamSubscription.unsubscribe();
		endTestWait(150);
	}
	//endregion

	//region Chaining Conditional

	/** works fine */
//@Test
	public void chainConditional() throws Exception {
		// given
		final int firstDelay = 1;
		final int secondDelay = 150;
		final Observable<Long> numObs =
				Observable.fromCallable(new SleepDelayNumCallable(firstDelay))
					.subscribeOn(Schedulers.io());


		final PublishSubject<String> publishSubject = PublishSubject.create();
		Observable<String> targetObsv = Observable.defer(() -> {
			numObs.subscribe( (theNum) -> {
				if (true || theNum % 2 == 0) {
					Observable.fromCallable(new SleepDelayCallable(secondDelay))
							.map(s -> {
								return s.toLowerCase();
							})
						.subscribe(publishSubject);
				} else {
					Observable.just("cached-response").subscribe(publishSubject);
				}
			});

			return publishSubject.serialize();
		}).subscribeOn(Schedulers.io()).cache();

		PrintSubscriber sbr = new PrintSubscriber("sbr");
		final TestSubscriber<String> testSubscriber = new TestSubscriber<>();


		// when
		targetObsv.subscribe(sbr);
		targetObsv.subscribe(testSubscriber);


		messageThenWait("allow exec", 750);


		endTestWait(150);
	}

	/** works fine */

	public Observable<String> getChainedObservable(final int firstDelay, final int secondDelay) {
		final Observable<Long> firstObs =
				Observable.fromCallable(new SleepDelayNumCallable(firstDelay))
						.subscribeOn(Schedulers.io());

		final PublishSubject<String> publishSubject = PublishSubject.create();

		return Observable.defer(() -> {
			firstObs.subscribe( (theNum) -> {
				if (true || theNum % 2 == 0) {
					Observable.fromCallable(new SleepDelayCallable(secondDelay)).subscribe(publishSubject);
				} else {
					Observable.just("cached-response").subscribe(publishSubject);
				}
			});

			return publishSubject.serialize();
		}).subscribeOn(Schedulers.io()).cache();
	}
//@Test
	public void chainConditionalExternal() throws Exception {
		final Observable<String> chainedObsv = getChainedObservable(300, 150);

		PrintSubscriber sbr = new PrintSubscriber("sbr");
		final TestSubscriber<String> testSubscriber = new TestSubscriber<>();


		// when
		chainedObsv.subscribe(sbr);
		chainedObsv.subscribe(testSubscriber);


		messageThenWait("allow exec", 750);


		endTestWait(150);
	}

	/** works fine */
//@Test
	public void chainConditionalWithAsyncSubject() throws Exception {
		// given
		final int firstDelay = 1;
		final int secondDelay = 150;
		final Observable<Long> numObs =
				Observable.fromCallable(new SleepDelayNumCallable(firstDelay))
						.subscribeOn(Schedulers.io());


		final AsyncSubject<String> subject = AsyncSubject.create();
		Observable<String> targetObsv = Observable.defer(() -> {
			numObs.subscribe( (theNum) -> {
				// fake condition
				if (true || theNum % 2 == 0) {
					Observable.fromCallable(new SleepDelayCallable(secondDelay))
							.map(s -> {
								return s.toLowerCase();
							})
							.subscribe(subject);
				} else {
					Observable.just("cached-response").subscribe(subject);
				}
			});

			return subject;
		}).subscribeOn(Schedulers.io()).cache();

		PrintSubscriber sbr = new PrintSubscriber("sbr");
		final TestSubscriber<String> testSubscriber = new TestSubscriber<>();


		// when
		targetObsv.subscribe(sbr);
		targetObsv.subscribe(testSubscriber);


		messageThenWait("allow exec", 750);


		endTestWait(150);
	}
	//endregion
}
