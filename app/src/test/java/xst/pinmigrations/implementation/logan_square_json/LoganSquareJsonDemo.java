package xst.pinmigrations.implementation.logan_square_json;

/*
import com.bluelinelabs.logansquare.LoganSquare;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import timber.log.Timber;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.positions.json.PositionJson;
import xst.pinmigrations.test.BaseUnitTest;
import xst.pinmigrations.domain.android_context.UT_FileUtil;
import xst.pinmigrations.domain.android_context.FileUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class LoganSquareJsonDemo extends BaseUnitTest {

	public final static String POS_JSON_REL_PATH = "/app/src/main/assets/example-positions.json";
	public final static String PROJECT_ROOT = UT_FileUtil.getCurrentDir();
	public final static String POS_JSON_ABS_PATH = PROJECT_ROOT + POS_JSON_REL_PATH;

	public final static String JSON_START = "[{\"id\":58,";
	public final static String JSON_END = "\"ord\":0}]";
	public static final double MAX_DELTA_DOUBLE_COMPARISONS = 1e-15;


	//region basic functions
	//@Test
	public void shouldReadSampleFile() throws Exception {
		Timber.d("PROJECT_ROOT: " + PROJECT_ROOT);
		Timber.d("POS_JSON_ABS_PATH: " + POS_JSON_ABS_PATH);

		String json = null;
		try {
			final InputStream is = UT_FileUtil.getFile(POS_JSON_ABS_PATH);
			json = FileUtil.readFile(is);
		} catch (IOException e) {
			Timber.e(e);
		}

		assertNotNull(json);
		assertEquals(json.length(), 5586);
		String jsonReadStart = json.substring(0, JSON_START.length());
		assertEquals(jsonReadStart, JSON_START);
		String jsonReadEnd = json.substring(json.length() - JSON_END.length());
		assertEquals(jsonReadEnd, JSON_END);
	}
	//endregion


	//region test util
	private static String readSamplePositionsIntoString() throws IOException {
		return FileUtil.readFile(UT_FileUtil.getFile(POS_JSON_ABS_PATH));
	}
	private static InputStream getSamplePositionsInputStream() throws FileNotFoundException {
		return UT_FileUtil.getFile(POS_JSON_ABS_PATH);
	}
	//endregion


	//region Parse JSON to Model
//		First JSON object:
//
//		{"id":58,"lat":"20.61222","lng":"-103.3374","description":"Julio arana","guid":"","ord":0}
//
//		expected serialization:
//		{"description":"Julio arana","guid":"","id":58,"lat":"20.61222","lng":"-103.3374","ord":0}
//
//		NOTE: fields are serialized in alphabetical order

	//@Test
	public void shouldParseJsonFromInputStreamToModel() throws Exception {
		// GIVEN
		final InputStream is = getSamplePositionsInputStream();


		// WHEN
		final List<PositionJson> positionJsonList = LoganSquare.parseList(is, PositionJson.class);
		final PositionJson firstPositionJsonObj = positionJsonList.get(0);


		// THEN
		assertEquals(58, firstPositionJsonObj.id);
		assertEquals("20.61222", firstPositionJsonObj.lat);
		assertEquals("-103.3374", firstPositionJsonObj.lng);
		assertEquals("Julio arana", firstPositionJsonObj.description);
		assertEquals("", firstPositionJsonObj.guid);
		assertEquals(0, firstPositionJsonObj.ord);
	}
	//@Test
	public void shouldParseJsonFromStringToModel() throws Exception {
		// GIVEN
		final String positionsJson = readSamplePositionsIntoString();


		// WHEN
		final List<PositionJson> positionJsonList = LoganSquare.parseList(positionsJson, PositionJson.class);
		final PositionJson firstPositionJsonObj = positionJsonList.get(0);


		// THEN
		assertEquals(58, firstPositionJsonObj.id);
		assertEquals("20.61222", firstPositionJsonObj.lat);
		assertEquals("-103.3374", firstPositionJsonObj.lng);
		assertEquals("Julio arana", firstPositionJsonObj.description);
		assertEquals("", firstPositionJsonObj.guid);
		assertEquals(0, firstPositionJsonObj.ord);
	}
	//endregion


	//region Serialize Model to JSON
//		JSON model:
//			final PositionJson p = new PositionJson();
//			p.id = 58;
//			p.lat = "20.61222";
//			p.lng = "-103.3374";
//			p.description = "Julio arana";
//			p.guid = "";
//			p.ord = 0;
//
//		expected-serialization:
//			{"description":"Julio arana","guid":"","id":58,"lat":"20.61222","lng":"-103.3374","ord":0}

	// NOTE: fields are serialized in alphabetical order
	public static final String EXPECTED_SERIALIZATION = "{\"description\":\"Julio arana\",\"guid\":\"\",\"id\":58,\"lat\":\"20.61222\",\"lng\":\"-103.3374\",\"ord\":0}";
	//@Test
	public void shouldSerializeModelToJsonOutputStream() throws Exception {
		// GIVEN
		// model object
		final PositionJson p = new PositionJson();
		p.id = 58;
		p.lat = "20.61222";
		p.lng = "-103.3374";
		p.description = "Julio arana";
		p.guid = "";
		p.ord = 0;

		// WHEN
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		LoganSquare.serialize(p, baos);
		final String outputString = new String(baos.toByteArray(), "UTF-8");

		// THEN
		assertEquals(EXPECTED_SERIALIZATION, outputString);
	}

	//@Test
	public void shouldSerializeModelToJsonString() throws Exception {
		// GIVEN
		// model object
		final PositionJson p = new PositionJson();
		p.id = 58;
		p.lat = "20.61222";
		p.lng = "-103.3374";
		p.description = "Julio arana";
		p.guid = "";
		p.ord = 0;

		// WHEN
		final String outputString = LoganSquare.serialize(p);

		// THEN
		assertEquals(EXPECTED_SERIALIZATION, outputString);
	}
	//endregion


	//region Parse JSON to Domain
	//@Test
	public void shouldParseJsonFromInputStreamToDomain() throws Exception {
		// GIVEN
		final InputStream is = getSamplePositionsInputStream();


		// WHEN
		final List<PositionJson> positionJsonList = LoganSquare.parseList(is, PositionJson.class);
		final Position firstPosition = new Position( positionJsonList.get(0) );


		// THEN
		assertEquals(58, firstPosition.id);
		assertEquals(20.61222, firstPosition.lat, MAX_DELTA_DOUBLE_COMPARISONS);
		assertEquals(-103.3374, firstPosition.lng, MAX_DELTA_DOUBLE_COMPARISONS);
		assertEquals("Julio arana", firstPosition.description);
		assertEquals("", firstPosition.guid);
		assertEquals(0, firstPosition.ord);
	}

	//@Test
	public void shouldParseJsonFromStringToDomain() throws Exception {
		// GIVEN
		final String positionsJson = readSamplePositionsIntoString();


		// WHEN
		final List<PositionJson> positionJsonList = LoganSquare.parseList(positionsJson, PositionJson.class);
		final Position firstPosition = new Position( positionJsonList.get(0) );


		// THEN
		assertEquals(58, firstPosition.id);
		assertEquals(20.61222, firstPosition.lat, MAX_DELTA_DOUBLE_COMPARISONS);
		assertEquals(-103.3374, firstPosition.lng, MAX_DELTA_DOUBLE_COMPARISONS);
		assertEquals("Julio arana", firstPosition.description);
		assertEquals("", firstPosition.guid);
		assertEquals(0, firstPosition.ord);
	}
	//endregion


	//region Serialize Domain to JSON
	//@Test
	public void shouldSerializeDomainToJsonOutputStream() throws Exception {
		// GIVEN
		// domain object
		final Position p = new Position();
		p.id = 58;
		p.lat = 20.61222;
		p.lng = -103.3374;
		p.description = "Julio arana";
		p.guid = "";
		p.ord = 0;

		// WHEN
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		LoganSquare.serialize(new PositionJson(p), baos);
		final String outputString = new String(baos.toByteArray(), "UTF-8");

		// THEN
		assertEquals(EXPECTED_SERIALIZATION, outputString);
	}
	//@Test
	public void shouldSerializeDomainToJsonString() throws Exception {
		// GIVEN
		// domain object
		final Position p = new Position();
		p.id = 58;
		p.lat = 20.61222;
		p.lng = -103.3374;
		p.description = "Julio arana";
		p.guid = "";
		p.ord = 0;

		// WHEN
		final String outputString = LoganSquare.serialize(new PositionJson(p));

		// THEN
		assertEquals(EXPECTED_SERIALIZATION, outputString);
	}
	//endregion
}
*/