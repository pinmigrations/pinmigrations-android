package xst.pinmigrations.implementation.mockito;

/**
 * Common Test includes. start with at least these
*/

/*
public class MockitoImplementationDemo extends BaseUnitTest {
	// NOTE: this can be re-used
	public static final TPositionAssertsCompare POSITION_ASSERTS = new TPositionAssertsCompare();

	// simple test, mostly to demo Mockito features
	// test works well, actual PathListService seems broken
	// @Test
	public void shouldRetriveAllPositionsAsPathList() {
		// GIVEN
		ArrayList<Position> positions = new ArrayList<>();
		final Position originalPosition = TPositionsFactory.realistic();
		positions.add(originalPosition);
		// setup observable
		Observable<ArrayList<Position>> obs = Observable.just(positions);
		TestSubscriber<ArrayList<Position>> inputSubscriber = new TestSubscriber<>();
		obs.subscribe(inputSubscriber);
		// setup PositionsService Mock
		PositionsResource positionsResourceMock = networkServiceMock(PositionsResource.class);
		PositionsSamplesResource samplesResourceMock = networkServiceMock(PositionsSamplesResource.class);
		RemoteLogResource remoteLogResourceMock = networkServiceMock(RemoteLogResource.class);

		when(positionsResourceMock.getAllPositions()).thenReturn(obs);

		// doesnt need PathsService for this test, but null-checks
		final PathListResource pathListResourceSUT =
				new PathListResource(positionsResourceMock, samplesResourceMock, remoteLogResourceMock);


		// WHEN
		Observable<PathList> pathListResult = pathListResourceSUT.getAllPaths();
		// NOTE: execution begins when the result is subscribed (so this belongs in when block)
		TestSubscriber<PathList> outputSubscriber = new TestSubscriber<>();
		pathListResult.subscribe(outputSubscriber);



		// THEN
		inputSubscriber.awaitTerminalEventAndUnsubscribeOnTimeout(600, MILLISECONDS);
		outputSubscriber.awaitTerminalEventAndUnsubscribeOnTimeout(600, MILLISECONDS);

		inputSubscriber.assertCompleted();
		outputSubscriber.assertCompleted();

		// verify networkServiceMock was invoked exactly once
		verify(positionsResourceMock, times(1)).getAllPositions();

		List<PathList> pathLists = outputSubscriber.getOnNextEvents();
		assertThat(pathLists).hasSize(1);

		PathList pathList = pathLists.get(0);
		assertThat(pathList.getPathsCount()).isEqualTo(1);
		assertThat(pathList.getPositionsCount()).isEqualTo(1);

		Position resultPosition = pathList.paths.get(0).positions.get(0);
		assertThat(resultPosition).usingComparator(POSITION_ASSERTS).isEqualTo(originalPosition);
	}
}
*/