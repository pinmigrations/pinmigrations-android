package xst.pinmigrations.implementation.ix;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ix.Ix;
import timber.log.Timber;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.positions.TPositionJson.Factory;
import xst.pinmigrations.domain.positions.json.PositionJson;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;

public class IxJavaDemo extends BaseUnitTest {
	public static final Random rand = new Random();

//	@Test
	public void simpleJsonMapping() {
		ArrayList<PositionJson> positionsJson = Factory.listRealistic(5);

		// WHEN
		List<Position> positions = Ix.from(positionsJson).map(Position::new).toList();

		// THEN
		Ix.from(positions).subscribe(pos -> Timber.d(pos.toString()) );
	}

//	@Test
	public void fizzBuzz() {
		Ix.range(0, 100)
				.subscribe((n) -> {
					if (0 == n%5 && 0 == n%3) Timber.d(n + " FizzBuzz");
					else if (0 == n%3) Timber.d(n + " Fizz");
					else if (0 == n%5) Timber.d(n + " Buzz");
				});

	}


//	@Test
	public void arrayListCopy() {
		PositionJson pristine = Factory.realistic();

		// #1 (ArrayList collections copy-ctor
		ArrayList<Position> list1 = new ArrayList<>();
		list1.add( new Position(pristine) );

		ArrayList<Position> list2 = new ArrayList<>(list1);
//		assert(list1 != list 2).as("lists equal").isNotSameAs(list2);
		assertThat(list1.get(0).hashCode()).as("pos equal").isNotEqualTo(list2.get(0).hashCode());
	}
}
