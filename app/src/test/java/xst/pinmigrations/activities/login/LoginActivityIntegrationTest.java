package xst.pinmigrations.activities.login;

import android.widget.Button;
import android.widget.EditText;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.mockwebserver.MockWebServer;
import timber.log.Timber;
import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.R;
import xst.pinmigrations.app.RI_App;
import xst.pinmigrations.app.util.condition_watcher.ConditionWatcher;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceSpy;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceSpyGenerator;
import xst.pinmigrations.domain.login.Login;
import xst.pinmigrations.domain.login.RI_LoginUtil;
import xst.pinmigrations.domain.login.TLogin;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.network.RI_NetworkService;
import xst.pinmigrations.domain.network.TInMemoryCookieJar;
import xst.pinmigrations.test.robolectric.BaseRobolectricIntegrationTest;

import static org.assertj.android.api.Assertions.assertThat;
import static xst.pinmigrations.domain.login.TLogin.CSRF_TOKEN_1;
import static xst.pinmigrations.domain.login.TLogin.CSRF_TOKEN_2;
import static xst.pinmigrations.domain.login.TLogin.CSRF_TOKEN_3;
import static xst.pinmigrations.domain.login.TLogin.REMEMBER_TOKEN_1;
import static xst.pinmigrations.domain.login.TLogin.SESSION_1;
import static xst.pinmigrations.domain.login.TLogin.SESSION_2;
import static xst.pinmigrations.domain.login.TLogin.SESSION_3;
import static xst.pinmigrations.domain.login.TLogin.TEST_EMAIL;
import static xst.pinmigrations.domain.login.TLogin.TEST_PASSWORD;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, application = RI_App.class, sdk=22)
public class LoginActivityIntegrationTest extends BaseRobolectricIntegrationTest {
	@Inject MockWebServer server;
	@Inject TLoginManager loginManager;
	@Inject UiServiceSpyGenerator uiServiceSpyGenerator;
	@Inject RI_NetworkService riNetworkService;

	ActivityController<LoginActivity> controller;
	LoginActivity loginActivity;
	UiServiceSpy uiServiceSpy;
	TInMemoryCookieJar cookieJar;

	@BindView(R.id.login_loginBtn) Button loginButton;
	@BindView(R.id.login_logoutBtn) Button logoutButton;
	@BindView(R.id.login_emailEdit) EditText emailEditText;
	@BindView(R.id.login_passwordEdit) EditText passwordEditText;
	LoginPresenter loginPresenter;

	String domain;
	String host;
	String referer;

	@Before public void setupActivity() {
		getInjector().inject(this);

		domain = riNetworkService.getApiDomain();
		host = riNetworkService.getApiHost();
		referer = riNetworkService.getApiReferer();

		cookieJar = riNetworkService.getTInMemoryCookieJar();

		controller = Robolectric.buildActivity(LoginActivity.class);
		loginActivity = controller.get();
	}

	void bindViewFields() {
		unbinder = ButterKnife.bind(this, loginActivity);
		loginPresenter = loginActivity.mLoginPresenter;
		uiServiceSpy = uiServiceSpyGenerator.getGeneratedUiService(LoginActivity.class);
	}

	@After public void teardown() {
		unbind();
		teardownActivityAndController(loginActivity, controller);
	}


	@Test public void clickingLoginButton_shouldLogin() throws Exception {
		// GIVEN
		RI_LoginUtil.setupServerForCheckSignInFalse(server, CSRF_TOKEN_1, SESSION_1);
		loginManager.setLoginObj(TLogin.builder()
				.loginState(Login.UNAUTHENTICATED)
				.appSessionCookie("")
				.authenticityToken("")
				.rememberUserToken("")
				.userEmail("")
				.userPassword("")
				.build());

		RI_LoginUtil.setupServerForLoginSuccess(server, CSRF_TOKEN_2, SESSION_2, CSRF_TOKEN_3, SESSION_3, REMEMBER_TOKEN_1);
		final Login expectedLogin = TLogin.builder()
				.loginState(LOGGED_IN)
				.appSessionCookie(SESSION_3)
				.authenticityToken(CSRF_TOKEN_3)
				.rememberUserToken(REMEMBER_TOKEN_1)
				.userEmail(TEST_EMAIL)
				.userPassword(TEST_PASSWORD)
				.build();
		controller.setup();

		bindViewFields();

		// WHEN
		emailEditText.setText(TEST_EMAIL);
		passwordEditText.setText(TEST_PASSWORD);
		loginButton.performClick();


		// THEN
		ConditionWatcher.waitForCondition(loginCompleted, 225, MILLISECONDS, 5, SECONDS);

		org.assertj.core.api.Assertions.
				assertThat(loginManager.getInternalLoginObj()).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin); //!error expect=SESSION3, actual=empty-string

		org.assertj.core.api.Assertions.
				assertThat(uiServiceSpy.getQuickNotices().get(0)).isEqualTo("Logged In");

		assertThat(loginButton).isGone();
		assertThat(logoutButton).isVisible();
		assertThat(emailEditText).isGone();
		assertThat(passwordEditText).isGone();
	}

	@Test public void clickingLogoutButton_shouldLogout() throws Exception {
		// GIVEN
		RI_LoginUtil.setupServerForCheckSignInTrue(server, CSRF_TOKEN_2, SESSION_2);
		loginManager.setLoginObj(TLogin.builder()
				.loginState(Login.LOGGED_IN)
				.appSessionCookie(SESSION_1)
				.authenticityToken(CSRF_TOKEN_1)
				.rememberUserToken(REMEMBER_TOKEN_1)
				.userEmail(TEST_EMAIL)
				.userPassword(TEST_PASSWORD)
				.build());

		RI_LoginUtil.setupServerForLogout(server, CSRF_TOKEN_3, SESSION_3);
		final Login expectedLogin = TLogin.builder()
				.loginState(Login.UNAUTHENTICATED)
				.appSessionCookie("")
				.authenticityToken("")
				.rememberUserToken("")
				.userEmail(TEST_EMAIL)
				.userPassword(TEST_PASSWORD)
				.build();
		controller.setup();
		bindViewFields();

		// WHEN
		logoutButton.performClick();

		// THEN
		ConditionWatcher.waitForCondition(logoutCompleted, 200, MILLISECONDS, 4, SECONDS);

		org.assertj.core.api.Assertions.
				assertThat(loginManager.getInternalLoginObj()).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		org.assertj.core.api.Assertions.
				assertThat(uiServiceSpy.getQuickNotices().get(0)).isEqualTo("Logged Out");

		assertThat(loginButton).isVisible();
		assertThat(logoutButton).isGone();
		assertThat(emailEditText).isVisible();
		assertThat(passwordEditText).isVisible();
	}


	final ConditionWatcher.Condition loginCompleted = new ConditionWatcher.Condition() {
		@Override public boolean checkCondition() {
			return 3 == server.getRequestCount()
					&& loginPresenter.mCheckSignInSubscriber.isUnsubscribed()
					&& loginPresenter.mLoginSubscriber.isUnsubscribed();
		}
		@Override public void failLog() {
			final int serverRequests = server.getRequestCount();
			if (3 != serverRequests) { Timber.d("MockServer requests=" + serverRequests); }
			if (!loginPresenter.mCheckSignInSubscriber.isUnsubscribed()) { Timber.d("CheckSignIn still subscribed"); }
			if (!loginPresenter.mLoginSubscriber.isUnsubscribed()) { Timber.d("LoginSubscriber still subscribed"); }
		}
	};
	final ConditionWatcher.Condition logoutCompleted = new ConditionWatcher.Condition() {
		@Override public boolean checkCondition() {
			return 2 == server.getRequestCount()
					&& loginPresenter.mCheckSignInSubscriber.isUnsubscribed()
					&& loginPresenter.mLogoutSubscriber.isUnsubscribed();
		}
		@Override public void failLog() {
			final int serverRequests = server.getRequestCount();
			if (2 != serverRequests) { Timber.d("MockServer requests=" + serverRequests); }
			if (!loginPresenter.mCheckSignInSubscriber.isUnsubscribed()) { Timber.d("CheckSignIn still subscribed"); }
			if (!loginPresenter.mLogoutSubscriber.isUnsubscribed()) { Timber.d("LoginSubscriber still subscribed"); }
		}
	};
}
