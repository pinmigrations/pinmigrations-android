package xst.pinmigrations.activities.login;


import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.R;
import xst.pinmigrations.app.RF_App;
import xst.pinmigrations.app.util.TCommonUtil;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceMockGenerator;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsResource;
import xst.pinmigrations.domain.api.MockApiGenerator;
import xst.pinmigrations.domain.login.LoginApi;
import xst.pinmigrations.domain.login.LoginResource;
import xst.pinmigrations.domain.login.RF_LoginUtil;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.network.TInMemoryCookieJar;
import xst.pinmigrations.test.robolectric.BaseRobolectricFunctionalTest;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.assertj.android.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static xst.pinmigrations.domain.login.TLogin.TEST_EMAIL;
import static xst.pinmigrations.domain.login.TLogin.TEST_PASSWORD;

@Config(constants = BuildConfig.class, application = RF_App.class, sdk=22)
@RunWith(RobolectricTestRunner.class)
public class LoginActivityFunctionalTest extends BaseRobolectricFunctionalTest {
	@Inject MockApiGenerator mockApiGenerator;
	@Inject TLoginManager loginManager;
	@Inject UiServiceMockGenerator uiServiceMockGenerator;
	@Inject TInMemoryCookieJar cookieJar;
	@Inject PrefsResource prefsResourceMock;
	@Inject LoginResource loginResource;

	ActivityController<LoginActivity> controller;
	LoginActivity loginActivity;
	UiService uiServiceMock;
	LoginApi loginApiMock;
	LoginPresenter loginPresenter;
	LoginMvpView loginView;

	@BindView(R.id.login_loginBtn) Button loginButton;
	@BindView(R.id.login_logoutBtn) Button logoutButton;
	@BindView(R.id.login_emailEdit) EditText emailEditText;
	@BindView(R.id.login_passwordEdit) EditText passwordEditText;

	@Before public void setupActivity() {
		getInjector().inject(this);

		loginApiMock = mockApiGenerator.createService(LoginApi.class);

		controller = Robolectric.buildActivity(LoginActivity.class);
		loginActivity = controller.get();
	}

	void bindViewFields() {
		ButterKnife.bind(this, loginActivity);
		uiServiceMock = uiServiceMockGenerator.getGeneratedUiService(LoginActivity.class);
		loginPresenter = checkNotNull(loginActivity.mLoginPresenter);
		loginView = checkNotNull(loginPresenter.mLoginView);
	}

	void startActivityCheckRemoteFalse() { startActivity(false); }
	void startActivityCheckRemoteTrue() { startActivity(true); }
	void startActivity(final boolean checkRemote) {
		final Bundle bundle = checkRemote ? null : new Bundle(); // null-bundle forces check-remote
		controller.create(bundle).start().resume().visible();
	}

	@After public void teardown() {
		unbind();
		teardownActivityAndController(loginActivity, controller);
	}

	@Test public void shouldLoadProperlyWhenLoggedIn() {
		// GIVEN
		RF_LoginUtil.setupMocksForCheckSignInTrue(mockApiGenerator, loginManager);

		// WHEN
		startActivityCheckRemoteTrue();

		// THEN
		assertThat(loginActivity).isNotFinishing();

		bindViewFields();
		assertThat(loginButton).isGone();
		assertThat(logoutButton).isVisible();

		assertThat(emailEditText).isGone();
		assertThat(passwordEditText).isGone();

		org.assertj.core.api.Assertions.
				assertThat(loginPresenter.mCheckSignInSubscriber.isUnsubscribed()).isTrue();
	}

	@Test public void shouldResumeProperlyWhenLoggedIn() {
		// GIVEN
		RF_LoginUtil.setupMocksForSoftCheckSignInTrue(loginManager, loginResource);

		// WHEN
		startActivityCheckRemoteFalse();

		// THEN
		assertThat(loginActivity).isNotFinishing();

		bindViewFields();
		assertThat(loginButton).isGone();
		assertThat(logoutButton).isVisible();

		assertThat(emailEditText).isGone();
		assertThat(passwordEditText).isGone();

		org.assertj.core.api.Assertions.
				assertThat(loginPresenter.mCheckSignInSubscriber.isUnsubscribed()).isTrue();
	}

	@Test public void shouldLoadProperlyWhenLoggedOut() {
		// GIVEN
		RF_LoginUtil.setupMocksForCheckSignInFalse(mockApiGenerator, loginManager);

		// WHEN
		startActivityCheckRemoteTrue();

		// THEN
		assertThat(loginActivity).isNotFinishing();
		bindViewFields();

		assertThat(loginButton).isVisible();
		assertThat(logoutButton).isGone();
		assertThat(emailEditText).isVisible();
		assertThat(passwordEditText).isVisible();

		org.assertj.core.api.Assertions.
				assertThat(loginPresenter.mCheckSignInSubscriber.isUnsubscribed()).isTrue();
	}

	@Test public void shouldResumeProperlyWhenLoggedOut() throws Exception {
		// GIVEN
		RF_LoginUtil.setupMocksForSoftCheckSignInFalse(loginManager, loginResource);

		// WHEN
		startActivityCheckRemoteFalse();
		TCommonUtil.messageThenWait("wait", 300, MILLISECONDS);

		// THEN
		assertThat(loginActivity).isNotFinishing();

		bindViewFields();
		assertThat(loginButton).isVisible();
		assertThat(logoutButton).isGone();
		assertThat(emailEditText).isVisible();
		assertThat(passwordEditText).isVisible();

		org.assertj.core.api.Assertions.
				assertThat(loginPresenter.mCheckSignInSubscriber.isUnsubscribed()).isTrue();
	}

	@Test public void clickingLoginButton_shouldLogin() throws Exception {
		// GIVEN
		RF_LoginUtil.setupMocksForCheckSignInFalse(mockApiGenerator, loginManager);
		RF_LoginUtil.setupMocksForLoginSuccess(mockApiGenerator, loginManager, TEST_EMAIL, TEST_PASSWORD);
		startActivityCheckRemoteTrue();

		bindViewFields();

		// WHEN
		emailEditText.setText(TEST_EMAIL);
		passwordEditText.setText(TEST_PASSWORD);
		loginButton.performClick();

		// THEN
		TCommonUtil.messageThenWait("wait for async exec", 300, MILLISECONDS);
		org.assertj.core.api.Assertions.
				assertThat(loginManager.isAuthed()).isTrue();

		assertThat(loginButton).isGone();
		assertThat(logoutButton).isVisible();
		assertThat(emailEditText).isGone();
		assertThat(passwordEditText).isGone();

		org.assertj.core.api.Assertions.
				assertThat(loginPresenter.mLoginSubscriber.isUnsubscribed()).isTrue();

		verify(uiServiceMock, times(1)).showQuickNotice("Logged In");
	}

	@Test public void clickingLogoutButton_shouldLogout() throws Exception {
		// GIVEN
		RF_LoginUtil.setupMocksForCheckSignInTrue(mockApiGenerator, loginManager);
		RF_LoginUtil.setupMocksForLogout(mockApiGenerator, loginManager, TEST_EMAIL, TEST_PASSWORD);
		startActivityCheckRemoteTrue();
		bindViewFields();

		// WHEN
		logoutButton.performClick();

		// THEN
		TCommonUtil.messageThenWait("wait for async exec", 300, MILLISECONDS);

		org.assertj.core.api.Assertions.
			assertThat(loginManager.isUnauthed()).isTrue();

		assertThat(loginButton).isVisible();
		assertThat(logoutButton).isGone();
		assertThat(emailEditText).isVisible();
		assertThat(passwordEditText).isVisible();

		org.assertj.core.api.Assertions.
				assertThat(loginPresenter.mLogoutSubscriber.isUnsubscribed()).isTrue();

		verify(uiServiceMock, times(1)).showQuickNotice("Logged Out");
	}

	@Test public void clickingLoginButtonWithoutCredentials_shouldShowMessageOnly() throws Exception {
		// GIVEN
		RF_LoginUtil.setupMocksForCheckSignInFalse(mockApiGenerator, loginManager);
		startActivityCheckRemoteTrue();
		bindViewFields();

		// WHEN
		emailEditText.setText("");
		passwordEditText.setText("");
		loginButton.performClick();

		// THEN
		TCommonUtil.messageThenWait("wait for async exec", 300, MILLISECONDS);

		verify(uiServiceMock, times(1)).showImportantMessage("Please complete all fields");

		verify(loginApiMock, times(0)).login(anyString(), anyString());
		org.assertj.core.api.Assertions.
			assertThat(loginManager.isUnauthed()).isTrue();
	}
}