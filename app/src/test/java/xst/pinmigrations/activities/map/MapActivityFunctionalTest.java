package xst.pinmigrations.activities.map;


import android.widget.Button;
import android.widget.ToggleButton;

import com.google.android.gms.maps.GoogleMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.R;
import xst.pinmigrations.app.RF_App;
import xst.pinmigrations.domain.activity_context.ui_service.UiService;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceMockGenerator;
import xst.pinmigrations.domain.api.MockApiGenerator;
import xst.pinmigrations.domain.positions.PositionsApi;
import xst.pinmigrations.test.robolectric.BaseRobolectricFunctionalTest;

import static org.assertj.android.api.Assertions.assertThat;
import static org.robolectric.Shadows.shadowOf;

/**
 * Maps-v2 not implemented in Robolectric
 * TODO: migrate to UiAutomator (device) test
 */
@Config(constants = BuildConfig.class, application = RF_App.class, sdk=22)
@RunWith(RobolectricTestRunner.class)
public class MapActivityFunctionalTest extends BaseRobolectricFunctionalTest {
	@Inject MockApiGenerator mockApiGenerator;
	@Inject UiServiceMockGenerator uiServiceMockGenerator;

	@BindView(R.id.map_btn1) Button btn1;
	@BindView(R.id.map_btn2) Button btn2;
	@BindView(R.id.map_lockRotationBtn) ToggleButton lockRotationToggle;

	ActivityController<MapActivity> controller;
	MapActivity mapActivity;
	ShadowActivity mapActivityShadow;
	UiService uiServiceMock;
	PositionsApi positionsApiMock;
	GMapFragment mapFragment;
	GoogleMap map;

	@Before public void setupActivity() {
		getInjector().inject(this);

		positionsApiMock = mockApiGenerator.createService(PositionsApi.class);

		controller = Robolectric.buildActivity(MapActivity.class);
		mapActivity = controller.get();
		mapActivityShadow = shadowOf(mapActivity);
	}

	void bindViewFields() {
		mapFragment = mapActivity.mMapFragment;
		unbinder = ButterKnife.bind(this, mapActivity);
		uiServiceMock = uiServiceMockGenerator.getGeneratedUiService(MapActivity.class);
	}

	@After public void teardown() {
		unbind();
		teardownActivityAndController(mapActivity, controller);
	}

	@Ignore // TODO: test GooglePlayServices confirmation instead, etc..
	@Test public void shouldLoadProperly() throws Exception {
		// GIVEN

		// WHEN
		controller.setup();
		bindViewFields();

		// THEN
		map = mapFragment.mMap; // should be ready now

		assertThat(mapActivity).isNotFinishing();
		org.assertj.core.api.Assertions.
				assertThat(map).isNotNull();

		assertThat(btn1).isVisible();
		assertThat(btn2).isVisible();
		assertThat(lockRotationToggle).isVisible();
	}
}