package xst.pinmigrations.activities.welcome;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.app.RF_App;

@Config(constants = BuildConfig.class, application = RF_App.class, sdk=22)
@RunWith(RobolectricTestRunner.class)
public class WelcomeActivityTestApi19 extends WelcomeActivityTestBase {
	@After public void after() { super.teardown(); }

	@Test public void clickingLoginButton_shouldLoginActivity() {
		super.TEST_clickingLoginButton_shouldLoginActivity();
	}

	@Test public void shouldShowLoginButtonIfNotSignedIn() {
		super.TEST_shouldShowLoginButtonIfNotSignedIn();
	}

	@Test public void shouldShowLogoutButtonIfSignedIn() {
		super.TEST_shouldShowLogoutButtonIfSignedIn();
	}
}