package xst.pinmigrations.activities.welcome;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import xst.pinmigrations.BuildConfig;

/**
 *
 */
@Config(constants = BuildConfig.class, sdk = 22)
@RunWith(RobolectricTestRunner.class)
public class SplashActivityTestApi22 extends SplashActivityTestBase {

	/** PASS THROUGH TO IMPLEMENTATION */
	@After public void after() { super.teardown(); }

	@Test public void shouldLoadProperlyToOnStart() {
		super.TEST_shouldLoadProperlyToOnStart();
	}
	@Test public void shouldRedirectToWelcomeOnResume() {
		super.TEST_shouldRedirectToWelcomeOnResume();
	}
}