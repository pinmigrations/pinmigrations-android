package xst.pinmigrations.activities.welcome;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.app.RF_App;

/**
 *
 */
@Config(constants = BuildConfig.class, application = RF_App.class, sdk=19)
@RunWith(RobolectricTestRunner.class)
public class SplashActivityTestApi19 extends SplashActivityTestBase {

	/** PASS THROUGH TO IMPLEMENTATION */
	@After public void after() { super.teardown(); }

	@Test public void shouldLoadProperlyToOnStart() {
		super.TEST_shouldLoadProperlyToOnStart();
	}
	@Test public void shouldRedirectToWelcomeOnResume() {
		super.TEST_shouldRedirectToWelcomeOnResume();
	}
}