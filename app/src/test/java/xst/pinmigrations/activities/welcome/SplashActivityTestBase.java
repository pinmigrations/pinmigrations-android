package xst.pinmigrations.activities.welcome;

import android.content.Intent;

import org.robolectric.Robolectric;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

import xst.pinmigrations.app.util.TCommonUtil;
import xst.pinmigrations.test.robolectric.BaseRobolectricFunctionalTest;

import static org.assertj.android.api.Assertions.assertThat;
import static org.robolectric.Shadows.shadowOf;

/**
 * Implementation for SplashActivityTestApi*
 * NOTE: ctor is used as @Before for subclass/test setup
 */
@Config()
public class SplashActivityTestBase extends BaseRobolectricFunctionalTest {
	ActivityController<SplashActivity> controller;
	SplashActivity splashActivity;

	/** @Before */
	public SplashActivityTestBase() {
		controller = Robolectric.buildActivity(SplashActivity.class);
		splashActivity = controller.get();
	}

	/** @After */
	void teardown() { teardownActivityAndController(splashActivity, controller); }


	/** @Test */
	public void TEST_shouldLoadProperlyToOnStart() {
		// GIVEN (seup is in @Before)
		// WHEN
		controller.create().start();

		// THEN
		assertThat(splashActivity).isNotNull();
		assertThat(splashActivity).isNotDestroyed();
	}

	/** @Test */
	public void TEST_shouldRedirectToWelcomeOnResume() {
		// GIVEN (setup is in @Before)
		// WHEN
		controller.create().start().resume();
		try {
			TCommonUtil.messageThenWait("wait for SplashActivity init delay runnable", SplashActivity.INIT_DELAY_MS * 1.5, MILLISECONDS);
		} catch (final InterruptedException e) {
			fail("interruped exception", e);
		}

		// THEN
		final Intent activityIntent = shadowOf(splashActivity).getNextStartedActivity();
		final Intent expectedIntent = new Intent(splashActivity, WelcomeActivity.class);

		assertThat(activityIntent).usingComparator(INTENT_ASSERTS).isEqualTo(expectedIntent);

		assertThat(splashActivity).isFinishing();
	}
}
