package xst.pinmigrations.activities.welcome;

import android.content.Intent;

import org.robolectric.Robolectric;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.shadows.ShadowActivity;

import javax.inject.Inject;

import xst.pinmigrations.R;
import xst.pinmigrations.activities.login.LoginActivity;
import xst.pinmigrations.domain.api.MockApiGenerator;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.login.RF_LoginUtil;
import xst.pinmigrations.test.robolectric.BaseRobolectricFunctionalTest;

import static org.assertj.android.api.Assertions.assertThat;
import static org.robolectric.Shadows.shadowOf;

/*
http://robolectric.org/javadoc/3.3/org/robolectric/shadows/ShadowActivity.html
http://robolectric.org/javadoc/3.0/org/robolectric/shadows/ShadowContext.html
 */
public class WelcomeActivityTestBase extends BaseRobolectricFunctionalTest {
	/** these 3 are minimum for setting up login state */
	@Inject MockApiGenerator mockApiGenerator;
	@Inject TLoginManager loginManager;

	ActivityController<WelcomeActivity> controller;
	WelcomeActivity welcomeActivity;
	ShadowActivity welcomeActivityShadow;

	/** @Before */
	public WelcomeActivityTestBase() {
		getInjector().inject(this);

		controller = Robolectric.buildActivity(WelcomeActivity.class);
		welcomeActivity = controller.get();
		welcomeActivityShadow = shadowOf(welcomeActivity);
	}
	/** @After */
	public void teardown() { teardownActivityAndController(welcomeActivity, controller); }


	/** @Test */
	public void TEST_clickingLoginButton_shouldLoginActivity() {
		// GIVEN
		RF_LoginUtil.setupMocksForCheckSignInTrue(mockApiGenerator, loginManager);

		// WHEN
		controller.setup();
		welcomeActivity.findViewById(R.id.welcome_loginBtn).performClick();

		// THEN
		final Intent expectedIntent = new Intent(welcomeActivity, LoginActivity.class);
		final Intent activityIntent = welcomeActivityShadow.getNextStartedActivity();
		assertThat(activityIntent).usingComparator(INTENT_ASSERTS).isEqualTo(expectedIntent);
	}

	/** @Test */
	public void TEST_shouldShowLoginButtonIfNotSignedIn() {
		// GIVEN
		RF_LoginUtil.setupMocksForCheckSignInFalse(mockApiGenerator, loginManager);

		// WHEN
		controller.setup();

		// THEN
		assertThat(welcomeActivity.findViewById(R.id.welcome_loginBtn)).isVisible();
		assertThat(welcomeActivity.findViewById(R.id.welcome_logoutBtn)).isGone();
	}

	/** @Test */
	public void TEST_shouldShowLogoutButtonIfSignedIn() {
		// GIVEN
		RF_LoginUtil.setupMocksForCheckSignInTrue(mockApiGenerator, loginManager);

		// WHEN
		controller.setup();

		// THEN
		assertThat(welcomeActivity.findViewById(R.id.welcome_loginBtn)).isGone();
		assertThat(welcomeActivity.findViewById(R.id.welcome_logoutBtn)).isVisible();
	}
}