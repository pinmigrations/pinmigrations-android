package xst.pinmigrations.test.archived;

import org.apache.commons.lang3.RandomStringUtils;

import timber.log.Timber;
import xst.pinmigrations.app.App;
import xst.pinmigrations.test.BaseUnitTest;

public class StringOpsJvmTiming extends BaseUnitTest {

	/**
	 * Results
	 *
	 * 3 or 4 strings even length-100 + operator is OK
	 * 3 or 4 strings builder is OK but initialize with first-string
	 * more than 4 strings, use builder
	 *
	 * avoid String.format if possible
	 */

	public static final String FORMAT_3 = "%s%s%s";
	public static final String FORMAT_4 = "%s%s%s%s";
	public static boolean shouldLog = true;

	// dont run this test with suite, informational only
//	@Test
	public void execToRunTest() {
		// warm-up VM (~400ms on JVM, ? on android)
		// semi-random string ops to get some JIT optimized code
		for (int i = 0; i<1000; i++) {
			final String s0 = RandomStringUtils.randomAlphanumeric(1024);
			final String s1 = s0.substring(s0.length() / 2) + s0.substring(1);
			final String s2 = s1.concat(s0);
			final String s3 = s2.concat(s1).concat(s0);
			final String s4 = String.format(FORMAT_3, s0, s1, s2);
			final String s5 = String.format(FORMAT_4, s0, s1, s2, s3);
		}
		final StringBuilder warmUpAppender = new StringBuilder();
		for (int i = 0; i<1000; i++) {
			final String s4 = RandomStringUtils.randomAlphanumeric(i);
			warmUpAppender.append(s4);
			warmUpAppender.setLength( 0 );

			warmUpAppender.append(s4 + s4.substring(s4.length() / 2));
			warmUpAppender.setLength( warmUpAppender.length() / 2);

			warmUpAppender.append( warmUpAppender.substring( warmUpAppender.length() / 2 ) );
			warmUpAppender.append( warmUpAppender.toString().concat( RandomStringUtils.randomAlphanumeric(i / 2)));
		}


		// disable loggign first
		App.disableLogging();
		shouldLog = false;


		// warm-up test logic
		for (int i = 0; i<1000; i++) {
			execTestConstantLength(3, 5, 200);
			execTestConstantLength(4, 5, 200);
			execTestConstantLength(3, 10, 200);
			execTestConstantLength(4, 10, 200);
			execTestConstantLength(3, 25, 200);
			execTestConstantLength(4, 25, 200);
			execTestConstantLength(3, 50, 200);
			execTestConstantLength(4, 50, 200);
			execTestConstantLength(3, 25, 200);
			execTestConstantLength(4, 25, 200);
		}

		// now log
		App.enableLogging();
		shouldLog = true;

		// exec tests
		execTestConstantLength(3, 5, 500);
		execTestConstantLength(4, 5, 500);

		execTestConstantLength(3, 10, 500);
		execTestConstantLength(4, 10, 500);

		execTestConstantLength(3, 25, 500);
		execTestConstantLength(4, 25, 500);

		execTestConstantLength(3, 50, 500);
		execTestConstantLength(4, 50, 500);

		execTestConstantLength(3, 100, 100);
		execTestConstantLength(4, 100, 100);
	}

	public class ElapsedTimes {public long t1=0, t2=0, t3=0, t4=0, t5=0; }
	public String execTestConstantLength(final int stringArgs, final int stringLength, final int reps) {
		final String arg1 = RandomStringUtils.randomAlphanumeric(stringLength);
		final String arg2 = RandomStringUtils.randomAlphanumeric(stringLength);
		final String arg3 = RandomStringUtils.randomAlphanumeric(stringLength);
		final String arg4 = RandomStringUtils.randomAlphanumeric(stringLength);

		String r1="", r2="", r3="", r4="", r5="";
		long s1=0, s2=0, s3=0, s4=0, s5=0;
		long e1=0, e2=0, e3=0, e4=0, e5=0;
		final ElapsedTimes[] elapsed = new ElapsedTimes[reps];
		for (int i=0; i<reps; i++) { elapsed[i] = new ElapsedTimes(); }

		if (3 == stringArgs) {
			for (int i=0; i<reps; i++) {
				s1 = System.nanoTime();
				r1 = concat3StringFormat(arg1, arg2, arg3);
				e1 = System.nanoTime();
				elapsed[i].t1 = e1 - s1;

				s2 = System.nanoTime();
				r2 = concat3WithConcat(arg1, arg2, arg3);
				e2 = System.nanoTime();
				elapsed[i].t2 = e2 - s2;

				s3 = System.nanoTime();
				r3 = concat3WithBuilder(arg1, arg2, arg3);
				e3 = System.nanoTime();
				elapsed[i].t3 = e3 - s3;

				s4 = System.nanoTime();
				r4 = concat3WithBuilder2(arg1, arg2, arg3);
				e4 = System.nanoTime();
				elapsed[i].t4 = e4 - s4;

				s5 = System.nanoTime();
				r5 = concat3WithPlusOperator(arg1, arg2, arg3);
				e5 = System.nanoTime();
				elapsed[i].t5 = e5 - s5;
			}
		} else if (4 == stringArgs) {
			for (int i=0; i<reps; i++) {
				s1 = System.nanoTime();
				r1 = concat4StringFormat(arg1, arg2, arg3, arg4);
				e1 = System.nanoTime();
				elapsed[i].t1 = e1 - s1;

				s2 = System.nanoTime();
				r2 = concat4WithConcat(arg1, arg2, arg3, arg4);
				e2 = System.nanoTime();
				elapsed[i].t2 = e2 - s2;

				s3 = System.nanoTime();
				r3 = concat4WithBuilder(arg1, arg2, arg3, arg4);
				e3 = System.nanoTime();
				elapsed[i].t3 = e3 - s3;

				s4 = System.nanoTime();
				r4 = concat4WithBuilder2(arg1, arg2, arg3, arg4);
				e4 = System.nanoTime();
				elapsed[i].t4 = e4 - s4;

				s5 = System.nanoTime();
				r5 = concat4WithPlusOperator(arg1, arg2, arg3, arg4);
				e5 = System.nanoTime();
				elapsed[i].t5 = e5 - s5;
			}
		}


		double sum1=0, sum2=0, sum3=0, sum4=0, sum5=0;
		for (int j=0; j<reps; j++) {
			sum1 += elapsed[j].t1;
			sum2 += elapsed[j].t2;
			sum3 += elapsed[j].t3;
			sum4 += elapsed[j].t4;
			sum5 += elapsed[j].t5;
		}

		double avg1=0, avg2=0, avg3=0, avg4=0, avg5=0;
		avg1 = sum1 / reps;
		avg2 = sum2 / reps;
		avg3 = sum3 / reps;
		avg4 = sum4 / reps;
		avg5 = sum5 / reps;

		if (shouldLog) { // skip if logging is loggingDisabled to exec faster (logging is slow)
			Timber.d("String.format   | %s-arg/str-len=%s | avg-duration: %f ns", stringArgs, stringLength, avg1);
			Timber.d("String.concat   | %s-arg/str-len=%s | avg-duration: %f ns", stringArgs, stringLength, avg2);
			Timber.d("StringBuilder-A | %s-arg/str-len=%s | avg-duration: %f ns", stringArgs, stringLength, avg3);
			Timber.d("StringBuilder-B | %s-arg/str-len=%s | avg-duration: %f ns", stringArgs, stringLength, avg4);
			Timber.d("String + String | %s-arg/str-len=%s | avg-duration: %f ns", stringArgs, stringLength, avg5);
			Timber.d("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		}

		// random string ops to prevent optimizing out concats
		// timing occurs above
		final String rN = r1.substring(1) + r2.substring(1) + r3.substring(1) + r4.substring(1) + r5.substring(1);
		return rN.substring(rN.length() / 2);
	}
	public static void execTestRandomLength(final int stringArgs, final int stringLength) {
		if (3 == stringArgs) {

		} else if (4 == stringArgs) {

		}
	}

	//region Implementation 3-Strings
	static String concat3StringFormat(final String s1, final String s2, final String s3) {
		return String.format(FORMAT_3, s1, s2, s3);
	}

	static String concat3WithConcat(final String s1, final String s2, final String s3) {
		return s1.concat(s2).concat(s3);
	}

	static String concat3WithBuilder(final String s1, final String s2, final String s3) {
		final StringBuilder builder = new StringBuilder();
		builder.append(s1).append(s2).append(s3);
		return builder.toString();
	}

	static String concat3WithBuilder2(final String s1, final String s2, final String s3) {
		return new StringBuilder(s1).append(s2).append(s3).toString();
	}

	static String concat3WithPlusOperator(final String s1, final String s2, final String s3) {
		return s1 + s2 + s3;
	}
	//endregion

	//region Implementation 4-Strings
	static String concat4StringFormat(final String s1, final String s2, final String s3, final String s4) {
		return String.format(FORMAT_4, s1, s2, s3, s4);
	}

	static String concat4WithConcat(final String s1, final String s2, final String s3, final String s4) {
		return s1.concat(s2).concat(s3).concat(s4);
	}

	static String concat4WithBuilder(final String s1, final String s2, final String s3, final String s4) {
		final StringBuilder builder = new StringBuilder();
		builder.append(s1).append(s2).append(s3).append(s4);
		return builder.toString();
	}

	static String concat4WithBuilder2(final String s1, final String s2, final String s3, final String s4) {
		return new StringBuilder(s1).append(s2).append(s3).append(s4).toString();
	}

	static String concat4WithPlusOperator(final String s1, final String s2, final String s3, final String s4) {
		return s1 + s2 + s3 + s4;
	}
	//endregion
}
