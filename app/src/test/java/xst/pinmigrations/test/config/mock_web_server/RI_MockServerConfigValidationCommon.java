package xst.pinmigrations.test.config.mock_web_server;

import org.robolectric.RuntimeEnvironment;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import xst.pinmigrations.app.RI_App;
import xst.pinmigrations.test.BaseTest;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;
import static org.assertj.core.api.Assertions.assertThat;

public abstract class RI_MockServerConfigValidationCommon extends BaseTest {
	private static OkHttpClient client;
	private static OkHttpClient newClient() {
		return new OkHttpClient.Builder()
				.connectTimeout(1000, TimeUnit.MILLISECONDS)
				.build();
	}
	static OkHttpClient getClient() {
		if (null == client) { client = newClient(); }
		return client;
	}

	static Response httpRequest(final HttpUrl url, final OkHttpClient client) throws IOException {
		return checkNotNull(client).newCall(new Request.Builder()
				.url(url)
				.build()).execute();
	}

	static MockWebServer getMockServer() {
		final RI_App app = (RI_App) verifyNotNull(RuntimeEnvironment.application);
		return app.getRIBaseComponent().getMockWebServer();
	}


	// region ASSERTIONS

	static void serverRequestAsserts(final MockWebServer server) throws IOException {
		assertThat(server).isNotNull();
		server.enqueue(new MockResponse()); // dont care about content

		final OkHttpClient client = newClient();
		httpRequest(server.url("/"), client); // fails if there is an error
	}
	static void serversCountAndStateAsserts(final ArrayList<MockWebServer> servers) {
		assertThat(servers).hasSize(2);

		final OkHttpClient client = getClient();
		for (final MockWebServer s: servers) {
			try {
				httpRequest(s.url("/"), client);

				fail("expected ConnectException/SocketTimeoutException when connecting to server");
			} catch(final IOException exc) {
				assertThat(exc).isOfAnyClassIn(ConnectException.class, SocketTimeoutException.class);
			}
		}

		final MockWebServer s1 = servers.get(0);
		final MockWebServer s2 = servers.get(1);

		assertThat(s1).isNotEqualTo(s2);
		assertThat(s1.hashCode()).isNotEqualTo(s2.hashCode());
		assertThat(s1.url("/")).isNotEqualTo(s2.url("/"));
	}
	// endregion
}
