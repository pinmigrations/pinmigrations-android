package xst.pinmigrations.test.config.mock_web_server;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.mockwebserver.MockWebServer;
import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.app.RI_App;
import xst.pinmigrations.test.activities.BlankActivity;
import xst.pinmigrations.test.robolectric.BaseRobolectricIntegrationTest;

import static xst.pinmigrations.test.config.mock_web_server.RI_MockServerConfigValidationCommon.getMockServer;
import static xst.pinmigrations.test.config.mock_web_server.RI_MockServerConfigValidationCommon.serverRequestAsserts;
import static xst.pinmigrations.test.config.mock_web_server.RI_MockServerConfigValidationCommon.serversCountAndStateAsserts;

/**
 * assertions on Robolectric test environment!
 * NOTE: do NOT add test-methods, this entire class is one test
 * NOTE: this should be identical to RobolectricMockServerConfigValidationApi16/23/25 just different sdk in @Config
 *
 * API 16 (through 22) are problematic for OkHttp3 (3.6.0) it attempted to use
 * 		android.security.NetworkSecurityPolicy.isCleartextTrafficPermitted(String)
 * project minimum is currently SDK 19
 * because it determined the platform was "Android" where it is available at SDK >= 23
 * Robolectric doesnt shadow it because it doesnt exist at SDK <23
 */
@Config(constants = BuildConfig.class, application = RI_App.class, sdk=19)
@RunWith(RobolectricTestRunner.class)
public class RobolectricMockServerConfigValidationApi19 extends BaseRobolectricIntegrationTest {
	BlankActivity blankActivity; // create activity so App is initialized through DI and torn down as well
	MockWebServer server; // instance var so each @Test can assert on it

	@Before public void setupActivity() {
		blankActivity = Robolectric.setupActivity(BlankActivity.class);
		server = getMockServer();

		// assertions for these are in `@AfterClass should*`
		servers.add( server );
	}
	@After public void teardownActivity() {
		blankActivity.finish(); // avoid mem-leaks
	}

	final static ArrayList<MockWebServer> servers = new ArrayList<>(2); // two @Test methods only

	// exactly 2 test methods
	@Test public void server1() throws IOException {
		serverRequestAsserts(server);
	}
	@Test public void server2() throws IOException {
		serverRequestAsserts(server);
	}
	// this is also a "test"
	@AfterClass public static void shouldHaveSeparateServersAndUrl() {
		serversCountAndStateAsserts(servers);
	}
}