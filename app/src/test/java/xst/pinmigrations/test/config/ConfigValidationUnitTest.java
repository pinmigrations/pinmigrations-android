package xst.pinmigrations.test.config;

import org.junit.Test;

import java.util.function.Consumer;

import timber.log.Timber;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;

// this is from `testCommon` source set

/**
 * assertions on test environment!
 */
public class ConfigValidationUnitTest extends BaseUnitTest {
	@Test
	public void shouldCompileAndLinkTestCommon() {
		assertThat( ConfigValidationTestCommon.validateConfig() ).isEqualTo("AWESOME");
	}

	@Test
	public void testCommonShouldLinkWithTestOnlyLibs() {
		final String str = "APACHE-COMMONS-LANG: random: " +
				ConfigValidationTestCommon.tryUsingApacheCommons();

		Timber.d(str);
		assertThat( str ).isNotEmpty();
	}

	@Test
	public void shouldCompileCodeWithMethodReferences() {
		Consumer<String> fnRef = System.out::println;
	}

	@Test
	public void shouldCompileCodeWithLambdas() {
		Consumer<String> lambda = (s) -> { };
	}
}