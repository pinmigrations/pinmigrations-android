package xst.pinmigrations.test;

import xst.pinmigrations.app.TestApp;

/**
 * full app initialization is not done on each unit-test.
 * instead of App ctor, App.onCreate, activity, etc...
 * the "application entry point" is each particular test method
 * the static initializer in BaseUnitTest (which all unit tests must extend)
 * ensures that logging is set correctly for unit tests
 * ok that its called again for every unit test
 * without setup, unit-tests' logs end up as Timber no-ops because no tree was planted
 */
public class BaseUnitTest extends BaseTest {
	static {
		TestApp.setup();
	}
}
