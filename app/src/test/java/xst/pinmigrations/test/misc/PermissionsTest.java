package xst.pinmigrations.test.misc;

import org.junit.Test;
import org.robolectric.manifest.AndroidManifest;
import org.robolectric.res.Fs;

import java.util.HashSet;

import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * http://jeremie-martinez.com/2016/02/16/unit-tests/
 */
public final class PermissionsTest extends BaseUnitTest {
	public static final String[] DEBUG_PERMISSIONS = {
			"android.permission.INTERNET",
			"android.permission.ACCESS_NETWORK_STATE",
			"android.permission.ACCESS_COARSE_LOCATION",
			"android.permission.READ_EXTERNAL_STORAGE", // LeakCanary
			"android.permission.WRITE_EXTERNAL_STORAGE" // LeakCanary
	};
	public static final String[] RELEASE_PERMISSIONS = {
			"android.permission.INTERNET",
			"android.permission.ACCESS_NETWORK_STATE",
			"android.permission.ACCESS_COARSE_LOCATION"
	};

	public static final String DEBUG_MERGED_MANIFEST =
			"build/intermediates/manifests/full/debug/AndroidManifest.xml";
	public static final String RELEASE_MERGED_MANIFEST =
			"build/intermediates/manifests/full/release/AndroidManifest.xml";

	@Test public void shouldMatchDebugPermissions() {
		final AndroidManifest manifest = new AndroidManifest(
				Fs.fileFromPath(DEBUG_MERGED_MANIFEST),
				null,
				null
		);

		assertThat(new HashSet<>(manifest.getUsedPermissions())).
				containsOnly(DEBUG_PERMISSIONS);
	}

	// NOTE: this may fail if `clean` task is executed before `debugUnitTest`
	// since the release manifest wont be generated
	@Test public void shouldMatchReleasePermissions() {
		final AndroidManifest manifest = new AndroidManifest(
				Fs.fileFromPath(RELEASE_MERGED_MANIFEST),
				null,
				null
		);

		assertThat(new HashSet<>(manifest.getUsedPermissions())).
				containsOnly(RELEASE_PERMISSIONS);
	}
}
