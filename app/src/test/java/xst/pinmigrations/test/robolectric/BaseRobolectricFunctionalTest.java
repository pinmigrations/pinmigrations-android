package xst.pinmigrations.test.robolectric;

import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.app.RF_App;
import xst.pinmigrations.app.di.functional.RF_InjectorComponent;

import static com.google.common.base.Verify.verifyNotNull;

/** NOTE: config is here to document defaults, must still be specified in test-class */
@Config(constants = BuildConfig.class, application = RF_App.class, sdk=22)
public class BaseRobolectricFunctionalTest extends BaseRobolectricTest {

	protected final RF_App app;
	public BaseRobolectricFunctionalTest() {
		app = (RF_App) verifyNotNull( RuntimeEnvironment.application );
	}

	protected RF_InjectorComponent getInjector() { return app.getRFInjector(); }
}
