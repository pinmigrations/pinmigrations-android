package xst.pinmigrations.test.robolectric;

import android.app.Activity;
import android.os.Bundle;

import org.robolectric.Robolectric;
import org.robolectric.android.controller.ActivityController;

public class RobolectricActivityCtrlUtil {

	public static class UtilResult<T extends Activity> {
		public ActivityController<T> newController;
		public T newActivity;
	}
	@SuppressWarnings("unchecked")
	public static <T extends Activity> UtilResult<T> simulateScreenRotate(final ActivityController<T> originalController,
																	   final Class<T> activityClass) {
		final Bundle bundle = new Bundle();
		// Destroy the original newActivity
		originalController
				.saveInstanceState(bundle)
				.pause()
				.stop()
				.destroy();

		final UtilResult<T> result = new UtilResult<>();


		// Bring up a new newActivity
		result.newController = Robolectric.buildActivity(activityClass)
				.create(bundle)
				.start()
				.restoreInstanceState(bundle)
				.resume()
				.visible();

		result.newActivity = result.newController.get();

		return result;
	}
}
