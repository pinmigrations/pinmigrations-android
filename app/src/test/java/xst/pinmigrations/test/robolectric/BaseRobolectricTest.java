package xst.pinmigrations.test.robolectric;

import android.app.Activity;
import android.support.annotation.Nullable;

import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.shadows.ShadowApplication;

import butterknife.Unbinder;
import xst.pinmigrations.test.BaseUnitTest;

/*
http://robolectric.org/javadoc/3.0/org/robolectric/RuntimeEnvironment.html
http://robolectric.org/javadoc/3.0/org/robolectric/Shadows.html
 */
class BaseRobolectricTest extends BaseUnitTest {
	protected Unbinder unbinder;

	protected ShadowApplication shadowApp;
	public BaseRobolectricTest() {
		shadowApp = Shadows.shadowOf( RuntimeEnvironment.application );
	}

	public <T extends Activity> void teardownActivityAndController(@Nullable final T activity, @Nullable final ActivityController<T> controller) {
		// avoid mem-leaks
		if (null != activity) { activity.finish(); }
		if (null != controller) { controller.destroy(); }
	}

	public void unbind() {
		if (null != unbinder) { unbinder.unbind(); }
	}
}
