package xst.pinmigrations.test.robolectric;

import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.app.RI_App;
import xst.pinmigrations.app.di.integration.RI_InjectorComponent;

import static com.google.common.base.Verify.verifyNotNull;

/** NOTE: config is here to document defaults, must still be specified in test-class */
@Config(constants = BuildConfig.class, application = RI_App.class, sdk=22)
public class BaseRobolectricIntegrationTest extends BaseRobolectricTest {

	protected RI_App app;
	public BaseRobolectricIntegrationTest() {
		app = (RI_App) verifyNotNull( RuntimeEnvironment.application );
	}

	public RI_InjectorComponent getInjector() { return app.getRIInjector(); }
}
