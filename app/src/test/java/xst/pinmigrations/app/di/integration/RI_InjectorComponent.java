package xst.pinmigrations.app.di.integration;

import dagger.Component;
import xst.pinmigrations.activities.login.LoginActivityIntegrationTest;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.app.di.scopes.ActivityScope;

@ActivityScope
@Component(dependencies = RI_BaseComponent.class)
public interface RI_InjectorComponent extends InjectorComponent {

	/** Tests */
	void inject(LoginActivityIntegrationTest test);
}
