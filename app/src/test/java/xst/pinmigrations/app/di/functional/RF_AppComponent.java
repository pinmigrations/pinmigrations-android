package xst.pinmigrations.app.di.functional;

import javax.inject.Singleton;

import dagger.Component;
import xst.pinmigrations.app.RF_App;
import xst.pinmigrations.app.di.AppComponent;

@Singleton
@Component(modules = RF_AppModule.class)
public interface RF_AppComponent extends AppComponent {
	RF_App getRFApp();
}
