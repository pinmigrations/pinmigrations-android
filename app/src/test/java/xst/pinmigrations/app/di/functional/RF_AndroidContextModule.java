package xst.pinmigrations.app.di.functional;

import org.mockito.Mockito;

import dagger.Module;
import dagger.Provides;
import xst.pinmigrations.app.RF_App;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceGenerator;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceMockGenerator;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.android_context.GoogleApiService;
import xst.pinmigrations.domain.android_context.TGoogleApiService;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsResource;
import xst.pinmigrations.domain.maps.MapsServiceGenerator;

@Module
public class RF_AndroidContextModule {
	@AppScope @Provides
	public PrefsResource providePrefsResource() {
		return Mockito.mock(PrefsResource.class);
	}

	@AppScope @Provides
	public PrefsManager providePrefsManager(final PrefsResource prefsResource) {
		return new PrefsManager(prefsResource);
	}

	@AppScope @Provides
	public UiServiceMockGenerator provideUiServiceMockGenerator() {
		return new UiServiceMockGenerator();
	}

	@AppScope @Provides
	public UiServiceGenerator provideUiServiceGenerator(final UiServiceMockGenerator uiServiceMockGenerator) {
		return uiServiceMockGenerator;
	}

	@AppScope @Provides
	public AndroidService provideAndroidResourcesService(final RF_App app) {
		return new AndroidService(app);
	}

	@AppScope @Provides public TGoogleApiService provideTGoogleApiService(final RF_App app) {
		return new TGoogleApiService(app);
	}

	@AppScope @Provides public GoogleApiService provideGoogleApiService(final TGoogleApiService service) {
		return service;
	}

	@AppScope @Provides
	public MapsServiceGenerator provideMapsServiceGenerator(final AndroidService androidService) {
		return new MapsServiceGenerator(androidService);
	}
}
