package xst.pinmigrations.app.di.integration;

import android.support.annotation.NonNull;

import java.io.IOException;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockWebServer;
import xst.pinmigrations.app.App;
import xst.pinmigrations.app.RI_App;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * NOTE: static initializers only run once when JVM is started for test-suite
 * Test*Module and Test*Component are re-instantiated for every test
 *
 * https://github.com/robolectric/robolectric/issues/595
 */
@Module
public class RI_AppModule {
	@NonNull private final RI_App app;

	public RI_AppModule(@NonNull final RI_App app) {
		this.app = checkNotNull(app);
	}

	@Singleton @Provides
	public @NonNull App provideApp() {
		return app;
	}

	@Singleton @Provides
	public @NonNull RI_App provideTestApp() {
		return app;
	}

	@Singleton @Provides @Named("PREFS_FILE_NAME")
	public String providePrefsFileName() {
		return "RI-SharedPreferences";
	}

	@Singleton @Provides
	public MockWebServer provideMockWebServer() {
		final MockWebServer server = new MockWebServer();

		try {
			server.start();
		} catch (final IOException e) {
			throw new RuntimeException("mock-server start failed", e);
		}

		return server;
	}

	@Singleton @Provides @Named("BASE_URL")
	public HttpUrl provideBaseUrl(final MockWebServer server) {
		return server.url("/");
	}
}
