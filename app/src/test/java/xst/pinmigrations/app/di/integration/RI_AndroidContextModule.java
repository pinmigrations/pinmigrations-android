package xst.pinmigrations.app.di.integration;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import xst.pinmigrations.app.RI_App;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceGenerator;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceSpyGenerator;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.android_context.GoogleApiService;
import xst.pinmigrations.domain.android_context.TGoogleApiService;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsResource;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsStore;
import xst.pinmigrations.domain.maps.MapsServiceGenerator;
import xst.pinmigrations.domain.rx.RxJavaService;

@Module
public class RI_AndroidContextModule {
	@AppScope @Provides
	public PrefsStore providePrefsStore(final RI_App app, @Named("PREFS_FILE_NAME") final String prefsFileName) {
		return new PrefsStore(app, prefsFileName);
	}

	@AppScope @Provides
	public PrefsResource providePrefsResource(final PrefsStore prefsStore, final RxJavaService rxJavaService) {
		return new PrefsResource(prefsStore, rxJavaService);
	}

	@AppScope @Provides
	public PrefsManager providePrefsManager(final PrefsResource prefsResource) {
		return new PrefsManager(prefsResource);
	}

	@AppScope @Provides
	public UiServiceSpyGenerator provideUiServiceMockGenerator(final RI_App app, final RxJavaService rxJavaService) {
		return new UiServiceSpyGenerator(app, rxJavaService);
	}

	@AppScope @Provides
	public UiServiceGenerator provideUiServiceGenerator(final UiServiceSpyGenerator uiServiceSpyGenerator) {
		return uiServiceSpyGenerator;
	}

	@AppScope @Provides
	public AndroidService provideAndroidResourcesService(final RI_App app) {
		return new AndroidService(app);
	}

	@AppScope @Provides public TGoogleApiService provideTGoogleApiService(final RI_App app) {
		return new TGoogleApiService(app);
	}

	@AppScope @Provides public GoogleApiService provideGoogleApiService(final TGoogleApiService service) {
		return service;
	}

	@AppScope @Provides
	public MapsServiceGenerator provideMapsServiceGenerator(final AndroidService androidService) {
		return new MapsServiceGenerator(androidService);
	}
}
