package xst.pinmigrations.app.di.functional;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.TLogin;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.network.RF_NetworkService;
import xst.pinmigrations.domain.network.TInMemoryCookieJar;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.domain.rx.TRxJavaService;

/*
	provides services w/o deps or only on other services, not on Activity/Context
 */
@Module
public class RF_ServicesModule {
	@AppScope @Provides
	public TLogin provideInitialLoginState() {
		return new TLogin();
	}

	@AppScope @Provides
	public TLoginManager provideTLoginManager(final TLogin initialLoginState, final PrefsManager prefsManager) {
		return new TLoginManager(initialLoginState, prefsManager);
	}
	@AppScope @Provides
	public LoginManager provideLoginManager(final TLoginManager tLoginManager) {
		return tLoginManager;
	}


	@AppScope @Provides
	public TInMemoryCookieJar provideTInMemoryCookieJar(final @Named("BASE_URL") HttpUrl baseUrl) {
		return new TInMemoryCookieJar(baseUrl);
	}

	@AppScope @Provides
	public RF_NetworkService provideRFNetworkService(final @Named("BASE_URL") HttpUrl baseUrl, final TLoginManager loginManager,
													 final TInMemoryCookieJar jar) {
		return new RF_NetworkService(baseUrl, loginManager, jar);
	}

	@AppScope @Provides
	public NetworkService provideNetworkService(final RF_NetworkService rfNetworkService) {
		return rfNetworkService;
	}

	@AppScope @Provides
	public RxJavaService provideRxJavaService() {
		// TODO: ensure this doesnt cause deadlock,
		// EG: UI-code blocks waiting on test-code
		return TRxJavaService.newServiceRobolectricFunctionalTests();
	}
}
