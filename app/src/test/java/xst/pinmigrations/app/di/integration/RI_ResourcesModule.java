package xst.pinmigrations.app.di.integration;

import dagger.Module;
import dagger.Provides;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.app.logging.remote_log.TRemoteLogResource;
import xst.pinmigrations.domain.android_context.AndroidService;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.LoginResource;
import xst.pinmigrations.domain.login.TLoginResource;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.paths.PathListResource;
import xst.pinmigrations.domain.positions.PositionsResource;
import xst.pinmigrations.domain.positions.PositionsSamplesResource;
import xst.pinmigrations.domain.positions.TPathListResource;
import xst.pinmigrations.domain.positions.TPositionsResource;
import xst.pinmigrations.domain.rx.RxJavaService;

/*

 */
@Module
public final class RI_ResourcesModule {
	@AppScope @Provides
	public ApiGenerator provideApiGenerator(final NetworkService networkService, final RxJavaService rxJavaService) {
		return new ApiGenerator(networkService, rxJavaService);
	}

	@AppScope @Provides
	public RemoteLogResource provideRemoteLogResource(final ApiGenerator apiGenerator) {
		return new TRemoteLogResource(apiGenerator);
	}

	@AppScope @Provides
	public PositionsResource providePositionsResource(final ApiGenerator apiGenerator,
													  final RemoteLogResource remoteLogResource, final LoginManager loginManager) {
		return new TPositionsResource(apiGenerator, remoteLogResource, loginManager);
	}


	@AppScope @Provides
	public PositionsSamplesResource providePositionsSamplesResource(final AndroidService androidService, final RxJavaService rxJavaService) {
		return new PositionsSamplesResource(androidService, rxJavaService);
	}


	@AppScope @Provides
	public PathListResource providePathListResource(final PositionsResource positionsResource,
													final PositionsSamplesResource positionsSamplesResource,
													final RemoteLogResource remoteLogResource) {
		return new TPathListResource(positionsResource, positionsSamplesResource, remoteLogResource);
	}

	/** provide Activities in Robolectric a mock by default
	 */
	@AppScope @Provides
	public LoginResource provideLoginResource(final ApiGenerator apiGenerator,
											  final LoginManager loginManager,
											  final NetworkService networkService,
											  final RemoteLogResource remoteLogResource) {
		return new TLoginResource(apiGenerator, loginManager, networkService, remoteLogResource);
	}
}
