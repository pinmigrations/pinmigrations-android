package xst.pinmigrations.app.di.integration;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.TLogin;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.network.RI_NetworkService;
import xst.pinmigrations.domain.rx.RxJavaService;
import xst.pinmigrations.domain.rx.TRxJavaService;

/*
	provides services w/o deps or only on other services, not on Activity/Context
 */
@Module
public class RI_ServicesModule {
	@AppScope @Provides
	public TLogin provideInitialLoginState() {
		return new TLogin();
	}

	@AppScope @Provides
	public TLoginManager provideTLoginManager(final TLogin initialLoginState, final PrefsManager prefsManager) {
		return new TLoginManager(initialLoginState, prefsManager);
	}
	@AppScope @Provides
	public LoginManager provideLoginManager(final TLoginManager tLoginManager) {
		return tLoginManager;
	}


	@AppScope @Provides
	public RI_NetworkService provideRINetworkService(@Named("BASE_URL") final HttpUrl rootUrl, final TLoginManager loginManager) {
		return RI_NetworkService.newRINetworkService(rootUrl, loginManager);
	}

	@AppScope @Provides
	public NetworkService provideNetworkService(final RI_NetworkService networkService) {
		return networkService;
	}

	@AppScope @Provides
	public RxJavaService provideRxJavaService() {
		return TRxJavaService.newServiceRobolectricIntegrationTests();
	}
}
