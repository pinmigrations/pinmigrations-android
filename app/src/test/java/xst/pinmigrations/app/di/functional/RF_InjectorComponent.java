package xst.pinmigrations.app.di.functional;

import dagger.Component;
import xst.pinmigrations.activities.login.LoginActivityFunctionalTest;
import xst.pinmigrations.activities.map.MapActivityFunctionalTest;
import xst.pinmigrations.activities.welcome.WelcomeActivityTestBase;
import xst.pinmigrations.app.di.InjectorComponent;
import xst.pinmigrations.app.di.scopes.ActivityScope;

@ActivityScope
@Component(dependencies = RF_BaseComponent.class)
public interface RF_InjectorComponent extends InjectorComponent {

	/** Tests */
	void inject(WelcomeActivityTestBase test);
	void inject(LoginActivityFunctionalTest test);
	void inject(MapActivityFunctionalTest test);
}
