package xst.pinmigrations.app.di.functional;

import dagger.Component;
import xst.pinmigrations.app.RF_App;
import xst.pinmigrations.app.di.BaseComponent;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceMockGenerator;
import xst.pinmigrations.domain.android_context.TGoogleApiService;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsResource;
import xst.pinmigrations.domain.api.MockApiGenerator;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.network.RF_NetworkService;
import xst.pinmigrations.domain.network.TInMemoryCookieJar;

/*
 */
@AppScope
@Component(dependencies = RF_AppComponent.class,
		modules = { RF_ServicesModule.class, RF_AndroidContextModule.class, RF_ResourcesModule.class })
public interface RF_BaseComponent extends BaseComponent {
	RF_App getRFApp();
	MockApiGenerator getMockApiGenerator();
	UiServiceMockGenerator getUiServiceMockGenerator();
	TLoginManager getTLoginManager();
	TInMemoryCookieJar getTInMemoryCookieJar();
	RF_NetworkService getRF_NetworkService();

	PrefsResource getPrefsResource();
	TGoogleApiService getTGoogleApiService();
}
