package xst.pinmigrations.app.di.functional;

import android.support.annotation.NonNull;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import xst.pinmigrations.app.App;
import xst.pinmigrations.app.RF_App;

import static com.google.common.base.Preconditions.checkNotNull;
import static xst.pinmigrations.domain.network.TNetUtil.GOOGLE_URL;


/**
 * NOTE: static initializers only run once when JVM is started for test-suite
 * Test*Module and Test*Component are re-instantiated for every test
 *
 * https://github.com/robolectric/robolectric/issues/595
 */
@Module
public class RF_AppModule {
	private final RF_App app;

	public RF_AppModule(@NonNull final RF_App app) {
		this.app = checkNotNull(app);
	}

	@Singleton @Provides
	public App provideApp() {
		return app;
	}

	@Singleton @Provides
	public RF_App provideTestApp() {
		return app;
	}

	@Singleton @Provides @Named("PREFS_FILE_NAME")
	public String providePrefsFileName() {
		return "RF-SharedPreferences";  // shouldnt actually be used, but need dummy value
	}

	@Singleton @Provides @Named("BASE_URL")
	public HttpUrl provideBaseUrl() {
		return GOOGLE_URL; // shouldnt actually be used, but need dummy value
	}
}
