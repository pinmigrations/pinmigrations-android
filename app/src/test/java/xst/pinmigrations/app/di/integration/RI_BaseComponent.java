package xst.pinmigrations.app.di.integration;

import dagger.Component;
import okhttp3.mockwebserver.MockWebServer;
import xst.pinmigrations.app.RI_App;
import xst.pinmigrations.app.di.BaseComponent;
import xst.pinmigrations.app.di.scopes.AppScope;
import xst.pinmigrations.domain.activity_context.ui_service.UiServiceSpyGenerator;
import xst.pinmigrations.domain.android_context.TGoogleApiService;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.network.RI_NetworkService;

/*
 */
@AppScope
@Component(dependencies = RI_AppComponent.class,
		modules = { RI_ServicesModule.class, RI_AndroidContextModule.class, RI_ResourcesModule.class })
public interface RI_BaseComponent extends BaseComponent {
	RI_App getRIApp();
	NetworkService getNetworkService();
	MockWebServer getMockWebServer();
	UiServiceSpyGenerator getUiServiceSpyGenerator();
	TLoginManager getTLoginManager();
	RI_NetworkService getRINetworkService();

	TGoogleApiService getTGoogleApiService();
}
