package xst.pinmigrations.app.di.integration;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.mockwebserver.MockWebServer;
import xst.pinmigrations.app.RI_App;
import xst.pinmigrations.app.di.AppComponent;

@Singleton
@Component(modules = RI_AppModule.class)
public interface RI_AppComponent extends AppComponent {
	RI_App getRIApp();

	MockWebServer getMockWebServer();
}
