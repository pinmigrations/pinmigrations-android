package xst.pinmigrations.app;

import xst.pinmigrations.app.di.functional.DaggerRF_AppComponent;
import xst.pinmigrations.app.di.functional.DaggerRF_BaseComponent;
import xst.pinmigrations.app.di.functional.DaggerRF_InjectorComponent;
import xst.pinmigrations.app.di.functional.RF_AppComponent;
import xst.pinmigrations.app.di.functional.RF_AppModule;
import xst.pinmigrations.app.di.functional.RF_BaseComponent;
import xst.pinmigrations.app.di.functional.RF_InjectorComponent;

public class RF_App extends TestApp {

	@Override public void onCreate() {
		super.onCreate();

		// Robolectric Functional Tests Specific DI
		final RF_AppComponent appComponent = DaggerRF_AppComponent.builder()
				.rF_AppModule(new RF_AppModule(this)).build();

		// directly override the field in `App`
		mBaseComponent = DaggerRF_BaseComponent.builder()
				.rF_AppComponent(appComponent)
				.build();
	}

	public RF_InjectorComponent getRFInjector() {
		return DaggerRF_InjectorComponent.builder()
				.rF_BaseComponent( (RF_BaseComponent) mBaseComponent ).build();
	}

	@Override protected void initializeLogin() { /* do nothing, done manually in tests */ }
}
