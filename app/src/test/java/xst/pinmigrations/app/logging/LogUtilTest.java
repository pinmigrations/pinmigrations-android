package xst.pinmigrations.app.logging;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import xst.pinmigrations.app.logging.hashlog.HashLogger;
import xst.pinmigrations.app.logging.hashlog.MemberLogger;
import xst.pinmigrations.app.logging.hashlog.StaticLog;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static xst.pinmigrations.app.logging.LogUtilTest.HL.CLASSHASH;
import static xst.pinmigrations.app.logging.LogUtilTest.HL.HASH;

public class LogUtilTest extends BaseUnitTest {
	@Test
	public void shouldIndentEachLine() throws Exception {
		// NOTE:
		// s-1 is string-1
		// e-1 is expected-1
		// a-1 is actual-1
		// all use DEFAULT_INDENT unless explicitly defined before s/e

		// GIVEN
		final int DEFAULT_INDENT_NUM = 4;
		final String DEFAULT_INDENT_STR = "    ";
		final String NL = "\n";

		final String s1 = "single line string" + NL;
		final String e1 = DEFAULT_INDENT_STR + s1;

		final String s2 = "two-lines" + NL + "string" + NL;
		final String e2 = DEFAULT_INDENT_STR + "two-lines" + NL + DEFAULT_INDENT_STR + "string" + NL;

		final String s3 = "";
		final String e3 = NL;

		// ensures multiple inner new-lines are compacted to a single new-line
		final String s4 =
				"string with" + NL
				+ "multiple" + NL
				+ NL + NL + NL + NL
				+ "new-lines" + NL;
		final String e4 =
				DEFAULT_INDENT_STR + "string with" + NL
				+ DEFAULT_INDENT_STR + "multiple" + NL
				+ DEFAULT_INDENT_STR + "new-lines" + NL;

		final String s5 = null;
		final String e5 = DEFAULT_INDENT_STR + LogUtil.NULL_STRING + NL;

		final int indent6 = 0;
		final String s6 = "zero indent" + NL;
		final String e6 = s6;

		final int indent7 = 0;
		final String s7 = "zero indent" + NL + "multi-line" + NL;
		final String e7 = s7;

		// ensures trailing new-line
		final String s8 = "missing trailing new-line";
		final String e8 = DEFAULT_INDENT_STR + s8 + NL;

		// ensures trailing new-line despite lack of indent
		final int indent9 = 0;
		final String s9 = "missing trailing new-line";
		final String e9 = s9 + NL;

		// ensures single new-line at end
		final String s10 =
				"multiple trailing" + NL
				+ "new-lines" + NL
				+ NL + NL + NL;
		final String e10 =
				DEFAULT_INDENT_STR + "multiple trailing" + NL
				+ DEFAULT_INDENT_STR + "new-lines" + NL;

		// WHEN
		final String a1 = LogUtil.indentEachLine(s1, DEFAULT_INDENT_NUM);
		final String a2 = LogUtil.indentEachLine(s2, DEFAULT_INDENT_NUM);
		final String a3 = LogUtil.indentEachLine(s3, DEFAULT_INDENT_NUM);
		final String a4 = LogUtil.indentEachLine(s4, DEFAULT_INDENT_NUM);
		final String a5 = LogUtil.indentEachLine(s5, DEFAULT_INDENT_NUM);
		final String a6 = LogUtil.indentEachLine(s6, indent6);
		final String a7 = LogUtil.indentEachLine(s7, indent7);
		final String a8 = LogUtil.indentEachLine(s8, DEFAULT_INDENT_NUM);
		final String a9 = LogUtil.indentEachLine(s9, indent9);
		final String a10 = LogUtil.indentEachLine(s10, DEFAULT_INDENT_NUM);


		// THEN
		assertThat(a1).isEqualTo(e1);
		assertThat(a2).isEqualTo(e2);
		assertThat(a3).isEqualTo(e3);
		assertThat(a4).isEqualTo(e4);
		assertThat(a5).isEqualTo(e5);
		assertThat(a6).isEqualTo(e6);
		assertThat(a7).isEqualTo(e7);
		assertThat(a8).isEqualTo(e8);
		assertThat(a9).isEqualTo(e9);
		assertThat(a10).isEqualTo(e10);
	}

	static final String NL = "\n";
	public static class HL extends HashLogger {
		public static final String HASH = "@1234";
		@Override public String hashString() { return HASH; }
		public static final String CLASSHASH = "HL@1234";
		@Override public String classHashString() { return CLASSHASH; }
	}
	public static class ML_Flat extends MemberLogger {
		public static final String HASH = "@5678";
		@Override public String hashString() { return HASH; }
		public static final String CLASSHASH = "ML@5678";

		public static final String MIN = "@5678{a=1, b=2}";
		@Override public String getMin() { return MIN; }
		public static final String SHORT = "ML@5678{a=1, b=2}";
		@Override public String getShort() { return SHORT; }
	}
	public static class ML_Nest extends MemberLogger {
		public static final String HASH = "@9091";
		@Override public String hashString() { return HASH; }
		public static final String CLASSHASH = "ML@9091";
		@Override public String classHashString() { return CLASSHASH; }

		public static final String MIN = "@9091{x=0.37, y=HL@1234, z=ML@5678}";
		@Override public String getMin() { return MIN; }
		public static final String SHORT = "ML@9091{x=0.37, y=HL@1234, z=ML@5678{a=1, b=2}}";
		@Override public String getShort() { return SHORT; }
	}
	public static class SomeObject {
		public static final String EXP_HASH = "SomeObject@4949";
		@Override
		public int hashCode() { return 4949; }
	}

	@Test
	public void shouldMakeShortFieldString() throws Exception {
		// NOTE:
		// f-1 is fields-1
		// v-1 is values-1
		// e-1 is expected-1
		// a-1 is actual-1

		/*
			GIVEN
		 */
		final HashLogger hl = new HL();
		final MemberLogger ml = new ML_Flat();
		final SomeObject o = new SomeObject();

		// fields array null, use class-hash on objects and print strings anyway
		final String[] f1 = null;
		final Object[] v1 = {"99", "33.4", "55.6", hl, o};
		final String e1 = "{99, 33.4, 55.6, " + CLASSHASH + ", " + SomeObject.EXP_HASH + "}";

		// values array null, do nothing (return error string so logging doesnt crash app)
		final String[] f2 = {"id", "lat", "lng", "hl", "ml"};
		final Object[] v2 = null;
		final String e2 = LogUtil.ERROR_VALUES_ARRAY_NULL;

		// values and fields null, do nothing (return error string so logging doesnt crash app)
		final String[] f3 = null;
		final Object[] v3 = null;
		final String e3 = LogUtil.ERROR_FIELDS_AND_VALUES_NULL;

		// null values should yield null-string
		final String[] f4 = {"id", "lat", "lng"};
		final Object[] v4 = {null, "33.4", "null"};
		final String e4 = "{id=null, lat=33.4, lng=null}";

		// if field is empty, omit equals-sign
		final String[] f5 = {"", "lat", "", ""};
		final Object[] v5 = {"99", "33.4", "55.6", hl};
		final String e5 = "{99, lat=33.4, 55.6, " + CLASSHASH + "}";

		// if field is null, omit equals-sign
		final String[] f6 = {null, "lat", null, null};
		final Object[] v6 = {"99", "33.4", "55.6", hl};
		final String e6 = "{99, lat=33.4, 55.6, " + CLASSHASH + "}";

		// if objects have fields, use hash-string (not class-hash)
		// NOTE: non-HashLog classes still return class-hash
		final String[] f7 = {"id", "lat", "lng", "hl", "obj"};
		final Object[] v7 = {"99", "33.4", "55.6", hl, o};
		final String e7 = "{id=99, lat=33.4, lng=55.6, hl=" + HASH + ", obj=" + SomeObject.EXP_HASH + "}";

		// omit missing field & value (empty-string)
		final String[] f8 = {"id", "", "lng"};
		final Object[] v8 = {"99", "", "55.6"};
		final String e8 = "{id=99, <MISSING>, lng=55.6}";

		// omit missing field & value (null)
		final String[] f9 = {"id", null, "lng"};
		final Object[] v9 = {"99", null, "55.6"};
		final String e9 = "{id=99, null, lng=55.6}";

		// fields array empty, use class-hash on objects and print strings anyway (same as #1)
		final String[] f10 = {};
		final Object[] v10 = {"99", "33.4", "55.6", hl, o};
		final String e10 = "{99, 33.4, 55.6, " + CLASSHASH + ", " + SomeObject.EXP_HASH + "}";

		// values array empty, do nothing (return error string so logging doesnt crash app) (same as #2)
		final String[] f11 = {"id", "lat", "lng", "hl", "ml"};
		final Object[] v11 = {};
		final String e11 = LogUtil.ERROR_VALUES_ARRAY_NULL;

		// values and fields empty, do nothing (return error string so logging doesnt crash app, same as #3)
		final String[] f12 = {};
		final Object[] v12 = {};
		final String e12 = LogUtil.ERROR_FIELDS_AND_VALUES_NULL;

		// missing fields (larger values array)
		final String[] f13 = {"id", "lat", };
		final Object[] v13 = {"99", "33.4", "55.6", hl};
		final String e13 = "{id=99, lat=33.4, 55.6, "  + CLASSHASH + "}";

		// empty string
		final String[] f14 = {"id", "description"};
		final Object[] v14 = {"99", "", hl};
		final String e14 = "{id=99, description=<EMPTY-STR>, " + CLASSHASH + "}";

		/*
			WHEN
		 */
		final String a1 = LogUtil.minMembersString(f1, v1);
		final String a2 = LogUtil.minMembersString(f2, v2);
		final String a3 = LogUtil.minMembersString(f3, v3);
		final String a4 = LogUtil.minMembersString(f4, v4);
		final String a5 = LogUtil.minMembersString(f5, v5);
		final String a6 = LogUtil.minMembersString(f6, v6);
		final String a7 = LogUtil.minMembersString(f7, v7);
		final String a8 = LogUtil.minMembersString(f8, v8);
		final String a9 = LogUtil.minMembersString(f9, v9);
		final String a10 = LogUtil.minMembersString(f10, v10);
		final String a11 = LogUtil.minMembersString(f11, v11);
		final String a12 = LogUtil.minMembersString(f12, v12);
		final String a13 = LogUtil.minMembersString(f13, v13);
		final String a14 = LogUtil.minMembersString(f14, v14);

		/*
			THEN
		 */
		assertThat(a1).isEqualTo(e1);
		assertThat(a2).isEqualTo(e2);
		assertThat(a3).isEqualTo(e3);
		assertThat(a4).isEqualTo(e4);
		assertThat(a5).isEqualTo(e5);
		assertThat(a6).isEqualTo(e6);
		assertThat(a7).isEqualTo(e7);
		assertThat(a8).isEqualTo(e8);
		assertThat(a9).isEqualTo(e9);
		assertThat(a10).isEqualTo(e10);
		assertThat(a11).isEqualTo(e11);
		assertThat(a12).isEqualTo(e12);
		assertThat(a13).isEqualTo(e13);
		assertThat(a14).isEqualTo(e14);
	}

	/**
	 * NOTE: better solution not possible
	 */
	@Test
	public void shouldGetClassForCollectionsAndMaps() throws Exception {
		// given
		final Position position = new Position(1);


		final List<Position> list1 = new ArrayList<>();
		final String e1 = "ArrayList<T>[EMPTY-COLLECTION]";

		final List<Position> list2 = new ArrayList<>();
		final String param2 = "Position";
		final String e2 = "ArrayList<Position>[EMPTY-COLLECTION]";

		final List<Position> list3 = new ArrayList<>();
		list3.add(position);
		final String e3 = "ArrayList<T>[size=1]";

		final List<Position> list4 = new ArrayList<>();
		list4.add(position);
		final String param4 = "Position";
		final String e4 = "ArrayList<Position>[size=1]";


		final Map<String, Position> map5 = new HashMap<>();
		final String e5 = "HashMap<K,V>[EMPTY-MAP]";


		final Map<String, Position> map6 = new HashMap<>();
		final String key6 = "String";
		final String value6 = "Position";
		final String e6 = "HashMap<String,Position>[EMPTY-MAP]";


		final Map<String, Position> map7 = new HashMap<>();
		map7.put("key", position);
		final String e7 = "HashMap<K,V>[size=1]";

		final Map<String, Position> map8 = new HashMap<>();
		map8.put("key", position);
		final String key8 = "String";
		final String value8 = "Position";
		final String e8 = "HashMap<String,Position>[size=1]";


		// when
		final String a1 = LogUtil.getEmptyCollectionString(list1);
		final String a2 = LogUtil.getEmptyCollectionString(param2, list2);
		final String a3 = LogUtil.getEmptyCollectionString(list3);
		final String a4 = LogUtil.getEmptyCollectionString(param4, list4);

		final String a5 = LogUtil.getEmptyMapString(map5);
		final String a6 = LogUtil.getEmptyMapString(key6, value6, map6);
		final String a7 = LogUtil.getEmptyMapString(map7);
		final String a8 = LogUtil.getEmptyMapString(key8, value8, map8);


		// then
		assertThat(a1).isEqualTo(e1);
		assertThat(a2).isEqualTo(e2);
		assertThat(a3).isEqualTo(e3);
		assertThat(a4).isEqualTo(e4);
		assertThat(a5).isEqualTo(e5);
		assertThat(a6).isEqualTo(e6);
		assertThat(a7).isEqualTo(e7);
		assertThat(a8).isEqualTo(e8);
	}

	@Test
	public void shouldGetClassForStaticMethods() throws Exception {
		// given

		// when
		final String logString = StaticLog._SLM();

		// then
		assertThat(logString).isEqualTo("LogUtilTest::shouldGetClassForStaticMethods");
	}
}