package xst.pinmigrations.app.logging.remote_log;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import rx.Observable;
import rx.observers.TestSubscriber;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.network.UnauthedNetworkService;
import xst.pinmigrations.domain.rx.TRxJavaService;
import xst.pinmigrations.test.BaseUnitTest;
import xst.pinmigrations.app.util.TCommonUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static xst.pinmigrations.app.logging.remote_log.RemoteLogResource.MS_PER_30_MIN;

public class RemoteLogResourceTest extends BaseUnitTest {
	MockWebServer server;
	UnauthedNetworkService unauthedNetworkService;
	RemoteLogResource remoteLogResourceSUT;
	ApiGenerator apiGenerator;

	@Before
	public void setup() throws IOException {
		server = new MockWebServer();
		server.start();

		unauthedNetworkService = new UnauthedNetworkService(server.url("/"));
		apiGenerator = new ApiGenerator(unauthedNetworkService, new TRxJavaService());
		remoteLogResourceSUT = new RemoteLogResource(apiGenerator);
	}


	@Test
	public void shouldPing() throws Exception {
		// GIVEN
		server.enqueue(newPingResponse());

		// WHEN
		Observable<Void> pingObs = remoteLogResourceSUT.ping();
		TCommonUtil.messageThenWait("let ping exec", 0.15, SECONDS);

		// THEN
		assertThat(server.getRequestCount()).isEqualTo(1);
		final RecordedRequest pingReq = server.takeRequest();
		assertThat(pingReq.getPath()).isEqualTo("/ping");
		assertThat(pingReq.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(pingReq.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");

		TestSubscriber<Void> testSubscriber = new TestSubscriber<>();
		pingObs.subscribe(testSubscriber);
		testSubscriber.assertNoErrors();
		testSubscriber.assertCompleted();
	}

	@Test
	public void shouldNotPingIfDebounceNotElapsed() throws Exception {
		// GIVEN
		server.enqueue(newPingResponse());
		server.enqueue(newPingResponse()); // should not be requested

		// WHEN
		final Observable<Void> ping1Obs = remoteLogResourceSUT.ping();
		TCommonUtil.messageThenWait("let ping exec", 0.15, SECONDS);
		final Observable<Void> ping2Obs = remoteLogResourceSUT.ping();
		TCommonUtil.messageThenWait("let ping exec", 0.15, SECONDS);

		// THEN
		assertThat(server.getRequestCount()).isEqualTo(1);
		final RecordedRequest pingReq = server.takeRequest();
		assertThat(pingReq.getPath()).isEqualTo("/ping");
		assertThat(pingReq.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(pingReq.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");

		final TestSubscriber<Void> testSubscriber1 = new TestSubscriber<>();
		ping1Obs.subscribe(testSubscriber1);
		testSubscriber1.assertNoErrors();
		testSubscriber1.assertCompleted();

		final TestSubscriber<Void> testSubscriber2 = new TestSubscriber<>();
		ping2Obs.subscribe(testSubscriber2);
		testSubscriber2.assertNoErrors();
		testSubscriber2.assertCompleted();
	}

	@Test
	public void shouldPingIfDebounceHasElapsed() throws Exception {
		// GIVEN
		server.enqueue(newPingResponse());
		server.enqueue(newPingResponse()); // should not be requested

		// replace RemoteLogResource with Test version with access
		remoteLogResourceSUT = null; // dont use by accident
		final TRemoteLogResource tRemoteLogResourceSUT = new TRemoteLogResource(apiGenerator);
		final long currentTime = System.currentTimeMillis();
		tRemoteLogResourceSUT.setCurrentTimeMs( currentTime );

		final long timePlus30m = currentTime + MS_PER_30_MIN;

		// WHEN
		final Observable<Void> ping1Obs = tRemoteLogResourceSUT.ping();
		TCommonUtil.messageThenWait("let ping exec", 0.15, SECONDS);
		tRemoteLogResourceSUT.setCurrentTimeMs(timePlus30m);

		final Observable<Void> ping2Obs = tRemoteLogResourceSUT.ping();
		TCommonUtil.messageThenWait("let ping exec", 0.15, SECONDS);

		// THEN
		assertThat(server.getRequestCount()).isEqualTo(2);
		final RecordedRequest pingReq1 = server.takeRequest();
		assertThat(pingReq1.getPath()).isEqualTo("/ping");
		assertThat(pingReq1.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(pingReq1.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");

		final RecordedRequest pingReq2 = server.takeRequest();
		assertThat(pingReq2.getPath()).isEqualTo("/ping");
		assertThat(pingReq2.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(pingReq2.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");

		final TestSubscriber<Void> testSubscriber1 = new TestSubscriber<>();
		ping1Obs.subscribe(testSubscriber1);
		testSubscriber1.assertNoErrors();
		testSubscriber1.assertCompleted();

		final TestSubscriber<Void> testSubscriber2 = new TestSubscriber<>();
		ping2Obs.subscribe(testSubscriber2);
		testSubscriber2.assertNoErrors();
		testSubscriber2.assertCompleted();
	}

	public static MockResponse newPingResponse() {
		return new MockResponse()
				.setHeader("Content-Type", "text/html")
				.setResponseCode(200);
	}
}
