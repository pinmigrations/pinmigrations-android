package xst.pinmigrations.app;

import org.robolectric.TestLifecycleApplication;

import java.io.IOException;
import java.lang.reflect.Method;

import okhttp3.mockwebserver.MockWebServer;
import xst.pinmigrations.app.di.integration.DaggerRI_AppComponent;
import xst.pinmigrations.app.di.integration.DaggerRI_BaseComponent;
import xst.pinmigrations.app.di.integration.DaggerRI_InjectorComponent;
import xst.pinmigrations.app.di.integration.RI_AppComponent;
import xst.pinmigrations.app.di.integration.RI_AppModule;
import xst.pinmigrations.app.di.integration.RI_BaseComponent;
import xst.pinmigrations.app.di.integration.RI_InjectorComponent;

public class RI_App extends TestApp implements TestLifecycleApplication {
	@Override public void onCreate() {
		super.onCreate();

		// Robolectric Integration Tests Specific DI
		final RI_AppComponent appComponent = DaggerRI_AppComponent.builder()
				.rI_AppModule(new RI_AppModule(this)).build();


		// directly override the field in `App`
		mBaseComponent = DaggerRI_BaseComponent.builder()
				.rI_AppComponent(appComponent)
				.build();
	}

	public RI_BaseComponent getRIBaseComponent() {
		return (RI_BaseComponent) mBaseComponent;
	}

	public RI_InjectorComponent getRIInjector() {
		return DaggerRI_InjectorComponent.builder()
				.rI_BaseComponent( (RI_BaseComponent) mBaseComponent ).build();
	}

	@Override protected void initializeLogin() { /* do nothing, done manually in tests */ }

	//region TestLifecycleApplication (Robolectric)

	/** invoked by RobolectricTestRunner before each test
	 * @param theTestMethod the @Test annotated test method
	 */
	@Override public void beforeTest(final Method theTestMethod) { /* do nothing */ }

	/** invoked by RobolectricTestRunner before each test
	 * @param theTestClassObject the instance of class containing @Test methods
	 */
	@Override public void prepareTest(final Object theTestClassObject) { /* do nothing */ }

	/** invoked by RobolectricTestRunner after each test
	 * @param theTestMethod the @Test annotated test method
	 */
	@Override public void afterTest(final Method theTestMethod) {
		final MockWebServer server = getRIBaseComponent().getMockWebServer();
		try {
			server.shutdown();
		} catch (final IOException e) {
			throw new RuntimeException("mock-server shutdown failed", e);
		}
	}
	//endregion
}
