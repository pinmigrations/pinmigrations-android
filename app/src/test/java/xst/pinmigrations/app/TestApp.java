package xst.pinmigrations.app;

import android.support.annotation.NonNull;

import timber.log.AppTimber;
import timber.log.Timber;
import xst.pinmigrations.app.logging.TestLogger;
import xst.pinmigrations.test.BaseUnitTest;
import xst.pinmigrations.test.StatefulStub;

// NOTE: TestLifecycleApplication is for Robolectric tests
/** Application class for unit tests w/o Robolectric
 *
 */
public class TestApp extends App {
	public TestApp() {
		setupTestLogging();
	}

	// NOTE: disabling LeakCanary is not necessary, `test` has a no-op dependency JAR for LeakCanary
	/**
	 * full app initialization is not done on each unit-test.
	 * instead of App ctor, App.onCreate, activity, etc...
	 * the "application entry point" is each particular test method
	 * @see BaseUnitTest triggers setup instead (out of necessity)
	 * ensures that logging is set correctly for unit tests
	 * without setup, unit-tests' logs end up as Timber no-ops because no tree was planted
	 */
	public static void setup() {
		setupTestLogging();
	}

	/**
	 * set to false when not developing new unit tests
	 * re-enable on failures / new unit tests dev
	 */
	public static final boolean INTERACTIVE_UNIT_TESTS = true;

	/**
	 * set the correct Log "pipeline" configuration for unit tests
	 * this signals AppTimber to send logs to System.out
	 * without setup, unit-tests' logs end up as Timber no-ops
	 * @see BaseUnitTest
	 */
	public static void setupTestLogging() {
		setExecModeUnitTests();

		if (INTERACTIVE_UNIT_TESTS) {
			AppTimber.setTestLogger(new TestLogger());

			// Robolectric tests re-instantiate `TestApp` for every test
			// however unit tests never instantiate app, so this is to setup from there
			if (0 == Timber.treeCount()) {
				// AppTimber will check `sExecMode` and then forward logging to `TLog`
				Timber.plant(new AppTimber());
			}
		}
	}


	@Override public void onCreate() {
		setupTestLogging(); // this is called externally, hence the duplicate set

		super.onCreate();

		Thread.setDefaultUncaughtExceptionHandler(new TestsUncaughtExceptionHandler());
	}


	@Override public void addStub(@NonNull final StatefulStub stub) {
		/* Robolectric destroys everything between tests so dont bother keeping these stubs */
	}
}
