package xst.pinmigrations.app.util;

import org.junit.Test;

import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;

public class ReflectionUtilTest extends BaseUnitTest {
	public static final String EXPECTED_STRING = "STRING-DATA";
	public static final int EXPECTED_NUM = 3;

	public static final String NEW_STRING = "NEW-STRING";
	public static final int NEW_NUM = 4;
	// this class is essentially an external class (publicly visibile / available without enclosing instance
	public static class SomeType {
		private String str = EXPECTED_STRING;
		private int num = EXPECTED_NUM;
	}
	@Test
	public void shouldRetrievePrivateField() throws Exception {
		// given
		SomeType someObj = new SomeType();

		// when
		String privateString = TReflectionUtil.getPrivateField(someObj, "str");
		int privateNum = TReflectionUtil.getPrivateField(someObj, "num");

		// then
		assertThat(privateString).isEqualTo(EXPECTED_STRING);
		assertThat(privateNum).isEqualTo(EXPECTED_NUM);
	}
	@Test
	public void shouldSetPrivateField() throws Exception {
		// given
		SomeType someObj = new SomeType();

		// when
		TReflectionUtil.setPrivateField(someObj, "str", NEW_STRING);
		TReflectionUtil.setPrivateField(someObj, "num", NEW_NUM);

		// then
		String newString = TReflectionUtil.getPrivateField(someObj, "str");
		int newNum = TReflectionUtil.getPrivateField(someObj, "num");
		assertThat(newString).isEqualTo(NEW_STRING);
		assertThat(newNum).isEqualTo(NEW_NUM);
	}



	public static class ClassWitPckPrivate {
		String str = EXPECTED_STRING;
	}
	@Test
	public void shouldRetrievePrivateFieldFromClassWitPackagePrivateField() throws Exception {
		// given
		ClassWitPckPrivate someObj = new ClassWitPckPrivate();

		// when
		String privateString = TReflectionUtil.getPrivateField(someObj, "str");

		// then
		assertThat(privateString).isEqualTo(EXPECTED_STRING);
	}

	@Test
	public void shouldRetrievePrivateFieldFromLocalClass() throws Exception {
		class LocalClass {
			private String str = EXPECTED_STRING;
		}

		// given
		LocalClass someObj = new LocalClass();

		// when
		String privateString = TReflectionUtil.getPrivateField(someObj, "str");

		// then
		assertThat(privateString).isEqualTo(EXPECTED_STRING);
	}


	public class NonStaticNestedClass {
		private String str = EXPECTED_STRING;
	}
	@Test
	public void shouldRetrievePrivateFieldFromNonStaticNestedClass() throws Exception {
		// given
		NonStaticNestedClass someObj = new NonStaticNestedClass();

		// when
		String privateString = TReflectionUtil.getPrivateField(someObj, "str");

		// then
		assertThat(privateString).isEqualTo(EXPECTED_STRING);
	}


	private static class StaticPrivateNestedClass {
		private String str = EXPECTED_STRING;
	}
	@Test
	public void shouldRetrievePrivateFieldFromStaticPrivateNestedClass() throws Exception {
		// given
		StaticPrivateNestedClass someObj = new StaticPrivateNestedClass();

		// when
		String privateString = TReflectionUtil.getPrivateField(someObj, "str");

		// then
		assertThat(privateString).isEqualTo(EXPECTED_STRING);
	}


	private class NonStaticPrivateNestedClass {
		private String str = EXPECTED_STRING;
	}
	@Test
	public void shouldRetrievePrivateFieldFromNonStaticPrivateNestedClass() throws Exception {
		// given
		NonStaticPrivateNestedClass someObj = new NonStaticPrivateNestedClass();

		// when
		String privateString = TReflectionUtil.getPrivateField(someObj, "str");

		// then
		assertThat(privateString).isEqualTo(EXPECTED_STRING);
	}
	@Test
	public void shouldSetPrivateFieldOnNonStaticPrivateNestedClass() throws Exception {
		// given
		NonStaticPrivateNestedClass someObj = new NonStaticPrivateNestedClass();

		// when
		TReflectionUtil.setPrivateField(someObj, "str", NEW_STRING);

		// then
		String newString = TReflectionUtil.getPrivateField(someObj, "str");
		assertThat(newString).isEqualTo(NEW_STRING);
	}
}