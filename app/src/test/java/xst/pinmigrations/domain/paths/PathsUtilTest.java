package xst.pinmigrations.domain.paths;

import org.junit.Test;

import java.util.ArrayList;

import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;

public class PathsUtilTest extends BaseUnitTest {
	@Test public void shouldGroupPositionsIntoPaths() throws Exception {
		// GIVEN
		final Position p1 = new Position(1, 0, 0, "", "GUID-0", 1);
		final Position p2 = new Position(2, 0, 0, "", "GUID-0", 0);
		final Position p3 = new Position(3, 0, 0, "", "GUID-0", 2);

		final Position p4 = new Position(4, 0, 0, "", "GUID-2", 3);
		final Position p5 = new Position(5, 0, 0, "", "GUID-1", 6);
		final Position p6 = new Position(6, 0, 0, "", "GUID-2", 2);
		final Position p7 = new Position(7, 0, 0, "", "GUID-1", 7);
		final Position p8 = new Position(8, 0, 0, "", "GUID-2", 4);
		final Position p9 = new Position(9, 0, 0, "", "GUID-1", 8);

		final ArrayList<Position> positions = new ArrayList<>();
		positions.add(p1);
		positions.add(p2);
		positions.add(p3);
		positions.add(p4);
		positions.add(p5);
		positions.add(p6);
		positions.add(p7);
		positions.add(p8);
		positions.add(p9);

		// WHEN
		final ArrayList<Path> paths = PathsUtil.groupPositionsIntoPaths(positions);


		// THEN
		final ArrayList<Position> pList1 = paths.get(0).positions;
		assertThat(pList1.get(0).id).isEqualTo(2);
		assertThat(pList1.get(1).id).isEqualTo(1);
		assertThat(pList1.get(2).id).isEqualTo(3);
		final ArrayList<Position> pList2 = paths.get(1).positions;
		assertThat(pList2.get(0).id).isEqualTo(5);
		assertThat(pList2.get(1).id).isEqualTo(7);
		assertThat(pList2.get(2).id).isEqualTo(9);
		final ArrayList<Position> pList3 = paths.get(2).positions;
		assertThat(pList3.get(0).id).isEqualTo(6);
		assertThat(pList3.get(1).id).isEqualTo(4);
		assertThat(pList3.get(2).id).isEqualTo(8);
	}

	@Test public void shouldFlattenPathList() {
		// GIVEN
		final Position p1 = new Position(1, 0, 0, "", "GUID-0", 0),
			p2 = new Position(2, 0, 0, "", "GUID-0", 1),
			p3 = new Position(3, 0, 0, "", "GUID-0", 2);
		final ArrayList<Position> posList1 = new ArrayList<>(3);
		posList1.add(p1); posList1.add(p2); posList1.add(p3);

		final Path path1 = new Path(posList1);
		final Position p4 = new Position(4, 0, 0, "", "GUID-1", 0),
			p5 = new Position(5, 0, 0, "", "GUID-1", 1);
		final ArrayList<Position> posList2 = new ArrayList<>(2);
		posList2.add(p4); posList2.add(p5);

		final ArrayList<Path> paths = new ArrayList<>(2);
		paths.add(new Path(posList1)); paths.add(new Path(posList2));
		final PathList pathList = new PathList(paths);

		// WHEN
		final ArrayList<Position> allPositions = PathsUtil.flattenPathList(pathList);

		// THEN
		assertThat(allPositions).contains(p1, p2, p3, p4, p5);
	}
}