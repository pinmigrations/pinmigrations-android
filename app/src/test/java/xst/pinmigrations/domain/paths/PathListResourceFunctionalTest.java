package xst.pinmigrations.domain.paths;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import rx.observers.TestSubscriber;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.app.logging.remote_log.TRemoteLogResource;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.positions.PositionsResource;
import xst.pinmigrations.domain.positions.PositionsSamplesResource;
import xst.pinmigrations.domain.positions.TPosition;
import xst.pinmigrations.domain.rx.TRxJavaService;
import xst.pinmigrations.domain.rx.TRxUtil;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static xst.pinmigrations.domain.positions.TPosition.POSITION_JSON_6_GUID;

public class PathListResourceFunctionalTest extends BaseUnitTest {
	MockWebServer server;
	HttpUrl serverBaseUrl;

	ApiGenerator apiGenerator;
	PositionsSamplesResource samplesResourceMock;
	PrefsManager prefsManagerMock = Mockito.mock(PrefsManager.class);  // re-use ok
	NetworkService networkService;
	TLoginManager loginManager;
	TRxJavaService rxJavaService;
	final RemoteLogResource remoteLogResourceMock = TRemoteLogResource.getMock(); // re-use ok

	PositionsResource positionsResourceSUT;
	PathListResource pathListResourceSUT;


	@Before public void setup() throws Exception {
		server = new MockWebServer();
		server.start();
		serverBaseUrl = server.url("/");

		loginManager = TLoginManager.loggedInFalseDummyManager(prefsManagerMock);
		networkService = new NetworkService(serverBaseUrl, loginManager);
		rxJavaService = TRxJavaService.newServiceAllTrampoline();
		apiGenerator = new ApiGenerator(networkService, rxJavaService);
		samplesResourceMock = mock(PositionsSamplesResource.class);

		positionsResourceSUT = new PositionsResource(apiGenerator, remoteLogResourceMock, loginManager);
		pathListResourceSUT = new PathListResource(positionsResourceSUT, samplesResourceMock, remoteLogResourceMock);
	}

	@Test public void shouldRetriveAllPositionsAsPathList() throws Exception {
		// GIVEN
		server.enqueue(new MockResponse().setBody("12345")); // feed-hash
		server.enqueue(new MockResponse().setBody(TPosition.POSITION_JSON_5)); // feed

		// WHEN
		final TestSubscriber<PathList> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(pathListResourceSUT.getAllPaths().toBlocking());
		testSubscriber.awaitTerminalEvent(350, MILLISECONDS);


		// THEN
		testSubscriber.assertNoErrors(); testSubscriber.assertCompleted();

		assertThat( server.getRequestCount() ).isEqualTo(2);

		final RecordedRequest feedHashReq = server.takeRequest();
		assertThat(feedHashReq.getMethod().toUpperCase()).isEqualTo("GET");
		assertThat(feedHashReq.getPath()).isEqualTo("/feed-hash");
		final RecordedRequest feedReq = server.takeRequest();
		assertThat(feedReq.getMethod().toUpperCase()).isEqualTo("GET");
		assertThat(feedReq.getPath()).isEqualTo("/feed");

		//
		final PathList pathList = testSubscriber.getOnNextEvents().get(0);
		final Path path1 = pathList.paths.get(0);
		assertThat(path1.positions).contains(TPosition.P5C_EXPECTED);
		final Path path2 = pathList.paths.get(1);
		assertThat(path2.positions).contains(TPosition.P5A_EXPECTED, TPosition.P5B_EXPECTED);
	}


	@Test public void shouldRetrivePathByGuid() throws Exception {
		// GIVEN
		server.enqueue(new MockResponse().setBody(TPosition.POSITION_JSON_6)); // for guid
		final String expectedUrl = "/locations/find/" + POSITION_JSON_6_GUID + ".json";

		// WHEN
		final TestSubscriber<Path> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(pathListResourceSUT.findByGuid(POSITION_JSON_6_GUID).toBlocking());
		testSubscriber.awaitTerminalEvent(350, MILLISECONDS);


		// THEN
		testSubscriber.assertNoErrors(); testSubscriber.assertCompleted();

		assertThat( server.getRequestCount() ).isEqualTo(1);

		final RecordedRequest feedHashReq = server.takeRequest();
		assertThat(feedHashReq.getMethod().toUpperCase()).isEqualTo("GET");
		assertThat(feedHashReq.getPath()).isEqualTo(expectedUrl);

		//
		final Path path = testSubscriber.getOnNextEvents().get(0);
		final Position pos1 = path.positions.get(0);
		assertThat(pos1).usingComparator(POSITION_ASSERTS).isEqualTo(TPosition.P6A_EXPECTED);
		final Position pos2 = path.positions.get(1);
		assertThat(pos2).usingComparator(POSITION_ASSERTS).isEqualTo(TPosition.P6B_EXPECTED);
	}
}