package xst.pinmigrations.domain.paths;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;
import xst.pinmigrations.app.logging.remote_log.TRemoteLogResource;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.positions.Position;
import xst.pinmigrations.domain.positions.PositionsResource;
import xst.pinmigrations.domain.positions.PositionsSamplesResource;
import xst.pinmigrations.domain.positions.TPosition.Factory;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class PathListResourceUnitTest extends BaseUnitTest {
	PositionsSamplesResource samplesResourceMock;
	PositionsResource positionsResourceMock;
	final RemoteLogResource remoteLogResourceMock = TRemoteLogResource.getMock(); // re-use ok
	PathListResource pathListResourceSUT;


	@Before public void setup() {
		positionsResourceMock = mock(PositionsResource.class);
		samplesResourceMock = mock(PositionsSamplesResource.class);
		pathListResourceSUT = new PathListResource(positionsResourceMock, samplesResourceMock, remoteLogResourceMock);
	}

	@Test public void shouldRetriveAllPositionsAsPathList() {
		/*
			GIVEN
		 */
		// setup return value / observable
		final ArrayList<Position> positions = Factory.listRealistic(1);
		Observable<ArrayList<Position>> obs = Observable.just(positions);
		final TestSubscriber<ArrayList<Position>> inputSubscriber = new TestSubscriber<>();
		obs.subscribe(inputSubscriber);

		// setup PositionsService Mock
		when(positionsResourceMock.getAllPositions()).thenReturn(obs);


		/*
			WHEN
		 */
		final Observable<PathList> pathListResult = pathListResourceSUT.getAllPaths();
		// NOTE: execution begins when the result is subscribed (so this belongs in when block)
		final TestSubscriber<PathList> outputSubscriber = new TestSubscriber<>();
		pathListResult.subscribe(outputSubscriber);
		// allow async to complete
		inputSubscriber.awaitTerminalEventAndUnsubscribeOnTimeout(200, MILLISECONDS);
		outputSubscriber.awaitTerminalEventAndUnsubscribeOnTimeout(200, MILLISECONDS);

		/*
			THEN
		 */
		inputSubscriber.assertCompleted(); // completes if SUT subscribes (Observable.just)
		outputSubscriber.assertCompleted();
		outputSubscriber.assertNoErrors();

		// verify networkServiceMock was invoked exactly once
		verify(positionsResourceMock, times(1)).getAllPositions();

		List<PathList> pathLists = outputSubscriber.getOnNextEvents();
		assertThat(pathLists).hasSize(1);

		PathList pathList = pathLists.get(0);
		assertThat(pathList.getPathsCount()).isEqualTo(1);
		assertThat(pathList.getPositionsCount()).isEqualTo(1);

		final Position originalPosition = positions.get(0);
		Position resultPosition = pathList.paths.get(0).positions.get(0);
		assertThat(resultPosition).usingComparator(POSITION_ASSERTS).isEqualTo(originalPosition);
	}



}