package xst.pinmigrations.domain.android_context.shared_preferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.HashMap;

import rx.observers.TestSubscriber;
import xst.pinmigrations.BuildConfig;
import xst.pinmigrations.app.RF_App;
import xst.pinmigrations.domain.rx.TRxJavaService;
import xst.pinmigrations.test.robolectric.BaseRobolectricFunctionalTest;

import static org.assertj.android.api.Assertions.assertThat;

/**
 * economical test for both PrefsResource/PrefsStore
 * http://robolectric.org/javadoc/3.3/org/robolectric/fakes/RoboSharedPreferences.html
 */
@Config(constants = BuildConfig.class, application = RF_App.class, sdk=22)
@RunWith(RobolectricTestRunner.class)
public class PrefsResourceTest extends BaseRobolectricFunctionalTest {

	static final String PREFS_NAME = "TEST";
	PrefsResource prefsResourceSUT;
	TPrefsStore prefsStoreSUT;
	TRxJavaService rxJavaService;

	public final static String STRING_KEY_1 = "KEY-1", STRING_KEY_2 = "KEY-2", STRING_KEY_3 = "KEY-3";
	public final static String STRING_VAL_1 = "VAL-1", STRING_VAL_2 = "VAL-2", STRING_VAL_3 = "VAL-3";

	public final static String INT_KEY_4 = "KEY-4";
	public final static int INT_VAL_4 = 4;

	public final static String BOOL_KEY_5 = "KEY-5";
	public final static boolean BOOL_VAL_5 = true;

	public final static String LONG_KEY_6 = "KEY-6";
	public final static long LONG_VAL_6 = 6;

	public final static String FLOAT_KEY_7 = "KEY-7";
	public final static float FLOAT_VAL_7 = 7.0f;

	@Before public void setup() throws Exception {
		prefsStoreSUT = new TPrefsStore(app, PREFS_NAME);
		rxJavaService = TRxJavaService.newServiceAllImmediate(); // makes PrefsResource block for these tests
		prefsResourceSUT = new PrefsResource(prefsStoreSUT, rxJavaService);
	}

	@Test public void shouldSaveString() {
		// GIVEN (setup in @before)

		// WHEN
		prefsResourceSUT.putString(STRING_KEY_1, STRING_VAL_1);

		// THEN
		assertThat(prefsStoreSUT.getPrefs()).contains(STRING_KEY_1, STRING_VAL_1);
	}
	@Test public void shouldSaveInt() {
		// GIVEN (setup in @before)

		// WHEN
		prefsResourceSUT.putInt(INT_KEY_4, INT_VAL_4);

		// THEN
		assertThat(prefsStoreSUT.getPrefs()).contains(INT_KEY_4, INT_VAL_4);
	}
	@Test public void shouldSaveBool() {
		// GIVEN (setup in @before)

		// WHEN
		prefsResourceSUT.putBool(BOOL_KEY_5, BOOL_VAL_5);

		// THEN
		assertThat(prefsStoreSUT.getPrefs()).contains(BOOL_KEY_5, BOOL_VAL_5);
	}
	@Test public void shouldSaveLong() {
		// GIVEN (setup in @before)

		// WHEN
		prefsResourceSUT.putLong(LONG_KEY_6, LONG_VAL_6);

		// THEN
		assertThat(prefsStoreSUT.getPrefs()).contains(LONG_KEY_6, LONG_VAL_6);
	}
	@Test public void shouldSaveFloat() {
		// GIVEN (setup in @before)

		// WHEN
		prefsResourceSUT.putFloat(FLOAT_KEY_7, FLOAT_VAL_7);

		// THEN
		assertThat(prefsStoreSUT.getPrefs()).contains(FLOAT_KEY_7, FLOAT_VAL_7);
	}

	@Test public void shouldRetrieveString() {
		// GIVEN
		TSharedPreferencesUtil.setupPrefs(prefsStoreSUT.getPrefs(), PrefsUtil.stringEntry(STRING_KEY_1, STRING_VAL_1));
		TestSubscriber<String> testSubscriber = new TestSubscriber<>();

		// WHEN
		prefsResourceSUT.getString(STRING_KEY_1)
				.subscribe(testSubscriber);

		// THEN
		testSubscriber.awaitTerminalEvent(150, MILLISECONDS);

		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		final String result = testSubscriber.getOnNextEvents().get(0);
		org.assertj.core.api.Assertions.
				assertThat(result).isEqualTo(STRING_VAL_1);
	}
	@Test public void shouldRetrieveInt() {
		// GIVEN
		TSharedPreferencesUtil.setupPrefs(prefsStoreSUT.getPrefs(), PrefsUtil.intEntry(INT_KEY_4, INT_VAL_4));
		TestSubscriber<Integer> testSubscriber = new TestSubscriber<>();

		// WHEN
		prefsResourceSUT.getInt(INT_KEY_4)
				.subscribe(testSubscriber);

		// THEN
		testSubscriber.awaitTerminalEvent(150, MILLISECONDS);

		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		final int result = testSubscriber.getOnNextEvents().get(0);
		org.assertj.core.api.Assertions.
				assertThat(result).isEqualTo(INT_VAL_4);
	}
	@Test public void shouldRetrieveBool() {
		// GIVEN
		TSharedPreferencesUtil.setupPrefs(prefsStoreSUT.getPrefs(), PrefsUtil.boolEntry(BOOL_KEY_5, BOOL_VAL_5));
		TestSubscriber<Boolean> testSubscriber = new TestSubscriber<>();

		// WHEN
		prefsResourceSUT.getBool(BOOL_KEY_5)
				.subscribe(testSubscriber);

		// THEN
		testSubscriber.awaitTerminalEvent(150, MILLISECONDS);

		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		final boolean result = testSubscriber.getOnNextEvents().get(0);
		org.assertj.core.api.Assertions.
				assertThat(result).isEqualTo(BOOL_VAL_5);
	}
	@Test public void shouldRetrieveLong() {
		// GIVEN
		TSharedPreferencesUtil.setupPrefs(prefsStoreSUT.getPrefs(), PrefsUtil.longEntry(LONG_KEY_6, LONG_VAL_6));
		TestSubscriber<Long> testSubscriber = new TestSubscriber<>();

		// WHEN
		prefsResourceSUT.getLong(LONG_KEY_6)
				.subscribe(testSubscriber);

		// THEN
		testSubscriber.awaitTerminalEvent(150, MILLISECONDS);

		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		final long result = testSubscriber.getOnNextEvents().get(0);
		org.assertj.core.api.Assertions.
				assertThat(result).isEqualTo(LONG_VAL_6);
	}
	@Test public void shouldRetrieveFloat() {
		// GIVEN
		TSharedPreferencesUtil.setupPrefs(prefsStoreSUT.getPrefs(), PrefsUtil.floatEntry(FLOAT_KEY_7, FLOAT_VAL_7));
		TestSubscriber<Float> testSubscriber = new TestSubscriber<>();

		// WHEN
		prefsResourceSUT.getFloat(FLOAT_KEY_7)
				.subscribe(testSubscriber);

		// THEN
		testSubscriber.awaitTerminalEvent(150, MILLISECONDS);

		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		final float result = testSubscriber.getOnNextEvents().get(0);
		org.assertj.core.api.Assertions.
				assertThat(result).isEqualTo(FLOAT_VAL_7);
	}

	@Test public void shouldFindWithContains() {
		// GIVEN
		TSharedPreferencesUtil.setupPrefs(prefsStoreSUT.getPrefs(), PrefsUtil.stringEntry(STRING_KEY_1, STRING_VAL_1));
		TestSubscriber<Boolean> testSubscriber1 = new TestSubscriber<>();
		TestSubscriber<Boolean> testSubscriber2 = new TestSubscriber<>();

		// WHEN
		prefsResourceSUT.contains(STRING_KEY_1)
				.subscribe(testSubscriber1);
		prefsResourceSUT.contains(STRING_KEY_2)
				.subscribe(testSubscriber2);

		// THEN
		testSubscriber1.awaitTerminalEvent(100, MILLISECONDS);
		testSubscriber2.awaitTerminalEvent(100, MILLISECONDS);

		testSubscriber1.assertCompleted();
		testSubscriber1.assertNoErrors();
		testSubscriber2.assertCompleted();
		testSubscriber2.assertNoErrors();

		org.assertj.core.api.Assertions.
				assertThat(testSubscriber1.getOnNextEvents().get(0)).isTrue();
		org.assertj.core.api.Assertions.
				assertThat(testSubscriber2.getOnNextEvents().get(0)).isFalse();
	}

	@Test public void shouldSaveEntry() {
		// GIVEN (setup in @before)

		// WHEN
		prefsResourceSUT.putEntry(PrefsUtil.stringEntry(STRING_KEY_1, STRING_VAL_1));

		// THEN
		assertThat(prefsStoreSUT.getPrefs()).contains(STRING_KEY_1, STRING_VAL_1);
	}

	@Test public void shouldSaveMultiple() {
		// GIVEN (setup in @before)

		// WHEN
		prefsResourceSUT.putEntries(
				PrefsUtil.stringEntry(STRING_KEY_1, STRING_VAL_1),
				PrefsUtil.intEntry(INT_KEY_4, INT_VAL_4),
				PrefsUtil.boolEntry(BOOL_KEY_5, BOOL_VAL_5),
				PrefsUtil.longEntry(LONG_KEY_6, LONG_VAL_6),
				PrefsUtil.floatEntry(FLOAT_KEY_7, FLOAT_VAL_7)
		);

		// THEN
		assertThat(prefsStoreSUT.getPrefs()).contains(STRING_KEY_1, STRING_VAL_1);
		assertThat(prefsStoreSUT.getPrefs()).contains(INT_KEY_4, INT_VAL_4);
		assertThat(prefsStoreSUT.getPrefs()).contains(BOOL_KEY_5, BOOL_VAL_5);
		assertThat(prefsStoreSUT.getPrefs()).contains(LONG_KEY_6, LONG_VAL_6);
		assertThat(prefsStoreSUT.getPrefs()).contains(FLOAT_KEY_7, FLOAT_VAL_7);
	}

	@Test public void shouldRetrieveMultiple() {
		// GIVEN (setup in @before)
		TSharedPreferencesUtil.setupPrefs(prefsStoreSUT.getPrefs(),
				PrefsUtil.stringEntry(STRING_KEY_1, STRING_VAL_1),
				PrefsUtil.intEntry(INT_KEY_4, INT_VAL_4),
				PrefsUtil.boolEntry(BOOL_KEY_5, BOOL_VAL_5),
				PrefsUtil.longEntry(LONG_KEY_6, LONG_VAL_6),
				PrefsUtil.floatEntry(FLOAT_KEY_7, FLOAT_VAL_7)
		);

		TestSubscriber<HashMap<String, Entry>> testSubscriber = new TestSubscriber<>();

		// WHEN
		prefsResourceSUT.getEntries(
				PrefsUtil.stringKey(STRING_KEY_1),
				PrefsUtil.intKey(INT_KEY_4),
				PrefsUtil.boolKey(BOOL_KEY_5),
				PrefsUtil.longKey(LONG_KEY_6),
				PrefsUtil.floatKey(FLOAT_KEY_7)
		).subscribe(testSubscriber);

		// THEN
		testSubscriber.awaitTerminalEvent(150, MILLISECONDS);

		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();

		final HashMap<String, Entry> results = testSubscriber.getOnNextEvents().get(0);

		org.assertj.core.api.Assertions.
				assertThat(results.get(STRING_KEY_1).value).isEqualTo(STRING_VAL_1);
		org.assertj.core.api.Assertions.
				assertThat(results.get(INT_KEY_4).value).isEqualTo(INT_VAL_4);
		org.assertj.core.api.Assertions.
				assertThat(results.get(BOOL_KEY_5).value).isEqualTo(BOOL_VAL_5);
		org.assertj.core.api.Assertions.
				assertThat(results.get(LONG_KEY_6).value).isEqualTo(LONG_VAL_6);
		org.assertj.core.api.Assertions.
				assertThat(results.get(FLOAT_KEY_7).value).isEqualTo(FLOAT_VAL_7);

		org.assertj.core.api.Assertions.
				assertThat(results.size()).isEqualTo(5);
	}

	@Test public void shouldRemove() {
		// GIVEN
		TSharedPreferencesUtil.setupPrefs(prefsStoreSUT.getPrefs(), PrefsUtil.stringEntry(STRING_KEY_1, STRING_VAL_1));

		// WHEN
		prefsResourceSUT.remove(STRING_KEY_1);

		// THEN
		assertThat(prefsStoreSUT.getPrefs()).doesNotHaveKey(STRING_KEY_1);
	}

	@Test public void shouldRemoveMultiple() {
		// GIVEN
		TSharedPreferencesUtil.setupPrefs(prefsStoreSUT.getPrefs(),
				PrefsUtil.stringEntry(STRING_KEY_1, STRING_VAL_1),
				PrefsUtil.stringEntry(STRING_KEY_2, STRING_VAL_2),
				PrefsUtil.stringEntry(STRING_KEY_3, STRING_VAL_3));

		// WHEN
		prefsResourceSUT.removeAll(STRING_KEY_1, STRING_KEY_2);

		// THEN
		assertThat(prefsStoreSUT.getPrefs()).doesNotHaveKey(STRING_KEY_1);
		assertThat(prefsStoreSUT.getPrefs()).doesNotHaveKey(STRING_KEY_2);
		assertThat(prefsStoreSUT.getPrefs()).contains(STRING_KEY_3, STRING_VAL_3);
	}
}
