package xst.pinmigrations.domain.android_context.shared_preferences;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;

import static com.google.common.base.Preconditions.checkNotNull;

public class TSharedPreferencesUtil {
	// NOTE: deliberately checking value instead of key-type class
	@SuppressLint("ApplySharedPref") // needs to block for testing
	public static void setupPrefs(@NonNull final SharedPreferences sharedPreferences, @NonNull final Entry... entries) {
		final SharedPreferences prefs = checkNotNull(sharedPreferences);

		final Editor editor = prefs.edit();
		for (final Entry entry: checkNotNull(entries)) {
			if (String.class.isInstance(entry.value)) {
				editor.putString(entry.keyType.key, (String) entry.value);
			} else if (Integer.class.isInstance(entry.value)) {
				editor.putInt(entry.keyType.key, (Integer) entry.value);
			} else if (Boolean.class.isInstance(entry.value)) {
				editor.putBoolean(entry.keyType.key, (Boolean) entry.value);
			} else if (Long.class.isInstance(entry.value)) {
				editor.putLong(entry.keyType.key, (Long) entry.value);
			} else if (Float.class.isInstance(entry.value)) {
				editor.putFloat(entry.keyType.key, (Float) entry.value);
			} else {
				throw new AssertionError("invalid ");
			}
		}

		editor.commit();
	}
}
