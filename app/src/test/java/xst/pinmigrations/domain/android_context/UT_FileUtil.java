package xst.pinmigrations.domain.android_context;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * util for unit tests
 * even if un-used, dont remove. this is first destination for any common logic factored out of unit tests
 */
public class UT_FileUtil {
	public static String getCurrentDir() {
		return System.getProperty("user.dir");
	}

	public static InputStream getFile(final String absolutePath) throws FileNotFoundException {
		return new BufferedInputStream( new FileInputStream( new File(absolutePath) ) );
	}
}
