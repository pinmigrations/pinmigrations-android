package xst.pinmigrations.domain.rx;

import org.junit.Before;
import org.junit.Test;

import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.Subscriber;
import rx.observers.TestSubscriber;
import rx.schedulers.TestScheduler;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;

public class RxJavaServiceTest extends BaseUnitTest {
	public static final String STRING_DATA = "DATA";
	TRxJavaService rxJavaServiceSUT;
	TestScheduler testScheduler;

	TestSubscriber<String> testSubscriber;

	public boolean callableExecuted = false;

	@Before public void setup() {
		callableExecuted = false;

		rxJavaServiceSUT = new TRxJavaService();
		testScheduler = new TestScheduler();
		testSubscriber = new TestSubscriber<>();
	}

	class ExecTrackObservable implements OnSubscribe<String> { // just sets callableExecuted to TRUE
		@Override public void call(final Subscriber<? super String> subscriber) {
			callableExecuted = true;
			subscriber.onNext(STRING_DATA);
			subscriber.onCompleted();
		}
	}

	@Test public void shouldNotExecDelayExecObservableEarly() throws Exception {
		// GIVEN
		rxJavaServiceSUT.ioScheduler = testScheduler;
		final Observable<String> execTrackObs = Observable.unsafeCreate(new ExecTrackObservable());

		// WHEN
		rxJavaServiceSUT.delayedExecutionObservable(execTrackObs, 3, SECONDS).subscribe(testSubscriber);
		testScheduler.advanceTimeBy(1, SECONDS);

		// THEN
		assertThat(callableExecuted).isFalse();
		testSubscriber.assertNoErrors();
		testSubscriber.assertNoValues();
		testSubscriber.assertNotCompleted();
	}

	@Test public void shouldCompleteDelayExecObservable() throws Exception {
		// GIVEN
		rxJavaServiceSUT.ioScheduler = testScheduler;
		final Observable<String> execTrackObs = Observable.unsafeCreate(new ExecTrackObservable());

		// WHEN
		rxJavaServiceSUT.delayedExecutionObservable(execTrackObs, 3, SECONDS).subscribe(testSubscriber);
		testScheduler.advanceTimeBy(4, SECONDS);

		// THEN
		assertThat(callableExecuted).isTrue();
		testSubscriber.assertNoErrors();
		testSubscriber.assertValue(STRING_DATA);
		testSubscriber.assertCompleted();
	}

	@Test public void shouldDelayTimerDelayObservable() throws Exception {
		// GIVEN
		rxJavaServiceSUT.ioScheduler = testScheduler;
		final Observable<String> execTrackObs = Observable.unsafeCreate(new ExecTrackObservable());

		// WHEN
		rxJavaServiceSUT.delayedReturnObservable(execTrackObs, 3, SECONDS).subscribe(testSubscriber);
		testScheduler.advanceTimeBy(1, SECONDS);

		// THEN
		assertThat(callableExecuted).isTrue(); // execution started but return value is delayed
		testSubscriber.assertNoErrors();
		testSubscriber.assertNoValues();
		testSubscriber.assertNotCompleted();
	}

	@Test public void shouldCompleteTimerDelayObservable() throws Exception {
		// GIVEN
		rxJavaServiceSUT.ioScheduler = testScheduler;
		final Observable<String> execTrackObs = Observable.unsafeCreate(new ExecTrackObservable());

		// WHEN
		rxJavaServiceSUT.delayedReturnObservable(execTrackObs, 3, SECONDS).subscribe(testSubscriber);
		testScheduler.advanceTimeBy(4, SECONDS);

		// THEN
		assertThat(callableExecuted).isTrue();
		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		testSubscriber.assertValue(STRING_DATA);
	}
}
