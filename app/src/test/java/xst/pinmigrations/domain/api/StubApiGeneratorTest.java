package xst.pinmigrations.domain.api;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import rx.Observable;
import rx.observers.TestSubscriber;
import xst.pinmigrations.app.App;
import xst.pinmigrations.app.logging.remote_log.RemoteLogApi;
import xst.pinmigrations.domain.login.LoginApi;
import xst.pinmigrations.domain.login.LoginApiStub;
import xst.pinmigrations.domain.login.LoginApiStub.CheckSigninStatusParams;
import xst.pinmigrations.domain.login.json.SignedInResponse;
import xst.pinmigrations.domain.login.json.TSignedInResponse;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.network.UnauthedNetworkService;
import xst.pinmigrations.domain.positions.PositionsApi;
import xst.pinmigrations.domain.rx.TRxJavaService;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static xst.pinmigrations.domain.network.TNetUtil.GOOGLE_URL;

/**
 * NOTE: just test that each required service is generated
 * NOTE: DO NOT test each method and params for stubs
 * one test for one stub method and params is enough, then ensure others follow pattern
 * failures will be evident in usage (implementing tests)
 *
 * NOTE: exception, if there some more complex stub, test if needed
 */
public class StubApiGeneratorTest extends BaseUnitTest {

	TRxJavaService rxJavaService;
	NetworkService networkService;
	StubApiGenerator stubApiGenerator;
	@Before public void setup() {
		networkService = new UnauthedNetworkService(GOOGLE_URL);
		rxJavaService = new TRxJavaService();
		stubApiGenerator = new StubApiGenerator(networkService, rxJavaService, Mockito.mock(App.class));
	}

	@Test public void shouldGenerateAndKeepLoginApiStub() {
		// WHEN
		LoginApi loginApi = stubApiGenerator.createService(LoginApi.class);
		ApiStub<LoginApi> loginApiStub = stubApiGenerator.getApiStub(LoginApi.class);

		// THEN
		assertThat(loginApi).isNotNull();
		assertThat(loginApiStub).isNotNull();
		assertThat(loginApi).isEqualTo(loginApiStub);
	}

	@Test public void shouldGenerateAndKeepPositionsApiStub() {
		// WHEN
		PositionsApi positionsApi = stubApiGenerator.createService(PositionsApi.class);
		ApiStub<PositionsApi> positionsApiStub = stubApiGenerator.getApiStub(PositionsApi.class);

		// THEN
		assertThat(positionsApi).isNotNull();
		assertThat(positionsApiStub).isNotNull();
		assertThat(positionsApi).isEqualTo(positionsApiStub);
	}

	@Test public void shouldGenerateAndKeepRemoteLogApiStub() {
		RemoteLogApi remoteLogApi = stubApiGenerator.createService(RemoteLogApi.class);

		ApiStub<RemoteLogApi> remoteLogApiStub = stubApiGenerator.getApiStub(RemoteLogApi.class);

		assertThat(remoteLogApi).isNotNull();
		assertThat(remoteLogApiStub).isNotNull();
		assertThat(remoteLogApi).isEqualTo(remoteLogApiStub);
	}

	@Test public void loginApiStubShouldReturnPresetValue() {
		// GIVEN
		stubApiGenerator.createService(LoginApi.class);
		final LoginApiStub loginApiStub = (LoginApiStub) stubApiGenerator.getApiStub(LoginApi.class);
		final String host = "HOST";
		final String origin = "ORIGIN";
		final String referer = "REFERER";
		TestSubscriber<SignedInResponse> testSubscriber = new TestSubscriber<>();

		// WHEN
		SignedInResponse signedInResponse = TSignedInResponse.newSignedInFalse();
		Observable<SignedInResponse> respObs = Observable.just( signedInResponse );
		loginApiStub.putCheckSigninStatus(respObs);
		Observable<SignedInResponse> stubResp = loginApiStub.checkSigninStatus(host, origin, referer);

		stubResp.subscribe(testSubscriber);
		testSubscriber.awaitTerminalEvent(STUB_RESP_MIN_WAIT_MS, MILLISECONDS);

		// THEN
		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		SignedInResponse stubSignedInResponse = testSubscriber.getOnNextEvents().get(0);
		assertThat(stubSignedInResponse).usingComparator(SIGNED_IN_RESPONSE_ASSERTS).isEqualTo(signedInResponse);

		final CheckSigninStatusParams params = loginApiStub.getCheckSigninStatusParams().get(0);
		assertThat(params.host).isEqualTo(host);
		assertThat(params.origin).isEqualTo(origin);
		assertThat(params.referer).isEqualTo(referer);
	}
}
