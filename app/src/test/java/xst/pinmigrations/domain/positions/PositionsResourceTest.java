package xst.pinmigrations.domain.positions;

import com.bluelinelabs.logansquare.LoganSquare;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import rx.observers.TestSubscriber;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.network.SevereBackendIssue;
import xst.pinmigrations.domain.network.TNetUtil;
import xst.pinmigrations.domain.network.UnauthedNetworkService;
import xst.pinmigrations.domain.positions.TPosition.Factory;
import xst.pinmigrations.domain.positions.json.PositionJson;
import xst.pinmigrations.domain.rx.TRxJavaService;
import xst.pinmigrations.domain.rx.TRxUtil;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static xst.pinmigrations.domain.positions.TPosition.EMPTY_POSITIONS_JSON;
import static xst.pinmigrations.domain.positions.TPosition.FEED_HASH;
import static xst.pinmigrations.domain.positions.TPosition.FEED_HASH_2;
import static xst.pinmigrations.domain.positions.TPosition.GUID_FOR_P3_AND_P4;
import static xst.pinmigrations.domain.positions.TPosition.P1_EXPECTED;
import static xst.pinmigrations.domain.positions.TPosition.P2_EXPECTED;
import static xst.pinmigrations.domain.positions.TPosition.P3_EXPECTED;
import static xst.pinmigrations.domain.positions.TPosition.P4_EXPECTED;
import static xst.pinmigrations.domain.positions.TPosition.PJ1_EXPECTED;
import static xst.pinmigrations.domain.positions.TPosition.PLIST_P3_AND_P4;
import static xst.pinmigrations.domain.positions.TPosition.POSITION_JSON_1;
import static xst.pinmigrations.domain.positions.TPosition.POSITION_JSON_2;

public class PositionsResourceTest extends BaseUnitTest {
	TPositionsResource positionsResourceSUT;
	MockWebServer server;

	// intermediary objects (mostly defaults from `setup` are used, few tests need custom though)
	UnauthedNetworkService unauthedNetworkService;
	ApiGenerator apiGenerator;
	final RemoteLogResource remoteLogResource = mock(RemoteLogResource.class); // dep not invoked in tests, just re-use
	final LoginManager dummyManager = TLoginManager.loggedInTrueDummyManager( mock(PrefsManager.class) );

	@Before public void setup() throws IOException {
		server = new MockWebServer();
		server.start();

		unauthedNetworkService = new UnauthedNetworkService(server.url("/"));
		apiGenerator = new ApiGenerator(unauthedNetworkService, TRxJavaService.newServiceAllImmediate());
		positionsResourceSUT = new TPositionsResource(apiGenerator, remoteLogResource, dummyManager);
	}

	@After public void tearDown() {
		try {
			server.shutdown();
		} catch (final IOException ignore) { /* tests may shutdown server prematurely */ }
	}

	@Test public void feedHashVerification() throws Exception {
		// GIVEN
		server.enqueue(new MockResponse().setBody(FEED_HASH));
		final PositionsApi api = apiGenerator.createService(PositionsApi.class);

		// WHEN
		final TestSubscriber<String> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber( api.feedHash() );

		// THEN
		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		testSubscriber.assertValueCount(1);
		testSubscriber.assertValue(FEED_HASH);
	}

	@Test public void positionsVerification() throws Exception {
		// GIVEN
		server.enqueue(new MockResponse().setBody(POSITION_JSON_1));
		final PositionsApi api = apiGenerator.createService(PositionsApi.class);

		// WHEN
		final TestSubscriber<List<PositionJson>> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber( api.allPositions() );

		// THEN
		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		testSubscriber.assertValueCount(1);

		PositionJson pj1 = testSubscriber.getOnNextEvents().get(0).get(0);
		assertThat(pj1).usingComparator(POSITION_JSON_ASSERTS).isEqualTo(PJ1_EXPECTED);
		// PJ1_EXPECTED
	}

	@Test public void shouldRequestAndParseResponse() throws Exception {
		/*
			GIVEN
		 */
		server.enqueue(new MockResponse().setBody(FEED_HASH));
		server.enqueue(new MockResponse().setBody(POSITION_JSON_1));


		/*
			WHEN
		 */
		final TestSubscriber<ArrayList<Position>> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber(positionsResourceSUT.getAllPositions());

		/*
			THEN
		 */
		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();
		// result
		final ArrayList<Position> positionsList = testSubscriber.getOnNextEvents().get(0);

		// assert data valid
		final Position position = positionsList.get(0);
		assertThat(positionsList).hasSize(1);
		assertThat(position).usingComparator(POSITION_ASSERTS).isEqualTo(P1_EXPECTED);

		// assert http requests
		assertThat(server.getRequestCount()).isEqualTo(2);

		final RecordedRequest feedHashReq = server.takeRequest();

		assertThat(feedHashReq.getPath()).isEqualTo("/feed-hash");
		assertThat(feedHashReq.getMethod().toUpperCase()).isEqualTo("GET");

		final RecordedRequest posListReq = server.takeRequest();
		assertThat(posListReq.getPath()).isEqualTo("/feed");
		assertThat(posListReq.getMethod().toUpperCase()).isEqualTo("GET");
	}

	@Test public void shouldReuseCachedPositionsIfFeedHashIsSame() throws Exception {
		/*
			GIVEN
		 */
		// setup server responses
		server.enqueue(new MockResponse().setBody(FEED_HASH));
		server.enqueue(new MockResponse().setBody(POSITION_JSON_1));
		server.enqueue(new MockResponse().setBody(FEED_HASH));
		// should not request a 4th time
		server.enqueue(new MockResponse().setBody(POSITION_JSON_2));


		/*
			WHEN
		 */
		final TestSubscriber<ArrayList<Position>> firstTestSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.getAllPositions().toBlocking());

		final TestSubscriber<ArrayList<Position>> secondTestSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.getAllPositions().toBlocking());

		/*
			THEN
		 */
		firstTestSubscriber.assertCompleted(); firstTestSubscriber.assertNoErrors();
		secondTestSubscriber.assertCompleted(); secondTestSubscriber.assertNoErrors();
		final ArrayList<Position> firstList = firstTestSubscriber.getOnNextEvents().get(0);
		final ArrayList<Position> secondList = secondTestSubscriber.getOnNextEvents().get(0);

		// assert http requests
		assertThat(server.getRequestCount()).isEqualTo(3);
		final RecordedRequest request1 = server.takeRequest();
		assertThat(request1.getPath()).isEqualTo("/feed-hash");
		assertThat(request1.getMethod().toUpperCase()).isEqualTo("GET");
		final RecordedRequest request2 = server.takeRequest();
		assertThat(request2.getPath()).isEqualTo("/feed");
		assertThat(request2.getMethod().toUpperCase()).isEqualTo("GET");
		final RecordedRequest request3 = server.takeRequest();
		assertThat(request3.getPath()).isEqualTo("/feed-hash");
		assertThat(request3.getMethod().toUpperCase()).isEqualTo("GET");


		// data assertions
		assertThat(firstList).hasSize(1);
		assertThat(firstList.get(0)).usingComparator(POSITION_ASSERTS).isEqualTo(P1_EXPECTED);
		assertThat(secondList).hasSize(1);
		assertThat(secondList.get(0)).usingComparator(POSITION_ASSERTS).isEqualTo(P1_EXPECTED);
	}

	@Test public void shouldRerequestPositionsIfFeedHashChanges() throws Exception {
		/*
			GIVEN
		 */
		// setup server responses
		server.enqueue(new MockResponse().setBody(FEED_HASH));
		server.enqueue(new MockResponse().setBody(POSITION_JSON_1));
		server.enqueue(new MockResponse().setBody(FEED_HASH_2));
		server.enqueue(new MockResponse().setBody(POSITION_JSON_2));


		/*
			WHEN
		 */
		final TestSubscriber<ArrayList<Position>> firstTestSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.getAllPositions().toBlocking());

		final TestSubscriber<ArrayList<Position>> secondTestSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.getAllPositions().toBlocking());

		/*
			THEN
		 */
		firstTestSubscriber.assertCompleted(); firstTestSubscriber.assertNoErrors();
		secondTestSubscriber.assertCompleted(); secondTestSubscriber.assertNoErrors();
		final ArrayList<Position> firstList = firstTestSubscriber.getOnNextEvents().get(0);
		final ArrayList<Position> secondList = secondTestSubscriber.getOnNextEvents().get(0);

		// assert http requests
		assertThat(server.getRequestCount()).isEqualTo(4);
		final RecordedRequest request1 = server.takeRequest();
		assertThat(request1.getPath()).isEqualTo("/feed-hash");
		assertThat(request1.getMethod().toUpperCase()).isEqualTo("GET");
		final RecordedRequest request2 = server.takeRequest();
		assertThat(request2.getPath()).isEqualTo("/feed");
		assertThat(request2.getMethod().toUpperCase()).isEqualTo("GET");
		final RecordedRequest request3 = server.takeRequest();
		assertThat(request3.getPath()).isEqualTo("/feed-hash");
		assertThat(request3.getMethod().toUpperCase()).isEqualTo("GET");
		final RecordedRequest request4 = server.takeRequest();
		assertThat(request4.getPath()).isEqualTo("/feed");
		assertThat(request4.getMethod().toUpperCase()).isEqualTo("GET");

		// data assertions
		assertThat(firstList).hasSize(1);
		assertThat( firstList.get(0) ).usingComparator(POSITION_ASSERTS).isEqualTo(P1_EXPECTED);
		assertThat(secondList).hasSize(1);
		assertThat( secondList.get(0) ).usingComparator(POSITION_ASSERTS).isEqualTo(P2_EXPECTED);
	}

	@Test public void shouldPropagateConnectExceptionIfNoConnection() throws Exception {
		// GIVEN
		server.shutdown(); // causes ConnectException at OkHttpClient exec


		// WHEN
		final TestSubscriber<ArrayList<Position>> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber(positionsResourceSUT.getAllPositions());

		// THEN
		testSubscriber.assertError(ConnectException.class);
	}

	// this may happen when connecting to Heroku for the first time
	// need to detect this and either retry or instruct user to do so
	@Test public void shouldPropagateSocketTimeoutExceptionIfReadTimesout() throws Exception {
		/*
			GIVEN
		 */
		server.enqueue(new MockResponse()  // delayed response, takes at least 10ms to respond
				.setBody(FEED_HASH)
				.setBodyDelay(10, MILLISECONDS));
		// setup short-connect HttpClient in NetworkService -> ApiGenerator -> PositionsApi -> PositionsResource
		unauthedNetworkService = new UnauthedNetworkService(server.url("/"));
		UnauthedNetworkService.setupShortConnectClient(unauthedNetworkService); // times-out after 1ms
		apiGenerator = new ApiGenerator(unauthedNetworkService, new TRxJavaService());
		positionsResourceSUT = new TPositionsResource(apiGenerator, remoteLogResource, dummyManager);

		// WHEN
		final TestSubscriber<ArrayList<Position>> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber(positionsResourceSUT.getAllPositions());

		// THEN
		testSubscriber.assertError(SocketTimeoutException.class);
	}

	// this may happen when connecting to Heroku for the first time
	// need to detect this and either retry or instruct user to do so
	@Test public void shouldPropagateSevereBackendExceptionIfNoFeedHash() throws Exception {
		// GIVEN
		server.enqueue(new MockResponse().setBody("")); // empty-string feed hash


		// WHEN
		final TestSubscriber<ArrayList<Position>> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber(positionsResourceSUT.getAllPositions());

		// THEN
		testSubscriber.assertError(SevereBackendIssue.class);
		final SevereBackendIssue err1 = (SevereBackendIssue) testSubscriber.getOnErrorEvents().get(0);
		assertThat(err1.type).isEqualTo(SevereBackendIssue.EMPTY_FEED_HASH);
	}

	// this happens when devs accidentally clobber the database
	// need to detect this and report
	@Test public void shouldPropagateSevereBackendExceptionIfNoPositionData() throws Exception {
		// GIVEN
		server.enqueue(new MockResponse().setBody(FEED_HASH));
		server.enqueue(new MockResponse().setBody(EMPTY_POSITIONS_JSON));


		// WHEN
		final TestSubscriber<ArrayList<Position>> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber(positionsResourceSUT.getAllPositions());


		// THEN
		testSubscriber.assertError(SevereBackendIssue.class);
		final SevereBackendIssue err1 = (SevereBackendIssue) testSubscriber.getOnErrorEvents().get(0);
		assertThat(err1.type).isEqualTo(SevereBackendIssue.NO_POSITION_DATA);
	}

	@Test public void shouldReturnExistingPositionsForSoftGetAll() throws Exception {
		// GIVEN
		final ArrayList<Position> positions = Factory.listRealistic(2);
		PositionsUtil.sortPositionsByGuid(positions);

		positionsResourceSUT.setPositions(positions);

		// WHEN
		final TestSubscriber<ArrayList<Position>> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber(positionsResourceSUT.softGetAllPositions());

		// THEN
		testSubscriber.awaitTerminalEvent(100, MILLISECONDS);
		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();

		assertThat( server.getRequestCount() ).isEqualTo(0);

		final ArrayList<Position> retrievedPositions = testSubscriber.getOnNextEvents().get(0);
		PositionsUtil.sortPositionsByGuid(retrievedPositions);

		assertThat(retrievedPositions.get(0)).usingComparator(POSITION_ASSERTS).isEqualTo(positions.get(0));
		assertThat(retrievedPositions.get(1)).usingComparator(POSITION_ASSERTS).isEqualTo(positions.get(1));
	}

	@Test public void shouldExecuteRemoteQueryForSoftGetAllIfCachedListEmpty() throws Exception {
		// GIVEN
		server.enqueue(new MockResponse().setBody(FEED_HASH));
		server.enqueue(new MockResponse().setBody(POSITION_JSON_1));

		// WHEN
		final TestSubscriber<ArrayList<Position>> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.softGetAllPositions().toBlocking());

		// THEN
		testSubscriber.awaitTerminalEvent(300, MILLISECONDS);

		testSubscriber.assertCompleted();
		testSubscriber.assertNoErrors();

		// result
		final ArrayList<Position> positionsList = testSubscriber.getOnNextEvents().get(0);

		// assert data valid
		final Position position = positionsList.get(0);
		assertThat(positionsList).hasSize(1);
		assertThat(position).usingComparator(POSITION_ASSERTS).isEqualTo(P1_EXPECTED);

		// assert http requests
		assertThat( server.getRequestCount() ).isEqualTo(2);

		final RecordedRequest feedHashReq = server.takeRequest();
		assertThat(feedHashReq.getPath()).isEqualTo("/feed-hash");
		assertThat(feedHashReq.getMethod().toUpperCase()).isEqualTo("GET");
		final RecordedRequest posListReq = server.takeRequest();
		assertThat(posListReq.getPath()).isEqualTo("/feed");
		assertThat(posListReq.getMethod().toUpperCase()).isEqualTo("GET");
	}

	@Test public void shouldFindPositionsByGuid() throws Exception {
		// GIVEN
		server.enqueue(new MockResponse().setBody(PLIST_P3_AND_P4));
		final String expectedUrl = "/locations/find/" + GUID_FOR_P3_AND_P4 + ".json";

		// WHEN
		final TestSubscriber<List<Position>> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.findPositionsForGuid(GUID_FOR_P3_AND_P4).toBlocking());

		// result
		final List<Position> positionsList = testSubscriber.getOnNextEvents().get(0);
		assertThat(positionsList).hasSize(2);

		// assert data valid
		final Position p3 = positionsList.get(0);
		final Position p4 = positionsList.get(1);

		assertThat(p3).usingComparator(POSITION_ASSERTS).isEqualTo(P3_EXPECTED);
		assertThat(p4).usingComparator(POSITION_ASSERTS).isEqualTo(P4_EXPECTED);

		// assert http request
		assertThat( server.getRequestCount() ).isEqualTo(1);
		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo(expectedUrl);
		assertThat(req.getMethod().toUpperCase()).isEqualTo("GET");

		// note: login not required
	}

	@Test public void shouldCreatePosition() throws Exception {
		// GIVEN
		final Position position = TPosition.Factory.realistic();
		server.enqueue(new MockResponse()
				.setBody(LoganSquare.serialize(new PositionJson(position)))
				.setResponseCode(201));

		// WHEN
		final TestSubscriber<Position> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.create(position).toBlocking());
		testSubscriber.awaitTerminalEvent(350, MILLISECONDS);


		// THEN
		testSubscriber.assertNoErrors(); testSubscriber.assertCompleted();

		// assert http request
		assertThat( server.getRequestCount() ).isEqualTo(1);

		final RecordedRequest req = server.takeRequest();
		assertThat(req.getMethod().toUpperCase()).isEqualTo("POST");
		assertThat(req.getPath()).isEqualTo("/locations.json");
		assertThat(req.getHeader("Content-Type")).startsWith("application/x-www-form-urlencoded"); // UTF-8 OK

		final Map<String, String> formParams = TNetUtil.decodeFormBody(req.getBody().readUtf8());
		assertThat(formParams.get("location[lat]")).isEqualTo( "" + position.lat );
		assertThat(formParams.get("location[lng]")).isEqualTo( "" + position.lng );
		assertThat(formParams.get("location[ord]")).isEqualTo( "" + position.ord );
		assertThat(formParams.get("location[description]")).isEqualTo(position.description);
		assertThat(formParams.get("location[guid]")).isEqualTo(position.guid);

		final Position positionCreated = testSubscriber.getOnNextEvents().get(0);
		assertThat(positionCreated).usingComparator(UNSAVED_POSITION_ASSERTS).isEqualTo(position);

		// note: login (remember-token/csrf) is required
	}

	@Test public void shouldUpdatePosition() throws Exception {
		// GIVEN
		final Position position = TPosition.Factory.realistic();
		server.enqueue(new MockResponse()
				.setBody(LoganSquare.serialize(new PositionJson(position)))
				.setResponseCode(201));

		// WHEN
		final TestSubscriber<Position> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.update(position).toBlocking());
		testSubscriber.awaitTerminalEvent(350, MILLISECONDS);


		// THEN
		testSubscriber.assertNoErrors(); testSubscriber.assertCompleted();
		final RecordedRequest req = server.takeRequest();

		// assert http request
		assertThat( server.getRequestCount() ).isEqualTo(1);

		assertThat(req.getMethod().toUpperCase()).isEqualTo("PUT");
		assertThat(req.getPath()).isEqualTo("/locations/" + position.id + ".json");
		assertThat(req.getHeader("Content-Type")).startsWith("application/x-www-form-urlencoded"); // UTF-8 OK

		final Map<String, String> formParams = TNetUtil.decodeFormBody(req.getBody().readUtf8());
		assertThat(formParams.get("location[id]")).isEqualTo( "" + position.id );
		assertThat(formParams.get("location[guid]")).isEqualTo( position.guid );
		assertThat(formParams.get("location[lat]")).isEqualTo( "" + position.lat );
		assertThat(formParams.get("location[lng]")).isEqualTo( "" + position.lng );
		assertThat(formParams.get("location[ord]")).isEqualTo( "" + position.ord );
		assertThat(formParams.get("location[description]")).isEqualTo(position.description);

		final Position positionUpdated = testSubscriber.getOnNextEvents().get(0);
		assertThat(positionUpdated).usingComparator(POSITION_ASSERTS).isEqualTo(position);

		// note: login (remember-token/csrf) is required
	}

	@Test public void shouldDeletePosition() throws Exception {
		// GIVEN
		final Position position = TPosition.Factory.realistic();
		server.enqueue(new MockResponse().setResponseCode(204));

		// WHEN
		final TestSubscriber<Void> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.delete(position.id).toBlocking());
		testSubscriber.awaitTerminalEvent(350, MILLISECONDS);


		// THEN
		testSubscriber.assertNoErrors(); testSubscriber.assertCompleted();
		final RecordedRequest req = server.takeRequest();

		// assert http request
		assertThat( server.getRequestCount() ).isEqualTo(1);

		assertThat(req.getMethod().toUpperCase()).isEqualTo("DELETE");
		assertThat(req.getPath()).isEqualTo("/locations/" + position.id + ".json");

		// note: login (remember-token/csrf) is required
	}
}
