package xst.pinmigrations.domain.positions;

import com.bluelinelabs.logansquare.LoganSquare;

import org.junit.Test;

import xst.pinmigrations.domain.positions.json.PositionJson;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static xst.pinmigrations.app.util.TStringUtil.MAX_DELTA_DOUBLE_COMPARISONS;

public class PositionJsonTest extends BaseUnitTest {

	public static final String EXPECTED_SERIALIZATION = "{\"description\":\"Julio arana\",\"guid\":\"\",\"id\":58,\"lat\":\"20.61222\",\"lng\":\"-103.3374\",\"ord\":0}";

	@Test
	public void shouldSerializeDomainPositionToJsonString() throws Exception {
		// GIVEN
		// domain object
		final Position p = new Position(58); // id=58
		p.lat = 20.61222;
		p.lng = -103.3374;
		p.description = "Julio arana";
		p.guid = "";
		p.ord = 0;

		// WHEN
		final String outputString = LoganSquare.serialize(new PositionJson(p));

		// THEN
		assertThat(outputString).isEqualTo(EXPECTED_SERIALIZATION);
	}

	@Test
	public void shouldParseJsonFromStringToDomain() throws Exception {
		// GIVEN ...


		// WHEN
		final Position position = new Position( LoganSquare.parse(EXPECTED_SERIALIZATION, PositionJson.class) );


		// THEN
		assertThat(position.id).isEqualTo(58);
		assertThat(position.lat).isCloseTo(20.61222, within(MAX_DELTA_DOUBLE_COMPARISONS));
		assertThat(position.lng).isCloseTo(-103.3374, within(MAX_DELTA_DOUBLE_COMPARISONS));
		assertThat(position.description).isEqualTo("Julio arana");
		assertThat(position.guid).isEqualTo("");
		assertThat(position.ord).isEqualTo(0);
	}
}
