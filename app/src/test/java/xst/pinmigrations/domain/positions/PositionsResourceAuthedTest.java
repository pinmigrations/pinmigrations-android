package xst.pinmigrations.domain.positions;

import com.bluelinelabs.logansquare.LoganSquare;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import rx.observers.TestSubscriber;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.app.logging.remote_log.TRemoteLogResource;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.login.Login;
import xst.pinmigrations.domain.login.TLogin;
import xst.pinmigrations.domain.login.TLoginManager;
import xst.pinmigrations.domain.login.TLoginResource;
import xst.pinmigrations.domain.login.UT_LoginUtil;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.network.TNetUtil;
import xst.pinmigrations.domain.positions.json.PositionJson;
import xst.pinmigrations.domain.rx.TRxJavaService;
import xst.pinmigrations.domain.rx.TRxUtil;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static xst.pinmigrations.domain.login.TLogin.CSRF_TOKEN_1;
import static xst.pinmigrations.domain.login.TLogin.REMEMBER_TOKEN_1;
import static xst.pinmigrations.domain.login.TLogin.SESSION_1;

// TODO: setup similarly to LoginSystem tests
public class PositionsResourceAuthedTest extends BaseUnitTest {
	TPositionsResource positionsResourceSUT;
	final RemoteLogResource remoteLogResource = TRemoteLogResource.getMock();
	TLoginResource loginResource;
	TLoginManager loginManager;
	ApiGenerator apiGenerator;
	MockWebServer server;
	NetworkService networkService;
	PrefsManager prefsManagerMock;
	HttpUrl serverBaseUrl;

	@Before public void setup() throws IOException {
		server = new MockWebServer();
		server.start();
		serverBaseUrl = server.url("/");

		prefsManagerMock = Mockito.mock(PrefsManager.class);
		loginManager = new TLoginManager(new TLogin(), prefsManagerMock);
		networkService = new NetworkService(serverBaseUrl, loginManager);

		apiGenerator = new ApiGenerator(networkService, TRxJavaService.newServiceAllImmediate());
		loginResource = new TLoginResource(apiGenerator, loginManager, networkService, remoteLogResource);
		positionsResourceSUT = new TPositionsResource(apiGenerator, remoteLogResource, loginManager);
	}

	@After public void tearDown() {
		try {
			server.shutdown();
		} catch (final IOException ignore) { /* tests may shutdown server prematurely */ }
	}

	public void setupAsLoggedIn() {
		UT_LoginUtil.setLoggedInCookies(networkService.getCookieJar(), SESSION_1, REMEMBER_TOKEN_1);
		loginManager.setLoginObj(TLogin.builder()
				.rememberUserToken(REMEMBER_TOKEN_1)
				.appSessionCookie(SESSION_1)
				.authenticityToken(CSRF_TOKEN_1)
				.loginState(Login.LOGGED_IN)
				.build());
	}

	@Test public void shouldAuthCreatePosition() throws Exception {
		// GIVEN
		setupAsLoggedIn();
		final Position position = TPosition.Factory.realistic();
		server.enqueue(new MockResponse()
				.setBody(LoganSquare.serialize(new PositionJson(position)))
				.setResponseCode(201));

		// WHEN
		final TestSubscriber<Position> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.create(position).toBlocking());
		testSubscriber.awaitTerminalEvent(350, MILLISECONDS);


		// THEN
		testSubscriber.assertNoErrors(); testSubscriber.assertCompleted();

		assertThat( server.getRequestCount() ).isEqualTo(1);

		final RecordedRequest req = server.takeRequest();
		assertThat(req.getMethod().toUpperCase()).isEqualTo("POST");
		assertThat(req.getPath()).isEqualTo("/locations.json");
		assertThat(req.getHeader("Content-Type")).startsWith("application/x-www-form-urlencoded"); // UTF-8 OK
		assertThat(req.getHeader("X-Requested-With")).isEqualTo("XMLHttpRequest");
		assertThat(req.getHeader("APP_PLATFORM")).isEqualTo(NetworkService.platformHdrVal);
		assertThat(req.getHeader("X-CSRF-Token")).isEqualTo(CSRF_TOKEN_1);

		final Map<String, String> reqCookies = TNetUtil.requestCookies(req.getHeaders());
		assertThat(reqCookies.get("_PinMigrations_session")).isEqualTo(SESSION_1);
		assertThat(reqCookies.get("remember_user_token")).isEqualTo(REMEMBER_TOKEN_1);
	}

	@Test public void shouldAuthUpdatePosition() throws Exception {
		// GIVEN
		setupAsLoggedIn();
		final Position position = TPosition.Factory.realistic();
		server.enqueue(new MockResponse()
				.setBody(LoganSquare.serialize(new PositionJson(position)))
				.setResponseCode(201));

		// WHEN
		final TestSubscriber<Position> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.update(position).toBlocking());
		testSubscriber.awaitTerminalEvent(350, MILLISECONDS);


		// THEN
		testSubscriber.assertNoErrors(); testSubscriber.assertCompleted();

		assertThat( server.getRequestCount() ).isEqualTo(1);

		final RecordedRequest req = server.takeRequest();
		assertThat(req.getMethod().toUpperCase()).isEqualTo("PUT");
		assertThat(req.getPath()).isEqualTo("/locations/" + position.id + ".json");
		assertThat(req.getHeader("Content-Type")).startsWith("application/x-www-form-urlencoded"); // UTF-8 OK
		assertThat(req.getHeader("X-Requested-With")).isEqualTo("XMLHttpRequest");
		assertThat(req.getHeader("APP_PLATFORM")).isEqualTo(NetworkService.platformHdrVal);
		assertThat(req.getHeader("X-CSRF-Token")).isEqualTo(CSRF_TOKEN_1);

		final Map<String, String> reqCookies = TNetUtil.requestCookies(req.getHeaders());
		assertThat(reqCookies.get("_PinMigrations_session")).isEqualTo(SESSION_1);
		assertThat(reqCookies.get("remember_user_token")).isEqualTo(REMEMBER_TOKEN_1);
	}

	@Test public void shouldAuthDeletePosition() throws Exception {
		// GIVEN
		setupAsLoggedIn();
		final Position position = TPosition.Factory.realistic();
		server.enqueue(new MockResponse().setResponseCode(204));

		// WHEN
		final TestSubscriber<Void> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriberFromBlocking(positionsResourceSUT.delete(position.id).toBlocking());
		testSubscriber.awaitTerminalEvent(350, MILLISECONDS);


		// THEN
		testSubscriber.assertNoErrors(); testSubscriber.assertCompleted();

		assertThat( server.getRequestCount() ).isEqualTo(1);

		final RecordedRequest req = server.takeRequest();
		assertThat(req.getMethod().toUpperCase()).isEqualTo("DELETE");
		assertThat(req.getPath()).isEqualTo("/locations/" + position.id + ".json");
		assertThat(req.getHeader("X-Requested-With")).isEqualTo("XMLHttpRequest");
		assertThat(req.getHeader("APP_PLATFORM")).isEqualTo(NetworkService.platformHdrVal);
		assertThat(req.getHeader("X-CSRF-Token")).isEqualTo(CSRF_TOKEN_1);

		final Map<String, String> reqCookies = TNetUtil.requestCookies(req.getHeaders());
		assertThat(reqCookies.get("_PinMigrations_session")).isEqualTo(SESSION_1);
		assertThat(reqCookies.get("remember_user_token")).isEqualTo(REMEMBER_TOKEN_1);
	}
}
