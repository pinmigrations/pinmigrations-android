package xst.pinmigrations.domain.positions;

import com.bluelinelabs.logansquare.LoganSquare;

import org.junit.Test;

import xst.pinmigrations.domain.positions.json.PositionWrapper;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;

public class PositionSendTest extends BaseUnitTest {

	final String EXPECTED_SERIALIZATION = "{\"location\":{\"description\":\"Julio arana\",\"guid\":\"\",\"id\":58,\"lat\":\"20.61222\",\"lng\":\"-103.3374\",\"ord\":0}}";
	@Test
	public void shouldSerializePositionForSend() throws Exception {
		// GIVEN
		// domain object
		final Position p = new Position(58); // id=58
		p.lat = 20.61222;
		p.lng = -103.3374;
		p.description = "Julio arana";
		p.guid = "";
		p.ord = 0;
		// model object
		PositionWrapper pSend = new PositionWrapper(p);

		// WHEN
		final String outputString = LoganSquare.serialize(pSend);

		// THEN
		assertThat(outputString).isEqualTo(EXPECTED_SERIALIZATION);
	}
}
