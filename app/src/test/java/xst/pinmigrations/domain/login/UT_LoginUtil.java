package xst.pinmigrations.domain.login;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import xst.pinmigrations.domain.network.AppCookieJar;
import xst.pinmigrations.domain.network.TServerCookies;
import xst.pinmigrations.domain.network.UT_NetUtil;

import static xst.pinmigrations.domain.login.LoginUtil.AUTHENTICITY_TOKEN_HEADER_RECEIVE;
import static xst.pinmigrations.domain.login.LoginUtil.REMEMBER_ME_COOKIE;
import static xst.pinmigrations.domain.login.LoginUtil.SERVICE_SESSION_COOKIE;
import static xst.pinmigrations.domain.login.TLogin.NOT_SIGNED_IN_JSON;
import static xst.pinmigrations.domain.login.TLogin.SIGNED_IN_JSON;

public class UT_LoginUtil {
	public static MockResponse getInitialLoginCheckFalseResponse(final String respCsrf, final String respSession, final HttpUrl domain) {
		final MockResponse resp = new MockResponse();
		resp.setBody(NOT_SIGNED_IN_JSON);
		resp.addHeader(AUTHENTICITY_TOKEN_HEADER_RECEIVE, respCsrf);

		final TServerCookies respCookies = new TServerCookies(domain);
		respCookies.setResponseSessionCookie(SERVICE_SESSION_COOKIE, respSession);
		UT_NetUtil.addCookiesToMockResponse(respCookies, resp);

		return resp;
	}

	public static MockResponse getInitialLoginCheckTrueResponse(final String respCsrf, final String respSession, final HttpUrl domain) {
		final MockResponse resp = new MockResponse();
		resp.setBody(SIGNED_IN_JSON);
		resp.addHeader(AUTHENTICITY_TOKEN_HEADER_RECEIVE, respCsrf);

		final TServerCookies respCookies = new TServerCookies(domain);
		respCookies.setResponseSessionCookie(SERVICE_SESSION_COOKIE, respSession);
		UT_NetUtil.addCookiesToMockResponse(respCookies, resp);

		return resp;
	}

	public static void setLoggedInCookies(AppCookieJar cookieJar, String session, String rememberToken) {
		cookieJar.setCookie(SERVICE_SESSION_COOKIE, session);
		cookieJar.setCookie(REMEMBER_ME_COOKIE, rememberToken);
	}

	/** Rails/Devise redirect to `/new` upon login success
	 */
	public static MockResponse getLoginSuccessResponse(final String respCsrf, final String respSession, final String respRembToken, final HttpUrl domain) {
		final MockResponse resp = new MockResponse();

		resp.addHeader(AUTHENTICITY_TOKEN_HEADER_RECEIVE, respCsrf);
		resp.setResponseCode( 302 );
		resp.setHeader("Location", domain.resolve("/new"));

		final TServerCookies respCookies = new TServerCookies(domain);
		respCookies.setResponseSessionCookie(SERVICE_SESSION_COOKIE, respSession);
		respCookies.setResponsePersistentCookie(REMEMBER_ME_COOKIE, respRembToken);

		UT_NetUtil.addCookiesToMockResponse(respCookies, resp);
		return resp;
	}

	public static MockResponse getLoginFailResponse(final String respCsrf, final String respSession, final HttpUrl domain) {
		final MockResponse resp = new MockResponse();

		resp.addHeader(AUTHENTICITY_TOKEN_HEADER_RECEIVE, respCsrf);
		resp.setResponseCode( 200 );

		final TServerCookies respCookies = new TServerCookies(domain);
		respCookies.setResponseSessionCookie(SERVICE_SESSION_COOKIE, respSession);
		// NOTE: the missing remember-token for fails

		UT_NetUtil.addCookiesToMockResponse(respCookies, resp);
		return resp;
	}

	/** Rails/Devise redirect to `/new` upon login success
	 *  NOTE: session is reset on each request
	 */
	public static MockResponse getLoginSuccessRedirectResponse(final String respCsrf, final String respSession, final HttpUrl domain) {
		final MockResponse resp = new MockResponse();

		resp.addHeader(AUTHENTICITY_TOKEN_HEADER_RECEIVE, respCsrf);

		final TServerCookies respCookies = new TServerCookies(domain);
		respCookies.setResponseSessionCookie(SERVICE_SESSION_COOKIE, respSession);

		UT_NetUtil.addCookiesToMockResponse(respCookies, resp);
		return resp;
	}

	public static MockResponse getLogoutResponse(final String respCsrf, final String respSession, final HttpUrl domain) {
		final MockResponse resp = new MockResponse();
		resp.setBody(SIGNED_IN_JSON);
		resp.addHeader(AUTHENTICITY_TOKEN_HEADER_RECEIVE, respCsrf);

		final TServerCookies respCookies = new TServerCookies(domain);
		respCookies.setResponseClearCookie(REMEMBER_ME_COOKIE);
		respCookies.setResponseSessionCookie(SERVICE_SESSION_COOKIE, respSession);
		UT_NetUtil.addCookiesToMockResponse(respCookies, resp);

		return resp;
	}
}
