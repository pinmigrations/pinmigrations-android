package xst.pinmigrations.domain.login;


import android.support.annotation.NonNull;

import rx.Observable;
import xst.pinmigrations.domain.api.MockApiGenerator;
import xst.pinmigrations.domain.login.json.TSignedInResponse;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;

public final class RF_LoginUtil {

	/** sets LOGGED_IN in LoginManager, stubs LoginApiMock to return TSignedInResponse w true */
	public static void setupMocksForCheckSignInTrue(@NonNull final MockApiGenerator mockApiGenerator,
									 @NonNull final TLoginManager loginManager) {
		// creates or retrieves it
		final LoginApi loginApiMock = mockApiGenerator.createService(LoginApi.class);
		loginManager.setLoginObj( TLogin.newTestAuthed() );

		doReturn( Observable.just( TSignedInResponse.newSignedInTrue() ) )
				.when(loginApiMock).checkSigninStatus(anyString(), anyString(), anyString());
	}


	public static void setupMocksForSoftCheckSignInTrue(@NonNull final TLoginManager loginManager,
													@NonNull final LoginResource loginResource) {
		loginResource.initialized = true;
		loginManager.setLoginObj( TLogin.newTestAuthed() );
	}

	/** sets LOGGED_IN in LoginManager, stubs LoginApiMock to return TSignedInResponse w true */
	public static void setupMocksForCheckSignInFalse(@NonNull final MockApiGenerator mockApiGenerator,
													@NonNull final TLoginManager loginManager) {
		// creates or retrieves it
		final LoginApi loginApiMock = mockApiGenerator.createService(LoginApi.class);
		loginManager.setLoginObj( TLogin.newPristineUnauthed() );

		doReturn( Observable.just( TSignedInResponse.newSignedInFalse() ) )
				.when(loginApiMock).checkSigninStatus(anyString(), anyString(), anyString());
	}

	public static void setupMocksForSoftCheckSignInFalse(@NonNull final TLoginManager loginManager,
														 @NonNull final LoginResource loginResource) {
		loginResource.initialized = true;
		loginManager.setLoginObj( TLogin.newPristineUnauthed() );
	}

	/** LoginManager, stubs LoginApiMock to return TSignedInResponse w true */
	public static void setupMocksForLogout(@NonNull final MockApiGenerator mockApiGenerator,
												 @NonNull final TLoginManager loginManager,
												 @NonNull final String email, @NonNull final String password) {
		// creates or retrieves it
		final LoginApi loginApiMock = mockApiGenerator.createService(LoginApi.class);

		doReturn( TLogin.LoginInterceptObservable.create(loginManager, TLogin.newLogoutUnauthed(email, password) ))
				.when(loginApiMock).logout();
	}

	/** LoginManager, stubs LoginApiMock to return TSignedInResponse w true */
	public static void setupMocksForLoginSuccess(@NonNull final MockApiGenerator mockApiGenerator,
												@NonNull final TLoginManager loginManager,
												@NonNull final String email, @NonNull final String password) {
		// creates or retrieves it
		final LoginApi loginApiMock = mockApiGenerator.createService(LoginApi.class);

		doReturn( TLogin.LoginInterceptObservable.create(loginManager, TLogin.newAuthed(email, password) ))
				.when(loginApiMock).login(email, password);
	}
}
