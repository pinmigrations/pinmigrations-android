package xst.pinmigrations.domain.login;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.ConnectException;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import rx.Observable;
import rx.observers.TestSubscriber;
import xst.pinmigrations.app.logging.remote_log.RemoteLogResource;
import xst.pinmigrations.app.logging.remote_log.TRemoteLogResource;
import xst.pinmigrations.app.util.TCommonUtil;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.api.ApiGenerator;
import xst.pinmigrations.domain.network.AppCookieJar;
import xst.pinmigrations.domain.network.NetworkService;
import xst.pinmigrations.domain.network.TNetUtil;
import xst.pinmigrations.domain.rx.TRxJavaService;
import xst.pinmigrations.domain.rx.TRxUtil;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static xst.pinmigrations.domain.login.TLogin.CSRF_TOKEN_1;
import static xst.pinmigrations.domain.login.TLogin.CSRF_TOKEN_2;
import static xst.pinmigrations.domain.login.TLogin.CSRF_TOKEN_3;
import static xst.pinmigrations.domain.login.TLogin.REMEMBER_TOKEN_1;
import static xst.pinmigrations.domain.login.TLogin.SESSION_1;
import static xst.pinmigrations.domain.login.TLogin.SESSION_2;
import static xst.pinmigrations.domain.login.TLogin.SESSION_3;
import static xst.pinmigrations.domain.login.TLogin.TEST_EMAIL;
import static xst.pinmigrations.domain.login.TLogin.TEST_PASSWORD;

/**
 * tests LoginResource, LoginApi, LoginInterceptor, LoginUtil, LoginManager
 *
 * NOTES:
 * 	+ Rails/Devise redirect to `/new` upon login success (302)
 * 	+ new authenticity-token (aka csrf-token) is sent for every request and responses must include it
 * 		- this is expected behavior
 * 	+ all public endpoints seem to create new session (except /feed, and css/js etc. assets)
 * 		- this is a configuration/implementation error, though ill-effects are not obvious yet
 */
public class LoginSystemTest extends BaseUnitTest {
	LoginResource loginResource;
	TLoginManager loginManager;
	MockWebServer server;
	NetworkService networkService;
	PrefsManager prefsManagerMock;
	HttpUrl serverBaseUrl;
	RemoteLogResource remoteLogResource = TRemoteLogResource.getMock();

	String serverDomain;
	String serverOrigin;
	String serverReferer;

	TestSubscriber<Login> testSubscriber;

	Login expectedLogin;

	@Before public void setup() throws IOException {
		server = new MockWebServer();
		server.start();
		serverBaseUrl = server.url("/");

		prefsManagerMock = Mockito.mock(PrefsManager.class);
		loginManager = new TLoginManager(new TLogin(), prefsManagerMock);
		networkService = new NetworkService(serverBaseUrl, loginManager);

		serverDomain = networkService.getApiDomain();
		serverOrigin = networkService.getApiOrigin();
		serverReferer = networkService.getApiReferer();

		loginResource = new TLoginResource(new ApiGenerator(networkService, TRxJavaService.newServiceAllImmediate()), loginManager, networkService, remoteLogResource);
		testSubscriber = new TestSubscriber<>();
	}

	@After
	public void tearDown() {
		try {
			server.shutdown();
		} catch (final IOException ignore) { /* tests may shutdown server prematurely */ }
	}


	/** start logged in, check-sign-in should confirm still logged in
	 */
	@Test public void shouldCheckSigninStatusAndConfirmLoggedIn() throws Exception {
		// GIVEN
		UT_LoginUtil.setLoggedInCookies(networkService.getCookieJar(), SESSION_1, REMEMBER_TOKEN_1);
		loginManager.setLoginObj(TLogin.builder()
				.rememberUserToken(REMEMBER_TOKEN_1) // note: not re-sent in response
				.appSessionCookie(SESSION_1) // session-1 initially
				.build());
		server.enqueue(UT_LoginUtil.getInitialLoginCheckTrueResponse(CSRF_TOKEN_1, SESSION_2, serverBaseUrl));

		expectedLogin = TLogin.builder()
				.loginState(LOGGED_IN)
				.appSessionCookie(SESSION_2) // session-2 from mock-server response
				.authenticityToken(CSRF_TOKEN_1) // not present initially, but set by server-response
				.rememberUserToken(REMEMBER_TOKEN_1)
				.build();


		// WHEN
		loginResource.checkLoginStatus().subscribe(testSubscriber);

		// THEN
		testSubscriber.assertCompleted(); testSubscriber.assertNoErrors();
		// request assertions
		assertThat(server.getRequestCount()).isEqualTo(1);
		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo("/check_signin_status");
		assertThat(req.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(req.getHeader("Host")).isEqualTo(serverDomain); // no protocol
		assertThat(req.getHeader("Origin")).isEqualTo(serverOrigin); // NOTE: not needed for check-sign-in
		assertThat(req.getHeader("Referer")).startsWith(serverReferer); // protocol and trailing slash
		assertThat(req.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");

		final Map<String, String> reqCookies = TNetUtil.requestCookies(req.getHeaders());
		assertThat(reqCookies.get("_PinMigrations_session")).isEqualTo(SESSION_1);
		assertThat(reqCookies.get("remember_user_token")).isEqualTo(REMEMBER_TOKEN_1);

		// request assertions
		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		// AppJar cookies assertions
		final AppCookieJar appCookieJar = networkService.getCookieJar();
		assertThat(appCookieJar.getCookie("_PinMigrations_session").get().value()).isEqualTo(SESSION_2);
		assertThat(appCookieJar.getCookie("remember_user_token").get().value()).isEqualTo(REMEMBER_TOKEN_1);

		verify(prefsManagerMock).updateLoginState(SESSION_2, REMEMBER_TOKEN_1);
	}

	/** start logged out, check-sign-in should confirm not logged in
	 */
	@Test public void shouldCheckSigninStatusAndConfirmUnauthedWithoutInitialSession() throws Exception {
		// GIVEN
		server.enqueue(UT_LoginUtil.getInitialLoginCheckFalseResponse(CSRF_TOKEN_1, SESSION_1, serverBaseUrl));

		final Login expectedLogin = TLogin.builder()
				.loginState(UNAUTHENTICATED)
				.appSessionCookie(SESSION_1)
				.authenticityToken(CSRF_TOKEN_1)
				.build();

		// WHEN
		final TestSubscriber<Login> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber(loginResource.checkLoginStatus());

		// THEN
		testSubscriber.assertNoErrors();
		assertThat(server.getRequestCount()).isEqualTo(1);
		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo("/check_signin_status");
		assertThat(req.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(req.getHeader("Host")).isEqualTo(serverDomain);
		assertThat(req.getHeader("Origin")).isEqualTo(serverOrigin); // NOTE: not needed for check-sign-in
		assertThat(req.getHeader("Referer")).startsWith(serverReferer); // protocol and trailing slash
		assertThat(req.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");


		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		// AppJar cookies assertions
		final AppCookieJar appCookieJar = networkService.getCookieJar();
		assertThat(appCookieJar.getCookie("_PinMigrations_session").get().value()).isEqualTo(SESSION_1);

		verify(prefsManagerMock).updateLoginState(SESSION_1, "");
	}

	/** start logged out, check-sign-in should confirm not logged in
	 */
	@Test public void shouldClearValuesOnCheckSigninIfRememberTokenExpired() throws Exception {
		// GIVEN
		UT_LoginUtil.setLoggedInCookies(networkService.getCookieJar(), SESSION_1, REMEMBER_TOKEN_1);
		server.enqueue(UT_LoginUtil.getInitialLoginCheckFalseResponse(CSRF_TOKEN_1, SESSION_2, serverBaseUrl));

		final Login expectedLogin = TLogin.builder()
				.loginState(UNAUTHENTICATED)
				.appSessionCookie(SESSION_2)
				.authenticityToken(CSRF_TOKEN_1)
				.rememberUserToken("")
				.build();

		// WHEN
		final TestSubscriber<Login> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber(loginResource.checkLoginStatus());

		// THEN
		testSubscriber.assertNoErrors();
		assertThat(server.getRequestCount()).isEqualTo(1);
		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo("/check_signin_status");
		assertThat(req.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(req.getHeader("Host")).isEqualTo(serverDomain);
		assertThat(req.getHeader("Origin")).isEqualTo(serverOrigin); // NOTE: not needed for check-sign-in
		assertThat(req.getHeader("Referer")).startsWith(serverReferer); // protocol and trailing slash
		assertThat(req.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");


		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		// AppJar cookies assertions
		final AppCookieJar appCookieJar = networkService.getCookieJar();
		assertThat(appCookieJar.getCookie("_PinMigrations_session").get().value()).isEqualTo(SESSION_2);
		assertThat(appCookieJar.getCookie("remember_user_token").isPresent()).isFalse();

		verify(prefsManagerMock).updateLoginState(SESSION_2, "");
	}

	/** server responds with 200 (signed_in: true) for first check
	 */
	@Test public void shouldLoginWithRememberTokenWithoutRetrying() throws Exception {
		// GIVEN
		UT_LoginUtil.setLoggedInCookies(networkService.getCookieJar(), SESSION_1, REMEMBER_TOKEN_1);
		loginManager.setLoginObj(TLogin.builder()
				.rememberUserToken(REMEMBER_TOKEN_1) // note: not re-sent in response
				.appSessionCookie(SESSION_1) // session-1 initially
				.build());
		server.enqueue(UT_LoginUtil.getInitialLoginCheckTrueResponse(CSRF_TOKEN_1, SESSION_2, serverBaseUrl));

		final Login expectedLogin = TLogin.builder()
				.loginState(LOGGED_IN)
				.appSessionCookie(SESSION_2)
				.authenticityToken(CSRF_TOKEN_1)
				.rememberUserToken(REMEMBER_TOKEN_1)
				.userEmail(TEST_EMAIL)
				.userPassword(TEST_PASSWORD)
				.build();

		// WHEN
		final TestSubscriber<Login> testSubscriber = TRxUtil.createAndSubscribeTestSubscriberFromBlocking(
				loginResource.login(TEST_EMAIL, TEST_PASSWORD).toBlocking());

		// THEN
		testSubscriber.assertCompleted(); testSubscriber.assertNoErrors();
		assertThat(server.getRequestCount()).isEqualTo(1);
		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo("/check_signin_status");
		assertThat(req.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(req.getHeader("Host")).isEqualTo(serverDomain); // no protocol
		assertThat(req.getHeader("Origin")).isEqualTo(serverOrigin); // NOTE: not needed for check-sign-in
		assertThat(req.getHeader("Referer")).startsWith(serverReferer); // protocol and trailing slash
		assertThat(req.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");

		final Map<String, String> reqCookies = TNetUtil.requestCookies(req.getHeaders());
		assertThat(reqCookies.get("_PinMigrations_session")).isEqualTo(SESSION_1);
		assertThat(reqCookies.get("remember_user_token")).isEqualTo(REMEMBER_TOKEN_1);

		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		// AppJar cookies assertions
		final AppCookieJar appCookieJar = networkService.getCookieJar();
		assertThat(appCookieJar.getCookie("_PinMigrations_session").get().value()).isEqualTo(SESSION_2);
		assertThat(appCookieJar.getCookie("remember_user_token").get().value()).isEqualTo(REMEMBER_TOKEN_1);

		verify(prefsManagerMock).updateLoginState(SESSION_2, REMEMBER_TOKEN_1);
	}

	@Test public void shouldClearValuesWhenSigninCheckFails() throws Exception {
		// GIVEN
		UT_LoginUtil.setLoggedInCookies(networkService.getCookieJar(), SESSION_1, REMEMBER_TOKEN_1);
		loginManager.setLoginObj(TLogin.builder()
				.rememberUserToken(REMEMBER_TOKEN_1) // note: not re-sent in response
				.appSessionCookie(SESSION_1) // session-1 initially
				.authenticityToken(CSRF_TOKEN_1)
				.userEmail(TEST_EMAIL)
				.userPassword(TEST_PASSWORD)
				.build());
		server.enqueue(UT_LoginUtil.getInitialLoginCheckFalseResponse(CSRF_TOKEN_2, SESSION_2, serverBaseUrl));

		expectedLogin = TLogin.builder()
				.loginState(Login.UNAUTHENTICATED)
				.appSessionCookie(SESSION_2)
				.authenticityToken(CSRF_TOKEN_2)
				.rememberUserToken("")
				.userEmail(TEST_EMAIL)
				.userPassword(TEST_PASSWORD)
				.build();

		// WHEN
		final TestSubscriber<Login> testSubscriber = TRxUtil.createAndSubscribeTestSubscriberFromBlocking(
				loginResource.checkLoginStatus().toBlocking());

		// THEN
		testSubscriber.assertCompleted(); testSubscriber.assertNoErrors();
		assertThat(server.getRequestCount()).isEqualTo(1);

		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo("/check_signin_status");
		assertThat(req.getMethod()).isEqualToIgnoringCase("GET");

		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		verify(prefsManagerMock).updateLoginState(SESSION_2, "");
	}

	@Test public void shouldLoginFromLoggedOutWithoutSession() throws Exception {
		// GIVEN
		server.enqueue(UT_LoginUtil.getInitialLoginCheckFalseResponse(CSRF_TOKEN_1, SESSION_1, serverBaseUrl));
		server.enqueue(UT_LoginUtil.getLoginSuccessResponse(CSRF_TOKEN_2, SESSION_2, REMEMBER_TOKEN_1, serverBaseUrl));
		// NOTE: this is the actual sequence by the server, app should not follow redirect to this though
		server.enqueue(UT_LoginUtil.getLoginSuccessRedirectResponse(CSRF_TOKEN_3, SESSION_3, serverBaseUrl));
		// since the redirect request isnt followed, expect req-2 vals at end
		final Login expectedLogin = TLogin.builder()
				.loginState(LOGGED_IN)
				.appSessionCookie(SESSION_2)
				.authenticityToken(CSRF_TOKEN_2)
				.rememberUserToken(REMEMBER_TOKEN_1)
				.userEmail(TEST_EMAIL)
				.userPassword(TEST_PASSWORD)
				.build();


		// WHEN
		final TestSubscriber<Login> testSubscriber = TRxUtil.createAndSubscribeTestSubscriberFromBlocking(
				loginResource.login(TEST_EMAIL, TEST_PASSWORD).toBlocking());
		// NOTE: sometimes mock-server returns low request count because it hasnt finished processing its requests (race-condition)
		testSubscriber.awaitTerminalEvent(2, SECONDS);

		TCommonUtil.messageThenWait("wait for mock-server", 0.4, SECONDS);

		// THEN
		testSubscriber.assertCompleted(); testSubscriber.assertNoErrors();
		assertThat(server.getRequestCount()).isEqualTo(2);

		// req-1
		final RecordedRequest req1 = server.takeRequest();
		assertThat(req1.getPath()).isEqualTo("/check_signin_status");
		assertThat(req1.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(req1.getHeader("Host")).isEqualTo(serverDomain);
		assertThat(req1.getHeader("Origin")).isEqualTo(serverOrigin);
		assertThat(req1.getHeader("Referer")).startsWith(serverReferer);
		assertThat(req1.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");

		// req-2
		final RecordedRequest req2 = server.takeRequest();
		assertThat(req2.getPath()).isEqualTo("/login");
		assertThat(req2.getMethod()).isEqualToIgnoringCase("POST");
		assertThat(req2.getHeader("X-Requested-With")).isNull(); // AJAX server logic breaks login
		assertThat(req2.getHeader("APP_PLATFORM")).isNull(); // AJAX server logic breaks login
		assertThat(req1.getHeader("Host")).isEqualTo(serverDomain);
		assertThat(req1.getHeader("Origin")).isEqualTo(serverOrigin);
		assertThat(req1.getHeader("Referer")).startsWith(serverReferer);
		assertThat(req2.getHeader("X-CSRF-Token")).isEqualTo(CSRF_TOKEN_1);
		assertThat(req2.getHeader("Content-Type")).startsWith("application/x-www-form-urlencoded"); // UTF-8 param is OK if passed here

		final Map<String, String> req2FormParams = TNetUtil.decodeFormBody(req2.getBody().readUtf8());
		assertThat(req2FormParams.get("user[email]")).isEqualTo(TEST_EMAIL);
		assertThat(req2FormParams.get("user[password]")).isEqualTo(TEST_PASSWORD);

		// req-2 cookies
		final Map<String, String> req2Cookies = TNetUtil.requestCookies(req2.getHeaders());
		assertThat(req2Cookies.get("_PinMigrations_session")).isEqualTo(SESSION_1);


		// AppJar cookies assertions
		final AppCookieJar appCookieJar = networkService.getCookieJar();
		assertThat(appCookieJar.getCookie("_PinMigrations_session").get().value()).isEqualTo(SESSION_2);
		assertThat(appCookieJar.getCookie("remember_user_token").get().value()).isEqualTo(REMEMBER_TOKEN_1);

		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		// assert shared prefs saved
		final ArgumentCaptor<String> sessionArgument = ArgumentCaptor.forClass(String.class);
		final ArgumentCaptor<String> rembTokenArgument = ArgumentCaptor.forClass(String.class);
		verify(prefsManagerMock, times(2)).updateLoginState(sessionArgument.capture(), rembTokenArgument.capture());
		assertThat(sessionArgument.getAllValues().get(0)).isEqualTo(SESSION_1);
		assertThat(rembTokenArgument.getAllValues().get(0)).isEqualTo("");
		assertThat(sessionArgument.getAllValues().get(1)).isEqualTo(SESSION_2);
		assertThat(rembTokenArgument.getAllValues().get(1)).isEqualTo(REMEMBER_TOKEN_1);
	}

	@Test public void shouldSetLoginFailedAndClearCredentials() throws Exception {
		// GIVEN
		server.enqueue(UT_LoginUtil.getInitialLoginCheckFalseResponse(CSRF_TOKEN_1, SESSION_1, serverBaseUrl));
		server.enqueue(UT_LoginUtil.getLoginFailResponse(CSRF_TOKEN_2, SESSION_2, serverBaseUrl));
		// since the redirect request isnt followed, expect req-2 vals at end
		final Login expectedLogin = TLogin.builder()
				.loginState(LOGIN_FAILED)
				.appSessionCookie(SESSION_2)
				.authenticityToken(CSRF_TOKEN_2)
				.rememberUserToken("")
				.userEmail("")
				.userPassword("")
				.build();

		// WHEN
		final TestSubscriber<Login> testSubscriber = TRxUtil.createAndSubscribeTestSubscriberFromBlocking(
				loginResource.login(TEST_EMAIL, TEST_PASSWORD).toBlocking());
		// NOTE: sometimes mock-server returns low request count because it hasnt finished processing its requests (race-condition)
		testSubscriber.awaitTerminalEvent(2, SECONDS);

		// THEN
		testSubscriber.assertCompleted(); testSubscriber.assertNoErrors();
		assertThat(server.getRequestCount()).isEqualTo(2);

		// req-1
		final RecordedRequest req1 = server.takeRequest();
		assertThat(req1.getPath()).isEqualTo("/check_signin_status");
		assertThat(req1.getMethod()).isEqualToIgnoringCase("GET");
		// req-2
		final RecordedRequest req2 = server.takeRequest();
		assertThat(req2.getPath()).isEqualTo("/login");
		assertThat(req2.getMethod()).isEqualToIgnoringCase("POST");

		// AppJar cookies assertions
		final AppCookieJar appCookieJar = networkService.getCookieJar();
		assertThat(appCookieJar.getCookie("_PinMigrations_session").get().value()).isEqualTo(SESSION_2);
		assertThat(appCookieJar.getCookie("remember_user_token").isPresent()).isFalse();

		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		// assert shared prefs saved
		final ArgumentCaptor<String> sessionArgument = ArgumentCaptor.forClass(String.class);
		final ArgumentCaptor<String> rembTokenArgument = ArgumentCaptor.forClass(String.class);
		verify(prefsManagerMock, times(2)).updateLoginState(sessionArgument.capture(), rembTokenArgument.capture());
		assertThat(sessionArgument.getAllValues().get(0)).isEqualTo(SESSION_1);
		assertThat(rembTokenArgument.getAllValues().get(0)).isEqualTo("");
		assertThat(sessionArgument.getAllValues().get(1)).isEqualTo(SESSION_2);
		assertThat(rembTokenArgument.getAllValues().get(1)).isEqualTo("");
	}

	@Test public void shouldLogout() throws Exception {
		// GIVEN
		UT_LoginUtil.setLoggedInCookies(networkService.getCookieJar(), SESSION_1, REMEMBER_TOKEN_1);// session-1 initially
		loginManager.setLoginObj(TLogin.builder()
				.rememberUserToken(REMEMBER_TOKEN_1) // note: not re-sent in response
				.appSessionCookie(SESSION_1) // session-1 initially
				.userEmail(TEST_EMAIL)
				.userPassword(TEST_PASSWORD)
				.build());

		// note: remember-token re-sent with epoch expiration and empty value in response
		server.enqueue(UT_LoginUtil.getLogoutResponse(CSRF_TOKEN_2, SESSION_2, serverBaseUrl));

		final Login expectedLogin = TLogin.builder()
				.loginState(UNAUTHENTICATED)
				.appSessionCookie("")
				.authenticityToken("")
				.rememberUserToken("") // should be empty/null?, cookie will have epoch expires
				.userEmail(TEST_EMAIL) // should still have these (not erased)
				.userPassword(TEST_PASSWORD)  // should still have these (not erased)
				.build();


		// WHEN
		final TestSubscriber<Login> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber( loginResource.logout() );
		testSubscriber.awaitTerminalEvent(2, SECONDS);


		// THEN
		testSubscriber.assertCompleted(); testSubscriber.assertNoErrors();
		assertThat(server.getRequestCount()).isEqualTo(1);

		// req-1
		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo("/logout");
		assertThat(req.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(req.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");


		final Map<String, String> reqCookies = TNetUtil.requestCookies(req.getHeaders());
		assertThat(reqCookies.get("_PinMigrations_session")).isEqualTo(SESSION_1);
		assertThat(reqCookies.get("remember_user_token")).isEqualTo(REMEMBER_TOKEN_1);


		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		// NOTE: in the future these may change (PersistentCookieJar)
		// AppJar cookies assertions
		final AppCookieJar appCookieJar = networkService.getCookieJar();
		org.assertj.guava.api.Assertions.
				assertThat(appCookieJar.getCookie("_PinMigrations_session")).isAbsent();
		org.assertj.guava.api.Assertions.
				assertThat(appCookieJar.getCookie("remember_user_token")).isAbsent();

		verify(prefsManagerMock).updateLoginState("", "");
	}

	@Test public void shouldLogoutWhenCallerIgnoresReturnObservable() throws Exception {
		// GIVEN
		UT_LoginUtil.setLoggedInCookies(networkService.getCookieJar(), SESSION_1, REMEMBER_TOKEN_1);
		loginManager.setLoginObj(TLogin.builder()
				.rememberUserToken(REMEMBER_TOKEN_1)
				.appSessionCookie(SESSION_1)
				.userEmail(TEST_EMAIL)
				.userPassword(TEST_PASSWORD)
				.build());

		// note: remember-token re-sent with epoch expiration and empty value in response
		server.enqueue(UT_LoginUtil.getLogoutResponse(CSRF_TOKEN_2, SESSION_2, serverBaseUrl));
		final Login expectedLogin = TLogin.builder()
				.loginState(UNAUTHENTICATED)
				.appSessionCookie("") // should be empty, cookie will have epoch expires
				.authenticityToken("") // should be empty, cookie will have epoch expires
				.rememberUserToken("") // should be empty, cookie will have epoch expires
				.userEmail(TEST_EMAIL) // should still have these (not erased)
				.userPassword(TEST_PASSWORD)  // should still have these (not erased)
				.build();


		// WHEN
		final Observable<Login> loginObservable = loginResource.logout(); // note: no subscriber, have to manually wait

		TCommonUtil.messageThenWait("let logout complete without subscriber", 0.3, SECONDS);

		// THEN
		assertThat(server.getRequestCount()).isEqualTo(1);

		// req-1
		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo("/logout");
		assertThat(req.getMethod()).isEqualToIgnoringCase("GET");
		assertThat(req.getHeader("Cache-Control")).isEqualTo("no-store, no-cache");


		final Map<String, String> reqCookies = TNetUtil.requestCookies(req.getHeaders());
		assertThat(reqCookies.get("_PinMigrations_session")).isEqualTo(SESSION_1);
		assertThat(reqCookies.get("remember_user_token")).isEqualTo(REMEMBER_TOKEN_1);


		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		// NOTE: in the future these may change (PersistentCookieJar)
		// AppJar cookies assertions
		final AppCookieJar appCookieJar = networkService.getCookieJar();
		org.assertj.guava.api.Assertions.
				assertThat(appCookieJar.getCookie("_PinMigrations_session")).isAbsent();
		org.assertj.guava.api.Assertions.
				assertThat(appCookieJar.getCookie("remember_user_token")).isAbsent();

		verify(prefsManagerMock).updateLoginState("", "");

		// lastly check observable
		final TestSubscriber<Login> testSubscriber = new TestSubscriber<>();
		loginObservable.subscribe(testSubscriber);

		testSubscriber.assertNoErrors();
		testSubscriber.assertCompleted();
		testSubscriber.assertValueCount(1);
	}

	@Test public void shouldPropagateConnectExceptionIfNoConnection() throws Exception {
		// GIVEN
		server.shutdown(); // causes ConnectException at OkHttpClient exec

		// WHEN
		final TestSubscriber<Login> testSubscriber =
				TRxUtil.createAndSubscribeTestSubscriber(loginResource.checkLoginStatus());

		// THEN
		testSubscriber.assertError(ConnectException.class);
	}

	@Test public void shouldReturnExistingLoginForSoftLoginCheck() {
		// GIVEN
		expectedLogin = TLogin.builder()
				.loginState(Login.LOGGED_IN)
				.rememberUserToken(REMEMBER_TOKEN_1)
				.appSessionCookie(SESSION_1)
				.build();

		loginManager.setLoginObj( expectedLogin );
		loginResource.initialized = true;

		// WHEN
		loginResource.softLoginCheck().subscribe(testSubscriber);
		testSubscriber.awaitTerminalEvent(100, MILLISECONDS);

		// THEN
		testSubscriber.assertCompleted(); testSubscriber.assertNoErrors();
		assertThat(server.getRequestCount()).isEqualTo(0);

		Login login = testSubscriber.getOnNextEvents().get(0);
		assertThat(login).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);
	}

	@Test public void shouldExecuteRemoteLoginCheckForSoftLoginCheckIfUninitialized() throws Exception {
		// GIVEN
		loginResource.initialized = false;
		UT_LoginUtil.setLoggedInCookies(networkService.getCookieJar(), SESSION_1, REMEMBER_TOKEN_1);
		loginManager.setLoginObj(TLogin.builder()
				.rememberUserToken(REMEMBER_TOKEN_1) // note: not re-sent in response
				.appSessionCookie(SESSION_1) // session-1 initially
				.build());
		server.enqueue(UT_LoginUtil.getInitialLoginCheckTrueResponse(CSRF_TOKEN_1, SESSION_2, serverBaseUrl));

		expectedLogin = TLogin.builder()
				.loginState(LOGGED_IN)
				.appSessionCookie(SESSION_2) // session-2 from mock-server response
				.authenticityToken(CSRF_TOKEN_1) // not present initially, but set by server-response
				.rememberUserToken(REMEMBER_TOKEN_1)
				.build();


		// WHEN
		loginResource.softLoginCheck().subscribe(testSubscriber);

		// THEN
		testSubscriber.assertCompleted(); testSubscriber.assertNoErrors();
		// request assertions
		assertThat(server.getRequestCount()).isEqualTo(1);
		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo("/check_signin_status");
		assertThat(req.getMethod()).isEqualToIgnoringCase("GET");

		// request assertions
		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		verify(prefsManagerMock).updateLoginState(SESSION_2, REMEMBER_TOKEN_1);
	}

	@Test public void shouldNotClearAuthValuesIfNoneSent() throws Exception {
		// GIVEN
		ApiGenerator apiGenerator = new ApiGenerator(networkService, TRxJavaService.newServiceAllImmediate());
		remoteLogResource = new TRemoteLogResource(apiGenerator); // need real resource
		loginResource = new TLoginResource(apiGenerator, loginManager, networkService, remoteLogResource);
		UT_LoginUtil.setLoggedInCookies(networkService.getCookieJar(), SESSION_1, REMEMBER_TOKEN_1);
		loginManager.setLoginObj(TLogin.builder()
				.rememberUserToken(REMEMBER_TOKEN_1) // note: not re-sent in response
				.appSessionCookie(SESSION_1) // session-1 initially
				.authenticityToken(CSRF_TOKEN_1)
				.build());
		server.enqueue(new MockResponse()); // unimportant req-url or response

		expectedLogin = TLogin.builder()
				.loginState(LOGGED_IN) // should keep
				.appSessionCookie(SESSION_1) // should keep
				.authenticityToken(CSRF_TOKEN_1) // should keep
				.rememberUserToken(REMEMBER_TOKEN_1) // should keep
				.build();

		TestSubscriber<Void> pingSubscriber = new TestSubscriber<>();


		// WHEN
		loginResource.ping().subscribe(pingSubscriber);
		pingSubscriber.awaitTerminalEvent(500, MILLISECONDS);

		// THEN
		pingSubscriber.assertCompleted(); testSubscriber.assertNoErrors();
		// request assertions
		assertThat(server.getRequestCount()).isEqualTo(1);
		final RecordedRequest req = server.takeRequest();
		assertThat(req.getPath()).isEqualTo("/ping");
		assertThat(req.getMethod()).isEqualToIgnoringCase("GET");

		final Login loginObj = loginManager.getInternalLoginObj();
		assertThat(loginObj).usingComparator(LOGIN_ASSERTS).isEqualTo(expectedLogin);

		// AppJar cookies assertions
		final AppCookieJar appCookieJar = networkService.getCookieJar();
		assertThat(appCookieJar.getCookie("_PinMigrations_session").get().value()).isEqualTo(SESSION_1);
		assertThat(appCookieJar.getCookie("remember_user_token").get().value()).isEqualTo(REMEMBER_TOKEN_1);

		verify(prefsManagerMock).updateLoginState(SESSION_1, REMEMBER_TOKEN_1);
	}
}
