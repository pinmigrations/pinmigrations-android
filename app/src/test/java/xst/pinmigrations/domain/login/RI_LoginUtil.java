package xst.pinmigrations.domain.login;


import android.support.annotation.NonNull;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockWebServer;

public final class RI_LoginUtil {

	/** sets LOGGED_IN in LoginManager, stubs LoginApiMock to return TSignedInResponse w true */
	public static void setupServerForCheckSignInTrue(@NonNull final MockWebServer server,
													 @NonNull final String respCsrf,
													 @NonNull final String respSession) {
		final HttpUrl domain = server.url("/");
		server.enqueue(UT_LoginUtil.getInitialLoginCheckTrueResponse(respCsrf, respSession, domain));
	}


	/** sets LOGGED_IN in LoginManager, stubs LoginApiMock to return TSignedInResponse w true */
	public static void setupServerForCheckSignInFalse(@NonNull final MockWebServer server,
													  @NonNull final String respCsrf,
													  @NonNull final String respSession) {
		final HttpUrl domain = server.url("/");
		server.enqueue(UT_LoginUtil.getInitialLoginCheckFalseResponse(respCsrf, respSession, domain));
	}

	/** LoginManager, stubs LoginApiMock to return TSignedInResponse w true */
	public static void setupServerForLogout(@NonNull final MockWebServer server,
											@NonNull final String respCsrf,
											@NonNull final String respSession) {
		final HttpUrl domain = server.url("/");
		server.enqueue(UT_LoginUtil.getLogoutResponse(respCsrf, respSession, domain));
	}

	/** LoginManager, stubs LoginApiMock to return TSignedInResponse w true */
	public static void setupServerForLoginSuccess(@NonNull final MockWebServer server,
												  @NonNull final String checkRespCsrf,
												  @NonNull final String checkRespSession,
												  @NonNull final String loginRespCsrf,
												  @NonNull final String loginRespSession,
												  @NonNull final String loginRespRembTok) {
		final HttpUrl domain = server.url("/");
		server.enqueue(UT_LoginUtil.getInitialLoginCheckFalseResponse(checkRespCsrf, checkRespSession, domain));
		server.enqueue(UT_LoginUtil.getLoginSuccessResponse(loginRespCsrf, loginRespSession, loginRespRembTok, domain));
	}
}
