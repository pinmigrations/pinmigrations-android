package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import okhttp3.HttpUrl;
import xst.pinmigrations.domain.login.TLoginManager;

import static com.google.common.base.Preconditions.checkNotNull;

/** bypass network layer (no OkHttpClient, Interceptors)
 * functional CookieJar
 */
public class RF_NetworkService extends NetworkService {
	@NonNull final TInMemoryCookieJar jar;
	public RF_NetworkService( @NonNull final HttpUrl baseUrl,
							  @NonNull final TLoginManager loginManager,
							  @NonNull final TInMemoryCookieJar jar) {
		super(baseUrl, loginManager);
		this.jar = checkNotNull(jar);
	}

	@NonNull @Override public AppCookieJar getCookieJar() {
		return jar;
	}

	public @NonNull TInMemoryCookieJar getTInMemoryCookieJar() {
		return jar;
	}
}
