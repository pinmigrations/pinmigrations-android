package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import xst.pinmigrations.domain.login.LoginManager;
import xst.pinmigrations.domain.login.TLoginManager;

public class RI_NetworkService extends NetworkService {
	public static RI_NetworkService newRINetworkService(@NonNull final HttpUrl baseUrl, @NonNull final TLoginManager loginManager) {
		return new RI_NetworkService(baseUrl, loginManager);
	}

	public RI_NetworkService(@NonNull final HttpUrl baseUrl, @NonNull final LoginManager loginManager) {
		super(baseUrl, loginManager);
	}

	@NonNull public TInMemoryCookieJar getTInMemoryCookieJar() {
		return (TInMemoryCookieJar) super.getCookieJar();
	}

	@NonNull @Override CookieJar newInMemoryCookieJar() {
		return new TInMemoryCookieJar(baseHttpUrl);
	}
}
