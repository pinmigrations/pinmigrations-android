package xst.pinmigrations.domain.network;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import okhttp3.Cookie;
import xst.pinmigrations.domain.network.InMemoryCookieStore.Key;
import xst.pinmigrations.test.BaseUnitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static xst.pinmigrations.domain.network.TNetUtil.FACEBOOK_URL;
import static xst.pinmigrations.domain.network.TNetUtil.GOOGLE_URL;

public class CookieJarTest extends BaseUnitTest {
	public static final String COOKIE_KEY_1 = "key-1", COOKIE_VAL_1 = "val-1";
	public static final String COOKIE_KEY_2 = "key-2", COOKIE_VAL_2 = "val-2";
	public static final String COOKIE_KEY_3 = "key-3", COOKIE_VAL_3 = "val-3";

	public static final String GOOGLE_DOMAIN = GOOGLE_URL.host();
	public static final String FACEBOOK_DOMAIN = FACEBOOK_URL.host();

	TInMemoryCookieJar jar;
	HashMap<Key, Cookie> cookieStore;
	@Before public void setup() {
		jar = new TInMemoryCookieJar(GOOGLE_URL);
		cookieStore = jar.getInternalCookieStore();
	}

	@Test public void shouldClearCookiePrecisely() { // not all cookies
		// GIVEN
		Cookie c1 = CookieUtil.newCookie(COOKIE_KEY_1, COOKIE_VAL_1, GOOGLE_DOMAIN);
		Key c1Key = new Key(c1.name(), c1.domain());
		cookieStore.put(c1Key, c1);

		Cookie c2 = CookieUtil.newCookie(COOKIE_KEY_2, COOKIE_VAL_2, GOOGLE_DOMAIN);
		Key c2Key = new Key(c2.name(), c2.domain());
		cookieStore.put(c2Key, c2);

		Cookie c3 = CookieUtil.newCookie(COOKIE_KEY_3, COOKIE_VAL_3, FACEBOOK_DOMAIN);
		Key c3Key = new Key(c3.name(), c3.domain());
		cookieStore.put(c3Key, c3);

		Cookie c4 = CookieUtil.newCookie(COOKIE_KEY_1, COOKIE_VAL_1, FACEBOOK_DOMAIN); // NOTE: same as c1 but with FB domain
		Key c4Key = new Key(c4.name(), c4.domain());
		cookieStore.put(c4Key, c4);

		// WHEN
		jar.clearCookie(COOKIE_KEY_1);

		// THEN
		assertThat(cookieStore).doesNotContainKey(c1Key);
		assertThat(cookieStore).containsKey(c2Key);
		assertThat(cookieStore).containsKey(c3Key);
		assertThat(cookieStore).containsKey(c4Key);
	}
}
