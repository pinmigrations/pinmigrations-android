package xst.pinmigrations.domain.network;

import okhttp3.mockwebserver.MockResponse;

public final class UT_NetUtil {
	public static void addCookiesToMockResponse(final TServerCookies cookies, final MockResponse mockResponse) {
		for (final String cookie: cookies.getServerCookieHeaders()) {
			mockResponse.addHeader(NetworkUtil.SET_COOKIE_HEADER, cookie);
		}
	}
}
