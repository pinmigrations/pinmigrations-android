package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import java.util.HashMap;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

public class TInMemoryCookieJar extends InMemoryCookieJar {

	public TInMemoryCookieJar(@NonNull final HttpUrl defaultDomainBaseUrl) {
		super(defaultDomainBaseUrl);
	}

	public HashMap<Key, Cookie> getInternalCookieStore() {
		return cookieStore();
	}
}
