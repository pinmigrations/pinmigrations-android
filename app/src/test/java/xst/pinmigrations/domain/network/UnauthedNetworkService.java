package xst.pinmigrations.domain.network;

import android.support.annotation.NonNull;

import org.mockito.Mockito;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import xst.pinmigrations.domain.android_context.shared_preferences.PrefsManager;
import xst.pinmigrations.domain.login.TLogin;
import xst.pinmigrations.domain.login.TLoginManager;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static xst.pinmigrations.domain.network.NetworkUtil.CLIENT_CONNECT_TIMEOUT;
import static xst.pinmigrations.domain.network.NetworkUtil.CLIENT_READ_TIMEOUT;
import static xst.pinmigrations.domain.network.NetworkUtil.CLIENT_WRITE_TIMEOUT;

/**
 * use for tests (that dont require auth)
 * TODO: simplify setting different client types, rename to TNetworkService
 */
public final class UnauthedNetworkService extends NetworkService {
	public UnauthedNetworkService(@NonNull final HttpUrl baseUrl) {
		super(baseUrl, new TLoginManager(new TLogin(), Mockito.mock(PrefsManager.class)));
	}

	public void setClient(@NonNull final OkHttpClient httpClient) {
		this.httpClient = checkNotNull(httpClient);
	}

	@NonNull @Override protected OkHttpClient createApplicationHttpClient() {
		return newDefaultUnAuthedHttpClient();
	}

	//region OkHttpClient & Builders
	public OkHttpClient.Builder baseUnauthedBuilder() {
		return new OkHttpClient.Builder()
				.followRedirects(true)
				.cookieJar(new InMemoryCookieJar(baseHttpUrl));
	}

	public final OkHttpClient newDefaultUnAuthedHttpClient() {
		return baseUnauthedBuilder()
				.connectTimeout(CLIENT_CONNECT_TIMEOUT, SECONDS)
				.readTimeout(CLIENT_READ_TIMEOUT, SECONDS)
				.writeTimeout(CLIENT_WRITE_TIMEOUT, SECONDS)
				.addNetworkInterceptor(new ConciseLoggingInterceptor())
				.build();
	}
	//endregion

	//region pre-defined client types

	// client with extremely short timeout to test error code-paths
	// causes connect/read timeouts
	public static OkHttpClient newShortConnectHttpClient(@NonNull final UnauthedNetworkService unauthedNetworkService) {
		return unauthedNetworkService.baseUnauthedBuilder()
				.connectTimeout(1, MILLISECONDS)
				.readTimeout(1, MILLISECONDS)
				.writeTimeout(CLIENT_WRITE_TIMEOUT, SECONDS)
				.build();
	}

	/** Network service lazily creates OkHttpClient when first requested so ensure that
	 * the custom client is set before dependent services/modules request one from the NetworkService
	 */
	public static void setupShortConnectClient(@NonNull final UnauthedNetworkService unauthedNetworkService) {
		final OkHttpClient shortConnectClient = newShortConnectHttpClient( checkNotNull(unauthedNetworkService) );
		unauthedNetworkService.setClient(shortConnectClient);
	}
	//endregion
}
