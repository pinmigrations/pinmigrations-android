package android.security;

import android.annotation.SuppressLint;

/**
 * fix for Robolectric 3.3 for SDK 16+ (fixed in SDK23)
 */
// https://github.com/square/okhttp/issues/2533#issuecomment-223093100
public class NetworkSecurityPolicy {
	private static final NetworkSecurityPolicy INSTANCE = new NetworkSecurityPolicy();

	@SuppressLint("NewApi")
	public static NetworkSecurityPolicy getInstance() { return INSTANCE; }
	public boolean isCleartextTrafficPermitted() { return true; }
}
