#!/bin/bash

echo 'shows information on some dependency'
echo 'specify configuration:
echo '    eg: compile / testCompile / androidTestCompile'
echo 'and dependency-name:'
echo '    eg: dagger / rxjava / timber'
echo
echo 'command template is'
echo
echo '    gradle app:dependencyInsight --configuration <CONF> --dependency <DEP>'
echo
