#!/bin/bash

if [ $# -eq 0 ]; then
	heroku logs -n 200 --app pinmigrations
else
	heroku logs -n $1 --app pinmigrations
fi
