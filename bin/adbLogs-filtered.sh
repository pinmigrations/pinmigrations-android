#!/bin/bash

# NOTE: application-id is specified in `build.gradle` under `android.defaultConfig.applicationId`
#l {V,D,I,W,E,F,v,d,i,w,e,f}, --min-level {V,D,I,W,E,F,v,d,i,w,e,f} Minimum level to be displayed
# -w N, --tag-width N   Width of log tag (default is 23)


if [ $# -eq 0 ]; then
	pidcat xst.pinmigrations --min-level D -w 0 --tag xst.PinMigrations
elif [ $# -eq 1 ]; then
	pidcat xst.pinmigrations --min-level "$1" -w 0 --tag xst.PinMigrations
else
	pidcat xst.pinmigrations --min-level "$1" -w "$2" --tag xst.PinMigrations
fi
