#!/bin/bash
# print INET info of a connected device
full_ip_addr=$(adb shell ip -f inet addr show wlan0 | sed -n 2p | awk '{print $2}')
ip_addr="${full_ip_addr/\/[0-9][0-9]/}"

adb_addr="${ip_addr}:5555"

echo "connecting adb over tcp/ip at ${adb_addr}"
adb tcpip 5555
adb connect "${adb_addr}"

echo "disconnect device..."
sleep 3
adb connect "${adb_addr}"

echo 'if adb doesnt connect automatically, use this:'
echo
echo '    'adb connect "${adb_addr}"
echo
