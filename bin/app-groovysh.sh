#!/bin/bash

rxJava="/Users/xst/.gradle/caches/modules-2/files-2.1/io.reactivex/rxjava/1.2.7/138872ff3173fb7bee8b1d6303d6edb57d76599/rxjava-1.2.7.jar"
okHttp="/Users/xst/.gradle/caches/modules-2/files-2.1/com.squareup.okhttp3/okhttp/3.6.0/69edde9fc4b01c9fd51d25b83428837478c27254/okhttp-3.6.0.jar"
okIo="/Users/xst/.gradle/caches/modules-2/files-2.1/com.squareup.okio/okio/1.11.0/840897fcd7223a8143f1d9b6f69714e7be34fd50/okio-1.11.0.jar"
guava="/Users/xst/.gradle/caches/modules-2/files-2.1/com.google.guava/guava/20.0/89507701249388e1ed5ddcf8c41f4ce1be7831ef/guava-20.0.jar"
retrofit="/Users/xst/.gradle/caches/modules-2/files-2.1/com.squareup.retrofit2/retrofit/2.2.0/41e67dba73c3347e4503761642c39d0e06ca1f2/retrofit-2.2.0.jar"

classpath="${rxJava}:${okHttp}:${okIo}:${guava}:${retrofit}"

echo "using classpath: ${classpath}"
echo ""
echo "common imports:"
echo ""
echo "import okhttp3.HttpUrl"
echo "import retrofit2.Retrofit"
echo ""

groovysh -cp "${classpath}"
